<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 *
 * Implementation of a password history management.
 *
 * This class provides some very basic methods to manage old passwords
 * once used by users.
 */
class SeedDMS_PasswordHistoryManager {
  /**
   * @var \BakeryDMS\Core\Persistency\IDatabaseAccess reference to database object.
   * @access protected
   */
  protected $db;

  /**
   * Create a new instance of the password history manager
   *
   * @param \BakeryDMS\Core\Persistency\IDatabaseAccess object to access the underlying database
   */
  function __construct($db) {
    $this->db = $db;
  }

  function add($user, $pwd) {
    $queryStr = "INSERT INTO tblUserPasswordHistory (userID, pwd, `date`) " .
      "VALUES (" . $this->db->qstr($user->getId()) . ", " . $this->db->qstr($pwd) . ", CURRENT_TIMESTAMP)";
    if (!$this->db->getResult($queryStr)) {
      return false;
    }
  }

  function search($user, $pwd) {
    $queryStr = "SELECT * FROM tblUserPasswordHistory WHERE userID = " . $this->db->qstr($user->getId()) . " AND pwd=" . $this->db->qstr($pwd);

    $resArr = $this->db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) == 0) {
      return array();
    }
    return $resArr[0];
  }
}

?>
