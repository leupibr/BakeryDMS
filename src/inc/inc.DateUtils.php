<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


if (!function_exists('strptime')) {
  /**
   * strptime() returns an array with the date parsed, or 0 on error.<br>
   * Author: JOE BREWER <http://www.brewsterware.com/strptime-for-windows.html>
   *
   * @param string $date The string to parse (e.g. returned from strftime()).
   * @param string $format The format used in date (e.g. the same as used in strftime()).
   * @return Returns an array or 0 on failure.
   */

  function strptime($date, $format) {
    $dateArray = array();

    $dateArray = date_parse_from_format($date, $format);

    if (is_array($dateArray)) {
      return mktime($dateArray['hour'],
        $dateArray['minute'],
        $dateArray['second'],
        $dateArray['month'],
        $dateArray['day'],
        $dateArray['year']);
    } else {
      return 0;
    }
  }
}

if (!function_exists('date_parse_from_format')) {
  /**
   * Author: JOE BREWER <http://www.brewsterware.com/strptime-for-windows.html>
   *
   * @param string $date The string to parse (e.g. returned from strftime()).
   * @param string $format The format used in date (e.g. the same as used in strftime()).
   * @return array An array with
   */
  function date_parse_from_format($date, $format) {
    $returnArray = array('hour' => 0,
      'minute' => 0,
      'second' => 0,
      'month' => 0,
      'day' => 0,
      'year' => 0);

    $dateArray = array();

    // array of valid date codes with keys for the return array as the values
    $validDateTimeCode = array('Y' => 'year',
      'y' => 'year',
      'm' => 'month',
      'n' => 'month',
      'd' => 'day',
      'j' => 'day',
      'H' => 'hour',
      'G' => 'hour',
      'i' => 'minute',
      's' => 'second');

    /* create an array of valid keys for the return array
     * in the order that they appear in $format
     */
    for ($i = 0; $i <= strlen($format) - 1; $i++) {
      $char = substr($format, $i, 1);

      if (array_key_exists($char, $validDateTimeCode)) {
        $dateArray[$validDateTimeCode[$char]] = '';
      }
    }

    // create array of reg ex things for each date part
    $regExArray = array('.' => '\.', // escape the period

      // parse d first so we dont mangle the reg ex
      // day
      'd' => '(\d{2})',
      // year
      'Y' => '(\d{4})',
      'y' => '(\d{2})',
      // month
      'm' => '(\d{2})',
      'n' => '(\d{1,2})',
      // day
      'j' => '(\d{1,2})',
      // hour
      'H' => '(\d{2})',
      'G' => '(\d{1,2})',
      // minutes
      'i' => '(\d{2})',
      // seconds
      's' => '(\d{2})');

    // create a full reg ex string to parse the date with
    $regEx = str_replace(array_keys($regExArray),
      array_values($regExArray),
      $format);

    // Parse the date
    preg_match("#$regEx#", $date, $matches);

    // some checks...
    if (!is_array($matches)
      || $matches[0] != $date
      || sizeof($dateArray) != (sizeof($matches) - 1)
    ) {
      return $returnArray;
    }

    // an iterator for the $matches array
    $i = 1;

    foreach ($dateArray AS $key => $value) {
      $dateArray[$key] = $matches[$i++];

      if (array_key_exists($key, $returnArray)) {
        $returnArray[$key] = $dateArray[$key];
      }
    }

    return $returnArray;
  }
}
?>