<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class
 */
use BakeryDMS\Core\Group;
use BakeryDMS\Core\User;

require_once("inc.ClassNotify.php");

/**
 * Class to send email notifications to individuals or groups.
 */
class SeedDMS_Email extends SeedDMS_Notify {

  function toIndividual($sender, $recipient, $subject, $message, $params = array()) {
    global $settings;
    if ($recipient->getEmail() == "") {
      return 0;
    }

    if ((!is_object($sender) && strcasecmp(get_class($sender), User::class)) ||
      (!is_object($recipient) && strcasecmp(get_class($recipient), User::class))
    ) {
      return -1;
    }

    $headers = array();
    $headers[] = "MIME-Version: 1.0";
    $headers[] = "Content-type: text/plain; charset=utf-8";
    //$headers[] = "From: ". $sender->getFullName() ." <". $sender->getEmail() .">";
    $headers[] = "From: " . $settings->_smtpSendFrom;
    $headers[] = "Reply-To: " . $sender->getFullName() . " <" . $sender->getEmail() . ">";

    $lang = $recipient->getLanguage();
    $message = getMLText("email_header", array(), "", $lang) . "\r\n\r\n" . getMLText($message, $params, "", $lang);
    $message .= "\r\n\r\n" . getMLText("email_footer", array(), "", $lang);

    $subject = "=?UTF-8?B?" . base64_encode(getMLText($subject, $params, "", $lang)) . "?=";
    mail($recipient->getEmail(), $subject, $message, implode("\r\n", $headers));

    return true;
  }

  function toGroup($sender, $groupRecipient, $subject, $message, $params = array()) {
    if ((!is_object($sender) && strcasecmp(get_class($sender), User::class)) ||
      (!is_object($groupRecipient) && strcasecmp(get_class($groupRecipient), Group::class))
    ) {
      return -1;
    }

    foreach ($groupRecipient->getUsers() as $recipient) {
      $this->toIndividual($sender, $recipient, $subject, $message, $params);
    }

    return true;
  }

  function toList($sender, $recipients, $subject, $message, $params = array()) {
    if ((!is_object($sender) && strcasecmp(get_class($sender), User::class)) ||
      (!is_array($recipients) && count($recipients) == 0)
    ) {
      return -1;
    }

    foreach ($recipients as $recipient) {
      $this->toIndividual($sender, $recipient, $subject, $message, $params);
    }

    return true;
  }

  function sendPassword($sender, $recipient, $subject, $message) {
    global $settings;

    $headers = array();
    $headers[] = "MIME-Version: 1.0";
    $headers[] = "Content-type: text/plain; charset=utf-8";
    $headers[] = "From: " . $settings->_smtpSendFrom;
    $headers[] = "Reply-To: " . $settings->_smtpSendFrom;

    $subject = "=?UTF-8?B?" . base64_encode($this->replaceMarker($subject)) . "?=";
    return (mail($recipient->getEmail(), $subject, $this->replaceMarker($message), implode("\r\n", $headers)) ? 0 : -1);
  }
}

?>
