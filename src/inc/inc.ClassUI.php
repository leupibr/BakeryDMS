<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use BakeryDMS\Extension\ExtensionManager;

require_once('inc.ClassUI_Default.php');
require_once('inc.ClassViewCommon.php');
require_once('inc.BasicEnum.php');

// HACK: cleanup this theme fix
if (!isset($theme) || strlen($theme) == 0) {
  $theme = $settings->_theme;
}
if (strlen($theme) == 0) {
  $theme = "blue";
}

/* Sooner or later the parent will be removed, because all output will
 * be done by the new view classes.
 */

class UI extends UI_Default {
  /**
   * Create a view from a class in the given theme
   *
   * This method will check for a class file in the theme directory
   * and returns an instance of it.
   *
   * @param string $theme theme
   * @param string $class name of view class
   * @param array $params parameter passed to constructor of view class
   * @return object an object of a class implementing the view
   */
  static function factory($theme, $class = '', $params = array()) {
    global $settings, $session;
    if (!$class) {
      $class = 'Bootstrap';
      $classname = "SeedDMS_Bootstrap_Style";
    }
    else {
      // HACK: don't play with classnames!
      if (strpos($class, "BakeryDMS") === false) {
        $classname = "SeedDMS_View_" . $class;
      }
      else {
        $classname = $class;
      }
    }

    $filename = $settings->_rootDir . "views/" . $theme . "/class." . $class . ".php";
    // HACK: find better way to create from namespace class
    if (file_exists($filename) || class_exists($classname)) {
      if (file_exists($filename)) {
        require_once($filename);
      }

      /** @var SeedDMS_Bootstrap_Style $view */
      $view = new $classname($params, $theme);

      /* set extension configuration */
      $navigationItems = ExtensionManager::getInstance()->getGlobalNavigationItems();
      $view->addGlobalNavigationItems($navigationItems);

      $adminItems = ExtensionManager::getInstance()->getAdminItems();
      $view->addAdminItems($adminItems);

      $folderItems = ExtensionManager::getInstance()->getFolderItems();
      $view->addFolderItems($folderItems);

      /* Set some configuration parameters */
      $view->setParam('refferer', $_SERVER['REQUEST_URI']);
      $view->setParam('session', $session);
      $view->setParam('sitename', $settings->_siteName);
      $view->setParam('rootfolderid', $settings->_rootFolderID);
      $view->setParam('disableselfedit', $settings->_disableSelfEdit);
      $view->setParam('enableusersview', $settings->_enableUsersView);
      $view->setParam('enablecalendar', $settings->_enableCalendar);
      $view->setParam('calendardefaultview', $settings->_calendarDefaultView);
      $view->setParam('enablefullsearch', $settings->_enableFullSearch);
      $view->setParam('enablelargefileupload', $settings->_enableLargeFileUpload);
      $view->setParam('printdisclaimer', $settings->_printDisclaimer);
      $view->setParam('footnote', $settings->_footNote);
      $view->setParam('logfileenable', $settings->_logFileEnable);
      $view->setParam('expandfoldertree', $settings->_expandFolderTree);
      $view->setParam('enablefoldertree', $settings->_enableFolderTree);
      $view->setParam('enablelanguageselector', $settings->_enableLanguageSelector);
      $view->setParam('workflowmode', $settings->_workflowMode);
      $view->setParam('partitionsize', $settings->_partitionSize);
      $view->setParam('showmissingtranslations', $settings->_showMissingTranslations);
      $view->setParam('settings', $settings);
      return $view;
    }
    return null;
  }

  static function getStyles() {
    global $settings;

    $themes = array();
    $path = $settings->_rootDir . "views/";
    $handle = opendir($path);

    while ($entry = readdir($handle)) {
      if ($entry == ".." || $entry == ".") {
        continue;
      }
      else if (is_dir($path . $entry) || is_link($path . $entry)) {
        array_push($themes, $entry);
      }
    }
    closedir($handle);
    return $themes;
  }

  public static function exitError($pagetitle, $error) {
    global $theme;
    $tmp = 'ErrorDlg';
    $view = UI::factory($theme, $tmp);
    $view->exitError($pagetitle, $error);
  }
}

?>
