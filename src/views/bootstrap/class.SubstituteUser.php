<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for SubstituteUser view.
 */
class SeedDMS_View_SubstituteUser extends SeedDMS_Bootstrap_Style {

function show() {
$dms = $this->params['dms'];
$user = $this->params['user'];
$allUsers = $this->params['allusers'];

$this->htmlStartPage(getMLText("substitute_user"));
$this->globalNavigation();
$this->contentStart();
$this->pageNavigation(getMLText("admin_tools"), "admin_tools");

$this->contentHeading(getMLText("substitute_user"));
$this->contentContainerStart();
?>
<table class="table table-condensed">
  <tr>
    <th><?php printMLText('name'); ?></th>
    <th><?php printMLText('email'); ?></th>
    <th><?php printMLText('groups'); ?></th>
    <th></th>
  </tr>
  <?php
  foreach ($allUsers as $currUser) {
    echo "<tr>";
    echo "<td>";
    echo $currUser->getFullName() . " (" . $currUser->getLogin() . ")<br />";
    echo "<small>" . $currUser->getComment() . "</small>";
    echo "</td>";
    echo "<td>";
    echo "<a href=\"mailto:" . $currUser->getEmail() . "\">" . $currUser->getEmail() . "</a><br />";
    echo "</td>";
    echo "<td>";
    $groups = $currUser->getGroups();
    if (count($groups) != 0) {
      for ($j = 0; $j < count($groups); $j++) {
        print $groups[$j]->getName();
        if ($j + 1 < count($groups)) {
          print ", ";
        }
      }
    }
    echo "</td>";
    echo "<td>";
    if ($currUser->getID() != $user->getID()) {
      echo "<a class=\"btn\" href=\"../op/op.SubstituteUser.php?userid=" . $currUser->getID() . "\"><span class=\"fa fa-exchange\"></span> " . getMLText('substitute_user') . "</a> ";
    }
    echo "</td>";
    echo "</tr>";
  }
  echo "</table>";
  $this->contentContainerEnd();

  $this->htmlEndPage();
  }
  }
  ?>

