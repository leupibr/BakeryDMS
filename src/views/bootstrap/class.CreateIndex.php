<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for CreateIndex view.
 */
class SeedDMS_View_CreateIndex extends SeedDMS_Bootstrap_Style {

  function show() {
    $this->htmlStartPage(getMLText("admin_tools"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText('admin_tools'), 'admin_tools');
    $this->contentHeading(getMLText("create_fulltext_index"));
    $this->contentContainerStart();

    echo '<p>' . getMLText('create_fulltext_index_warning') . '</p>';
    echo '<a href="out.Indexer.php?create=1&confirm=1" class="btn">' . getMLText('confirm_create_fulltext_index') . '</a>';

    $this->contentContainerEnd();
    $this->htmlEndPage();
  }
}

?>

