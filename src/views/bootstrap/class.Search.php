<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
use BakeryDMS\Core\AccessMode;
use BakeryDMS\Core\DocumentState;
use BakeryDMS\Core\Folder;
use BakeryDMS\Preview\Previewer;

require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for Search result view.
 */
class SeedDMS_View_Search extends SeedDMS_Bootstrap_Style {

  /**
   * Mark search query sting in a given string
   *
   * @param string $str mark this text
   * @param string $tag wrap the marked text with this html tag
   * @return string marked text
   */
  function markQuery($str, $tag = "b") {
    $querywords = preg_split("/ /", $this->query);

    foreach ($querywords as $queryword) {
      $str = str_ireplace("($queryword)", "<" . $tag . ">\\1</" . $tag . ">", $str);
    }

    return $str;
  }

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $fullsearch = $this->params['fullsearch'];
    $totaldocs = $this->params['totaldocs'];
    $totalfolders = $this->params['totalfolders'];
    $attrdefs = $this->params['attrdefs'];
    $allCats = $this->params['allcategories'];
    $allUsers = $this->params['allusers'];
    $mode = $this->params['mode'];
    $workflowmode = $this->params['workflowmode'];
    $enablefullsearch = $this->params['enablefullsearch'];
    $attributes = $this->params['attributes'];
    $categories = $this->params['categories'];
    $owner = $this->params['owner'];
    $startfolder = $this->params['startfolder'];
    $startdate = $this->params['startdate'];
    $stopdate = $this->params['stopdate'];
    $expstartdate = $this->params['expstartdate'];
    $expstopdate = $this->params['expstopdate'];
    $creationdate = $this->params['creationdate'];
    $expirationdate = $this->params['expirationdate'];
    $status = $this->params['status'];
    $this->query = $this->params['query'];
    $entries = $this->params['searchhits'];
    $totalpages = $this->params['totalpages'];
    $pageNumber = $this->params['pagenumber'];
    $searchTime = $this->params['searchtime'];
    $urlparams = $this->params['urlparams'];
    $searchin = $this->params['searchin'];
    $cachedir = $this->params['cachedir'];
    $previewwidth = $this->params['previewWidthList'];

    $this->htmlStartPage(getMLText("search_results"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText("search_results"), "");

    echo "<div class=\"row-fluid\">\n";
    echo "<div class=\"span4\">\n";
    ?>
    <ul class="nav nav-tabs" id="searchtab">
      <li <?php echo ($fullsearch == false) ? 'class="active"' : ''; ?>><a data-target="#database"
                                                                           data-toggle="tab"><?php printMLText('databasesearch'); ?></a>
      </li>
      <?php
      if ($enablefullsearch) {
        ?>
        <li <?php echo ($fullsearch == true) ? 'class="active"' : ''; ?>><a data-target="#fulltext"
                                                                            data-toggle="tab"><?php printMLText('fullsearch'); ?></a>
        </li>
      <?php
      }
      ?>
    </ul>
    <div class="tab-content">
      <div class="tab-pane <?php echo ($fullsearch == false) ? 'active' : ''; ?>" id="database">
        <form action="../op/op.Search.php" name="form1" onsubmit="return checkForm();">
          <?php
          // Database search Form {{{
          $this->contentContainerStart();
          ?>
          <table class="table-condensed">
            <tr>
              <td><?php printMLText("search_query"); ?>:</td>
              <td>
                <input type="text" name="query" value="<?php echo htmlspecialchars($this->query); ?>"/>
                <select name="mode">
                  <option value="1" <?php echo ($mode == 'AND') ? "selected" : ""; ?>><?php printMLText("search_mode_and"); ?>
                  <option value="0"<?php echo ($mode == 'OR') ? "selected" : ""; ?>><?php printMLText("search_mode_or"); ?>
                </select>
              </td>
            </tr>
            <tr>
              <td><?php printMLText("search_in"); ?>:</td>
              <td>
                <label class="checkbox" for="keywords"><input type="checkbox"
                                                              id="keywords"
                                                              name="searchin[]"
                                                              value="1" <?php if (in_array('1', $searchin)) {
                    echo " checked";
                  } ?>><?php printMLText("keywords"); ?> (<?php printMLText('documents_only'); ?>)</label>
                <label class="checkbox" for="searchName"><input type="checkbox"
                                                                name="searchin[]"
                                                                id="searchName"
                                                                value="2" <?php if (in_array('2', $searchin)) {
                    echo " checked";
                  } ?>><?php printMLText("name"); ?></label>
                <label class="checkbox" for="comment"><input type="checkbox"
                                                             name="searchin[]"
                                                             id="comment"
                                                             value="3" <?php if (in_array('3', $searchin)) {
                    echo " checked";
                  } ?>><?php printMLText("comment"); ?></label>
                <label class="checkbox" for="attributes"><input type="checkbox"
                                                                name="searchin[]"
                                                                id="attributes"
                                                                value="4" <?php if (in_array('4', $searchin)) {
                    echo " checked";
                  } ?>><?php printMLText("attributes"); ?></label>
              </td>
            </tr>
            <tr>
              <td><?php printMLText("owner"); ?>:</td>
              <td>
                <select class="chzn-select-deselect"
                        name="ownerid"
                        data-placeholder="<?php printMLText('select_users'); ?>"
                        data-no_results_text="<?php printMLText('unknown_owner'); ?>">
                  <option value="-1"></option>
                  <?php
                  foreach ($allUsers as $userObj) {
                    if ($userObj->isGuest()) {
                      continue;
                    }
                    print "<option value=\"" . $userObj->getID() . "\" " . (($owner && $userObj->getID() == $owner->getID()) ? "selected" : "") . ">" . htmlspecialchars($userObj->getLogin() . " - " . $userObj->getFullName()) . "\n";
                  }
                  ?>
                </select>
              </td>
            </tr>
            <tr>
              <td><?php printMLText("under_folder") ?>:</td>
              <td><?php $this->printFolderChooser("form1", AccessMode::READ, -1, $startfolder); ?></td>
            </tr>
            <tr>
              <td><?php printMLText("creation_date"); ?>:</td>
              <td>
                <label class="checkbox inline">
                  <input type="checkbox" name="creationdate" value="true" <?php if ($creationdate) {
                    echo "checked";
                  } ?>/><?php printMLText("between"); ?>
                </label><br/>
        <span class="input-append date"
              style="display: inline;"
              id="createstartdate"
              data-date="<?php echo date('d-m-Y'); ?>"
              data-date-format="dd-mm-yyyy"
              data-date-language="<?php echo str_replace('_', '-', $this->params['session']->getLanguage()); ?>">
          <input class="span4" size="16" name="createstart" type="text" value="<?php if ($startdate) {
            printf("%02d-%02d-%04d", $startdate['day'], $startdate['month'], $startdate['year']);
          }
          else {
            echo date('d-m-Y');
          } ?>">
          <span class="add-on"><span class="fa fa-calendar"></span></span>
        </span>&nbsp;
                <?php printMLText("and"); ?>
                <span class="input-append date"
                      style="display: inline;"
                      id="createenddate"
                      data-date="<?php echo date('d-m-Y'); ?>"
                      data-date-format="dd-mm-yyyy"
                      data-date-language="<?php echo str_replace('_',
                        '-',
                        $this->params['session']->getLanguage()); ?>">
          <input class="span4" size="16" name="createend" type="text" value="<?php if ($stopdate) {
            printf("%02d-%02d-%04d", $stopdate['day'], $stopdate['month'], $stopdate['year']);
          }
          else {
            echo date('d-m-Y');
          } ?>">
          <span class="add-on"><span class="fa fa-calendar"></span></span>
        </span>
              </td>
            </tr>
            <tr>
              <td></td>
              <td>
                <button type="submit" class="btn"><span class="fa fa-search"></span> <?php printMLText("search"); ?>
                </button>
              </td>
            </tr>

          </table>
          <?php
          $this->contentContainerEnd();
          // }}}

          /* First check if any of the folder filters are set. If it is,
           * open the accordion.
           */
          $openfilterdlg = false;
          if ($attrdefs) {
            foreach ($attrdefs as $attrdef) {
              $attricon = '';
              if ($attrdef->getObjType() == \BakeryDMS\Core\AttributeDefinition::objtype_document || $attrdef->getObjType() == \BakeryDMS\Core\AttributeDefinition::objtype_documentcontent) {
                if (!empty($attributes[$attrdef->getID()])) {
                  $openfilterdlg = true;
                }
              }
            }
          }
          if ($categories) {
            $openfilterdlg = true;
          }
          if ($status) {
            $openfilterdlg = true;
          }
          if ($expirationdate) {
            $openfilterdlg = true;
          }
          ?>
          <div class="accordion" id="accordion2">
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                  <?php printMLText('filter_for_documents'); ?>
                </a>
              </div>
              <div id="collapseOne" class="accordion-body <?php if (!$openfilterdlg) {
                echo "collapse";
              } ?>" style="_height: 0px;">
                <div class="accordion-inner">
                  <table class="table-condensed">
                    <tr>
                      <td><?php printMLText("category"); ?>:</td>
                      <td>
                        <select class="chzn-select"
                                name="categoryids[]"
                                multiple="multiple"
                                data-placeholder="<?php printMLText('select_category'); ?>"
                                data-no_results_text="<?php printMLText('unknown_document_category'); ?>">
                          <!--
<option value="-1"><?php printMLText("all_categories"); ?>
-->
                          <?php
                          $tmpcatids = array();
                          foreach ($categories as $tmpcat) {
                            $tmpcatids[] = $tmpcat->getID();
                          }
                          foreach ($allCats as $catObj) {
                            print "<option value=\"" . $catObj->getID() . "\" " . (in_array($catObj->getID(),
                                $tmpcatids) ? "selected" : "") . ">" . htmlspecialchars($catObj->getName()) . "\n";
                          }
                          ?>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td><?php printMLText("status"); ?>:</td>
                      <td>
                        <?php if ($workflowmode == 'traditional') { ?>
                          <label class="checkbox" for='pendingReview'><input type="checkbox"
                                                                             id="pendingReview"
                                                                             name="pendingReview"
                                                                             value="1" <?php echo in_array(DocumentState::DRAFT_REV,
                              $status) ? "checked" : ""; ?>><?php printOverallStatusText(DocumentState::DRAFT_REV); ?></label>
                          <label class="checkbox" for='pendingApproval'><input type="checkbox"
                                                                               id="pendingApproval"
                                                                               name="pendingApproval"
                                                                               value="1" <?php echo in_array(DocumentState::DRAFT_APP,
                              $status) ? "checked" : ""; ?>><?php printOverallStatusText(DocumentState::DRAFT_APP); ?></label>
                        <?php }
                        else { ?>
                          <label class="checkbox" for='inWorkflow'><input type="checkbox"
                                                                          id="inWorkflow"
                                                                          name="inWorkflow"
                                                                          value="1" <?php echo in_array(DocumentState::IN_WORKFLOW,
                              $status) ? "checked" : ""; ?>><?php printOverallStatusText(DocumentState::IN_WORKFLOW); ?></label>
                        <?php } ?>
                        <label class="checkbox" for='released'><input type="checkbox"
                                                                      id="released"
                                                                      name="released"
                                                                      value="1" <?php echo in_array(DocumentState::RELEASED,
                            $status) ? "checked" : ""; ?>><?php printOverallStatusText(DocumentState::RELEASED); ?></label>
                        <label class="checkbox" for='rejected'><input type="checkbox"
                                                                      id="rejected"
                                                                      name="rejected"
                                                                      value="1" <?php echo in_array(DocumentState::REJECTED,
                            $status) ? "checked" : ""; ?>><?php printOverallStatusText(DocumentState::REJECTED); ?></label>
                        <label class="checkbox" for='obsolete'><input type="checkbox"
                                                                      id="obsolete"
                                                                      name="obsolete"
                                                                      value="1" <?php echo in_array(DocumentState::OBSOLETE,
                            $status) ? "checked" : ""; ?>><?php printOverallStatusText(DocumentState::OBSOLETE); ?></label>
                        <label class="checkbox" for='expired'><input type="checkbox"
                                                                     id="expired"
                                                                     name="expired"
                                                                     value="1" <?php echo in_array(DocumentState::EXPIRED,
                            $status) ? "checked" : ""; ?>><?php printOverallStatusText(DocumentState::EXPIRED); ?></label>
                      </td>
                    </tr>
                    <tr>
                      <td><?php printMLText("expires"); ?>:</td>
                      <td>
                        <label class="checkbox inline">
                          <input type="checkbox" name="expirationdate" value="true" <?php if ($expirationdate) {
                            echo "checked";
                          } ?>/><?php printMLText("between"); ?>
                        </label><br/>
        <span class="input-append date"
              style="display: inline;"
              id="expirationstartdate"
              data-date="<?php echo date('d-m-Y'); ?>"
              data-date-format="dd-mm-yyyy"
              data-date-language="<?php echo str_replace('_', '-', $this->params['session']->getLanguage()); ?>">
          <input class="span4" size="16" name="expirationstart" type="text" value="<?php if ($expstartdate) {
            printf("%02d-%02d-%04d", $expstartdate['day'], $expstartdate['month'], $expstartdate['year']);
          }
          else {
            echo date('d-m-Y');
          } ?>">
          <span class="add-on"><span class="fa fa-calendar"></span></span>
        </span>&nbsp;
                        <?php printMLText("and"); ?>
                        <span class="input-append date"
                              style="display: inline;"
                              id="expirationenddate"
                              data-date="<?php echo date('d-m-Y'); ?>"
                              data-date-format="dd-mm-yyyy"
                              data-date-language="<?php echo str_replace('_',
                                '-',
                                $this->params['session']->getLanguage()); ?>">
          <input class="span4" size="16" name="expirationend" type="text" value="<?php if ($expstopdate) {
            printf("%02d-%02d-%04d", $expstopdate['day'], $expstopdate['month'], $expstopdate['year']);
          }
          else {
            echo date('d-m-Y');
          } ?>">
          <span class="add-on"><span class="fa fa-calendar"></span></span>
        </span>
                      </td>
                    </tr>
                    <?php
                    if ($attrdefs) {
                      foreach ($attrdefs as $attrdef) {
                        $attricon = '';
                        if ($attrdef->getObjType() == \BakeryDMS\Core\AttributeDefinition::objtype_document || $attrdef->getObjType() == \BakeryDMS\Core\AttributeDefinition::objtype_documentcontent) {
                          ?>
                          <tr>
                            <td><?php echo htmlspecialchars($attrdef->getName()); ?>:</td>
                            <td><?php $this->printAttributeEditField($attrdef,
                                isset($attributes[$attrdef->getID()]) ? $attributes[$attrdef->getID()] : '') ?></td>
                          </tr>

                        <?php
                        }
                      }
                    }
                    ?>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <?php
          /* First check if any of the folder filters are set. If it is,
           * open the accordion.
           */
          $openfilterdlg = false;
          if ($attrdefs) {
            foreach ($attrdefs as $attrdef) {
              $attricon = '';
              if ($attrdef->getObjType() == \BakeryDMS\Core\AttributeDefinition::objtype_folder) {
                if (!empty($attributes[$attrdef->getID()])) {
                  $openfilterdlg = true;
                }
              }
            }
          }
          ?>
          <div class="accordion" id="accordion3">
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseFolder">
                  <?php printMLText('filter_for_folders'); ?>
                </a>
              </div>
              <div id="collapseFolder" class="accordion-body <?php if (!$openfilterdlg) {
                echo "collapse";
              } ?>" style="_height: 0px;">
                <div class="accordion-inner">
                  <table class="table-condensed">
                    <?php
                    if ($attrdefs) {
                      foreach ($attrdefs as $attrdef) {
                        $attricon = '';
                        if ($attrdef->getObjType() == \BakeryDMS\Core\AttributeDefinition::objtype_folder) {
                          ?>
                          <tr>
                            <td><?php echo htmlspecialchars($attrdef->getName()); ?>:</td>
                            <td><?php $this->printAttributeEditField($attrdef,
                                isset($attributes[$attrdef->getID()]) ? $attributes[$attrdef->getID()] : '') ?></td>
                          </tr>
                        <?php
                        }
                      }
                    }
                    ?>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <?php
      if ($enablefullsearch) {
        echo "<div class=\"tab-pane " . (($fullsearch == true) ? 'active' : '') . "\" id=\"fulltext\">\n";
        $this->contentContainerStart();
        ?>
        <form action="../op/op.Search.php" name="form2" onsubmit="return checkForm();">
          <input type="hidden" name="fullsearch" value="1"/>
          <table class="table-condensed">
            <tr>
              <td><?php printMLText("search_query"); ?>:</td>
              <td>
                <input type="text" name="query" value="<?php echo htmlspecialchars($this->query); ?>"/>
                <!--
<select name="mode">
<option value="1" selected><?php printMLText("search_mode_and"); ?>
<option value="0"><?php printMLText("search_mode_or"); ?>
</select>
-->
              </td>
            </tr>
            <tr>
              <td><?php printMLText("owner"); ?>:</td>
              <td>
                <select class="chzn-select-deselect" name="ownerid">
                  <option value="-1"></option>
                  <?php
                  foreach ($allUsers as $userObj) {
                    if ($userObj->isGuest()) {
                      continue;
                    }
                    print "<option value=\"" . $userObj->getID() . "\" " . (($owner && $userObj->getID() == $owner->getID()) ? "selected" : "") . ">" . htmlspecialchars($userObj->getLogin() . " - " . $userObj->getFullName()) . "\n";
                  }
                  ?>
                </select>
              </td>
            </tr>
            <tr>
              <td><?php printMLText("category_filter"); ?>:</td>
              <td>
                <select class="chzn-select"
                        name="categoryids[]"
                        multiple="multiple"
                        data-placeholder="<?php printMLText('select_category'); ?>"
                        data-no_results_text="<?php printMLText('unknown_document_category'); ?>">
                  <!--
<option value="-1"><?php printMLText("all_categories"); ?>
-->
                  <?php
                  $tmpcatids = array();
                  foreach ($categories as $tmpcat) {
                    $tmpcatids[] = $tmpcat->getID();
                  }
                  foreach ($allCats as $catObj) {
                    print "<option value=\"" . $catObj->getID() . "\" " . (in_array($catObj->getID(),
                        $tmpcatids) ? "selected" : "") . ">" . htmlspecialchars($catObj->getName()) . "\n";
                  }
                  ?>
                </select>
              </td>
            </tr>
            <tr>
              <td></td>
              <td>
                <button type="submit" class="btn"><span class="fa fa-search"></span> <?php printMLText("search"); ?>
                </button>
              </td>
            </tr>
          </table>

        </form>
        <?php
        $this->contentContainerEnd();
        echo "</div>\n";
      }
      ?>
    </div>
    <?php
    echo "</div>\n";
    echo "<div class=\"span8\">\n";
// Database search Result {{{
    $foldercount = $doccount = 0;
    if ($entries) {
      foreach ($entries as $entry) {
        if (get_class($entry) == Document::class) {
          $doccount++;
        }
        elseif (get_class($entry) == Folder::class) {
          $foldercount++;
        }
      }
      print "<div class=\"alert\">" . getMLText("search_report",
          array("doccount" => $totaldocs, "foldercount" => $totalfolders, 'searchtime' => $searchTime)) . "</div>";
      $this->pageList($pageNumber, $totalpages, "../op/op.Search.php", $urlparams);
//			$this->contentContainerStart();

      print "<table class=\"table\">";
      print "<thead>\n<tr>\n";
      print "<th></th>\n";
      print "<th>" . getMLText("name") . "</th>\n";
      print "<th>" . getMLText("attributes") . "</th>\n";
      print "<th>" . getMLText("status") . "</th>\n";
      print "<th>" . getMLText("action") . "</th>\n";
      print "</tr>\n</thead>\n<tbody>\n";

      $previewer = new Previewer($cachedir, $previewwidth);
      foreach ($entries as $entry) {
        if (get_class($entry) == Document::class) {
          $document = $entry;
          $owner = $document->getOwner();
          $lc = $document->getLatestContent();
          $version = $lc->getVersion();
          $previewer->createPreview($lc);

          if (in_array(3, $searchin)) {
            $comment = $this->markQuery(htmlspecialchars($document->getComment()));
          }
          else {
            $comment = htmlspecialchars($document->getComment());
          }
          if (strlen($comment) > 150) {
            $comment = substr($comment, 0, 147) . "...";
          }
          print "<tr id=\"table-row-document-" . $document->getID() . "\">";
          //print "<td><img src=\"../out/images/file.gif\" class=\"mimeicon\"></td>";
          if (in_array(2, $searchin)) {
            $docName = $this->markQuery(htmlspecialchars($document->getName()), "i");
          }
          else {
            $docName = htmlspecialchars($document->getName());
          }
          print "<td><a class=\"standardText\" href=\"../out/out.ViewDocument.php?documentid=" . $document->getID() . "\">";
          if ($previewer->hasPreview($lc)) {
            print "<img class=\"mimeicon\" width=\"" . $previewwidth . "\"src=\"../op/op.Preview.php?documentid=" . $document->getID() . "&version=" . $lc->getVersion() . "&width=" . $previewwidth . "\" title=\"" . htmlspecialchars($lc->getMimeType()) . "\">";
          }
          else {
            print "<img class=\"mimeicon\" src=\"" . $this->getMimeIcon($lc->getFileType()) . "\" title=\"" . htmlspecialchars($lc->getMimeType()) . "\">";
          }
          print "</a></td>";
          print "<td><a class=\"standardText\" href=\"../out/out.ViewDocument.php?documentid=" . $document->getID() . "\">/";
          $folder = $document->getFolder();
          $path = $folder->getPath();
          for ($i = 1; $i < count($path); $i++) {
            print htmlspecialchars($path[$i]->getName()) . "/";
          }
          print $docName;
          print "</a>";
          print "<br /><span style=\"font-size: 85%; font-style: italic; color: #666; \">" . getMLText('owner') . ": <b>" . htmlspecialchars($owner->getFullName()) . "</b>, " . getMLText('creation_date') . ": <b>" . date('Y-m-d',
              $document->getDate()) . "</b>, " . getMLText('version') . " <b>" . $version . "</b> - <b>" . date('Y-m-d',
              $lc->getDate()) . "</b></span>";
          if ($comment) {
            print "<br /><span style=\"font-size: 85%;\">" . htmlspecialchars($comment) . "</span>";
          }
          print "</td>";

          print "<td>";
          print "<ul class=\"unstyled\">\n";
          $lcattributes = $lc->getAttributes();
          if ($lcattributes) {
            foreach ($lcattributes as $lcattribute) {
              $attrdef = $lcattribute->getAttributeDefinition();
              print "<li>" . htmlspecialchars($attrdef->getName()) . ": " . htmlspecialchars($lcattribute->getValue()) . "</li>\n";
            }
          }
          print "</ul>\n";
          print "<ul class=\"unstyled\">\n";
          $docttributes = $document->getAttributes();
          if ($docttributes) {
            foreach ($docttributes as $docttribute) {
              $attrdef = $docttribute->getAttributeDefinition();
              print "<li>" . htmlspecialchars($attrdef->getName()) . ": " . htmlspecialchars($docttribute->getValue()) . "</li>\n";
            }
          }
          print "</ul>\n";
          print "</td>";

          $display_status = $lc->getStatus();
          print "<td>" . getOverallStatusText($display_status["status"]) . "</td>";
          print "<td>";
          print "<div class=\"list-action\">";
          if ($document->getAccessMode($user) >= AccessMode::ALL) {
            $this->printDeleteDocumentButton($document, 'splash_rm_document');
          }
          else {
            ?>
            <span style="padding: 2px; color: #CCC;"><span class="fa fa-remove"></span></span>
          <?php
          }
          if ($document->getAccessMode($user) >= AccessMode::READWRITE) {
            ?>
            <a href="../out/out.EditDocument.php?documentid=<?php echo $document->getID(); ?>"><span class="fa fa-edit"></span></a>
          <?php
          }
          else {
            ?>
            <span style="padding: 2px; color: #CCC;"><span class="fa fa-edit"></span></span>
          <?php
          }
          ?>
          <a class="addtoclipboard"
             rel="<?php echo "D" . $document->getID(); ?>"
             msg="<?php printMLText('splash_added_to_clipboard'); ?>"
             _href="../op/op.AddToClipboard.php?documentid=<?php echo $document->getID(); ?>&type=document&id=<?php echo $document->getID(); ?>&refferer=<?php echo urlencode($this->params['refferer']); ?>"
             title="<?php printMLText("add_to_clipboard"); ?>"><span class="fa fa-copy"></span></a>
          <?php
          print "</div>";
          print "</td>";

          print "</tr>\n";
        }
        elseif (get_class($entry) == Folder::class) {
          $folder = $entry;
          $owner = $folder->getOwner();
          if (in_array(2, $searchin)) {
            $folderName = $this->markQuery(htmlspecialchars($folder->getName()), "i");
          }
          else {
            $folderName = htmlspecialchars($folder->getName());
          }
          print "<tr id=\"table-row-folder-" . $folder->getID() . "\" rel=\"folder_" . $folder->getID() . "\" class=\"folder\" ondragover=\"allowDrop(event)\" ondrop=\"onDrop(event)\">";
          print "<td><a class=\"standardText\" href=\"../out/out.ViewFolder.php?folderid=" . $folder->getID() . "\"><img src=\"" . $this->imgpath . "folder.png\" width=\"24\" height=\"24\" border=0></a></td>";
          print "<td><a class=\"standardText\" href=\"../out/out.ViewFolder.php?folderid=" . $folder->getID() . "\">";
          $path = $folder->getPath();
          print "/";
          for ($i = 1; $i < count($path) - 1; $i++) {
            print htmlspecialchars($path[$i]->getName()) . "/";
          }
          print $folderName;
          print "</a>";
          print "<br /><span style=\"font-size: 85%; font-style: italic; color: #666;\">" . getMLText('owner') . ": <b>" . htmlspecialchars($owner->getFullName()) . "</b>, " . getMLText('creation_date') . ": <b>" . date('Y-m-d',
              $folder->getDate()) . "</b></span>";
          if (in_array(3, $searchin)) {
            $comment = $this->markQuery(htmlspecialchars($folder->getComment()));
          }
          else {
            $comment = htmlspecialchars($folder->getComment());
          }
          if (strlen($comment) > 50) {
            $comment = substr($comment, 0, 47) . "...";
          }
          if ($comment) {
            print "<br /><span style=\"font-size: 85%;\">" . htmlspecialchars($comment) . "</span>";
          }
          print "</td>";
          print "<td>";
          print "<ul class=\"unstyled\">\n";
          $folderattributes = $folder->getAttributes();
          if ($folderattributes) {
            foreach ($folderattributes as $folderattribute) {
              $attrdef = $folderattribute->getAttributeDefinition();
              print "<li>" . htmlspecialchars($attrdef->getName()) . ": " . htmlspecialchars($folderattribute->getValue()) . "</li>\n";
            }
          }
          print "</td>";
          print "<td></td>";
          print "<td>";
          print "<div class=\"list-action\">";
          if ($folder->getAccessMode($user) >= AccessMode::ALL) {
            $this->printDeleteFolderButton($folder, 'splash_rm_folder');
          }
          else {
            ?>
            <span style="padding: 2px; color: #CCC;"><span class="fa fa-remove"></span></span>
          <?php
          }
          if ($folder->getAccessMode($user) >= AccessMode::READWRITE) {
            ?>
            <a class_="btn btn-mini"
               href="../out/out.EditFolder.php?folderid=<?php echo $folder->getID(); ?>"><span class="fa fa-edit"></span></a>
          <?php
          }
          else {
            ?>
            <span style="padding: 2px; color: #CCC;"><span class="fa fa-edit"></span></span>
          <?php
          }
          ?>
          <a class="addtoclipboard"
             rel="<?php echo "F" . $folder->getID(); ?>"
             msg="<?php printMLText('splash_added_to_clipboard'); ?>"
             _href="../op/op.AddToClipboard.php?folderid=<?php echo $folder->getID(); ?>&type=folder&id=<?php echo $folder->getID(); ?>&refferer=<?php echo urlencode($this->params['refferer']); ?>"
             title="<?php printMLText("add_to_clipboard"); ?>"><span class="fa fa-copy"></span></a>
          <?php
          print "</div>";
          print "</td>";
          print "</tr>\n";
        }
      }
      print "</tbody></table>\n";
//			$this->contentContainerEnd();
      $this->pageList($pageNumber, $totalpages, "../op/op.Search.php", $_GET);
    }
    else {
      $numResults = $doccount + $foldercount;
      if ($numResults == 0) {
        print "<div class=\"alert alert-error\">" . getMLText("search_no_results") . "</div>";
      }
    }
// }}}
    echo "</div>";
    echo "</div>";
    $this->htmlEndPage();
  }
}

?>

