<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
use BakeryDMS\Core\Group;
use BakeryDMS\Core\User;

require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for GroupMgr view.
 */
class SeedDMS_View_GroupMgr extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    /** @var User $user */
    $user = $this->params['user'];
    /** @var Group $selectedGroup */
    $selectedGroup = $this->params['selgroup'];
    /** @var User[] $allUsers */
    $allUsers = $this->params['allusers'];
    /** @var Group[] $allGroups */
    $allGroups = $this->params['allgroups'];
    $strictformcheck = $this->params['strictformcheck'];

    $this->htmlStartPage(t("admin_tools"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(t("admin_tools"), "admin_tools");

    ?>
    <script language="JavaScript">

      function checkForm1(num) {
        var msg = [];
        eval("var formObj = document.form" + num + "_1;");

        if (formObj.name.value == "") msg.push("<?= t("js_no_name"); ?>");
        <?php
          if ($strictformcheck) {
        ?>
        if (formObj.comment.value == "") msg.push("<?= t("js_no_comment"); ?>");
        <?php
          }
        ?>
        if (msg != "") {
          noty({
            text: msg.join('<br />'),
            type: 'error',
            dismissQueue: true,
            layout: 'topRight',
            theme: 'defaultTheme',
            _timeout: 1500
          });
          return false;
        }
        else
          return true;
      }

      function checkForm2(num) {
        var msg = "";
        eval("var formObj = document.form" + num + "_2;");

        if (formObj.userid.options[formObj.userid.selectedIndex].value == -1) msg += "<?= t("js_select_user"); ?>\n";

        if (msg != "") {
          noty({
            text: msg,
            type: 'error',
            dismissQueue: true,
            layout: 'topRight',
            theme: 'defaultTheme',
            _timeout: 1500
          });
          return false;
        }
        else
          return true;
      }

      var obj = -1;
      function showUser(selectObj) {
        if (obj != -1)
          obj.style.display = "none";

        id = selectObj.options[selectObj.selectedIndex].value;
        if (id == -1)
          return;

        obj = document.getElementById("keywords" + id);
        obj.style.display = "";
      }
    </script>
    <?php
    $this->contentHeading(t("group_management"));
    ?>

    <div class="row-fluid">
      <div class="span4">
        <div class="well form-horizontal">
          <div class="control-group">
            <label class="control-label span2" for="selector"><?php echo t("selection") ?>:</label>
            <div class="controls span10">
              <select onchange="showUser(this)" id="selector" class="span9">
                <option value="-1"><?php echo t("choose_group") ?>
                <option value="0"><?php echo t("add_group") ?>
                  <?php
                  $selected = 0;
                  $count = 2;
                  foreach ($allGroups as $group) {
                    if ($selectedGroup && $group->getID() == $selectedGroup->getID()) {
                      $selected = $count;
                    }
                    echo '<option value="' . $group->getID() . '">' . htmlspecialchars($group->getName());
                    $count++;
                  }
                  ?>
              </select>
            </div>
          </div>
        </div>
      </div>

      <div class="span8">
        <div class="well">
          <table class="table-condensed">
            <tr>
              <td id="keywords0" style="display : none;">
                <form action="../op/op.GroupMgr.php" name="form0_1" method="post" onsubmit="return checkForm1('0');">
                  <?php echo createHiddenFieldWithKey('addgroup'); ?>
                  <input type="Hidden" name="action" value="addgroup">
                  <table>
                    <tr>
                      <td><?= t("name"); ?>:</td>
                      <td><input type="text" name="name"></td>
                    </tr>
                    <tr>
                      <td><?= t("comment"); ?>:</td>
                      <td><textarea name="comment" rows="4" cols="50"></textarea></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><input type="submit" class="btn" value="<?= t("add_group"); ?>"></td>
                    </tr>
                  </table>
                </form>

              <?php
                foreach ($allGroups as $group) {
                  echo '<td id="keywords' . $group->getID() . '" style="display:none;">';
              ?>
                  <form action="../op/op.GroupMgr.php" name="form<?php print $group->getID(); ?>_1" method="post" onsubmit="return checkForm1('<?= $group->getID(); ?>');">
                  <?php echo createHiddenFieldWithKey('editgroup'); ?>
                  <input type="Hidden" name="groupid" value="<?php print $group->getID(); ?>">
                  <input type="Hidden" name="action" value="editgroup">
                  <table>
                    <tr>
                      <td></td>
                      <td><a href="../out/out.RemoveGroup.php?groupid=<?= $group->getID(); ?>" class="btn"><span class="fa fa-remove"></span> <?= t("rm_group"); ?></a></td>
                    </tr>
                    <tr>
                      <td><?= t("name"); ?>:</td>
                      <td><input type="text" name="name" value="<?= htmlspecialchars($group->getName()); ?>"></td>
                    </tr>
                    <tr>
                      <td><?= t("comment"); ?>:</td>
                      <td><textarea name="comment" rows="4" cols="50"><?= htmlspecialchars($group->getComment()); ?></textarea></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><button type="submit" class="btn"><span class="fa fa-save"></span> <?= t("save") ?></button></td>
                    </tr>
                  </table>
                  </form>
                  <?php $this->contentSubHeading(t("group_members")); ?>
                  <table class="table-condensed">
                    <?php
                      $members = $group->getUsers();
                      if (count($members) == 0) {
                        echo '<tr><td>' . t("no_group_members") . '</td></tr>';
                      } else {
                        foreach ($members as $member) {
                          echo '<tr>';
                          echo '<td><span class="fa fa-user"></span></td>';
                          echo '<td>' . htmlspecialchars($member->getFullName()) . '</td>';
                          echo '<td>' . ($group->isMember($member, true) ? t("manager") : "&nbsp;") . '</td>';
                          echo '<td>';
                          echo '<form action="../op/op.GroupMgr.php" method="post" class="form-inline" style="display: inline-block; margin-bottom: 0px;"><input type="hidden" name="action" value="rmmember" /><input type="hidden" name="groupid" value="' . $group->getID() . '" /><input type="hidden" name="userid" value="' . $member->getID() . '" />' . createHiddenFieldWithKey('rmmember') . '<button type="submit" class="btn btn-mini"><span class="fa fa-remove"></span> ' . t("remove") . '</button></form>';
                          echo "&nbsp;";
                          echo '<form action="../op/op.GroupMgr.php" method="post" class="form-inline" style="display: inline-block; margin-bottom: 0px;"><input type="hidden" name="groupid" value="' . $group->getID() . '" /><input type="hidden" name="action" value="tmanager" /><input type="hidden" name="userid" value="' . $member->getID() . "\" />" . createHiddenFieldWithKey('tmanager') . '<button type="submit" class="btn btn-mini"><span class="fa fa-random"></span> ' . t("toggle_manager") . '</button></form>';
                          echo '</td></tr>';
                        }
                      }
                    ?>
                  </table>
                  <br/>
                  <form action="../op/op.GroupMgr.php?groupid=<?= $group->getID(); ?>" method="POST" name="form<?= $group->getID(); ?>_3" onsubmit="return checkForm1('<?= $group->getID(); ?>');">
                    <input type="Hidden" name="groupid" value="<?= $group->getID(); ?>">
                    <input type="Hidden" name="action" value="remove-members">
                    <button type="submit" class="btn btn-small btn-default"><span class="fa fa-remove"></span> <?= t('remove_all_users_from_group'); ?></button>
                  </form>
                  <form action="../out/out.RemoveUser.php?groupid=<?= $group->getID(); ?>" method="POST" name="form<?= $group->getID(); ?>_4" onsubmit="return checkForm1('<?= $group->getID(); ?>');">
                    <button type="submit" class="btn btn-small btn-danger"><span class="fa fa-trash"></span> <?= t('delete_all_users_from_system'); ?></button>
                  </form>
                  <?php $this->contentSubHeading(t("add_member")); ?>
                  <form class="form-inline" action="../op/op.GroupMgr.php" method="POST" name="form<?= $group->getID(); ?>_2" onsubmit="return checkForm2('<?= $group->getID(); ?>');">
                    <?= createHiddenFieldWithKey('addmember'); ?>
                    <input type="Hidden" name="action" value="addmember">
                    <input type="Hidden" name="groupid" value="<?= $group->getID(); ?>">
                    <table class="table-condensed">
                    <tr>
                      <td>
                        <select name="userid">
                          <option value="-1"><?= t("select_one"); ?>
                          <?php
                          foreach ($allUsers as $currUser) {
                            if (!$group->isMember($currUser)) {
                              echo '<option value="' . $currUser->getID() . '">' . htmlspecialchars($currUser->getLogin() . " - " . $currUser->getFullName());
                            }
                          }
                          ?>
                        </select>
                      </td>
                      <td>
                        <label class="checkbox"><input type="checkbox" name="manager" value="1"><?= t("manager"); ?></label>
                      </td>
                      <td>
                        <input type="submit" class="btn" value="<?= t("add"); ?>">
                      </td>
                    </tr>
                    </table>
                  </form>
                </td>
              <?php } ?>
            </tr>
          </table>
        </div>
      </div>
    </div>

    <script language="JavaScript">

      sel = document.getElementById("selector");
      sel.selectedIndex =<?php print $selected ?>;
      showUser(sel);

    </script>

    <?php
    $this->contentContainerEnd();
    $this->htmlEndPage();
  }
}

?>
