<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for ChangePassword view.
 */
class SeedDMS_View_ChangePassword extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $referuri = $this->params['referuri'];
    $hash = $this->params['hash'];
    $passwordstrength = $this->params['passwordstrength'];

    $this->htmlStartPage(getMLText("change_password"), "login");
    $this->globalBanner();
    $this->contentStart();
    $this->pageNavigation(getMLText("change_password"));
    $this->contentContainerStart();
    ?>
    <form action="../op/op.ChangePassword.php" method="post" name="form1" onsubmit="return checkForm();">
      <?php
      if ($referuri) {
        echo "<input type='hidden' name='referuri' value='" . $referuri . "'/>";
      }
      if ($hash) {
        echo "<input type='hidden' name='hash' value='" . $hash . "'/>";
      }
      ?>
      <table class="table-condensed">
        <tr>
          <td><?php printMLText("password"); ?>:</td>
          <td><input class="pwd" type="password" rel="strengthbar" name="newpassword" id="password"></td>
        </tr>
        <?php
        if ($passwordstrength > 0) {
          ?>
          <tr>
            <td><?php printMLText("password_strength"); ?>:</td>
            <td>
              <div id="strengthbar" class="progress" style="width: 220px; height: 30px; margin-bottom: 8px;">
                <div class="bar bar-danger" style="width: 0%;"></div>
              </div>
            </td>
          </tr>
        <?php
        }
        ?>
        <tr>
          <td><?php printMLText("confirm_pwd"); ?>:</td>
          <td><input type="password" name="newpasswordrepeat" id="passwordrepeat"></td>
        </tr>
        <tr>
          <td></td>
          <td><input class="btn" type="submit" value="<?php printMLText("submit_password") ?>"></td>
        </tr>
      </table>
    </form>
    <?php $this->contentContainerEnd(); ?>
    <script language="JavaScript">document.form1.newpassword.focus();</script>
    <p><a href="../out/out.Login.php"><?php echo getMLText("login"); ?></a></p>
    <?php
    $this->htmlEndPage();
  }
}

?>
