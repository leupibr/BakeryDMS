<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for EditAttributes view.
 */
class SeedDMS_View_EditAttributes extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $folder = $this->params['folder'];
    $document = $this->params['document'];
    $version = $this->params['version'];
    $attrdefs = $this->params['attrdefs'];

    $this->htmlStartPage(getMLText("document_title", array("documentname" => htmlspecialchars($document->getName()))));
    $this->globalNavigation($folder);
    $this->contentStart();
    $this->pageNavigation($this->getFolderPathHTML($folder, true, $document), "view_document", $document);

    $this->contentHeading(getMLText("edit_attributes"));
    $this->contentContainerStart();
    ?>
    <form action="../op/op.EditAttributes.php" name="form1" method="POST">
      <?php echo createHiddenFieldWithKey('editattributes'); ?>
      <input type="Hidden" name="documentid" value="<?php print $document->getID(); ?>">
      <input type="Hidden" name="version" value="<?php print $version->getVersion(); ?>">
      <table class="table-condensed">
        <?php
        if ($attrdefs) {
          foreach ($attrdefs as $attrdef) {
            ?>
            <tr>
              <td><?php echo htmlspecialchars($attrdef->getName()); ?></td>
              <td><?php $this->printAttributeEditField($attrdef, $version->getAttributeValue($attrdef)) ?></td>
            </tr>
          <?php
          }
        }
        ?>
        <tr>
          <td colspan="2">
            <button type="submit" class="btn"><span class="fa fa-save"></span> <?php printMLText("save") ?></button>
          </td>
        </tr>
      </table>
    </form>
    <?php
    $this->contentContainerEnd();
    $this->htmlEndPage();
  }
}

?>
