<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for EditEvent view.
 */
class SeedDMS_View_EditEvent extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $event = $this->params['event'];
    $strictformcheck = $this->params['strictformcheck'];

    $this->htmlStartPage(getMLText("calendar"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText("calendar"), "calendar");

    $this->contentHeading(getMLText("edit_event"));
    $this->contentContainerStart();
    ?>
    <script language="JavaScript">
      function checkForm() {
        msg = new Array()
        if (document.form1.name.value == "") msg.push("<?php printMLText("js_no_name");?>");
        <?php
          if ($strictformcheck) {
        ?>
        if (document.form1.comment.value == "") msg.push("<?php printMLText("js_no_comment");?>");
        <?php
          }
        ?>
        if (msg != "") {
          noty({
            text: msg.join('<br />'),
            type: 'error',
            dismissQueue: true,
            layout: 'topRight',
            theme: 'defaultTheme',
            _timeout: 1500,
          });
          return false;
        }
        else
          return true;
      }
    </script>

    <form action="../op/op.EditEvent.php" name="form1" onsubmit="return checkForm();" method="POST">
      <?php echo createHiddenFieldWithKey('editevent'); ?>

      <input type="Hidden" name="eventid" value="<?php echo (int) $event["id"]; ?>">

      <table class="table-condensed">
        <tr>
          <td><?php printMLText("from"); ?>:</td>
          <td><?php //$this->printDateChooser($event["start"], "from");?>
            <span class="input-append date span12"
                  id="fromdate"
                  data-date="<?php echo date('d-m-Y', $event["start"]); ?>"
                  data-date-format="dd-mm-yyyy">
      		<input class="span6" size="16" name="from" type="text" value="<?php echo date('d-m-Y', $event["start"]); ?>">
      		<span class="add-on"><span class="fa fa-calendar"></span></span>
    		</span>
          </td>
        </tr>
        <tr>
          <td><?php printMLText("to"); ?>:</td>
          <td><?php //$this->printDateChooser($event["stop"], "to");?>
            <span class="input-append date span12"
                  id="todate"
                  data-date="<?php echo date('d-m-Y', $event["stop"]); ?>"
                  data-date-format="dd-mm-yyyy">
      		<input class="span6" size="16" name="to" type="text" value="<?php echo date('d-m-Y', $event["stop"]); ?>">
      		<span class="add-on"><span class="fa fa-calendar"></span></span>
    		</span>
          </td>
        </tr>
        <tr>
          <td class="inputDescription"><?php printMLText("name"); ?>:</td>
          <td><input type="text" name="name" value="<?php echo htmlspecialchars($event["name"]); ?>" size="60"></td>
        </tr>
        <tr>
          <td valign="top" class="inputDescription"><?php printMLText("comment"); ?>:</td>
          <td><textarea name="comment" rows="4" cols="80"><?php echo htmlspecialchars($event["comment"]) ?></textarea>
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <button type="submit" class="btn"><span class="fa fa-save"></span> <?php printMLText("save") ?></button>
          </td>
        </tr>
      </table>
    </form>
    <?php
    $this->contentContainerEnd();
    $this->htmlEndPage();
  }
}

?>
