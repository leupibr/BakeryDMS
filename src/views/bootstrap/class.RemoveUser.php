<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
use BakeryDMS\Core\Group;
use BakeryDMS\Core\User;

require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for RemoveUser view.
 */
class SeedDMS_View_RemoveUser extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    /** @var User|null $rmuser */
    $rmuser = $this->params['rmuser'];
    /** @var Group|null $group */
    $group = $this->params['group'];
    /** @var User[] $allusers */
    $allusers = $this->params['allusers'];

    $this->htmlStartPage(t("admin_tools"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(t("admin_tools"), "admin_tools");
    $this->contentHeading(t("rm_user"));
    $this->contentContainerStart();

    if(!empty($rmuser)) {
      echo '<form action="../op/op.UsrMgr.php" name="form1" method="post">';
      echo '<input type="hidden" name="userid" value="' . $rmuser->getID() . '">';
      echo '<input type="hidden" name="action" value="removeuser">';
      echo createHiddenFieldWithKey('removeuser');
      echo '<p>' . s(t("confirm_rm_user"), ["username" => htmlspecialchars($rmuser->getFullName())]) . '</p>';
    }
    else {
      echo '<form action="../op/op.GroupMgr.php?groupid='. $group->getID() . '" name="form1" method="post">';
      echo '<input type="hidden" name="groupid" value="' . $group->getID() . '">';
      echo '<input type="hidden" name="action" value="delete-members">';
      echo '<p>' . s(t("confirm_rm_groupuser"), ["groupname" => htmlspecialchars($group->getName())]) . '</p>';
    }

    ?>

      <p>
        <?= t("assign_user_property_to"); ?>:
        <select name="assignTo">
          <?php
          foreach ($allusers as $currUser) {
            echo '<option value="' . $currUser->getID() . '">' . htmlspecialchars($currUser->getLogin() . " - " . $currUser->getFullName()) . '</option>';
          }
          ?>
        </select>
      </p>

      <p>
        <button type="submit" class="btn btn-danger"><span class="fa fa-trash"></span> <?= t("remove"); ?></button>
      </p>

    </form>
    <?php
    $this->contentContainerEnd();
    $this->htmlEndPage();
  }
}

?>
