<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for DefaultKeywords view.
 */
class SeedDMS_View_DefaultKeywords extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $categories = $this->params['categories'];
    $selcategoryid = $this->params['selcategoryid'];

    $this->htmlStartPage(getMLText("admin_tools"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText("admin_tools"), "admin_tools");

    ?>
    <script language="JavaScript">

      function checkForm(num) {
        msg = new Array();
        eval("var formObj = document.form" + num + ";");

        if (formObj.name.value == "") msg.push("<?php printMLText("js_no_name");?>");
        if (msg != "") {
          noty({
            text: msg.join('<br />'),
            type: 'error',
            dismissQueue: true,
            layout: 'topRight',
            theme: 'defaultTheme',
            _timeout: 1500,
          });
          return false;
        }
        else
          return true;
      }

      function checkKeywordForm(num) {
        msg = new Array();
        eval("var formObj = document.formk" + num + ";");

        if (formObj.keywords.value == "") msg.push("<?php printMLText("js_no_name");?>");
        if (msg != "") {
          noty({
            text: msg.join('<br />'),
            type: 'error',
            dismissQueue: true,
            layout: 'topRight',
            theme: 'defaultTheme',
            _timeout: 1500,
          });
          return false;
        }
        else
          return true;
      }

      obj = -1;
      function showKeywords(selectObj) {
        if (obj != -1)
          obj.style.display = "none";

        id = selectObj.options[selectObj.selectedIndex].value;
        if (id == -1)
          return;

        obj = document.getElementById("keywords" + id);
        obj.style.display = "";
      }
    </script>
    <?php

    $this->contentHeading(getMLText("global_default_keywords"));
    ?>
    <div class="row-fluid">
      <div class="span4">
        <div class="well">
          <?php echo getMLText("selection") ?>:
          <select onchange="showKeywords(this)" id="selector" class="span9">
            <option value="-1"><?php echo getMLText("choose_category") ?>
            <option value="0"><?php echo getMLText("new_default_keyword_category") ?>
              <?php

              $selected = 0;
              $count = 2;
              foreach ($categories as $category) {

                $owner = $category->getOwner();
                if ((!$user->isAdmin()) && ($owner->getID() != $user->getID())) {
                  continue;
                }

                if ($selcategoryid && $category->getID() == $selcategoryid) {
                  $selected = $count;
                }
                print "<option value=\"" . $category->getID() . "\">" . htmlspecialchars($category->getName());
                $count++;
              }
              ?>
          </select>
        </div>
      </div>

      <div class="span8">
        <div class="well">

          <table class="table-condensed">
            <tr>
              <td id="keywords0" style="display : none;">
                <form class="form-inline"
                      action="../op/op.DefaultKeywords.php"
                      method="post"
                      name="form0"
                      onsubmit="return checkForm('0');">
                  <?php echo createHiddenFieldWithKey('addcategory'); ?>
                  <input type="hidden" name="action" value="addcategory">
                  <?php printMLText("name"); ?>: <input type="text" name="name">
                  <input type="submit" class="btn" value="<?php printMLText("new_default_keyword_category"); ?>">
                </form>
              </td>
              <?php
              foreach ($categories as $category) {

                $owner = $category->getOwner();
                if ((!$user->isAdmin()) && ($owner->getID() != $user->getID())) {
                  continue;
                }

                print "<td id=\"keywords" . $category->getID() . "\" style=\"display : none;\">";
                ?>
			<table class="table-condensed">
				<tr>
					<td></td>
					<td>
						<form action="../op/op.DefaultKeywords.php" method="post">
  						<?php echo createHiddenFieldWithKey('removecategory'); ?>
							<input type="Hidden" name="action" value="removecategory">
							<input type="Hidden" name="categoryid" value="<?php echo $category->getID() ?>">
							<button type="submit" class="btn" title="<?php echo getMLText("delete") ?>"><span class="fa fa-remove"></span> <?php printMLText("rm_default_keyword_category"); ?></button>
						</form>
					</td>
				</tr>
				<tr>
					<td><?php echo getMLText("name") ?>:</td>
					<td>
						<form class="form-inline" action="../op/op.DefaultKeywords.php" method="post" name="form<?php echo $category->getID() ?>" onsubmit="return checkForm('<?php echo $category->getID() ?>');">
  						<?php echo createHiddenFieldWithKey('editcategory'); ?>
							<input type="hidden" name="action" value="editcategory">
							<input type="hidden" name="categoryid" value="<?php echo $category->getID() ?>">
							<input name="name" type="text" value="<?php echo htmlspecialchars($category->getName()) ?>">
							<button type="submit" class="btn"><span class="fa fa-save"></span> <?php printMLText("save"); ?></button>
						</form>
					</td>
				</tr>
				<tr>
					<td><?php echo getMLText("default_keywords") ?>:</td>
					<td>
						<?php
                $lists = $category->getKeywordLists();
                if (count($lists) == 0) {
                  print getMLText("no_default_keywords");
                } else {
                  foreach ($lists as $list) {
                    ?>
                    <form class="form-inline"
                          style="display: inline-block;"
                          method="post"
                          action="../op/op.DefaultKeywords.php"
                          name="formk<?php echo $list['id'] ?>"
                          onsubmit="return checkKeywordForm('<?php echo $list['id'] ?>');">
                      <?php echo createHiddenFieldWithKey('editkeywords'); ?>
                      <input type="Hidden" name="categoryid" value="<?php echo $category->getID() ?>">
                      <input type="Hidden" name="keywordsid" value="<?php echo $list["id"] ?>">
                      <input type="Hidden" name="action" value="editkeywords">
                      <input name="keywords" type="text" value="<?php echo htmlspecialchars($list["keywords"]) ?>">
                      <button class="btn" title="<?php echo getMLText("save") ?>">
                        <span class="fa fa-save"></span> <?php echo getMLText("save") ?></button>
                    </form>
                    <form style="display: inline-block;" method="post" action="../op/op.DefaultKeywords.php">
                      <?php echo createHiddenFieldWithKey('removekeywords'); ?>
                      <input type="hidden" name="categoryid" value="<?php echo $category->getID() ?>">
                      <input type="hidden" name="keywordsid" value="<?php echo $list["id"] ?>">
                      <input type="hidden" name="action" value="removekeywords">
                      <button class="btn" title="<?php echo getMLText("delete") ?>">
                        <span class="fa fa-remove"></span> <?php echo getMLText("delete") ?></button>
                    </form>
                    <br>
                  <?php }
                } ?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
					  <form class="form-inline" action="../op/op.DefaultKeywords.php" method="post">
  				  <?php echo createHiddenFieldWithKey('newkeywords'); ?>
						<input type="Hidden" name="action" value="newkeywords">
						<input type="Hidden" name="categoryid" value="<?php echo $category->getID() ?>">
						<input type="text" name="keywords">
					
					  <input type="submit" class="btn" value="<?php printMLText("new_default_keywords"); ?>">
						</form>
					</td>
				</tr>

			</table>
		</td>
<?php } ?>
            </tr>
          </table>
        </div>
      </div>
    </div>

    <script language="JavaScript">

      sel = document.getElementById("selector");
      sel.selectedIndex =<?php print $selected ?>;
      showKeywords(sel);

    </script>

    <?php
    $this->htmlEndPage();
  }
}

?>
