<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
use BakeryDMS\Core\AccessMode;
use BakeryDMS\Core\DMS;
use BakeryDMS\Core\Folder;
use BakeryDMS\Preview\Previewer;
use BakeryDMS\Utils\File;
use BakeryDMS\View\Bootstrap\Elements\DocumentMetaInfo;

require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for ViewFolder view.
 */
class SeedDMS_View_ViewFolder extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    /** @var Folder $folder */
    $folder = $this->params['folder'];
    $orderby = $this->params['orderby'];
    $enableFolderTree = $this->params['enableFolderTree'];
    $enableClipboard = $this->params['enableClipboard'];
    $enableDropUpload = $this->params['enableDropUpload'];
    $expandFolderTree = $this->params['expandFolderTree'];
    $showtree = $this->params['showtree'];
    $cachedir = $this->params['cachedir'];
    $workflowmode = $this->params['workflowmode'];
    $enableRecursiveCount = $this->params['enableRecursiveCount'];
    $maxRecursiveCount = $this->params['maxRecursiveCount'];
    $previewwidth = $this->params['previewWidthList'];

    $folderid = $folder->getId();

    $this->htmlStartPage(getMLText("folder_title", array("foldername" => htmlspecialchars($folder->getName()))));

    $this->globalNavigation($folder);
    $this->contentStart();
    $this->pageNavigation($this->getFolderPathHTML($folder), "view_folder", $folder);

    echo "<div class=\"row-fluid\">\n";

    // dynamic columns - left column removed if no content and right column then fills span12.
    if (!($enableFolderTree || $enableClipboard)) {
      $LeftColumnSpan = 0;
      $RightColumnSpan = 12;
    } else {
      $LeftColumnSpan = 3;
      $RightColumnSpan = 9;
    }
    if ($LeftColumnSpan > 0) {
      echo "<div class=\"span" . $LeftColumnSpan . " folder-tree\">\n";
      if ($enableFolderTree) {
        if ($showtree == 1) {
          $this->contentHeading("<a href=\"../out/out.ViewFolder.php?folderid=" . $folderid . "&showtree=0\"><span class=\"fa fa-minus-square\"></span></a>",
            true);
          $this->contentContainerStart();
          ?>
          <script language="JavaScript">
            function folderSelected(id, name) {
              window.location = '../out/out.ViewFolder.php?folderid=' + id;
            }
          </script>
          <?php
          $this->printNewTreeNavigation($folderid, AccessMode::READ, 0, '', $expandFolderTree == 2, $orderby);
          $this->contentContainerEnd();
        } else {
          $this->contentHeading("<a href=\"../out/out.ViewFolder.php?folderid=" . $folderid . "&showtree=1\"><span class=\"fa fa-plus-square\"></span></a>",
            true);
        }
      }
      if ($enableClipboard) {
        $this->printClipboard($this->params['session']->getClipboard());
      }

      echo "</div>\n";
    }
    echo "<div class=\"span" . $RightColumnSpan . "\">\n";


    if ($enableDropUpload) {
      echo "<div class=\"row-fluid\">";
      echo "<div class=\"span8\">";
    }
    $owner = $folder->getOwner();
    $this->contentHeading($folder->getName());
    // Print meta information of this folder
    $this->contentContainerStart();
    (new DocumentMetaInfo($user, $folder, $owner))->display();
    $this->contentContainerEnd();

    if ($enableDropUpload) {
      echo "</div>";
      echo "<div class=\"span4\">";
      $this->contentHeading(getMLText("dropupload"), true);
      $this->addFooterJS("SeedDMSUpload.setUrl('../op/op.Ajax.php');");
      $this->addFooterJS("SeedDMSUpload.setAbortBtnLabel('" . getMLText("cancel") . "');");
      $this->addFooterJS("SeedDMSUpload.setEditBtnLabel('" . getMLText("edit_document_props") . "');");
      $this->addFooterJS("SeedDMSUpload.setMaxFileSize(" . File::parse_filesize(ini_get("upload_max_filesize")) . ");");
      $this->addFooterJS("SeedDMSUpload.setMaxFileSizeMsg('" . getMLText("uploading_maxsize") . "');");
      ?>
      <!-- TODO: Handle access restriction -->
      <div id="dragandrophandler"
           class="well <?php echo $folder->getAccessMode($user) <= AccessMode::READ ? 'disabled' : 'alert'?>"
           data-target="<?php echo $folder->getID(); ?>"
           data-formtoken="<?php echo createFormKey('adddocument'); ?>">
        <?php
          echo $folder->getAccessMode($user) <= AccessMode::READ
            ? printMLText('dropupload_insufficient_rights')
            : printMLText('drop_files_here');
        ?>
      </div>
      <?php
      echo "</div>";
      echo "</div>";
    }

    $this->contentHeading(getMLText("folder_contents"));

    $subFolders = $folder->getSubFolders($orderby);
    $subFolders = DMS::filterAccess($subFolders, $user, AccessMode::READ);
    $documents = $folder->getDocuments($orderby);
    $documents = DMS::filterAccess($documents, $user, AccessMode::READ);

    if ((count($subFolders) > 0) || (count($documents) > 0)) {
      print "<table id=\"viewfolder-table\" class=\"table\">";
      print "<thead>\n<tr>\n";
      print "<th></th>\n";
      print "<th><a href=\"../out/out.ViewFolder.php?folderid=" . $folderid . ($orderby == "n" ? "&orderby=s" : "&orderby=n") . "\">" . getMLText("name") . "</a></th>\n";
//			print "<th>".getMLText("owner")."</th>\n";
      print "<th>" . getMLText("status") . "</th>\n";
//			print "<th>".getMLText("version")."</th>\n";
      print "<th>" . getMLText("action") . "</th>\n";
      print "</tr>\n</thead>\n<tbody>\n";
    } else {
      printMLText("empty_folder_list");
    }


    foreach ($subFolders as $subFolder) {
      echo $this->folderListRow($subFolder);
    }

    $previewer = new Previewer($cachedir, $previewwidth);
    foreach ($documents as $document) {
      echo $this->documentListRow($document, $previewer);
    }

    if ((count($subFolders) > 0) || (count($documents) > 0)) {
      echo "</tbody>\n</table>\n";
    }

    echo "</div>\n"; // End of right column div

    $this->contentEnd();

    $this->htmlEndPage();
  }
}

?>
