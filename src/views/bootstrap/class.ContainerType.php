<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Class ContainerType
 * Enumeration with Bootstrap container types.
 */
abstract class ContainerType extends BasicEnum {
  const Standard = 0;
  const Primary = 1;
  const Success = 2;
  const Info = 3;
  const Warning = 4;
  const Danger = 5;

  /**
   * @var array[string] The array with the valid container types.
   */
  private static $typeNames = array('default','primary','success','info','warning','danger');

  /**
   * Returns the type name of a given ContainerType.
   * @param int $type the given type.
   * @return string the type according to the given integer <c>$type</c>.
   */
  public static function toString($type) {
    return ContainerType::$typeNames[$type];
  }
}