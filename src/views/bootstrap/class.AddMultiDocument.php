<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for AddMultiDocument view.
 */
class SeedDMS_View_AddMultiDocument extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $folder = $this->params['folder'];
    $enableadminrevapp = $this->params['enableadminrevapp'];
    $enableownerrevapp = $this->params['enableownerrevapp'];
    $enableselfrevapp = $this->params['enableselfrevapp'];

    $this->htmlStartPage(getMLText("folder_title", array("foldername" => htmlspecialchars($folder->getName()))));
    $this->globalNavigation($folder);
    $this->contentStart();
    $this->pageNavigation($this->getFolderPathHTML($folder, true), "view_folder", $folder);

    ?>
    <script language="JavaScript">
      var openDlg;
      function chooseKeywords(target) {
        openDlg = open("out.KeywordChooser.php?target=" + target,
          "openDlg",
          "width=500,height=400,scrollbars=yes,resizable=yes");
      }
      function chooseCategory(form, cats) {
        openDlg = open("out.CategoryChooser.php?form=" + form + "&cats=" + cats,
          "openDlg",
          "width=480,height=480,scrollbars=yes,resizable=yes,status=yes");
      }
    </script>

    <?php
    $this->contentHeading(getMLText("add_document"));
    $this->contentContainerStart();

    // Retrieve a list of all users and groups that have review / approve
    // privileges.
    $docAccess = $folder->getReadAccessList($enableadminrevapp, $enableownerrevapp);

    $this->printUploadApplet('../op/op.AddMultiDocument.php', array('folderid' => $folder->getId()));

    $this->contentContainerEnd();
    $this->htmlEndPage();

  }
}

?>
