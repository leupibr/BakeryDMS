<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for DashBoard view.
 */
class SeedDMS_View_DashBoard extends SeedDMS_Bootstrap_Style {

function show() {
$dms = $this->params['dms'];
$user = $this->params['user'];
$folder = $this->params['folder'];
$orderby = $this->params['orderby'];
$enableFolderTree = $this->params['enableFolderTree'];
$showtree = $this->params['showtree'];
$cachedir = $this->params['cachedir'];

$this->htmlStartPage(getMLText("dashboard"));

$this->globalNavigation($folder);
$this->contentStart();

$this->contentHeading("Willkommen im Onlineportal");
?>
<div class="row-fluid">
  <div class="span12">
    <?php $this->contentHeading('Gruppen'); ?>
    <div class="well">
      Hier eine Übersicht der Gruppen, auf die der Anwender zugreifen darf.
    </div>
  </div>
  <div>
    <div class="row-fluid">
      <div class="span4">
        <?php $this->contentHeading('Lesezeichen'); ?>
        <div class="well">
          <table class="table">
            <thead>
            <tr>
              <th></th>
              <th>Name</th>
              <th>Besitzer</th>
              <th>Status</th>

            </tr>
            </thead>
            <tbody>
            <tr>
              <td><a href="../op/op.Download.php?documentid=403&version=1"><img class="mimeicon"
                                                                                width="40"
                                                                                src="../op/op.Preview.php?documentid=403&version=1&width=40"
                                                                                title="application/pdf"></a></td>
              <td><a href="out.ViewDocument.php?documentid=403&showtree=1">walking-paper-4hxq62d9.pdf</a></td>
              <td>Admin</td>
              <td>freigegeben</td>
            </tr>
            </tbody>
          </table>
        </div>
        <?php $this->contentHeading('Neue Dokumente'); ?>
        <div class="well">
        </div>
        <?php $this->contentHeading('Dokumente zur Prüfung'); ?>
        <div class="well">
        </div>
        <?php $this->contentHeading('Dokumente zur Genehmigung'); ?>
        <div class="well">
        </div>
      </div>
      <div class="span4">
        <?php $this->contentHeading('Neue Beiträge im Wiki'); ?>
        <div class="well">
          <table class="table">
            <thead>
            <tr>
              <th></th>
              <th>Name</th>
              <th>Besitzer</th>
              <th>Geändert</th>

            </tr>
            </thead>
            <tbody>
            <tr>
              <td><a href="../op/op.Download.php?documentid=403&version=1"><img class="mimeicon"
                                                                                width="40"
                                                                                src="../op/op.Preview.php?documentid=403&version=1&width=40"
                                                                                title="application/pdf"></a></td>
              <td><a href="out.ViewDocument.php?documentid=403&showtree=1">Konzept Bebauung Waldstr.</a></td>
              <td>H. Huber</td>
              <td>28.11.2013</td>
            </tr>
            </tbody>
          </table>
        </div>
        <?php $this->contentHeading('Zuletzt bearbeitet'); ?>
        <div class="well">
        </div>
      </div>
      <div class="span4">
        <?php $this->contentHeading('Neue Beiträge im Diskussionsforum'); ?>
        <div class="well">
        </div>
      </div>
    </div>

    <?

    $this->htmlEndPage();
    }
    }

    ?>
