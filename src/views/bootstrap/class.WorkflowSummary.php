<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
use BakeryDMS\Core\DMS;
use BakeryDMS\Core\User;
use BakeryDMS\Preview\Previewer;
use BakeryDMS\Settings;

require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for WorkflowSummary view.
 */
class SeedDMS_View_WorkflowSummary extends SeedDMS_Bootstrap_Style {

  function show() {
    /** @var DMS $dms */
    $dms = $this->params['dms'];
    /** @var User $user */
    $user = $this->params['user'];

    $this->htmlStartPage(t('my_documents'));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(t('my_documents'), 'my_documents');


    // Get document list for the current user.
    $workflowStatus = $user->getWorkflowStatus();

    $this->contentHeading(t('workflow_user_summary'));
    $this->contentContainerStart();
    if ($this->hasVersions($dms, $workflowStatus['u'])) {
      $this->printUserWorkflowSummary($dms, $workflowStatus['u']);
    }
    else {
      echo t('no_docs_to_look_at');
    }
    $this->contentContainerEnd();

    $this->contentHeading(t('workflow_group_summary'));
    $this->contentContainerStart();
    if ($this->hasVersions($dms, $workflowStatus['g'])) {
      $this->printGroupWorkflowSummary($dms, $workflowStatus['g']);
    }
    else {
      echo t('no_docs_to_look_at');
    }
    $this->contentContainerEnd();

    $this->htmlEndPage();
  }

  private function printHeader($fields) {
    echo '<thead><tr>';
    foreach ($fields as $field) {
      echo '<th>' . (empty($field) ? '' : t($field)) . '</th>';
      echo "<th>";
    }
    echo '</tr></thead>';
  }

  private function hasVersions(DMS &$dms, $workflowStates) {
    if (empty($workflowStates)) {
      return false;
    }

    foreach ($workflowStates as $workflowState) {
      $document = $dms->getDocument($workflowState['document']);
      if (empty($document)) {
        continue;
      }
      $version = $document->getContentByVersion($workflowState['version']);
      if (!empty($version)) {
        return true;
      }
    }

    return false;
  }

  private function printUserWorkflowSummary(DMS &$dms, $workflowStates) {
    print '<table class="table table-condensed">';
    $this->printHeader(['', 'name', 'version', 'owner', 'workflow', 'workflow_state', 'last_update', 'expires']);
    print '<tbody>';

    $previewer = new Previewer(Settings::getInstance()->_cacheDir, Settings::getInstance()->_previewWidthList);

    foreach ($workflowStates as $workflowState) {
      $document = $dms->getDocument($workflowState['document']);
      if (empty($document)) {
        continue;
      }

      $version = $document->getContentByVersion($workflowState['version']);
      if (empty($version)) {
        continue;
      }

      $workflow = $dms->getWorkflow($workflowState['workflow']);
      $state = $dms->getWorkflowState($workflowState['state']);
      $owner = $document->getOwner();
      $modifiedUser = $dms->getUser($workflowState['userid']);

      $previewer->createPreview($version);
      echo '<tr>';
      echo '<td><a href="../op/op.Download.php?documentid=' . $document->getID() . '&version=' . $workflowState['version'] . '">';
      if ($previewer->hasPreview($version)) {
        echo '<img class="mimeicon" width="' . Settings::getInstance()->_previewWidthList . '"src="../op/op.Preview.php?documentid=' . $document->getID() . '&version=' . $version->getVersion() . '&width=' . Settings::getInstance()->_previewWidthList . '" title="' . htmlspecialchars($version->getMimeType()) . '">';
      }
      else {
        echo '<img class="mimeicon" src="' . $this->getMimeIcon($version->getFileType()) . '" title="' . htmlspecialchars($version->getMimeType()) . '">';
      }
      echo '</a></td>';
      echo '<td><a href="out.DocumentVersionDetail.php?documentid=' . $workflowState["document"] . '&version=' . $workflowState["version"] . '">' . htmlspecialchars($document->getName());
      echo '</a></td>';
      echo '<td>' . $workflowState["version"] . '</td>';
      echo '<td>' . htmlspecialchars($owner->getFullName()) . '</td>';
      echo '<td>' . $workflow->getName() . '</td>';
      echo '<td>' . $state->getName() . '</td>';
      echo '<td>' . $workflowState["date"] . ' ' . htmlspecialchars($modifiedUser->getFullName()) . '</td>';
      echo '<td>' . (!$document->expires() ? '-' : getReadableDate($document->getExpires())) . '</td>';
      echo '</tr>';
    }

    echo '</tbody></table>';
  }

  private function printGroupWorkflowSummary(DMS &$dms, $workflowStates) {
    print '<table class="table table-condensed">';
    $this->printHeader(['', 'name', 'version', 'owner', 'workflow', 'last_update', 'expires']);
    print '<tbody>';

    $previewer = new Previewer(Settings::getInstance()->_cacheDir, Settings::getInstance()->_previewWidthList);

    foreach ($workflowStates as $workflowState) {
      $document = $dms->getDocument($workflowState['document']);
      if (empty($document)) {
        continue;
      }

      $version = $document->getContentByVersion($workflowState['version']);
      if (empty($version)) {
        continue;
      }
      $workflow = $dms->getWorkflow($workflowState['workflow']);
      $state = $dms->getworkflowstate($workflowState['state']);
      $owner = $document->getowner();
      $modgroup = $dms->getgroup($workflowState['groupid']);

      $previewer->createPreview($version);
      echo '<tr>';
      echo '<td><a href="../op/op.Download.php?documentid=' . $document->getID() . '&version=' . $workflowState['version'] . '">';
      if ($previewer->hasPreview($version)) {
        echo '<img class="mimeicon" width="' . Settings::getInstance()->_previewWidthList . '" src="../op/op.Preview.php?documentid=' . $document->getID() . '&version=' . $version->getVersion() . '&width=' . Settings::getInstance()->_previewWidthList . '" title="' . htmlspecialchars($version->getMimeType()) . '">';
      }
      else {
        echo '<img class="mimeicon" src="' . $this->getMimeIcon($version->getFileType()) . '" title="' . htmlspecialchars($version->getMimeType()) . '">';
      }
      echo '</a></td>';
      echo '<td><a href="out.DocumentVersionDetail.php?documentid=' . $workflowState["document"] . '&version=' . $workflowState["version"] . '">' . htmlspecialchars($document->getName()) . '</a></td>';
      echo '<td>' . $workflowState["version"] . '</td>';
      echo '<td>' . htmlspecialchars($owner->getFullName()) . '</td>';
      echo '<td>' . $workflow->getName() . '</td>';
      echo '<td>' . $state->getName() . '</td>';
      echo '<td>' . $workflowState["date"] . ' ' . htmlspecialchars($modgroup->getName()) . '</td>';
      echo '<td>' . (!$document->expires() ? '-' : getReadableDate($document->getExpires())) . '</td>';
      echo '</tr>';
    }
    echo '</tbody></table>';
  }
}
