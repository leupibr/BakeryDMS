<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
use BakeryDMS\Core\AccessMode;
use BakeryDMS\Utils\File;

require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for BackupTools view.
 */
class SeedDMS_View_BackupTools extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $contentdir = $this->params['contentdir'];

    $this->htmlStartPage(getMLText("backup_tools"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText("admin_tools"), "admin_tools");

    $this->contentHeading(getMLText("backup_tools"));
    $this->contentContainerStart();
    print getMLText("space_used_on_data_folder") . " : " . File::format_filesize(dskspace($contentdir));
    $this->contentContainerEnd();

    // versioning file creation ////////////////////////////////////////////////////

    $this->contentHeading(getMLText("versioning_file_creation"));
    $this->contentContainerStart();
    print "<p>" . getMLText("versioning_file_creation_warning") . "</p>\n";

    print "<form class=\"form-inline\" action=\"../op/op.CreateVersioningFiles.php\" name=\"form1\">";
    $this->printFolderChooser("form1", AccessMode::READWRITE);
    print "<input type='submit' class='btn' name='' value='" . getMLText("versioning_file_creation") . "'/>";
    print "</form>\n";

    $this->contentContainerEnd();

    // archive creation ////////////////////////////////////////////////////////////

    $this->contentHeading(getMLText("archive_creation"));
    $this->contentContainerStart();
    print "<p>" . getMLText("archive_creation_warning") . "</p>\n";

    print "<form action=\"../op/op.CreateFolderArchive.php\" name=\"form2\">";
    $this->printFolderChooser("form2", AccessMode::READWRITE);
    print "<label class=\"checkbox\"><input type=\"checkbox\" name=\"human_readable\" value=\"1\">" . getMLText("human_readable") . "</label>";
    print "<input type='submit' class='btn' name='' value='" . getMLText("archive_creation") . "'/>";
    print "</form>\n";

    // list backup files
    $this->contentSubHeading(getMLText("backup_list"));

    $print_header = true;

    $handle = opendir($contentdir);
    $entries = array();
    while ($e = readdir($handle)) {
      if (is_dir($contentdir . $e)) {
        continue;
      }
      if (strpos($e, ".tar.gz") == FALSE) {
        continue;
      }
      $entries[] = $e;
    }
    closedir($handle);

    sort($entries);
    $entries = array_reverse($entries);

    foreach ($entries as $entry) {

      if ($print_header) {
        print "<table class=\"table-condensed\">\n";
        print "<thead>\n<tr>\n";
        print "<th></th>\n";
        print "<th>" . getMLText("folder") . "</th>\n";
        print "<th>" . getMLText("creation_date") . "</th>\n";
        print "<th>" . getMLText("file_size") . "</th>\n";
        print "<th></th>\n";
        print "</tr>\n</thead>\n<tbody>\n";
        $print_header = false;
      }

      $folderid = substr($entry, strpos($entry, "_") + 1);
      $folder = $dms->getFolder((int) $folderid);

      print "<tr>\n";
      print "<td><a href=\"../op/op.Download.php?arkname=" . $entry . "\">" . $entry . "</a></td>\n";
      if (is_object($folder)) {
        print "<td>" . htmlspecialchars($folder->getName()) . "</td>\n";
      } else {
        print "<td>" . getMLText("unknown_id") . "</td>\n";
      }
      print "<td>" . getLongReadableDate(filectime($contentdir . $entry)) . "</td>\n";
      print "<td>" . File::format_filesize(filesize($contentdir . $entry)) . "</td>\n";
      print "<td>";
      print "<a href=\"out.RemoveArchive.php?arkname=" . $entry . "\" class=\"btn btn-mini\"><spa class=\"fa fa-remove\"></span> " . getMLText("backup_remove") . "</a>";
      print "</td>\n";
      print "</tr>\n";
    }

    if ($print_header) {
      printMLText("empty_notify_list");
    } else {
      print "</table>\n";
    }

    $this->contentContainerEnd();

    // dump creation ///////////////////////////////////////////////////////////////

    $this->contentHeading(getMLText("dump_creation"));
    $this->contentContainerStart();
    print "<p>" . getMLText("dump_creation_warning") . "</p>\n";

    print "<form action=\"../op/op.CreateDump.php\" name=\"form4\">";
    print "<input type='submit' class='btn' name='' value='" . getMLText("dump_creation") . "'/>";
    print "</form>\n";

    // list backup files
    $this->contentSubHeading(getMLText("dump_list"));

    $print_header = true;

    $handle = opendir($contentdir);
    $entries = array();
    while ($e = readdir($handle)) {
      if (is_dir($contentdir . $e)) {
        continue;
      }
      if (strpos($e, ".sql.gz") == FALSE) {
        continue;
      }
      $entries[] = $e;
    }
    closedir($handle);

    sort($entries);
    $entries = array_reverse($entries);

    foreach ($entries as $entry) {

      if ($print_header) {
        print "<table class=\"table-condensed\">\n";
        print "<thead>\n<tr>\n";
        print "<th></th>\n";
        print "<th>" . getMLText("creation_date") . "</th>\n";
        print "<th>" . getMLText("file_size") . "</th>\n";
        print "<th></th>\n";
        print "</tr>\n</thead>\n<tbody>\n";
        $print_header = false;
      }

      print "<tr>\n";
      print "<td><a href=\"../op/op.Download.php?dumpname=" . $entry . "\">" . $entry . "</a></td>\n";
      print "<td>" . getLongReadableDate(filectime($contentdir . $entry)) . "</td>\n";
      print "<td>" . File::format_filesize(filesize($contentdir . $entry)) . "</td>\n";
      print "<td>";
      print "<a href=\"out.RemoveDump.php?dumpname=" . $entry . "\" class=\"btn btn-mini\"><span class=\"fa fa-remove\"></span> " . getMLText("dump_remove") . "</a>";
      print "</td>\n";
      print "</tr>\n";
    }

    if ($print_header) {
      printMLText("empty_notify_list");
    } else {
      print "</table>\n";
    }

    $this->contentContainerEnd();

    // files deletion //////////////////////////////////////////////////////////////

    $this->contentHeading(getMLText("files_deletion"));
    $this->contentContainerStart();
    print "<p>" . getMLText("files_deletion_warning") . "</p>\n";

    print "<form class=\"form-inline\" action=\"../out/out.RemoveFolderFiles.php\" name=\"form3\">";
    $this->printFolderChooser("form3", AccessMode::READWRITE);
    print "<input type='submit' class='btn' name='' value='" . getMLText("files_deletion") . "'/>";
    print "</form>\n";

    $this->contentContainerEnd();

    $this->htmlEndPage();
  }
}

?>
