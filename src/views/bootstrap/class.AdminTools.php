<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
use BakeryDMS\Settings;

require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for AdminTools view.
 */
class SeedDMS_View_AdminTools extends SeedDMS_Bootstrap_Style {

  function show() {
    $this->htmlStartPage(t("admin_tools"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(t("admin_tools"), "admin_tools");
    $this->contentContainerStart();

    echo '<div id="admin-tools">';
    $this->printCoreItems();
    echo '<hr/>';
    $this->printExtensionAdminItems();
    echo '</div>';
    $this->contentContainerEnd();
    $this->htmlEndPage();
  }

  /**
   * @param $url string The url to link to.
   * @param $text string The text to display.
   * @param $icon string The icon to display.
   */
  private function printButton($url, $text, $icon) {
    echo '<a href="' . $url . '" class="span3 btn btn-medium">';
    echo '<span class="fa ' . $icon . ' fa-3x"></span>';
    echo '<br/>';
    echo $text;
    echo '</a>';
  }

  /**
   * @param $url string The url to link to.
   * @param $textKey string The text key of the text to display.
   * @param $icon string The icon to display.
   */
  private function printInternalButton($url, $textKey, $icon) {
    $this->printButton($this->settings->_httpRoot . $url, t($textKey), $icon);
  }

  /**
   * Print the items from the extension.
   */
  private function printExtensionAdminItems() {
    echo '<div class="row-fluid">';
    foreach ($this->adminItems as $action) {
      $this->printButton($action->getUrl(), $action->getText(), $action->getIcon());
    }
    echo '</div>';
  }

  /**
   * Print the items from the core.
   */
  private function printCoreItems() {
    echo '<div class="row-fluid">';
    $this->printInternalButton("out/out.UsrMgr.php", "user_management", "fa-user");
    $this->printInternalButton("out/out.GroupMgr.php", "group_management", "fa-group");
    echo '</div>';

    echo '<div class="row-fluid">';
    $this->printInternalButton("out/out.BackupTools.php", "backup_tools", "fa-hdd-o");
    if ($this->params['logfileenable']) {
      $this->printInternalButton("out/out.LogManagement.php", "log_management", "fa-list");
    }
    echo '</div>';

    echo '<div class="row-fluid">';
    $this->printInternalButton("out/out.DefaultKeywords.php", "global_default_keywords", "fa-reorder");
    $this->printInternalButton("out/out.Categories.php", "global_document_categories", "fa-columns");
    $this->printInternalButton("out/out.AttributeMgr.php", "global_attributedefinitions", "fa-tags");
    echo '</div>';

    if ($this->params['workflowmode'] != 'traditional') {
      echo '<div class="row-fluid">';
      $this->printInternalButton("out/out.WorkflowMgr.php", "global_workflows", "fa-sitemap");
      $this->printInternalButton("out/out.WorkflowStatesMgr.php", "global_workflow_states", "fa-star");
      $this->printInternalButton("out/out.WorkflowActionsMgr.php", "global_workflow_actions", "fa-bolt");
      echo '</div>';
    }

    if ($this->params['enablefullsearch']) {
      echo '<div class="row-fluid">';
      $this->printInternalButton("out/out.Indexer.php", "update_fulltext_index", "fa-refresh");
      $this->printInternalButton("out/out.CreateIndex.php", "create_fulltext_index", "fa-search");
      $this->printInternalButton("out/out.IndexInfo.php", "fulltext_info", "fa-info-circle");
      echo '</div>';
    }

    echo '<div class="row-fluid">';
    $this->printInternalButton("out/out.Statistic.php", "folders_and_documents_statistic", "fa-tasks");
    $this->printInternalButton("out/out.Charts.php", "charts", "fa-bar-chart");
    $this->printInternalButton("out/out.ObjectCheck.php", "objectcheck", "fa-check");
    $this->printInternalButton("out/out.Info.php", "version_info", "fa-info-circle");
    echo '</div>';

    echo '<div class="row-fluid">';
    $this->printInternalButton("out/out.Settings.php", "settings", "fa-cogs");
    echo '</div>';
  }
}

?>
