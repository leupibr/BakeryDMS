<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for Info view.
 */
class SeedDMS_View_Info extends SeedDMS_Bootstrap_Style {

  function show() {
    /** @var SeedDMS_Version $version */
    $version = $this->params['version'];
    $availableVersions = $this->params['availversions'];

    $this->htmlStartPage(getMLText("admin_tools"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText("admin_tools"), "admin_tools");
    if ($availableVersions) {
      $newestVersion = $this->getNewestVersion($availableVersions);
      $currentVersion = explode('.', $version->_number);
      if ($newestVersion[0] > $currentVersion[0]
        || $newestVersion[0] == $currentVersion[0] && $newestVersion[1] > $currentVersion[1]
        || $newestVersion[0] == $currentVersion[0] && $newestVersion[1] == $currentVersion[1] && $newestVersion[2] > $currentVersion[2]
      ) {
        $this->warningMsg(getMLText('no_current_version',
          array('latestversion'
          => $newestVersion[0] . "." . $newestVersion[1] . "." . $newestVersion[2]
          )));
      }
    }
    else {
      $this->warningMsg(getMLText('no_version_check'));
    }
    $this->printVersion($version);
    $this->printChangeLog($this->params['changeLog']);

    $this->htmlEndPage();
  }

  /**
   * Gets the newest version
   *
   * @param $availableVersions array List of all available version.
   * @return string The newest version.
   */
  private function getNewestVersion($availableVersions) {
    $result = '';
    foreach ($availableVersions as $availableVersion) {
      $result = $availableVersion;
    }
    return $result;
  }

  /**
   * Prints the version number.
   *
   * @param $version SeedDMS_Version The version to print.
   */
  private function printVersion($version) {
    $this->contentContainerStart();
    echo $version->banner();
    $this->contentContainerEnd();
  }

  /**
   * Prints the change log.
   *
   * @param $changeLogFile string The path to the change log file.
   */
  private function printChangeLog($changeLogFile) {
    $changeLog = file_get_contents($changeLogFile);
    $this->contentContainerStart();
    echo (new \BakeryDMS\Utils\Parsedown())->text($changeLog);
    $this->contentContainerEnd();
  }
}

?>
