<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for UsrView view.
 */
class SeedDMS_View_UsrView extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $users = $this->params['allusers'];
    $enableuserimage = $this->params['enableuserimage'];
    $httproot = $this->params['httproot'];

    $this->htmlStartPage(getMLText("my_account"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText("my_account"), "my_account");

    $this->contentHeading(getMLText("users"));
    $this->contentContainerStart();

    echo "<table class=\"table-condensed\">\n";
    echo "<thead>\n<tr>\n";
    echo "<th>" . getMLText("name") . "</th>\n";
    echo "<th>" . getMLText("email") . "</th>\n";
    echo "<th>" . getMLText("comment") . "</th>\n";
    if ($enableuserimage) {
      echo "<th>" . getMLText("user_image") . "</th>\n";
    }
    echo "</tr>\n</thead>\n";

    foreach ($users as $currUser) {

      if ($currUser->isGuest()) {
        continue;
      }

      if ($currUser->isHidden() == "1") {
        continue;
      }

      echo "<tr>\n";

      print "<td>" . htmlspecialchars($currUser->getFullName()) . "</td>";

      print "<td><a href=\"mailto:" . htmlspecialchars($currUser->getEmail()) . "\">" . htmlspecialchars($currUser->getEmail()) . "</a></td>";
      print "<td>" . htmlspecialchars($currUser->getComment()) . "</td>";

      if ($enableuserimage) {
        print "<td>";
        if ($currUser->hasImage()) {
          print "<img src=\"" . $httproot . "out/out.UserImage.php?userid=" . $currUser->getId() . "\">";
        } else {
          printMLText("no_user_image");
        }
        print "</td>";
      }

      echo "</tr>\n";
    }

    echo "</table>\n";

    $this->contentContainerEnd();
    $this->htmlEndPage();
  }
}

?>
