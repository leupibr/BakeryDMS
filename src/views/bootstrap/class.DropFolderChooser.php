<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
use BakeryDMS\Utils\File;

require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for CategoryChooser view.
 */
class SeedDMS_View_DropFolderChooser extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $dropfolderfile = $this->params['dropfolderfile'];
    $form = $this->params['form'];
    $dropfolderdir = $this->params['dropfolderdir'];

    $this->htmlStartPage(getMLText("choose_target_file"));
//		$this->globalBanner();
//		$this->pageNavigation(getMLText("choose_target_file"));
    ?>

    <script language="JavaScript">
      var targetName = document.<?php echo $form?>.dropfolderfile<?php print $form ?>;
    </script>
    <?php
    $this->contentContainerStart();

    $dir = $dropfolderdir . '/' . $user->getLogin();
    /* Check if we are still looking in the configured directory and
     * not somewhere else, e.g. if the login was '../test'
     */
    if (dirname($dir) == $dropfolderdir) {
      if (is_dir($dir)) {
        $d = dir($dir);
        echo "<table>\n";
        while (false !== ($entry = $d->read())) {
          if ($entry != '..' && $entry != '.') {
            if (!is_dir($entry)) {
              echo "<tr><td><span style=\"cursor: pointer;\" onClick=\"fileSelected('" . $entry . "');\">" . $entry . "</span></td><td align=\"right\">" . File::format_filesize(filesize($dir . '/' . $entry)) . "</td></tr>\n";
            }
          }
        }
        echo "</table>\n";
      }
    }

    $this->contentContainerEnd();
    echo "</body>\n</html>\n";
//		$this->htmlEndPage();
  }
}

?>
