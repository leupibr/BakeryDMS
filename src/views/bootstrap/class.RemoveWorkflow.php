<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for RemoveWorkflow view.
 */
class SeedDMS_View_RemoveWorkflow extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $workflow = $this->params['workflow'];

    $this->htmlStartPage(getMLText("document_title", array("documentname" => htmlspecialchars($workflow->getName()))));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText("admin_tools"), "admin_tools");
    $this->contentHeading(getMLText("rm_workflow"));
    $this->contentContainerStart();
    // Display the Workflow form.
    ?>
    <div class="row-fluid">
      <div class="span4">
        <p><?php printMLText("rm_workflow_warning"); ?></p>

        <form method="post" action="../op/op.RemoveWorkflow.php" name="form1" onsubmit="return checkForm();">
          <?php echo createHiddenFieldWithKey('removeworkflow'); ?>
          <table>
            <tr>
              <td></td>
              <td>
                <input type='hidden' name='workflowid' value='<?php echo $workflow->getId(); ?>'/>
                <button type='submit' class="btn"><span class="fa fa-remove"></span> <?php printMLText("rm_workflow"); ?>
                </button>
              </td>
            </tr>
          </table>
        </form>
      </div>
      <div id="workflowgraph" class="span8">
        <iframe src="out.WorkflowGraph.php?workflow=<?php echo $workflow->getID(); ?>"
                width="100%"
                height="500"
                style="border: 1px solid #AAA;"></iframe>
      </div>
    </div>
    <?php
    $this->contentContainerEnd();

    $this->htmlEndPage();
  }
}

?>
