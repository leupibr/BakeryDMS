<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for RemoveEvent view.
 */
class SeedDMS_View_RemoveEvent extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $event = $this->params['event'];

    $this->htmlStartPage(getMLText("calendar"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText("calendar"), "calendar");

    $this->contentHeading(getMLText("edit_event"));
    $this->contentContainerStart();

    ?>
    <form action="../op/op.RemoveEvent.php" name="form1" method="post">
      <?php echo createHiddenFieldWithKey('removeevent'); ?>
      <input type="hidden" name="eventid" value="<?php echo intval($event["id"]); ?>">

      <p><?php printMLText("confirm_rm_event", array("name" => htmlspecialchars($event["name"]))); ?></p>
      <button class="btn" type="submit"><span class="fa fa-remove"></span> <?php printMLText("delete"); ?></button>
    </form>
    <?php
    $this->contentContainerEnd();
    $this->htmlEndPage();
  }
}

?>
