<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for RunSubWorkflow view.
 */
class SeedDMS_View_RunSubWorkflow extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $folder = $this->params['folder'];
    $document = $this->params['document'];
    $subworkflow = $this->params['subworkflow'];

    $latestContent = $document->getLatestContent();

    $this->htmlStartPage(getMLText("document_title", array("documentname" => htmlspecialchars($document->getName()))));
    $this->globalNavigation($folder);
    $this->contentStart();
    $this->pageNavigation($this->getFolderPathHTML($folder, true, $document), "view_document", $document);
    $this->contentHeading(getMLText("run_subworkflow"));

    $currentstate = $latestContent->getWorkflowState();
    $wkflog = $latestContent->getWorkflowLog();
    $workflow = $latestContent->getWorkflow();

    $msg = "The document is currently in state: " . $currentstate->getName() . "<br />";
    if ($wkflog) {
      foreach ($wkflog as $entry) {
        if ($entry->getTransition()->getNextState()->getID() == $currentstate->getID()) {
          $enterdate = $entry->getDate();
          $d = strptime($enterdate, '%Y-%m-%d %H:%M:%S');
          $enterts = mktime($d['tm_hour'],
            $d['tm_min'],
            $d['tm_sec'],
            $d['tm_mon'] + 1,
            $d['tm_mday'],
            $d['tm_year'] + 1900);
        }
      }
      $msg .= "The state was entered at " . $enterdate . " which was ";
      $msg .= getReadableDuration((time() - $enterts)) . " ago.<br />";
    }
    $msg .= "The document may stay in this state for " . $currentstate->getMaxTime() . " sec.";
    $this->infoMsg($msg);

    $this->contentContainerStart();
    // Display the Workflow form.
    ?>
    <div class="row-fluid">
      <div class="span4">
        <form method="POST" action="../op/op.RunSubWorkflow.php" name="form1">
          <?php echo createHiddenFieldWithKey('runsubworkflow'); ?>
          <table>
            <tr>
              <td></td>
              <td>
                <input type='hidden' name='documentid' value='<?php echo $document->getId(); ?>'/>
                <input type='hidden' name='version' value='<?php echo $latestContent->getVersion(); ?>'/>
                <input type='hidden' name='subworkflow' value='<?php echo $subworkflow->getID(); ?>'/>
                <input type='submit' class="btn" value='<?php printMLText("run_subworkflow"); ?>'/>
              </td>
            </tr>
          </table>
        </form>
      </div>
      <div id="workflowgraph" class="span8">
        <iframe src="out.WorkflowGraph.php?workflow=<?php echo $subworkflow->getID(); ?>"
                width="100%"
                height="400"
                style="border: 1px solid #AAA;"></iframe>
      </div>
    </div>
    <?php
    $this->contentContainerEnd();

    if ($wkflog) {
      $this->contentContainerStart();
      echo "<table class=\"table-condensed\">";
      echo "<tr><th>" . getMLText('action') . "</th><th>Start state</th><th>End state</th><th>" . getMLText('date') . "</th><th>" . getMLText('user') . "</th><th>" . getMLText('comment') . "</th></tr>";
      foreach ($wkflog as $entry) {
        echo "<tr>";
        echo "<td>" . getMLText('action_' . $entry->getTransition()->getAction()->getName()) . "</td>";
        echo "<td>" . $entry->getTransition()->getState()->getName() . "</td>";
        echo "<td>" . $entry->getTransition()->getNextState()->getName() . "</td>";
        echo "<td>" . $entry->getDate() . "</td>";
        echo "<td>" . $entry->getUser()->getFullname() . "</td>";
        echo "<td>" . $entry->getComment() . "</td>";
        echo "</tr>";
      }
      echo "</table>\n";
      $this->contentContainerEnd();
    }

    $this->htmlEndPage();
  }
}

?>
