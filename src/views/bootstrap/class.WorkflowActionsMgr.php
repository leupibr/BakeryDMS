<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for WorkspaceActionsMgr view.
 */
class SeedDMS_View_WorkflowActionsMgr extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $selworkflowaction = $this->params['selworkflowaction'];

    $workflowactions = $dms->getAllWorkflowActions();

    $this->htmlStartPage(getMLText("admin_tools"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText("admin_tools"), "admin_tools");

    ?>
    <script language="JavaScript">

      function checkForm(num) {
        msg = new Array()
        eval("var formObj = document.form" + num + ";");

        if (formObj.name.value == "") msg.push("<?php printMLText("js_no_name");?>");
        if (msg != "") {
          noty({
            text: msg.join('<br />'),
            type: 'error',
            dismissQueue: true,
            layout: 'topRight',
            theme: 'defaultTheme',
            _timeout: 1500,
          });
          return false;
        }
        else
          return true;
      }


      obj = -1;
      function showWorkflowAction(selectObj) {
        if (obj != -1) {
          obj.style.display = "none";
        }

        id = selectObj.options[selectObj.selectedIndex].value;
        if (id == -1)
          return;

        obj = document.getElementById("keywords" + id);
        obj.style.display = "";

      }
    </script>
    <?php
    $this->contentHeading(getMLText("workflow_actions_management"));
    ?>

    <div class="row-fluid">
      <div class="span4">
        <div class="well">
          <?php echo getMLText("selection") ?>:
          <select onchange="showWorkflowAction(this)" id="selector" class="span9">
            <option value="-1"><?php echo getMLText("choose_workflow_action") ?>
            <option value="0"><?php echo getMLText("add_workflow_action") ?>
              <?php
              $selected = 0;
              $count = 2;
              foreach ($workflowactions as $currWorkflowAction) {
                if ($selworkflowaction && $currWorkflowAction->getID() == $selworkflowaction->getID()) {
                  $selected = $count;
                }
                print "<option value=\"" . $currWorkflowAction->getID() . "\">" . htmlspecialchars($currWorkflowAction->getName());
                $count++;
              }
              ?>
          </select>
        </div>
      </div>

      <div class="span8">
        <div class="well">
          <table class="table-condensed">
            <tr>
              <td id="keywords0" style="display : none;">

                <form action="../op/op.WorkflowActionsMgr.php"
                      method="post"
                      name="form0"
                      onsubmit="return checkForm('0');">
                  <?php echo createHiddenFieldWithKey('addworkflowaction'); ?>
                  <input type="Hidden" name="action" value="addworkflowaction">
                  <table class="table-condensed">
                    <tr>
                      <td><?php printMLText("workflow_action_name"); ?>:</td>
                      <td><input type="text" name="name"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><input type="submit" class="btn" value="<?php printMLText("add_workflow_action"); ?>"></td>
                    </tr>
                  </table>
                </form>
              </td>

              <?php
              foreach ($workflowactions as $currWorkflowAction) {

                print "<td id=\"keywords" . $currWorkflowAction->getID() . "\" style=\"display : none;\">";
                ?>
	<table class="table-condensed">
		<tr>
			<td></td>
			<td>
<?php
                if ($currWorkflowAction->isUsed()) {
                  ?>
                  <p><?php echo getMLText('workflow_action_in_use') ?></p>
                <?php
                } else {
                  ?>
                  <form class="form-inline" action="../op/op.RemoveWorkflowAction.php" method="post">
                    <?php echo createHiddenFieldWithKey('removeworkflowaction'); ?>
                    <input type="hidden" name="workflowactionid" value="<?php print $currWorkflowAction->getID(); ?>">
                    <button type="submit" class="btn">
                      <span class="fa fa-remove"></span> <?php printMLText("rm_workflow_action"); ?></button>
                  </form>
                <?php
                }
                ?>
			</td>
		</tr>
	<form action="../op/op.WorkflowActionsMgr.php" method="post" name="form<?php print $currWorkflowAction->getID(); ?>" onsubmit="return checkForm('<?php print $currWorkflowAction->getID(); ?>');">
	<?php echo createHiddenFieldWithKey('editworkflowaction'); ?>
	<input type="Hidden" name="workflowactionid" value="<?php print $currWorkflowAction->getID(); ?>">
	<input type="Hidden" name="action" value="editworkflowaction">
		<tr>
			<td><?php printMLText("workflow_action_name"); ?>:</td>
			<td><input type="text" name="name" value="<?php print htmlspecialchars($currWorkflowAction->getName()); ?>"></td>
		</tr>
		<tr>
			<td></td>
			<td><button type="submit" class="btn"><span class="fa fa-save"></span> <?php printMLText("save") ?></button></td>
		</tr>
	</form>
	</table>
</td>
<?php } ?>
            </tr>
          </table>
        </div>
      </div>
    </div>

    <script language="JavaScript">

      sel = document.getElementById("selector");
      sel.selectedIndex =<?php print $selected ?>;
      showWorkflowAction(sel);

    </script>


    <?php
    $this->htmlEndPage();
  }
}

?>

