<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for ViewEvent view.
 */
class SeedDMS_View_ViewEvent extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $event = $this->params['event'];

    $this->htmlStartPage(getMLText("calendar"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText("calendar"), "calendar");

    $this->contentHeading(getMLText("event_details"));
    $this->contentContainerStart();

    $u = $dms->getUser($event["userID"]);

    echo "<table class=\"table-condensed\">";

    echo "<tr>";
    echo "<td>" . getMLText("name") . ": </td>";
    echo "<td>" . htmlspecialchars($event["name"]) . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>" . getMLText("comment") . ": </td>";
    echo "<td>" . htmlspecialchars($event["comment"]) . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>" . getMLText("from") . ": </td>";
    echo "<td>" . getReadableDate($event["start"]) . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>" . getMLText("to") . ": </td>";
    echo "<td>" . getReadableDate($event["stop"]) . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>" . getMLText("last_update") . ": </td>";
    echo "<td>" . getLongReadableDate($event["date"]) . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td>" . getMLText("user") . ": </td>";
    echo "<td>" . (is_object($u) ? htmlspecialchars($u->getFullName()) : getMLText("unknown_user")) . "</td>";
    echo "</tr>";

    if (($user->getID() == $event["userID"]) || ($user->isAdmin())) {
      echo "<tr>";
      echo "<td></td>";
      echo "<td>";
      print "<a href=\"../out/out.RemoveEvent.php?id=" . $event["id"] . "\" class=\"btn\"><span class=\"fa fa-remove\"></span> " . getMLText("delete") . "</a> ";
      print "<a href=\"../out/out.EditEvent.php?id=" . $event["id"] . "\" class=\"btn\"><span class=\"fa fa-edit\"></span> " . getMLText("edit") . "</a>";
      echo "</tr>";
    }

    echo "</table>";

    $this->contentContainerEnd();

    $this->htmlEndPage();
  }
}

?>
