<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for SetExpires view.
 */
class SeedDMS_View_SetExpires extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $folder = $this->params['folder'];
    $document = $this->params['document'];

    $this->htmlStartPage(getMLText("document_title", array("documentname" => htmlspecialchars($document->getName()))));
    $this->globalNavigation($folder);
    $this->contentStart();
    $this->pageNavigation($this->getFolderPathHTML($folder, true, $document), "view_document", $document);
    $this->contentHeading(getMLText("set_expiry"));
    $this->contentContainerStart();

    if ($document->expires()) {
      $expdate = date('d-m-Y', $document->getExpires());
    } else {
      $expdate = '';
    }
    ?>

    <form action="../op/op.SetExpires.php" method="post">
      <input type="hidden" name="documentid" value="<?php print $document->getID(); ?>">

      <table class="table-condensed">
        <tr>
          <td><?php printMLText("expires"); ?>:</td>
          <td>
    <span class="input-append date span12"
          id="expirationdate"
          data-date="<?php echo $expdate; ?>"
          data-date-format="dd-mm-yyyy"
          data-date-language="<?php echo str_replace('_', '-', $this->params['session']->getLanguage()); ?>">
      <input class="span4" size="16" name="expdate" type="text" value="<?php echo $expdate; ?>">
      <span class="add-on"><span class="fa fa-calendar"></span></span>
    </span><br/>
            <label class="checkbox inline">
              <input type="checkbox" name="expires" value="false"<?php if (!$document->expires()) {
                print " checked";
              } ?>><?php printMLText("does_not_expire"); ?><br>
            </label>
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <button type="submit" class="btn"><span class="fa fa-save"></span> <?php printMLText("save") ?></button>
          </td>
        </tr>
      </table>
    </form>
    <?php
    $this->contentContainerEnd();
    $this->htmlEndPage();
  }
}

?>
