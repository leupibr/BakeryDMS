<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for GroupView view.
 */
class SeedDMS_View_GroupView extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $allGroups = $this->params['allgroups'];
    $allUsers = $this->params['allusers'];

    $this->htmlStartPage(getMLText("my_account"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText("my_account"), "my_account");

    $this->contentHeading(getMLText("groups"));
    $this->contentContainerStart();

    echo "<ul class=\"groupView\">\n";

    foreach ($allGroups as $group) {

      $members = $group->getUsers();
      $managers = $group->getManagers();
      $ismanager = false; /* set to true if current user is manager */

      echo "<li>" . htmlspecialchars($group->getName());
      if ($group->getComment()) {
        echo " : " . htmlspecialchars($group->getComment());
      }
      foreach ($managers as $manager) {
        if ($manager->getId() == $user->getId()) {
          echo " : " . getMLText("manager_of_group");
          $ismanager = true;
        }
      }
      echo "</li>";

      echo "<ul>\n";
      $memberids = array();
      foreach ($members as $member) {
        $memberids[] = $member->getId();

        echo "<li>" . htmlspecialchars($member->getFullName());
        if ($member->getEmail() != "") {
          echo " (<a href=\"mailto:" . htmlspecialchars($member->getEmail()) . "\">" . htmlspecialchars($member->getEmail()) . "</a>)";
        }
        foreach ($managers as $manager) {
          if ($manager->getId() == $member->getId()) {
            echo ", " . getMLText("manager");
          }
        }
        if ($ismanager) {
          echo ' <a href="../op/op.GroupView.php?action=del&groupid=' . $group->getId() . '&userid=' . $member->getId() . '"><img src="images/del.gif" width="15" height="15" border="0" align="absmiddle" alt=""> ' . getMLText("rm_user") . '</a>';
        }
        echo "</li>";
      }
      if ($ismanager) {
        echo "<li>" . getMLText("add_user_to_group") . ":";
        echo "<form action=\"../op/op.GroupView.php\">";
        echo "<input type=\"hidden\" name=\"action\" value=\"add\" /><input type=\"hidden\" name=\"groupid\" value=\"" . $group->getId() . "\" />";
        echo "<select name=\"userid\" onChange=\"javascript: submit();\">";
        echo "<option value=\"\"></option>";
        foreach ($allUsers as $u) {
          if (!$u->isAdmin() && !$u->isGuest() && !in_array($u->getId(), $memberids)) {
            echo "<option value=\"" . $u->getId() . "\">" . htmlspecialchars($u->getFullName()) . "</option>";
          }
        }
        echo "</select>";
        echo "</form>";
        echo "</li>";
      }
      echo "</ul>\n";
    }
    echo "</ul>\n";

    $this->contentContainerEnd();
    $this->htmlEndPage();
  }
}

?>
