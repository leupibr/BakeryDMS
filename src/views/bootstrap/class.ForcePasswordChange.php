<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for ForcePasswordChange view.
 */
class SeedDMS_View_ForcePasswordChange extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $passwordstrength = $this->params['passwordstrength'];

    $this->htmlStartPage(getMLText("sign_in"), "login");
    $this->globalBanner();
    $this->contentStart();
    echo "<h3>" . getMLText('password_expiration') . "</h3>";
    echo "<div class=\"alert\">" . getMLText('password_expiration_text') . "</div>";
    $this->contentContainerStart();
    ?>
    <form action="../op/op.EditUserData.php" method="post" name="form1" onsubmit="return checkForm();">
      <table>
        <tr>
          <td><?php printMLText("current_password"); ?>:</td>
          <td><input id="currentpwd" type="Password" name="currentpwd" size="30"></td>
        </tr>
        <tr>
          <td><?php printMLText("password"); ?>:</td>
          <td><input class="pwd" type="Password" rel="strengthbar" name="pwd" size="30"></td>
        </tr>
        <tr>
          <td><?php printMLText("password_strength"); ?>:</td>
          <td>
            <div id="strengthbar" class="progress" style="width: 220px; height: 30px; margin-bottom: 8px;">
              <div class="bar bar-danger" style="width: 0%;"></div>
            </div>
          </td>
        </tr>
        <tr>
          <td><?php printMLText("confirm_pwd"); ?>:</td>
          <td><input id="pwdconf" type="Password" name="pwdconf" size="30"></td>
        </tr>
        <tr>
          <td></td>
          <td><input class="btn" type="submit" value="<?php printMLText("submit_userinfo") ?>"></td>
        </tr>
      </table>
      <input type="hidden" name="fullname" value="<?php print htmlspecialchars($user->getFullName()); ?>"/>
      <input type="hidden" name="email" value="<?php print htmlspecialchars($user->getEmail()); ?>"/>
      <input type="hidden" name="comment" value="<?php print htmlspecialchars($user->getComment()); ?>"/>
    </form>

    <?php
    $this->contentContainerEnd();
    $tmpfoot = array();
    $tmpfoot[] = "<a href=\"../op/op.Logout.php\">" . getMLText("logout") . "</a>\n";
    print "<p>";
    print implode(' | ', $tmpfoot);
    print "</p>\n";
    $this->htmlEndPage();
  }
}

?>
