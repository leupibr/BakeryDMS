<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for EditComment view.
 */
class SeedDMS_View_EditComment extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $folder = $this->params['folder'];
    $document = $this->params['document'];
    $version = $this->params['version'];
    $strictformcheck = $this->params['strictformcheck'];

    $this->htmlStartPage(getMLText("document_title", array("documentname" => htmlspecialchars($document->getName()))));
    $this->globalNavigation($folder);
    $this->contentStart();
    $this->pageNavigation($this->getFolderPathHTML($folder, true, $document), "view_document", $document);

    ?>
    <script language="JavaScript">
      function checkForm() {
        msg = new Array();
        <?php
            if ($strictformcheck) {
        ?>
        if (document.form1.comment.value == "") msg.push("<?php printMLText("js_no_comment");?>");
        <?php
            }
        ?>
        if (msg != "") {
          noty({
            text: msg.join('<br />'),
            type: 'error',
            dismissQueue: true,
            layout: 'topRight',
            theme: 'defaultTheme',
            _timeout: 1500,
          });
          return false;
        }
        else return true;
      }
    </script>

    <?php
    $this->contentHeading(getMLText("edit_comment"));
    $this->contentContainerStart();
    ?>
    <form action="../op/op.EditComment.php" name="form1" onsubmit="return checkForm();" method="POST">
      <?php echo createHiddenFieldWithKey('editcomment'); ?>
      <input type="Hidden" name="documentid" value="<?php print $document->getID(); ?>">
      <input type="Hidden" name="version" value="<?php print $version->getVersion(); ?>">
      <table class="table-condensed">
        <tr>
          <td class="inputDescription"><?php printMLText("comment"); ?>:</td>
          <td><textarea name="comment"
                        rows="4"
                        cols="80"><?php print htmlspecialchars($version->getComment()); ?></textarea></td>
        </tr>
        <tr>
          <td></td>
          <td>
            <button type="submit" class="btn"><span class="fa fa-save"></span> <?php printMLText("save") ?></button>
          </td>
        </tr>
      </table>
    </form>
    <?php
    $this->contentContainerEnd();
    $this->htmlEndPage();
  }
}

?>
