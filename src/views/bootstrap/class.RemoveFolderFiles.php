<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for RemoveFolderFiles view.
 */
class SeedDMS_View_RemoveFolderFiles extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $folder = $this->params['folder'];

    $this->htmlStartPage(getMLText("files_deletion"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText("admin_tools"), "admin_tools");

    $this->contentHeading(getMLText("files_deletion"));
    $this->contentContainerStart();
    ?>
    <form action="../op/op.RemoveFolderFiles.php" name="form1" method="post">
      <?php echo createHiddenFieldWithKey('removefolderfiles'); ?>
      <input type="hidden" name="folderid" value="<?php echo $folder->getID() ?>">

      <p><?php printMLText("confirm_rm_folder_files",
          array("foldername" => htmlspecialchars($folder->getName()))); ?></p>
      <input class="btn" type="submit" value="<?php printMLText("accept"); ?>">
    </form>
    <?php
    $this->contentContainerEnd();
    $this->htmlEndPage();
  }
}

?>
