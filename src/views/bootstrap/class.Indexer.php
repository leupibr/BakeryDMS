<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
use BakeryDMS\Lucene\IndexedDocument;

require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for Indexer view.
 */
class SeedDMS_View_Indexer extends SeedDMS_Bootstrap_Style {

  function tree($dms, $index, $folder, $indent = '') {
    set_time_limit(30);
    echo $indent . "D " . htmlspecialchars($folder->getName()) . "\n";
    $subfolders = $folder->getSubFolders();
    foreach ($subfolders as $subfolder) {
      $this->tree($dms, $index, $subfolder, $indent . '  ');
    }
    $documents = $folder->getDocuments();
    foreach ($documents as $document) {
      echo $indent . "  " . $document->getId() . ":" . htmlspecialchars($document->getName()) . " ";
      /* If the document wasn't indexed before then just add it */
      if (!($hits = $index->find('document_id:' . $document->getId()))) {
        $index->addDocument(new IndexedDocument($dms,
          $document,
          $this->converters ? $this->converters : null));
        echo "(document added)";
      } else {
        $hit = $hits[0];
        /* Check if the attribute created is set or has a value older
         * than the lasted content. Documents without such an attribute
         * where added when a new document was added to the dms. In such
         * a case the document content  wasn't indexed.
         */
        try {
          $created = (int) $hit->getDocument()->getFieldValue('created');
        } catch (Zend_Search_Lucene_Exception $e) {
          $created = 0;
        }
        $content = $document->getLatestContent();
        if ($created >= $content->getDate()) {
          echo $indent . "(document unchanged)";
        } else {
          $index->delete($hit->id);
          $index->addDocument(new IndexedDocument($dms,
            $document,
            $this->converters ? $this->converters : null));
          echo $indent . "(document updated)";
        }
      }
      echo "\n";
    }
  }

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $index = $this->params['index'];
    $recreate = $this->params['recreate'];
    $folder = $this->params['folder'];
    $this->converters = $this->params['converters'];

    $this->htmlStartPage(getMLText("admin_tools"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText("admin_tools"), "admin_tools");
    $this->contentHeading(getMLText("update_fulltext_index"));

    echo "<pre>";
    $this->tree($dms, $index, $folder);
    echo "</pre>";

    $index->commit();
    $index->optimize();

    $this->htmlEndPage();
  }
}

?>
