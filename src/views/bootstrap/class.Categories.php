<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for Categories view.
 */
class SeedDMS_View_Categories extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $categories = $this->params['categories'];

    $this->htmlStartPage(getMLText("admin_tools"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText("admin_tools"), "admin_tools");
    ?>

    <script language="JavaScript">
      obj = -1;
      function showCategories(selectObj) {
        if (obj != -1) {
          obj.style.display = "none";
        }

        id = selectObj.options[selectObj.selectedIndex].value;
        if (id == -1) {
          return;
        }

        obj = document.getElementById("categories" + id);
        obj.style.display = "";
      }
    </script>
    <?php
    $this->contentHeading(getMLText("global_document_categories"));
    ?>
    <div class="row-fluid">
      <div class="span4">
        <div class="well">
          <?php echo getMLText("selection") ?>:
          <select onchange="showCategories(this)" id="selector" class="span9">
            <option value="-1"><?php echo getMLText("choose_category") ?>
            <option value="0"><?php echo getMLText("new_document_category") ?>

              <?php

              $selected = 0;
              $count = 2;
              foreach ($categories as $category) {

                if (isset($_GET["categoryid"]) && $category->getID() == $_GET["categoryid"]) {
                  $selected = $count;
                }
                print "<option value=\"" . $category->getID() . "\">" . htmlspecialchars($category->getName());
                $count++;
              }
              ?>
          </select>

        </div>
      </div>

      <div class="span8">
        <div class="well">

          <table class="table-condensed">
            <tr>
              <td id="categories0" style="display : none;">
                <form class="form-inline" action="../op/op.Categories.php" method="post">
                  <?php echo createHiddenFieldWithKey('addcategory'); ?>
                  <input type="Hidden" name="action" value="addcategory">
                  <?php printMLText("name"); ?> : <input type="text" name="name">
                  <input type="submit" class="btn" value="<?php printMLText("new_document_category"); ?>">
                </form>
              </td>

              <?php
              foreach ($categories as $category) {
                print "<td id=\"categories" . $category->getID() . "\" style=\"display : none;\">";
                ?>
			<table class="table-condensed">
				<tr>
					<td></td><td>
<?php
                if (!$category->isUsed()) {
                  ?>
                  <form style="display: inline-block;" method="post" action="../op/op.Categories.php">
                    <?php echo createHiddenFieldWithKey('removecategory'); ?>
                    <input type="Hidden" name="categoryid" value="<?php echo $category->getID() ?>">
                    <input type="Hidden" name="action" value="removecategory">
                    <button class="btn" type="submit">
                      <span class="fa fa-remove"></span> <?php echo getMLText("rm_document_category") ?></button>
                  </form>
                <?php
                } else {
                  ?>
                  <p><?php echo getMLText('category_in_use') ?></p>
                <?php
                }
                ?>
					</td>
				</tr>
				<tr>
					<td><?php echo getMLText("name") ?>:</td>
					<td>
						<form class="form-inline" style="margin-bottom: 0px;" action="../op/op.Categories.php" method="post">
  		        <?php echo createHiddenFieldWithKey('editcategory'); ?>
							<input type="Hidden" name="action" value="editcategory">
							<input type="Hidden" name="categoryid" value="<?php echo $category->getID() ?>">
							<input name="name" type="text" value="<?php echo htmlspecialchars($category->getName()) ?>">&nbsp;
							<button type="submit" class="btn"><sapn class="fa fa-save"></span> <?php printMLText("save"); ?></button>
						</form>
					</td>
				</tr>
				
			</table>
		</td>
<?php } ?>
            </tr>
          </table>
        </div>
      </div>
    </div>

    <script language="JavaScript">
      sel = document.getElementById("selector");
      sel.selectedIndex =<?php print $selected ?>;
      showCategories(sel);
    </script>
    <?php
    $this->htmlEndPage();
  }
}

?>
