<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
use BakeryDMS\Core\DocumentState;

require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for OverrideContentStatus view.
 */
class SeedDMS_View_OverrideContentStatus extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $folder = $this->params['folder'];
    $document = $this->params['document'];
    $content = $this->params['version'];

    $overallStatus = $content->getStatus();
    $reviewStatus = $content->getReviewStatus();
    $approvalStatus = $content->getApprovalStatus();

    $this->htmlStartPage(getMLText("document_title", array("documentname" => htmlspecialchars($document->getName()))));
    $this->globalNavigation($folder);
    $this->contentStart();
    $this->pageNavigation($this->getFolderPathHTML($folder, true, $document), "view_document", $document);

    $this->contentHeading(getMLText("change_status"));

    ?>
    <script language="JavaScript">
      function checkForm() {
        msg = new Array();
        if (document.form1.overrideStatus.value == "") msg.push("<?php printMLText("js_no_override_status");?>");
        if (document.form1.comment.value == "") msg.push("<?php printMLText("js_no_comment");?>");
        if (msg != "") {
          noty({
            text: msg.join('<br />'),
            type: 'error',
            dismissQueue: true,
            layout: 'topRight',
            theme: 'defaultTheme',
            _timeout: 1500,
          });
          return false;
        }
        else
          return true;
      }
    </script>
    <?php

    $this->contentContainerStart();

// Display the Review form.
    ?>
    <form method="post" action="../op/op.OverrideContentStatus.php" name="form1" onsubmit="return checkForm();">
      <table class="table-condensed">
        <tr>
          <td><?php echo(printMLText("comment")); ?>:</td>
          <td><textarea name="comment" cols="40" rows="4"></textarea>
          </td>
        </tr>
        <tr>
          <td><?php echo(printMLText("status")); ?>:</td>
          <td><select name="overrideStatus">
              <option value=''></option>
              <?php

              if ($overallStatus["status"] == DocumentState::OBSOLETE) {
                echo "<option value='" . DocumentState::RELEASED . "'>" . getOverallStatusText(DocumentState::RELEASED) . "</option>";
              }
              if ($overallStatus["status"] == DocumentState::RELEASED) {
                echo "<option value='" . DocumentState::OBSOLETE . "'>" . getOverallStatusText(DocumentState::OBSOLETE) . "</option>";
              }

              ?>
            </select>
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <input type='hidden' name='documentid' value='<?php echo $document->getID() ?>'/>
            <input type='hidden' name='version' value='<?php echo $content->getVersion() ?>'/>
            <input type='submit'
                   class="btn"
                   name='overrideContentStatus'
                   value='<?php echo(printMLText("update")); ?>'/>
          </td>
        </tr>
      </table>
    </form>
    <?php
    $this->contentContainerEnd();
    $this->htmlEndPage();
  }
}

?>
