<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for RemoveLog view.
 */
class SeedDMS_View_RemoveLog extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $lognames = $this->params['lognames'];
    $mode = $this->params['mode'];

    $this->htmlStartPage(getMLText("backup_tools"));
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation(getMLText("admin_tools"), "admin_tools");
    $this->contentHeading(getMLText("rm_file"));
    $this->contentContainerStart();
    ?>
    <form action="../op/op.RemoveLog.php" name="form1" method="post">
      <?php echo createHiddenFieldWithKey('removelog'); ?>
      <input type="hidden" name="mode" value="<?php echo $mode; ?>"/>
      <?php
      foreach ($lognames as $logname) {
        echo "<input type=\"hidden\" name=\"lognames[]\" value=\"" . $logname . "\">\n";

      }
      ?>
      <p><?php printMLText("confirm_rm_log", array("logname" => implode(', ', $lognames))); ?></p>

      <p>
        <button type="submit" class="btn"><span class="fa fa-remove"></span> <?php printMLText("rm_file"); ?></button>
      </p>
    </form>
    <?php
    $this->contentContainerEnd();
    $this->htmlEndPage();
  }
}

?>
