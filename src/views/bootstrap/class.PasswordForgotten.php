<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for PasswordForgotten view.
 */
class SeedDMS_View_PasswordForgotten extends SeedDMS_Bootstrap_Style {

  function show() {
    $referrer = $this->params['referrer'];

    $this->htmlStartPage(getMLText("password_forgotten"), "login");
    $this->globalBanner();
    $this->contentStart();
    $this->pageNavigation(getMLText("password_forgotten"));
    ?>

    <?php $this->contentContainerStart(); ?>
    <form action="../op/op.PasswordForgotten.php" method="post" name="form1" onsubmit="return checkForm();">
      <?php
      if ($referrer) {
        echo "<input type='hidden' name='referuri' value='" . $referrer . "'/>";
      }
      ?>
      <p><?php printMLText("password_forgotten_text"); ?></p>
      <table class="table-condensed">
        <tr>
          <td><?php printMLText("login"); ?>:</td>
          <td><input type="text" name="login" id="login"></td>
        </tr>
        <tr>
          <td><?php printMLText("email"); ?>:</td>
          <td><input type="text" name="email" id="email"></td>
        </tr>
        <tr>
          <td></td>
          <td><input class="btn" type="submit" value="<?php printMLText("submit_password_forgotten") ?>"></td>
        </tr>
      </table>
    </form>
    <?php $this->contentContainerEnd(); ?>
    <script language="JavaScript">document.form1.email.focus();</script>
    <p><a href="../out/out.Login.php"><?php echo getMLText("login"); ?></a></p>
    <?php
    $this->htmlEndPage();
  }
}

?>
