<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
use BakeryDMS\Core\AccessMode;

require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for MoveDocument view.
 */
class SeedDMS_View_MoveDocument extends SeedDMS_Bootstrap_Style {

  function show() {
    $dms = $this->params['dms'];
    $user = $this->params['user'];
    $folder = $this->params['folder'];
    $document = $this->params['document'];
    $target = $this->params['target'];

    $this->htmlStartPage(getMLText("document_title", array("documentname" => htmlspecialchars($document->getName()))));
    $this->globalNavigation($folder);
    $this->contentStart();
    $this->pageNavigation($this->getFolderPathHTML($folder, true, $document), "view_document", $document);
    $this->contentHeading(getMLText("move_document"));
    $this->contentContainerStart('warning');
    ?>
    <form action="../op/op.MoveDocument.php" name="form1">
      <input type="hidden" name="documentid" value="<?php print $document->getID(); ?>">
      <table>
        <tr>
          <td><?php printMLText("choose_target_folder"); ?>:</td>
          <td><?php $this->printFolderChooser("form1", AccessMode::READWRITE, -1, $target); ?></td>
        </tr>
        <tr>
          <td colspan="2"><input type="submit" class="btn" value="<?php printMLText("move"); ?>"></td>
        </tr>
      </table>
    </form>
    <?php
    $this->contentContainerEnd();
    $this->htmlEndPage();
  }
}

?>
