<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * Include parent class.
 */
use BakeryDMS\Core\AccessMode;
use BakeryDMS\Core\DMS;
use BakeryDMS\Core\Document;
use BakeryDMS\Core\DocumentCategory;
use BakeryDMS\Core\DocumentContent;
use BakeryDMS\Core\DocumentFile;
use BakeryDMS\Core\DocumentLink;
use BakeryDMS\Core\DocumentState;
use BakeryDMS\Core\User;
use BakeryDMS\Core\Workflow\Transition;
use BakeryDMS\Preview\Previewer;
use BakeryDMS\Utils\File;

require_once("class.Bootstrap.php");

/**
 * Class which outputs the html page for ViewDocument view.
 */
class SeedDMS_View_ViewDocument extends SeedDMS_Bootstrap_Style {
  function printAccessList(Document $obj) {
    $accessList = $obj->getAccessList();
    if (count($accessList["users"]) == 0 && count($accessList["groups"]) == 0) {
      return;
    }

    for ($i = 0; $i < count($accessList["groups"]); $i++) {
      $group = $accessList["groups"][$i]->getGroup();
      $accesstext = AccessMode::getText($accessList["groups"][$i]->getMode());
      print $accesstext . ": " . htmlspecialchars($group->getName());
      if ($i + 1 < count($accessList["groups"]) || count($accessList["users"]) > 0) {
        print "<br />";
      }
    }
    for ($i = 0; $i < count($accessList["users"]); $i++) {
      $user = $accessList["users"][$i]->getUser();
      $accesstext = AccessMode::getText($accessList["users"][$i]->getMode());
      print $accesstext . ": " . htmlspecialchars($user->getFullName());
      if ($i + 1 < count($accessList["users"])) {
        print "<br />";
      }
    }
  }

  function show() {
    /** @var DMS $dms */
    $dms = $this->params['dms'];
    /** @var User $user */
    $user = $this->params['user'];
    $folder = $this->params['folder'];
    /** @var Document $document */
    $document = $this->params['document'];
    /** @var SeedDMS_AccessOperation $accessop */
    $accessop = $this->params['accessobject'];
    $viewonlinefiletypes = $this->params['viewonlinefiletypes'];
    $enableownerrevapp = $this->params['enableownerrevapp'];
    $workflowmode = $this->params['workflowmode'];
    $cachedir = $this->params['cachedir'];
    $previewwidthlist = $this->params['previewWidthList'];
    $previewwidthdetail = $this->params['previewWidthDetail'];
    $documentid = $document->getId();

    $versions = $document->getContent();

    $this->htmlStartPage(s(t("document_title"), ["documentname" => htmlspecialchars($document->getName())]));
    $this->globalNavigation($folder);
    $this->contentStart();
    $this->pageNavigation($this->getFolderPathHTML($folder, true, $document), "view_document", $document);

    $this->printLockInfo($document);

    /** @var DocumentFile[] $files */
    $files = $document->getDocumentFiles();

    /* Retrieve linked documents */
    $links = $document->getDocumentLinks();
    $links = DMS::filterDocumentLinks($user, $links);

    /* Retrieve reverse linked documents */
    $reverselinks = $document->getReverseDocumentLinks();
    $reverselinks = DMS::filterDocumentLinks($user, $reverselinks);

    /* Retrieve latest content */
    $latestContent = $document->getLatestContent();

    $this->printWorkflowInfo($workflowmode, $latestContent, $user);

    $status = $latestContent->getStatus();
    $reviewStatus = $latestContent->getReviewStatus();
    $approvalStatus = $latestContent->getApprovalStatus();
    ?>

    <div class="row-fluid">
      <div class="span3">
        <?php
        $this->contentHeading(t("document_infos"));
        $this->contentContainerStart();
        ?>
        <table class="table-condensed">
          <?php
          if ($user->isAdmin()) {
            echo "<tr>";
            echo "<td>" . t("id") . ":</td>\n";
            echo "<td>" . htmlspecialchars($document->getID()) . "</td>\n";
            echo "</tr>";
          }
          ?>
          <tr>
            <td><?php echo t("name"); ?>:</td>
            <td><?php print htmlspecialchars($document->getName()); ?></td>
          </tr>
          <tr>
            <td><?php echo t("owner"); ?>:</td>
            <td>
              <?php
              $owner = $document->getOwner();
              print "<a class=\"infos\" href=\"mailto:" . $owner->getEmail() . "\">" . htmlspecialchars($owner->getFullName()) . "</a>";
              ?>
            </td>
          </tr>
          <?php
          if ($document->getComment()) {
            ?>
            <tr>
              <td><?php t("comment"); ?>:</td>
              <td><?php print htmlspecialchars($document->getComment()); ?></td>
            </tr>
            <?php
          }
          if ($user->isAdmin()) {
            if ($document->inheritsAccess()) {
              echo "<tr>";
              echo "<td>" . t("access_mode") . ":</td>\n";
              echo "<td>";
              echo t("inherited");
              echo "</tr>";
            }
            else {
              echo "<tr>";
              echo "<td>" . t('default_access') . ":</td>";
              echo "<td>" . AccessMode::getText($document->getDefaultAccess()) . "</td>";
              echo "</tr>";
              echo "<tr>";
              echo "<td>" . t('access_mode') . ":</td>";
              echo "<td>";
              $this->printAccessList($document);
              echo "</td>";
              echo "</tr>";
            }
          }
          ?>
          <tr>
            <td><?php echo t("used_discspace"); ?>:</td>
            <td><?php echo File::format_filesize($document->getUsedDiskSpace()); ?></td>
          </tr>
          <tr>
            <td><?php echo t("creation_date"); ?>:</td>
            <td><?php echo getLongReadableDate($document->getDate()); ?></td>
          </tr>
          <?php
          if ($document->expires()) {
            ?>
            <tr>
              <td><?php echo t("expires"); ?>:</td>
              <td><?php echo getReadableDate($document->getExpires()); ?></td>
            </tr>
            <?php
          }
          if ($document->getKeywords()) {
            ?>
            <tr>
              <td><?php t("keywords"); ?>:</td>
              <td><?php print htmlspecialchars($document->getKeywords()); ?></td>
            </tr>
            <?php
          }
          /** @var DocumentCategory[] $cats */
          if ($cats = $document->getCategories()) {
            ?>
            <tr>
              <td><?php t("categories"); ?>:</td>
              <td>
                <?php
                $ct = array();
                foreach ($cats as $cat) {
                  $ct[] = htmlspecialchars($cat->getName());
                }
                echo implode(', ', $ct);
                ?>
              </td>
            </tr>
            <?php
          }
          ?>
          <?php
          $attributes = $document->getAttributes();
          if ($attributes) {
            foreach ($attributes as $attribute) {
              $attrdef = $attribute->getAttributeDefinition();
              ?>
              <tr>
                <td><?php echo htmlspecialchars($attrdef->getName()); ?>:</td>
                <td><?php echo htmlspecialchars($attribute->getValue()); ?></td>
              </tr>
              <?php
            }
          }
          ?>
        </table>
        <?php
        $this->contentContainerEnd();
        ?>
      </div>
      <div class="span9">
        <ul class="nav nav-tabs" id="docinfotab">
          <li class="active"><a data-target="#docinfo" data-toggle="tab"><?php echo t('current_version'); ?></a>
          </li>
          <?php if (count($versions) > 1) { ?>
            <li><a data-target="#previous" data-toggle="tab"><?php echo t('previous_versions'); ?></a></li>
            <?php
          }
          if ($workflowmode == 'traditional') {
            if ((is_array($reviewStatus) && count($reviewStatus) > 0)
              || (is_array($approvalStatus) && count($approvalStatus) > 0)
            ) {
              ?>
              <li><a data-target="#revapp"
                     data-toggle="tab"><?php echo t('reviewers') . "/" . t('approvers'); ?></a></li>
              <?php
            }
          }
          else {
            if ($latestContent->getWorkflow()) {
              ?>
              <li><a data-target="#workflow" data-toggle="tab"><?php echo t('workflow'); ?></a></li>
              <?php
            }
          }
          ?>
          <li><a data-target="#attachments" data-toggle="tab"><?php echo t('linked_files');
              echo (count($files)) ? " (" . count($files) . ")" : ""; ?></a></li>
          <li><a data-target="#links" data-toggle="tab"><?php echo t('linked_documents');
              echo (count($links)) ? " (" . count($links) . ")" : ""; ?></a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="docinfo">
            <?php
            if (!$latestContent) {
              $this->contentContainerStart();
              echo t('document_content_missing');
              $this->contentContainerEnd();
              $this->htmlEndPage();
              exit;
            }

            // verify if file exists
            $file_exists = file_exists($dms->contentDir . $latestContent->getPath());

            $this->contentContainerStart();
            echo "<table class=\"table\">";
            echo "<thead><tr>";
            echo "<th width='*'></th>";
            echo "<th width='*'>" . t("file") . "</th>";
            echo "<th width='25%'>" . t("comment") . "</th>";
            echo "<th width='15%'>" . t("status") . "</th>";
            echo "<th width='20%'></th>";
            echo "</tr></thead><tbody>";
            echo "<tr>";
            echo "<td>";

            $previewer = new Previewer($cachedir, $previewwidthdetail);
            $previewer->createPreview($latestContent);
            if ($viewonlinefiletypes && in_array(strtolower($latestContent->getFileType()), $viewonlinefiletypes)) {
              echo '<a target="_blank" href="../op/op.ViewOnline.php?documentid=' . $documentid . '&version=' . $latestContent->getVersion() . '">';
            }
            else {
              echo '<a href="../op/op.Download.php?documentid=' . $documentid . '&version=' . $latestContent->getVersion() . '">';
            }
            if ($previewer->hasPreview($latestContent)) {
              echo('<img class="mimeicon" width="' . $previewwidthdetail . '" src="../op/op.Preview.php?documentid=' . $document->getID() . '&version=' . $latestContent->getVersion() . "&width=" . $previewwidthdetail . '" title="' . htmlspecialchars($latestContent->getMimeType()) . '">');
            }
            else {
              echo '<img class="mimeicon" src="' . $this->getMimeIcon($latestContent->getFileType()) . '" title="' . htmlspecialchars($latestContent->getMimeType()) . '">';
            }
            echo '</a>';
            echo '</td>';

            echo '<td><ul class="actions unstyled">';
            echo '<li>' . $latestContent->getOriginalFileName() . '</li>';
            echo '<li><strong>' . t('version') . ": " . $latestContent->getVersion() . '</strong></li>';

            if ($file_exists) {
              echo '<li>' . File::format_filesize($latestContent->getFileSize()) . ', ' . htmlspecialchars($latestContent->getMimeType()) . '</li>';
            }
            else {
              echo '<li><span class="warning">' . t("document_deleted") . "</span></li>";
            }

            /** @var User $updatingUser */
            $updatingUser = $latestContent->getUser();
            echo '<li>' . t("uploaded_by") . ' <a href="mailto:' . $updatingUser->getEmail() . '">' . htmlspecialchars($updatingUser->getFullName()) . '</a></li>';
            echo '<li>' . getLongReadableDate($latestContent->getDate()) . '</li>';

            echo '</ul>';
            echo '<ul class="actions unstyled">';
            $attributes = $latestContent->getAttributes();
            if ($attributes) {
              foreach ($attributes as $attribute) {
                $attrdef = $attribute->getAttributeDefinition();
                echo '<li>' . htmlspecialchars($attrdef->getName()) . ': ' . htmlspecialchars($attribute->getValue()) . '</li>';
              }
            }
            echo '</ul>';

            echo '<td>' . htmlspecialchars($latestContent->getComment()) . '</td>';

            echo '<td width="10%">';
            echo getOverallStatusText($status["status"]);
            if ($status["status"] == DocumentState::DRAFT_REV || $status["status"] == DocumentState::DRAFT_APP || $status["status"] == DocumentState::IN_WORKFLOW || $status["status"] == DocumentState::EXPIRED) {
              echo '<br><span' . ($document->hasExpired() ? ' class="warning" ' : '') . '>' . (!$document->getExpires() ? t("does_not_expire") : t("expires") . ': ' . getReadableDate($document->getExpires())) . '</span>';
            }
            echo '</td>';
            echo '<td>';

            echo '<ul class="unstyled actions">';
            if ($file_exists) {
              echo '<li><a href="../op/op.Download.php?documentid=' . $documentid . '&version=' . $latestContent->getVersion() . '"><span class="fa fa-download"></span> ' . t("download") . '</a></li>';
              if ($viewonlinefiletypes && in_array(strtolower($latestContent->getFileType()), $viewonlinefiletypes)) {
                echo '<li><a target="_blank" href="../op/op.ViewOnline.php?documentid=' . $documentid . '&version=' . $latestContent->getVersion() . '"><span class="fa fa-star"></span> ' . t("view_online") . '</a></li>';
              }
            }
            echo '</ul>';
            echo '<ul class="unstyled actions">';
            /* Only admin has the right to remove version in any case or a regular
             * user if enableVersionDeletion is on
             */
            if ($accessop->mayRemoveVersion()) {
              echo '<li><a href="out.RemoveVersion.php?documentid=' . $documentid . '&version=' . $latestContent->getVersion() . '"><span class="fa fa-remove"></span> ' . t("rm_version") . '</a></li>';
            }
            if ($accessop->mayOverwriteStatus()) {
              echo '<li><a href=\'../out/out.OverrideContentStatus.php?documentid=' . $documentid . '&version=' . $latestContent->getVersion() . '\'><span class="fa fa-align-justify"></span> ' . t("change_status") . '</a></li>';
            }
            if ($workflowmode == 'traditional') {
              // Allow changing reviewers/approvals only if not reviewed
              if ($accessop->maySetReviewersApprovers()) {
                echo "<li><a href='../out/out.SetReviewersApprovers.php?documentid=" . $documentid . "&version=" . $latestContent->getVersion() . "'><span class=\"fa fa-edit\"></span> " . t("change_assignments") . "</a></li>";
              }
            }
            else {
              if ($accessop->maySetWorkflow()) {
                if (!$latestContent->getWorkflow()) {
                  echo "<li><a href='../out/out.SetWorkflow.php?documentid=" . $documentid . "&version=" . $latestContent->getVersion() . "'><span class=\"fa fa-random\"></span> " . t("set_workflow") . "</a></li>";
                }
              }
            }
            if ($accessop->mayEditComment()) {
              echo '<li><a href="out.EditComment.php?documentid=' . $documentid . "&version=" . $latestContent->getVersion() . '"><span class="fa fa-comment"></span> ' . t("edit_comment") . "</a></li>";
            }
            if ($accessop->mayEditAttributes()) {
              echo '<li><a href="out.EditAttributes.php?documentid=' . $documentid . "&version=" . $latestContent->getVersion() . '"><span class="fa fa-edit"></span> ' . t("edit_attributes") . "</a></li>";
            }

            echo '<li><a href="../op/op.Download.php?documentid=' . $documentid . '&vfile=1"><span class="fa fa-info-circle"></span> ' . t("versioning_info") . "</a></li>";

            echo "</ul>";
            echo "</td>";
            echo "</tr></tbody></table>";
            $this->contentContainerEnd();


            // shows information about this document, such as hints, workflow status and comments
            if ($accessop->mayReview()) {
              $this->contentHeading(t("status"));
              $this->contentContainerStart();
              $statuslog = $latestContent->getStatusLog();
              echo '<table class="table table-condensed"><thead>';
              echo "<th>" . t('date') . "</th><th>" . t('status') . "</th><th>" . t('user') . "</th><th>" . t('comment') . "</th></tr>";
              echo "</thead><tbody>";
              foreach ($statuslog as $entry) {
                if ($suser = $dms->getUser($entry['userID'])) {
                  $fullname = $suser->getFullName();
                }
                else {
                  $fullname = "--";
                }
                echo "<tr><td>" . $entry['date'] . "</td><td>" . getOverallStatusText($entry['status']) . "</td><td>" . $fullname . "</td><td>" . $entry['comment'] . "</td></tr>\n";
              }
              echo "</tbody></table>";
              $this->contentContainerEnd();

              // show workflow status and comments
              /** @var DocumentContent $latestContent */
              $wkflogs = $latestContent->getWorkflowLog();
              if ($wkflogs) {
                $this->contentHeading(t("workflow_summary"));
                $this->contentContainerStart(ContainerType::Info);
                echo '<table class="table table-condensed"><thead>';
                echo "<th>" . t('date') . "</th><th>" . t('status') . "</th><th>" . t('user') . "</th><th>" . t('comment') . "</th></tr>";
                echo "</thead><tbody>";

                /** @var \BakeryDMS\Core\Workflow\Log $wkflog */
                foreach ($wkflogs as $wkflog) {
                  echo "<tr>";
                  echo "<td>" . $wkflog->getDate() . "</td>";
                  echo "<td>" . $wkflog->getTransition()->getNextState()->getName() . "</td>";
                  echo "<td>" . $wkflog->getUser()->getFullName() . "</td>";
                  echo "<td>" . $wkflog->getComment() . "</td>";
                  echo "</tr>";
                }
                echo "</tbody></table>";
                $this->contentContainerEnd();
              }
            }
            ?>
          </div>
          <?php
          if ($workflowmode == 'traditional') {
            if ((is_array($reviewStatus) && count($reviewStatus) > 0)
              || (is_array($approvalStatus) && count($approvalStatus) > 0)
            ) {
              ?>
              <div class="tab-pane" id="revapp">
                <?php
                $this->contentContainerstart();
                echo '<table class="table-condensed">';

                if (is_array($reviewStatus) && count($reviewStatus) > 0) {

                  echo "<tr><td colspan=5>";
                  $this->contentSubHeading(getMLText("reviewers"));
                  echo "</tr>";

                  echo "<tr>";
                  echo "<td width='20%'><b>" . t("name") . "</b></td>";
                  echo "<td width='20%'><b>" . t("last_update") . "</b></td>";
                  echo "<td width='25%'><b>" . t("comment") . "</b></td>";
                  echo "<td width='15%'><b>" . t("status") . "</b></td>";
                  echo "<td width='20%'></td>";
                  echo "</tr>";

                  foreach ($reviewStatus as $r) {
                    $required = null;
                    $reqName = "";
                    $is_reviewer = false;
                    switch ($r["type"]) {
                      case 0: // Reviewer is an individual.
                        $required = $dms->getUser($r["required"]);
                        if (!is_object($required)) {
                          $reqName = t("unknown_user") . " '" . $r["required"] . "'";
                        }
                        else {
                          $reqName = htmlspecialchars($required->getFullName() . " (" . $required->getLogin() . ")");
                        }
                        if ($r["required"] == $user->getId() && ($user->getId() != $owner->getId() || $enableownerrevapp == 1)) {
                          $is_reviewer = true;
                        }
                        break;
                      case 1: // Reviewer is a group.
                        $required = $dms->getGroup($r["required"]);
                        if (!is_object($required)) {
                          $reqName = t("unknown_group") . " '" . $r["required"] . "'";
                        }
                        else {
                          $reqName = "<i>" . htmlspecialchars($required->getName()) . "</i>";
                          if ($required->isMember($user) && ($user->getId() != $owner->getId() || $enableownerrevapp == 1)) {
                            $is_reviewer = true;
                          }
                        }
                        break;
                    }
                    echo "<tr>";
                    echo "<td>" . $reqName . "</td>";
                    echo "<td><ul class=\"unstyled\"><li>" . $r["date"] . "</li>";
                    /* $updateUser is the user who has done the review */
                    $updateUser = $dms->getUser($r["userID"]);
                    echo "<li>" . (is_object($updateUser) ? htmlspecialchars($updateUser->getFullName() . " (" . $updateUser->getLogin() . ")") : "unknown user id '" . $r["userID"] . "'") . "</li></ul></td>";
                    echo "<td>" . htmlspecialchars($r["comment"]) . "</td>\n";
                    echo "<td>" . getReviewStatusText($r["status"]) . "</td>\n";
                    echo "<td><ul class=\"unstyled\">";

                    if ($accessop->mayReview()) {
                      if ($is_reviewer && $r["status"] == 0) {
                        print "<li><a href=\"../out/out.ReviewDocument.php?documentid=" . $documentid . "&version=" . $latestContent->getVersion() . "&reviewid=" . $r['reviewID'] . "\" class=\"btn btn-mini\">" . getMLText("submit_review") . "</a></li>";
                      }
                      else {
                        if (($updateUser == $user) && (($r["status"] == 1) || ($r["status"] == -1)) && (!$document->hasExpired())) {
                          print "<li><a href=\"../out/out.ReviewDocument.php?documentid=" . $documentid . "&version=" . $latestContent->getVersion() . "&reviewid=" . $r['reviewID'] . "\" class=\"btn btn-mini\">" . getMLText("edit") . "</a></li>";
                        }
                      }
                    }

                    print "</ul></td>\n";
                    print "</td>\n</tr>\n";
                  }
                }

                if (is_array($approvalStatus) && count($approvalStatus) > 0) {

                  print "<tr><td colspan=5>\n";
                  $this->contentSubHeading(getMLText("approvers"));
                  print "</tr>";

                  print "<tr>\n";
                  print "<td width='20%'><b>" . t("name") . "</b></td>\n";
                  print "<td width='20%'><b>" . t("last_update") . "</b></td>\n";
                  print "<td width='25%'><b>" . t("comment") . "</b></td>";
                  print "<td width='15%'><b>" . t("status") . "</b></td>\n";
                  print "<td width='20%'></td>\n";
                  print "</tr>\n";

                  foreach ($approvalStatus as $a) {
                    $required = null;
                    $reqName = "";
                    $is_approver = false;
                    switch ($a["type"]) {
                      case 0: // Approver is an individual.
                        $required = $dms->getUser($a["required"]);
                        if (!is_object($required)) {
                          $reqName = t("unknown_user") . " '" . $a["required"] . "'";
                        }
                        else {
                          $reqName = htmlspecialchars($required->getFullName() . " (" . $required->getLogin() . ")");
                        }
                        if ($a["required"] == $user->getId()) {
                          $is_approver = true;
                        }
                        break;
                      case 1: // Approver is a group.
                        $required = $dms->getGroup($a["required"]);
                        if (!is_object($required)) {
                          $reqName = t("unknown_group") . " '" . $a["required"] . "'";
                        }
                        else {
                          $reqName = "<i>" . htmlspecialchars($required->getName()) . "</i>";
                        }
                        if ($required->isMember($user) && ($user->getId() != $owner->getId() || $enableownerrevapp == 1)) {
                          $is_approver = true;
                        }
                        break;
                    }
                    print "<tr>\n";
                    print "<td>" . $reqName . "</td>\n";
                    print "<td><ul class=\"unstyled\"><li>" . $a["date"] . "</li>";
                    /* $updateUser is the user who has done the approval */
                    $updateUser = $dms->getUser($a["userID"]);
                    print "<li>" . (is_object($updateUser) ? htmlspecialchars($updateUser->getFullName() . " (" . $updateUser->getLogin() . ")") : "unknown user id '" . $a["userID"] . "'") . "</li></ul></td>";
                    print "<td>" . htmlspecialchars($a["comment"]) . "</td>\n";
                    print "<td>" . getApprovalStatusText($a["status"]) . "</td>\n";
                    print "<td><ul class=\"unstyled\">";

                    if ($accessop->mayApprove()) {
                      if ($is_approver && $status["status"] == DocumentState::DRAFT_APP) {
                        print '<li><a class="btn btn-mini" href="../out/out.ApproveDocument.php?documentid=' . $documentid . "&version=" . $latestContent->getVersion() . "&approveid=" . $a['approveID'] . "\">" . getMLText("submit_approval") . "</a></li>";
                      }
                      else {
                        if (($updateUser == $user) && (($a["status"] == 1) || ($a["status"] == -1)) && (!$document->hasExpired())) {
                          print '<li><a class="btn btn-mini" href="../out/out.ApproveDocument.php?documentid=' . $documentid . "&version=" . $latestContent->getVersion() . "&approveid=" . $a['approveID'] . "\">" . getMLText("edit") . "</a></li>";
                        }
                      }
                    }

                    print "</ul>";
                    print "</td>\n";
                    print "</td></tr>";
                  }
                }

                print "</table>\n";
                if ($user->isAdmin()) {
                $this->contentContainerEnd();
                ?>
                <div class="row-fluid">
                  <div class="span6">
                    <legend><?php echo t('approval_log'); ?></legend>
                    <table class="table condensed">
                      <tr>
                        <th><?php echo t('name'); ?></th>
                        <th><?php echo t('last_update'); ?>/<?php echo t('comment'); ?></th>
                        <th><?php echo t('status'); ?></th>
                      </tr>
                      <?php
                      $reviewStatusList = $latestContent->getReviewStatus(10);
                      foreach ($reviewStatusList as $rec) {
                        $reqName = "";
                        $required = null;
                        echo "<tr>";
                        echo "<td>";
                        switch ($rec["type"]) {
                          case 0: // Approver is an individual.
                            $required = $dms->getUser($rec["required"]);
                            if (!is_object($required)) {
                              $reqName = t("unknown_user") . " '" . $rec["required"] . "'";
                            }
                            else {
                              $reqName = htmlspecialchars($required->getFullName() . " (" . $required->getLogin() . ")");
                            }
                            break;
                          case 1: // Approver is a group.
                            $required = $dms->getGroup($rec["required"]);
                            if (!is_object($required)) {
                              $reqName = t("unknown_group") . " '" . $rec["required"] . "'";
                            }
                            else {
                              $reqName = "<i>" . htmlspecialchars($required->getName()) . "</i>";
                            }
                            break;
                        }
                        echo $reqName;
                        echo "</td>";
                        echo "<td>";
                        echo '<i style="font-size: 80%;">' . $rec['date'] . " - ";
                        $updateuser = $dms->getUser($rec["userID"]);
                        if (!is_object($required)) {
                          echo t("unknown_user");
                        }
                        else {
                          echo htmlspecialchars($updateuser->getFullName() . " (" . $updateuser->getLogin() . ")");
                        }
                        echo "</i>";
                        if ($rec['comment']) {
                          echo "<br />" . htmlspecialchars($rec['comment']);
                        }
                        echo "</td>";
                        echo "<td>";
                        echo getApprovalStatusText($rec["status"]);
                        echo "</td>";
                        echo "</tr>";
                      }
                      ?>
                    </table>
                  </div>
                  <div class="span6">
                    <legend><?php echo t('review_log'); ?></legend>
                    <table class="table condensed">
                      <tr>
                        <th><?php echo t('name'); ?></th>
                        <th><?php echo t('last_update'); ?>/<?php echo t('comment'); ?></th>
                        <th><?php echo t('status'); ?></th>
                      </tr>
                      <?php
                      $approvalStatusList = $latestContent->getApprovalStatus(10);
                      foreach ($approvalStatusList as $rec) {
                        $reqName = "";
                        $required = null;
                        echo "<tr>";
                        echo "<td>";
                        switch ($rec["type"]) {
                          case 0: // Approver is an individual.
                            $required = $dms->getUser($rec["required"]);
                            if (!is_object($required)) {
                              $reqName = t("unknown_user") . " '" . $rec["required"] . "'";
                            }
                            else {
                              $reqName = htmlspecialchars($required->getFullName() . " (" . $required->getLogin() . ")");
                            }
                            break;
                          case 1: // Approver is a group.
                            $required = $dms->getGroup($rec["required"]);
                            if (!is_object($required)) {
                              $reqName = t("unknown_group") . " '" . $rec["required"] . "'";
                            }
                            else {
                              $reqName = "<i>" . htmlspecialchars($required->getName()) . "</i>";
                            }
                            break;
                        }
                        echo $reqName;
                        echo "</td>";
                        echo "<td>";
                        echo '<i style="font-size: 80%;">' . $rec['date'] . " - ";
                        $updateuser = $dms->getUser($rec["userID"]);
                        if (!is_object($required)) {
                          echo getMLText("unknown_user");
                        }
                        else {
                          echo htmlspecialchars($updateuser->getFullName() . " (" . $updateuser->getLogin() . ")");
                        }
                        echo "</i>";
                        if ($rec['comment']) {
                          echo "<br />" . htmlspecialchars($rec['comment']);
                        }
                        echo "</td>";
                        echo "<td>";
                        echo getApprovalStatusText($rec["status"]);
                        echo "</td>";
                        echo "</tr>";
                      }
                      ?>
                    </table>
                  </div>
                  <?php
                  }
                  ?>
                </div>
              </div>
              <?php
            }
          }
          else {
            // show workflow pane
            if ($latestContent->getWorkflow()) {
              $workflow = $latestContent->getWorkflow();
              $workflowstate = $latestContent->getWorkflowState();
              $transitions = $workflow->getNextTransitions($workflowstate);
              ?>
              <div class="tab-pane" id="workflow">
                <?php
                $this->contentContainerStart();
                if ($user->isAdmin()) {
                  if (DMS::checkIfEqual($workflow->getInitState(), $latestContent->getWorkflowState())) {
                    print '<form action="../out/out.RemoveWorkflowFromDocument.php" method="post">' . createHiddenFieldWithKey('removeworkflowfromdocument') . "<input type=\"hidden\" name=\"documentid\" value=\"" . $documentid . "\" /><input type=\"hidden\" name=\"version\" value=\"" . $latestContent->getVersion() . "\" /><button type=\"submit\" class=\"btn\"><span class=\"fa fa-remove\"></span> " . getMLText('rm_workflow') . "</button></form>";
                  }
                  else {
                    print '<form action="../out/out.RewindWorkflow.php" method="post">' . createHiddenFieldWithKey('rewindworkflow') . "<input type=\"hidden\" name=\"documentid\" value=\"" . $documentid . "\" /><input type=\"hidden\" name=\"version\" value=\"" . $latestContent->getVersion() . "\" /><button type=\"submit\" class=\"btn\"><span class=\"fa fa-refresh\"></span>" . getMLText('rewind_workflow') . "</button></form>";
                  }
                }

                echo '<div class="well" style="padding: 3px 8px;">';
                echo "<h4>" . $workflow->getName() . "</h4>";
                echo "</div>";

                if ($parentworkflow = $latestContent->getParentWorkflow()) {
                  echo "<p>Sub workflow of '" . $parentworkflow->getName() . "'</p>";
                }
                echo '<div class="row-fluid">';
                echo '<div class="span10">';
                echo '<table class="table table-condensed">';
                echo "<tr>";
                echo "<td><h5>" . t('current_state') . ":</h5></td>";
                echo '<td colspan="' . sizeof($transitions) . '"><h5>' . $workflowstate->getName() . "</h5></td>";
                echo "</tr>";
                echo "<tr>";
                echo "<td><i>" . t('action') . "</i> &#x2192 " . t('next_state') . ":</td>";
                foreach ($transitions as $transition) {
                  $action = $transition->getAction();
                  echo "<td><i>" . $action->getName() . "</i> &#x2192 " . $transition->getNextState()->getName() . "</td>";
                }
                echo "</tr>";
                echo "<tr>";
                echo "<td>" . t('users') . ":</td>";
                foreach ($transitions as $transition) {
                  $transusers = $transition->getUsers();
                  echo "<td>";
                  foreach ($transusers as $transuser) {
                    $u = $transuser->getUser();
                    echo $u->getFullName();
                    if ($document->getAccessMode($u) < AccessMode::READ) {
                      echo " (" . t('access_mode_none') . ")";
                    }
                    echo "<br />";
                  }
                  echo "</td>";
                }
                echo "</tr>";
                echo "<tr>";
                echo "<td>" . t('groups') . ":</td>";
                foreach ($transitions as $transition) {
                  $transgroups = $transition->getGroups();
                  echo "<td>";
                  foreach ($transgroups as $transgroup) {
                    $g = $transgroup->getGroup();
                    echo s(t('at_least_n_users_of_group'), ["number_of_users" => $transgroup->getNumOfUsers(), "group" => "«" . $g->getName() . "»"]);
                    if ($document->getGroupAccessMode($g) < AccessMode::READ) {
                      echo " (" . t('access_mode_none') . ")";
                    }
                    echo "<br />";
                  }
                  echo "</td>";
                }
                echo "</tr>";
                echo '<tr class="success">';
                echo "<td>" . t('users_done_work') . ":</td>";
                foreach ($transitions as $transition) {
                  echo "<td>";
                  if ($latestContent->executeWorkflowTransitionIsAllowed($transition)) {
                    echo "[" . t('done') . "] ";
                  }
                  $wkflogs = $latestContent->getWorkflowLog($transition);
                  foreach ($wkflogs as $wkflog) {
                    $loguser = $wkflog->getUser();
                    echo $loguser->getFullName() . " (" . t('group') . " ";
                    $names = array();
                    foreach ($loguser->getGroups() as $loggroup) {
                      $names[] = "«" . $loggroup->getName() . "»";
                    }
                    echo implode(", ", $names);
                    echo "), ";
                    echo $wkflog->getDate();
                  }
                  echo "</td>";
                }
                echo "</tr>";
                echo "<tr>";
                echo "<td><h5>" . t('decision') . ": </h5></td>";
                foreach ($transitions as $transition) {
                  echo "<td>";
                  if ($latestContent->triggerWorkflowTransitionIsAllowed($user, $transition)) {
                    $action = $transition->getAction();
                    print '<form action="../out/out.TriggerWorkflow.php" method="post">' . createHiddenFieldWithKey('triggerworkflow') . '<input type="hidden" name="documentid" value="' . $documentid . '" /><input type="hidden" name="version" value="' . $latestContent->getVersion() . '" /><input type="hidden" name="transition" value="' . $transition->getID() . '" /><input type="submit" class="btn" value="' . $action->getName() . '" /></form>';
                  }
                  echo "</td>";
                }
                echo "</tr>";
                echo "</table>";

                $workflows = $dms->getAllWorkflows();
                if ($workflows) {
                  $subworkflows = array();
                  foreach ($workflows as $wkf) {
                    if ($wkf->getInitState()->getID() == $workflowstate->getID()) {
                      if ($workflow->getID() != $wkf->getID()) {
                        $subworkflows[] = $wkf;
                      }
                    }
                  }

                }
                /* If in a sub workflow, the check if return the parent workflow
                 * is possible.
                 */
                if ($parentworkflow = $latestContent->getParentWorkflow()) {
                  $states = $parentworkflow->getStates();
                  foreach ($states as $state) {
                    /* Check if the current workflow state is also a state in the
                     * parent workflow
                     */
                    if ($latestContent->getWorkflowState()->getID() == $state->getID()) {
                      echo "Switching from sub workflow '" . $workflow->getName() . "' into state " . $state->getName() . " of parent workflow '" . $parentworkflow->getName() . "' is possible<br />";
                      /* Check if the transition from the state where the sub workflow
                       * starts into the current state is also allowed in the parent
                       * workflow. Checking at this point is actually too late, because
                       * the sub workflow shouldn't be entered in the first place,
                       * but that is difficult to check.
                       */
                      /* If the init state has not been left, return is always possible */
                      if ($workflow->getInitState()->getID() == $latestContent->getWorkflowState()->getID()) {
                        echo "Initial state of sub workflow has not been left. Return to parent workflow is possible<br />";
                        echo '<form action="../out/out.ReturnFromSubWorkflow.php" method="post">' . createHiddenFieldWithKey('returnfromsubworkflow') . "<input type=\"hidden\" name=\"documentid\" value=\"" . $documentid . "\" /><input type=\"hidden\" name=\"version\" value=\"" . $latestContent->getVersion() . "\" />";
                        echo '<input type="submit" class="btn" value="' . t('return_from_subworkflow') . '" />';
                        echo "</form>";
                      }
                      else {
                        /* Get a transition from the last state in the parent workflow
                         * (which is the initial state of the sub workflow) into
                         * current state.
                         */
                        echo "Check for transition from " . $workflow->getInitState()->getName() . " into " . $latestContent->getWorkflowState()->getName() . " is possible in parentworkflow " . $parentworkflow->getID() . "<br />";
                        $transitions = $parentworkflow->getTransitionsByStates($workflow->getInitState(),
                          $latestContent->getWorkflowState());
                        if ($transitions) {
                          echo "Found transitions in workflow " . $parentworkflow->getID() . "<br />";
                          foreach ($transitions as $transition) {
                            if ($latestContent->triggerWorkflowTransitionIsAllowed($user, $transition)) {
                              echo "Triggering transition is allowed<br />";
                              echo '<form action="../out/out.ReturnFromSubWorkflow.php" method="post">' . createHiddenFieldWithKey('returnfromsubworkflow') . "<input type=\"hidden\" name=\"documentid\" value=\"" . $documentid . "\" /><input type=\"hidden\" name=\"version\" value=\"" . $latestContent->getVersion() . "\" /><input type=\"hidden\" name=\"transition\" value=\"" . $transition->getID() . "\" />";
                              echo '<input type="submit" class="btn" value="' . getMLText('return_from_subworkflow') . "\" />";
                              echo "</form>";

                            }
                          }
                        }
                      }
                    }
                  }
                }
                echo "</div>";
                echo "</div>";
                $this->contentContainerEnd();
                ?>
              </div>
              <?php
            }
          }
          // show pane with previous versions
          if (count($versions) > 1) {
            ?>
            <div class="tab-pane" id="previous">
              <?php
              $this->contentContainerStart();

              print '<table class="table">';
              print "<thead><tr>";
              print "<th width='10%'></th>";
              print "<th width='30%'>" . t("file") . "</th>";
              print "<th width='25%'>" . t("comment") . "</th>";
              print "<th width='15%'>" . t("status") . "</th>";
              print "<th width='20%'></th>";
              print "</tr></thead><tbody>";

              // list each version
              for ($i = count($versions) - 2; $i >= 0; $i--) {
                /** @var DocumentContent $version */
                $version = $versions[$i];
                $vstat = $version->getStatus();

                // verify if file exists
                $file_exists = file_exists($dms->contentDir . $version->getPath());
                print "<tr>\n";
                print "<td nowrap>";
                if ($viewonlinefiletypes && in_array(strtolower($version->getFileType()), $viewonlinefiletypes)) {
                  print '<a target="_blank" href="../op/op.ViewOnline.php?documentid=' . $documentid . "&version=" . $version->getVersion() . '">';
                }
                else {
                  print '<a href="../op/op.Download.php?documentid=' . $documentid . "&version=" . $version->getVersion() . '">';
                }
                $previewer->createPreview($version);
                if ($previewer->hasPreview($version)) {
                  print('<img class="mimeicon" width="' . $previewwidthdetail . '" src="../op/op.Preview.php?documentid=' . $document->getID() . "&version=" . $version->getVersion() . "&width=" . $previewwidthdetail . "\" title=\"" . htmlspecialchars($version->getMimeType()) . "\">");
                }
                else {
                  print '<img class="mimeicon" src="' . $this->getMimeIcon($version->getFileType()) . '" title="' . htmlspecialchars($version->getMimeType()) . '">';
                }
                print "</a>";
                print "</td>";
                print '<td><ul class="unstyled">';
                print "<li>" . $version->getOriginalFileName() . "</li>";
                print "<li><strong>" . t('version') . ": " . $version->getVersion() . "</strong></li>\n";
                if ($file_exists) {
                  print "<li>" . File::format_filesize($version->getFileSize()) . ", " . htmlspecialchars($version->getMimeType()) . "</li>";
                }
                else {
                  print "<li><span class=\"warning\">" . getMLText("document_deleted") . "</span></li>";
                }
                $updatingUser = $version->getUser();
                print "<li>" . t("uploaded_by") . " <a href=\"mailto:" . $updatingUser->getEmail() . "\">" . htmlspecialchars($updatingUser->getFullName()) . "</a></li>";
                print "<li>" . getLongReadableDate($version->getDate()) . "</li>";
                print "</ul>\n";
                print '<ul class="documentDetail">\n';
                $attributes = $version->getAttributes();
                if ($attributes) {
                  foreach ($attributes as $attribute) {
                    $attrdef = $attribute->getAttributeDefinition();
                    print "<li>" . htmlspecialchars($attrdef->getName()) . ": " . htmlspecialchars($attribute->getValue()) . "</li>\n";
                  }
                }
                print "</ul>\n";
                print "<td>" . htmlspecialchars($version->getComment()) . "</td>";
                print "<td>" . getOverallStatusText($vstat["status"]) . "</td>";
                print "<td>";
                print '<ul class="actions unstyled">';
                if ($file_exists) {
                  print '<li><a href="../op/op.Download.php?documentid=' . $documentid . "&version=" . $version->getVersion() . '"><span class="fa fa-download"></span> ' . t("download") . "</a>";
                  if ($viewonlinefiletypes && in_array(strtolower($version->getFileType()), $viewonlinefiletypes)) {
                    print '<li><a target="_blank" href="../op/op.ViewOnline.php?documentid=' . $documentid . "&version=" . $version->getVersion() . '"><span class="fa fa-star"></span> ' . t("view_online") . "</a>";
                  }
                }
                print "</ul>";
                print "<ul class=\"actions unstyled\">";
                /* Only admin has the right to remove version in any case or a regular
                 * user if enableVersionDeletion is on
                 */
                if ($accessop->mayRemoveVersion()) {
                  print '<li><a href="out.RemoveVersion.php?documentid=' . $documentid . "&version=" . $version->getVersion() . '"><span class="fa fa-remove"></span> ' . t("rm_version") . "</a></li>";
                }
                if ($accessop->mayEditComment()) {
                  print '<li><a href="out.EditComment.php?documentid=' . $document->getID() . "&version=" . $version->getVersion() . '"><span class="fa fa-comment"></span> ' . t("edit_comment") . "</a></li>";
                }
                if ($accessop->mayEditAttributes()) {
                  print '<li><a href="out.EditAttributes.php?documentid=' . $document->getID() . "&version=" . $latestContent->getVersion() . '"><span class="fa fa-edit"></span> ' . t("edit_attributes") . "</a></li>";
                }
                print "</ul>";
                print "</td></tr>";


                // append latest status of previous workflows to the version
                $actualWorkflowLogs = $version->getWorkflowLog(); // gets all workflow information of the version

                // skip if there are no workflows.
                if (!empty($actualWorkflowLogs)) {
                  /** @var \BakeryDMS\Core\Workflow\Log $lastWorkflowLog */
                  $lastWorkflowLog = $actualWorkflowLogs[0]; // the first (latest!) workflow element

                  echo "<tr><td colspan='5'>";
                  echo "<h5>" . t("workflow_last_status_comment") . "</h5>";
                  echo '<table class="table table-condensed">';
                  echo "<thead><tr><th>" . t("date") . "</th><th>" . t("status") . "</th><th>" . t("user") . "</th><th colspan='2'>" . t("comment") . "</th></tr></thead>";
                  echo "<tbody>";
                  echo "<tr>";
                  echo "<td>" . $lastWorkflowLog->getDate() . "</td>";
                  echo "<td>" . $lastWorkflowLog->getTransition()->getNextState()->getName() . "</td>";
                  echo "<td>" . $lastWorkflowLog->getUser()->getFullName() . "</td>";
                  echo "<td>" . $lastWorkflowLog->getComment() . "</td>";
                  echo "</tr>";
                  echo "</tbody>";
                  echo "</table>";
                  echo "</td></tr>";
                }
              }
              echo "</tbody>\n</table>\n";

              $this->contentContainerEnd();

              ?>
            </div>
            <?php
          }
          ?>
          <div class="tab-pane" id="attachments">
            <?php

            $this->contentContainerStart();

            if (count($files) > 0) {

              echo '<table class="table">';
              echo "<thead><tr>";
              echo "<th width='20%'></th>";
              echo "<th width='20%'>" . t("file") . "</th>";
              echo "<th width='40%'>" . t("comment") . "</th>";
              echo "<th width='20%'></th>";
              echo "</tr></thead><tbody>";

              foreach ($files as $file) {

                $file_exists = file_exists($dms->contentDir . $file->getPath());

                $responsibleUser = $file->getUser();

                print "<tr>";
                print "<td>";
                $previewer->createPreview($file, $previewwidthdetail);
                if ($viewonlinefiletypes && in_array(strtolower($file->getFileType()), $viewonlinefiletypes)) {
                  print '<a target="_blank" href="../op/op.ViewOnline.php?documentid=' . $documentid . "&file=" . $file->getID() . "\">";
                }
                else {
                  print '<a href="../op/op.Download.php?documentid=' . $documentid . "&file=" . $file->getID() . "\">";
                }
                if ($previewer->hasPreview($file)) {
                  print('<img class="mimeicon" width="' . $previewwidthdetail . '" src="../op/op.Preview.php?documentid=' . $document->getID() . "&file=" . $file->getID() . "&width=" . $previewwidthdetail . '" title="' . htmlspecialchars($file->getMimeType()) . '">');
                }
                else {
                  print '<img class="mimeicon" src="' . $this->getMimeIcon($file->getFileType()) . '" title="' . htmlspecialchars($file->getMimeType()) . "\">";
                }
                print "</a>";
                print "</td>";

                print '<td><ul class="unstyled">';
                print "<li>" . htmlspecialchars($file->getName()) . "</li>\n";
                print "<li>" . htmlspecialchars($file->getOriginalFileName()) . "</li>\n";
                if ($file_exists) {
                  print "<li>" . File::format_filesize(filesize($dms->contentDir . $file->getPath())) . " bytes, " . htmlspecialchars($file->getMimeType()) . "</li>";
                }
                else {
                  print "<li>" . htmlspecialchars($file->getMimeType()) . ' - <span class="warning">' . t("document_deleted") . "</span></li>";
                }

                print "<li>" . t("uploaded_by") . ' <a href="mailto:' . $responsibleUser->getEmail() . '">' . htmlspecialchars($responsibleUser->getFullName()) . "</a></li>";
                print "<li>" . getLongReadableDate($file->getDate()) . "</li>";

                print "<td>" . htmlspecialchars($file->getComment()) . "</td>";

                print '<td><ul class="unstyled actions">';
                if ($file_exists) {
                  print '<li><a href="../op/op.Download.php?documentid=' . $documentid . "&file=" . $file->getID() . '"><span class="fa fa-download"></span> ' . t('download') . "</a>";
                  if ($viewonlinefiletypes && in_array(strtolower($file->getFileType()), $viewonlinefiletypes)) {
                    print '<li><a target="_blank" href="../op/op.ViewOnline.php?documentid=' . $documentid . "&file=" . $file->getID() . '"><span class="fa fa-star"></span> ' . t("view_online") . "</a></li>";
                  }
                }
                else {
                  print '<li><img class="mimeicon" src="images/icons/' . $this->getMimeIcon($file->getFileType()) . "\" title=\"" . htmlspecialchars($file->getMimeType()) . "\">";
                }
                echo '</ul><ul class="unstyled actions">';
                if (($document->getAccessMode($user) == AccessMode::ALL) || ($file->getUserID() == $user->getID())) {
                  print '<li><a href="out.RemoveDocumentFile.php?documentid=' . $documentid . "&fileid=" . $file->getID() . '"><span class="fa fa-remove"></span> ' . t("delete") . "</a></li>";
                }
                print "</ul></td>";

                print "</tr>";
              }
              print "</tbody></table>";

            }
            else {
              echo t("no_attached_files");
            }

            if ($document->getAccessMode($user) >= AccessMode::READWRITE) {
              print '<ul class="unstyled"><li><a href="../out/out.AddFile.php?documentid=' . $documentid . '" class="btn">' . t("add") . "</a></ul>\n";
            }
            $this->contentContainerEnd();
            ?>
          </div>
          <div class="tab-pane" id="links">
            <?php
            $this->contentContainerStart();
            if (count($links) > 0) {

              print '<table class="table table-condensed">';
              print '<thead><tr>';
              print '<th></th>';
              print '<th></th>';
              print "<th>" . t("comment") . "</th>";
              print "<th></th>";
              print "<th></th>";
              print "</tr></thead><tbody>";

              /** @var DocumentLink $link */
              foreach ($links as $link) {
                /** @var User $responsibleUser */
                $responsibleUser = $link->getUser();
                $targetDoc = $link->getTarget();
                $targetlc = $targetDoc->getLatestContent();

                $previewer->createPreview($targetlc, $previewwidthlist);
                print "<tr>";
                print '<td><a href="../op/op.Download.php?documentid=' . $targetDoc->getID() . "&version=" . $targetlc->getVersion() . "\">";
                if ($previewer->hasPreview($targetlc)) {
                  print '<img class="mimeicon" width="' . $previewwidthlist . '"src="../op/op.Preview.php?documentid=' . $targetDoc->getID() . "&version=" . $targetlc->getVersion() . "&width=" . $previewwidthlist . '" title="' . htmlspecialchars($targetlc->getMimeType()) . '">';
                }
                else {
                  print '<img class="mimeicon" src="' . $this->getMimeIcon($targetlc->getFileType()) . '" title="' . htmlspecialchars($targetlc->getMimeType()) . "\">";
                }
                print "</td>";
                print '<td><a href="out.ViewDocument.php?documentid=' . $targetDoc->getID() . "\" class=\"linklist\">" . htmlspecialchars($targetDoc->getName()) . "</a></td>";
                print "<td>" . htmlspecialchars($targetDoc->getComment()) . "</td>";
                print "<td>" . t("document_link_by") . " " . htmlspecialchars($responsibleUser->getFullName());
                if (($user->getID() == $responsibleUser->getID()) || ($document->getAccessMode($user) == AccessMode::ALL)) {
                  print "<br />" . t("document_link_public") . ": " . (($link->isPublic()) ? t("yes") : t("no"));
                }
                print "</td>";
                print '<td><span class="actions">';
                if (($user->getID() == $responsibleUser->getID()) || ($document->getAccessMode($user) == AccessMode::ALL)) {
                  print '<form action="../op/op.RemoveDocumentLink.php" method="post">' . createHiddenFieldWithKey('removedocumentlink') . '<input type="hidden" name="documentid" value="' . $documentid . '" /><input type="hidden" name="linkid" value="' . $link->getID() . '" /><button type="submit" class="btn btn-mini"><span class="fa fa-remove"></span> ' . t("delete") . "</button></form>";
                }
                print "</span></td>";
                print "</tr>";
              }
              print "</tbody></table>";
            }
            else {
              echo t("no_linked_files");
            }

            if (!$user->isGuest()) {
              ?>
              <br>
              <form action="../op/op.AddDocumentLink.php" name="form1">
                <input type="hidden" name="documentid" value="<?php print $documentid; ?>">
                <table class="table-condensed">
                  <tr>
                    <td><?php echo t("add_document_link"); ?>:</td>
                    <td><?php $this->printDocumentChooser("form1"); ?></td>
                  </tr>
                  <?php
                  if ($document->getAccessMode($user) >= AccessMode::READWRITE) {
                    print "<tr><td>" . t("document_link_public") . "</td>";
                    print "<td>";
                    print '<input type="checkbox" name="public" value="true" checked />';
                    print "</td></tr>";
                  }
                  ?>
                  <tr>
                    <td></td>
                    <td>
                      <button type="submit" class="btn"><span class="fa fa-save"></span> <?php echo t("save") ?>
                      </button>
                    </td>
                  </tr>
                </table>
              </form>
              <?php
            }
            $this->contentContainerEnd();

            if (count($reverselinks) > 0) {
              $this->contentHeading(t("reverse_links"));
              $this->contentContainerStart();

              print '<table class="table table-condensed">';
              print "<thead><tr>";
              print "<th></th>";
              print "<th></th>";
              print "<th>" . t("comment") . "</th>";
              print "<th></th>";
              print "<th></th>";
              print "</tr></thead><tbody>";

              foreach ($reverselinks as $link) {
                $responsibleUser = $link->getUser();
                $sourceDoc = $link->getDocument();
                $sourcelc = $sourceDoc->getLatestContent();

                $previewer->createPreview($sourcelc, $previewwidthlist);
                print "<tr>";
                print '<td><a href="../op/op.Download.php?documentid=' . $sourceDoc->getID() . "&version=" . $sourcelc->getVersion() . '">';
                if ($previewer->hasPreview($sourcelc)) {
                  print '<img class="mimeicon" width="' . $previewwidthlist . '"src="../op/op.Preview.php?documentid=' . $sourceDoc->getID() . "&version=" . $sourcelc->getVersion() . "&width=" . $previewwidthlist . '" title="' . htmlspecialchars($sourcelc->getMimeType()) . '">';
                }
                else {
                  print '<img class="mimeicon" src="' . $this->getMimeIcon($sourcelc->getFileType()) . "\" title=\"" . htmlspecialchars($sourcelc->getMimeType()) . '">';
                }
                print "</td>";
                print '<td><a href="out.ViewDocument.php?documentid=' . $sourceDoc->getID() . '" class="linklist">' . htmlspecialchars($sourceDoc->getName()) . "</a></td>";
                print "<td>" . htmlspecialchars($sourceDoc->getComment()) . "</td>";
                print "<td>" . t("document_link_by") . " " . htmlspecialchars($responsibleUser->getFullName());
                if (($user->getID() == $responsibleUser->getID()) || ($document->getAccessMode($user) == AccessMode::ALL)) {
                  print "<br />" . t("document_link_public") . ": " . (($link->isPublic()) ? t("yes") : t("no"));
                }
                print "</td>";
                print '<td><span class="actions">';
                if (($user->getID() == $responsibleUser->getID()) || ($document->getAccessMode($user) == AccessMode::ALL)) {
                  print '<form action="../op/op.RemoveDocumentLink.php" method="post">' . createHiddenFieldWithKey('removedocumentlink') . '<input type="hidden" name="documentid" value="' . $documentid . '" /><input type="hidden" name="linkid" value="' . $link->getID() . '" /><button type="submit" class="btn btn-mini"><span class="fa fa-remove"></span> ' . t("delete") . "</button></form>";
                }
                print "</span></td>";
                print "</tr>";
              }
              print "</tbody></table>";
              $this->contentContainerEnd();
            }
            ?>
          </div>
        </div>
      </div>
    </div>
    <?php
    $this->htmlEndPage();

  }

  /**
   * @param string $workflowMode
   * @param DocumentContent $latestContent
   * @param User $user
   *
   * @return bool <c>true</c> if action needed, <c>false</c> otherwise
   */
  private function needsWorkflowActionOfUser($workflowMode, $latestContent, $user) {
    if ($workflowMode == 'traditional') {
      return false;
    }

    // there's no action required if no workflow is attach
    $workflow = $latestContent->getWorkflow();
    if (!$workflow) {
      return false;
    }

    return $latestContent->needsWorkflowAction($user);
  }

  /**
   * @param DocumentContent $latestContent
   *
   * @return string[]
   */
  private function getNextStatesNames($latestContent) {
    $transitions = $latestContent->getWorkflow()->getNextTransitions($latestContent->getWorkflowState());
    return array_map(
      function (Transition $t) {
        return $t->getNextState()->getName();
      },
      $transitions);
  }

  /**
   * @param string $workflowMode
   * @param DocumentContent $latestContent
   * @param User $user
   */
  private function printWorkflowInfo($workflowMode, $latestContent, $user) {
    if ($this->needsWorkflowActionOfUser($workflowMode, $latestContent, $user)) {
      $nextStates = $this->getNextStatesNames($latestContent);
      if (count($nextStates) > 1) {
        $this->errorMsg(s(t('needs_workflow_action_multiple'),
          ["possible_states" => implode(", ", array_slice($nextStates, 1)), "possible_state" => $nextStates[0]]),
          "fa fa-exclamation-triangle");
      }
      else {
        $this->errorMsg(s(t('needs_workflow_action_one'), ["possible_state" => $nextStates[0]]),
          "fa fa-exclamation-triangle");
      }
    }
  }

  /**
   * @param Document $document
   */
  private function printLockInfo($document) {
    if ($document->isLocked()) {
      $lockingUser = $document->getLockingUser();
      echo '<div class="alert alert-warning">';
      echo s(t("lock_message"),
        ["email" => $lockingUser->getEmail(), "username" => htmlspecialchars($lockingUser->getFullName())]);
      echo "</div>";
    }
  }
}

?>
