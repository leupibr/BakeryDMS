<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require_once('BakeryDMS/Core.php');
include("../inc/inc.Version.php");
include("../inc/inc.Language.php");
include("../inc/inc.ClassUI.php");

if (!$user->isAdmin()) {
  UI::exitError(getMLText("admin_tools"), getMLText("access_denied"));
}

$v = new SeedDMS_Version();

// TODO: allow to configure repository and channel.
$repository = "http://repository.softwarebakery.ch/BakeryDMS";
$channel = "master";

$versions = array();
if (@ini_get('allow_url_fopen') == '1') {
  $lines = json_decode(file_get_contents("$repository/$channel"));
  if ($lines) {
    foreach ($lines as $line) {
      $version = array_values(array_slice(explode('/', $line), -1, 1, true))[0];
      $versions[] = preg_split('/(\.|-)/', substr(substr($version, 0, -4), 1));
    }
  }
}

$tmp = explode('.', basename($_SERVER['SCRIPT_FILENAME']));
$view = UI::factory($theme,
  $tmp[1],
  array(
    'dms' => $dms,
    'user' => $user,
    'version' => $v,
    'availversions' => $versions,
    'changeLog' => '../CHANGELOG.md'));

if ($view) {
  $view->show();
  exit;
}
