<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require_once('BakeryDMS/Core.php');
include("../inc/inc.Language.php");
include("../inc/inc.ClassUI.php");

if ($user->isGuest()) {
  UI::exitError(getMLText("my_account"), getMLText("access_denied"));
}

if (!$settings->_enableUsersView) {
  UI::exitError(getMLText("my_account"), getMLText("access_denied"));
}

$allUsers = $dms->getAllUsers();
if (is_bool($allUsers)) {
  UI::exitError(getMLText("my_account"), getMLText("internal_error"));
}

$allGroups = $dms->getAllGroups();
if (is_bool($allGroups)) {
  UI::exitError(getMLText("admin_tools"), getMLText("internal_error"));
}

$tmp = explode('.', basename($_SERVER['SCRIPT_FILENAME']));
$view = UI::factory($theme,
  $tmp[1],
  array('dms' => $dms, 'user' => $user, 'allusers' => $allUsers, 'allgroups' => $allGroups));
if ($view) {
  $view->show();
  exit;
}

?>
