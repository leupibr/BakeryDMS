<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010-2012 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use BakeryDMS\Core\AccessMode;

require_once('BakeryDMS/Core.php');
include("../inc/inc.Language.php");
include("../inc/inc.ClassUI.php");
include("../inc/inc.ClassAccessOperation.php");

$tmp = explode('.', basename($_SERVER['SCRIPT_FILENAME']));
$view = UI::factory($theme, $tmp[1]);
if (!$view) {
}

if (!isset($_GET["documentid"]) || !is_numeric($_GET["documentid"]) || intval($_GET["documentid"]) < 1) {
  $view->exitError(s(t("document_title"), ["documentname" => t("invalid_doc_id")]), t("invalid_doc_id"));
}

$document = $dms->getDocument($_GET["documentid"]);
if (!is_object($document)) {
  $view->exitError(s(t("document_title"), ["documentname" => t("invalid_doc_id")]), t("invalid_doc_id"));
}

/* Create object for checking access to certain operations */
$accessop = new SeedDMS_AccessOperation($document, $user, $settings);

$folder = $document->getFolder();

if ($document->getAccessMode($user) < AccessMode::READ) {
  $view->exitError(s(t("document_title"), ["documentname" => htmlspecialchars($document->getName())]), t("access_denied"));
}

/* Recalculate the status of a document and reload the page if the status
 * has changed. A status change may occur if the document has expired in
 * the mean time
 */
if ($document->verifyLastestContentExpriry()) {
  header("Location:../out/out.ViewDocument.php?documentid=" . $document->getID());
}

if ($view) {
  $view->setParam('dms', $dms);
  $view->setParam('user', $user);
  $view->setParam('folder', $folder);
  $view->setParam('document', $document);
  $view->setParam('accessobject', $accessop);
  $view->setParam('viewonlinefiletypes', $settings->_viewOnlineFileTypes);
  $view->setParam('enableownerrevapp', $settings->_enableOwnerRevApp);
  $view->setParam('cachedir', $settings->_cacheDir);
  $view->setParam('workflowmode', $settings->_workflowMode);
  $view->setParam('previewWidthList', $settings->_previewWidthList);
  $view->setParam('previewWidthDetail', $settings->_previewWidthDetail);
  $view->show();
  exit;
}

?>
