<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010-2012 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use BakeryDMS\Core\User;

require_once('BakeryDMS/Core.php');
include("../inc/inc.Language.php");
include("../inc/inc.ClassUI.php");

if (!$user->isAdmin()) {
  UI::exitError(t("admin_tools"), t("access_denied"));
}

if (!isset($_GET["userid"]) && !isset($_GET["groupid"])) {
  UI::exitError(t("rm_user"), t("invalid_group_or_user_id"));
}

$mode = isset($_GET["userid"]) ? "single" : "multiple";

if($mode == "single" && (!is_numeric($_GET["userid"]) || intval($_GET["userid"]) < 1)) {
  UI::exitError(t("rm_user"), t("invalid_user_id"));
}
elseif($mode == "single" && (!is_numeric($_GET["userid"]) || intval($_GET["userid"]) < 1)) {
  UI::exitError(t("rm_user"), t("invalid_group_id"));
}

$userToChoose = $dms->getAllUsers($settings->_sortUsersInList);
$group = null;
$rmuser = null;

if($mode == "single") {
  $rmuser = $dms->getUser(intval($_GET["userid"]));
  if (!is_object($rmuser)) {
    UI::exitError(t("rm_user"), t("invalid_user_id"));
  }

  if (in_array($rmuser->getID(), explode(',', $settings->_undelUserIds))) {
    UI::exitError(t("rm_user"), t("cannot_delete_user"));
  }

  if ($rmuser->getID() == $user->getID()) {
    UI::exitError(t("rm_user"), t("cannot_delete_yourself"));
  }

  $userToChoose = array_filter($userToChoose, function($o) use(&$rmuser) { return $o->getID() != $rmuser->getID(); });
}
else {
  $group = $dms->getGroup(intval($_GET["groupid"]));
  if (!is_object($group)) {
    UI::exitError(t("rm_user"), t("invalid_group_id"));
  }
  $group->setDMS($dms);

  $userToChoose = array_filter($userToChoose, function($o) use(&$group) { return !$group->isMember($o); });
}

/** @var User[] $userToChoose */
$userToChoose = array_filter($userToChoose, function($o) { return !$o->isGuest(); });

$tmp = explode('.', basename($_SERVER['SCRIPT_FILENAME']));
$view = UI::factory($theme, $tmp[1], array('dms' => $dms, 'user' => $user, 'rmuser' => $rmuser, 'group' => $group, 'allusers' => $userToChoose, 'mode' => $mode));
if ($view) {
  $view->show();
  die();
}
