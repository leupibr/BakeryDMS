<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010-2011 Matteo Lucarelli
// Copyright (C) 2011-2013 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use BakeryDMS\Lucene\Indexer;

require_once('BakeryDMS/Core.php');
include("../inc/inc.Version.php");
include("../inc/inc.Language.php");
include("../inc/inc.ClassUI.php");

if (!$user->isAdmin()) {
  UI::exitError(getMLText("admin_tools"), getMLText("access_denied"));
}

if (!$settings->_enableFullSearch) {
  UI::exitError(getMLText("admin_tools"), getMLText("fulltextsearch_disabled"));
}

if (isset($_GET['create']) && $_GET['create'] == 1) {
  if (isset($_GET['confirm']) && $_GET['confirm'] == 1) {
    $index = Indexer::create($settings->_luceneDir);
    Indexer::init($settings->_stopWordsFile);
  } else {
    header('Location: out.CreateIndex.php');
    exit;
  }
} else {
  $index = Indexer::open($settings->_luceneDir);
  if (!$index) {
    UI::exitError(getMLText("admin_tools"), getMLText("no_fulltextindex"));
  }
  Indexer::init($settings->_stopWordsFile);
}

if (!isset($_GET["folderid"]) || !is_numeric($_GET["folderid"]) || intval($_GET["folderid"]) < 1) {
  $folderid = $settings->_rootFolderID;
} else {
  $folderid = intval($_GET["folderid"]);
}
$folder = $dms->getFolder($folderid);

$tmp = explode('.', basename($_SERVER['SCRIPT_FILENAME']));
$view = UI::factory($theme,
  $tmp[1],
  array('dms' => $dms,
    'user' => $user,
    'index' => $index,
    'recreate' => (isset($_GET['create']) && $_GET['create'] == 1),
    'folder' => $folder,
    'converters' => $settings->_converters));
if ($view) {
  $view->show();
  exit;
}

?>
