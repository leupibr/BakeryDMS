<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require_once("BakeryDMS/Core.php");

use BakeryDMS\Core\Attribute;
use BakeryDMS\Core\DMS;
use BakeryDMS\Core\Document;
use BakeryDMS\Core\DocumentContent;
use BakeryDMS\Core\Folder;
use BakeryDMS\Core\GroupAccess;
use BakeryDMS\Core\User;
use BakeryDMS\Settings;

function usage() {
  echo "Usage:\n";
  echo "  php -f copyfolder.php -- --source <folderid> --destination <folderid> [--owner <owner>] [--config <config>] [--name <foldername>]\n";
  echo "\n";
  echo "Description:\n";
  echo "  This program will clone a source folder into another folder.\n";
  echo "\n";
  echo "Options:\n";
  echo "  -h, --help: print usage information and exit.\n";
  echo "  -v, --version: print version and exit.\n";
  echo "  -c, --config: set alternative config file.\n";
  echo "  -s, --source: the id of the folder to clone.\n";
  echo "  -d, --destination: the id of the folder to clone the source into.\n";
  echo "  -o, --owner: the id of the owner of the new files and folders.\n";
  echo "  -n, --name: the name of the new folder.\n";
  echo "  -D, --documents: copy the documents too.\n";
  echo "  -P, --permissions: copy the permission of folder and files too.\n";
}

$version = "1.0";
$shortoptions = "hvc:s:d:o:n::D::P";
$longoptions = array('help',
  'version',
  'config:',
  'source::',
  'destination::',
  'owner:',
  'name:',
  'documents',
  'permissions');
if (false === ($options = getopt($shortoptions, $longoptions))) {
  usage();
  exit(0);
}

/* Print help and exit */
if (isset($options['h']) || isset($options['help'])) {
  usage();
  exit(0);
}

/* Print version and exit */
if (isset($options['v']) || isset($options['verѕion'])) {
  echo "copyfolder.php v$version\n";
  echo "Copyright (c) 2014-2015 Bruno Leupi, Tobias Maestrini\n";
  exit(0);
}

/** @var Settings $settings The settings */
if (isset($options['config'])) {
  $settings = Settings::getInstance($options['config']);
}
elseif (isset($options['c'])) {
  $settings = Settings::getInstance($options['c']);
}
else {
  $settings = Settings::getInstance();
}

if (isset($settings->_extraPath)) {
  ini_set('include_path', $settings->_extraPath . PATH_SEPARATOR . ini_get('include_path'));
}


$sourceid = 0;
if (isset($options['s'])) {
  $sourceid = $options['s'];
}
if (isset($options['source'])) {
  $sourceid = $options['source'];
}

$destinationid = 0;
if (isset($options['d'])) {
  $destinationid = $options['d'];
}
if (isset($options['destination'])) {
  $destinationid = $options['destination'];
}

$copyDocuments = false;
if (isset($options['D'])) {
  $copyDocuments = true;
}
if (isset($options['documents'])) {
  $copyDocuments = true;
}

$copyPermissions = false;
if (isset($options['P'])) {
  $copyPermissions = true;
}
if (isset($options['permissions'])) {
  $copyPermissions = true;
}

$db = \BakeryDMS\Core\Persistency\DatabaseAccessFactory::createDatabaseAccess(
  $settings->_dbDriver,
  $settings->_dbHostname,
  $settings->_dbUser,
  $settings->_dbPass,
  $settings->_dbDatabase);
$db->connect() or die ("Could not connect to db-server \"" . $settings->_dbHostname . "\"");

/* init dms */
$dms = new DMS($db, $settings->_contentDir . $settings->_contentOffsetDir);
if (!$dms->checkVersion()) {
  echo "Database update needed.";
  exit;
}
$dms->setRootFolderID($settings->_rootFolderID);
$dms->setMaxDirID($settings->_maxDirID);
$dms->setEnableConverting($settings->_enableConverting);
$dms->setViewOnlineFileTypes($settings->_viewOnlineFileTypes);

/** @var Folder $sourceFolder The source folder */
$sourceFolder = $dms->getFolder($sourceid);

/** @var Folder $destinationFolder The destination folder */
$destinationFolder = $dms->getFolder($destinationid);

/** @var User $owner The owner of the new files and folders */
$owner = $destinationFolder->getOwner();
if (isset($options['o'])) {
  $owner = $dms->getUser($options['o']);
}
if (isset($options['owner'])) {
  $owner = $dms->getUser($options['owner']);
}

/** @var string $name The name of the new folder */
$name = $sourceFolder->getName();
if (isset($options['n'])) {
  $name = $options['n'];
}
if (isset($options['name'])) {
  $name = $options['name'];
}

/**
 * Function for copying sub folders.
 *
 * @param $dms DMS The dms system.
 * @param $source Folder The source to copy.
 * @param $destination Folder The destination to copy the folder in.
 * @param $owner User The owner to set for the new files and folders.
 * @param $copyDocuments bool If set to true, the document will be copied too.
 * @param $copyPermission bool If set to true, the permissions will be copied too.
 */
function copySubFolders($dms, $source, $destination, $owner, $copyDocuments, $copyPermission) {
  $subFolders = $source->getSubFolders();
  $position = 0;

  /** @var Folder $subFolder */
  foreach ($subFolders as $subFolder) {
    $currentFolder = $destination->addSubFolder($subFolder->getName(),
      $subFolder->getComment(),
      $owner,
      $position++,
      readAttributeValues($subFolder));

    // copy the permission
    if ($copyPermission) {
      copyPermissions($subFolder, $currentFolder);
    }

    // copy the documents
    if ($copyDocuments) {
      copyDocuments($dms, $subFolder, $currentFolder, $owner, 0, $copyPermission);
    }

    // recursion anchor
    if ($subFolder->hasSubFolders()) {
      // recursion step
      copySubFolders($dms, $subFolder, $currentFolder, $owner, $copyDocuments, $copyPermission);
    }
  }
}

/**
 * Reads all attributes and their values of a given folder and packs them into a key value pair structure.
 *
 * @param Folder $folder The folder to copy the attributes from.
 *
 * @return array The key value structure for the attributes.
 */
function readAttributeValues($folder) {
  /** @var Attribute[] $attributes The attributes to read the values for. */
  $attributes = $folder->getAttributes();

  $result = [];
  foreach ($attributes as $attrdefid => $attribute) {
    $result[$attrdefid] = $attribute->getValue();
  }

  return $result;
}

/**
 * Function for copying documents.
 *
 * @param $dms DMS The dms system.
 * @param $source Folder The source to copy.
 * @param $destination Folder The destination to copy the folder in.
 * @param $owner User The owner to set for the new files and folders.
 * @param $sequence int The sequence, where to add the documents.
 * @param $copyPermissions boolean If <c>true</c> permissions are copied, otherwise not.
 */
function copyDocuments($dms, $source, $destination, $owner, $sequence = 0, $copyPermissions = false) {
  $sourceDocuments = $source->getDocuments();

  /** @var Document $document */
  foreach ($sourceDocuments as $document) {
    /** @var DocumentContent $content */
    $content = $document->getLatestContent();

    // add document
    $result = $destination->addDocument($document->getName(),
      $document->getComment(),
      $document->getExpires(),
      $owner,
      $document->getKeywords(),
      $document->getCategories(),
      $dms->contentDir . $content->getPath(),
      $content->getOriginalFileName(),
      $content->getFileType(),
      $content->getMimeType(),
      $sequence++,
      array(),
      array(),
      0,
      "",
      array(),
      array(),
      $workflow = $content->getWorkflow());

    /** @var Document $newDocument */
    if ($copyPermissions) {
      copyPermissions($document, $result[0]);
    }
  }
}

/**
 * Copies the permissions of a document or a folder to an other.
 *
 * @param $source Document|Folder The source document/folder to copy the restrictions from.
 * @param $target Document|Folder The target document/folder to copy the restrictions to.
 */
function copyPermissions($source, $target) {
  // ignore access overwriting if inherit access is active.
  if ($source->inheritsAccess()) {
    return;
  }

  // copy default access
  $target->setInheritAccess(false);
  $target->setDefaultAccess($source->getDefaultAccess());

  // get ACL from source folder
  $restrictions = $source->getAccessList();

  // copy ACL for groups
  /** @var GroupAccess $restriction */
  foreach ($restrictions["groups"] as $restriction) {
    $target->addAccess($restriction->getMode(), $restriction->getGroupID(), 0);
  }

  // copy ACL for users
  /** @var GroupAccess $restriction */
  foreach ($restrictions["users"] as $restriction) {
    $target->addAccess($restriction->getMode(), $restriction->getUserID(), 1);
  }
}


/* create new root folder */
$newFolder = $destinationFolder->addSubFolder($name,
  '',
  $owner,
  count($destinationFolder->getSubFolders()) + 1,
  readAttributeValues($sourceFolder));
if ($copyPermissions) {
  copyPermissions($sourceFolder, $newFolder);
}
copySubFolders($dms, $sourceFolder, $newFolder, $owner, $copyDocuments, $copyPermissions);
if ($copyDocuments) {
  copyDocuments($dms, $sourceFolder, $newFolder, $owner, $copyPermissions);
}

?>