<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require_once("BakeryDMS/Core.php");

use BakeryDMS\Core\DMS;
use BakeryDMS\Lucene\IndexedDocument;
use BakeryDMS\Lucene\Indexer;
use BakeryDMS\Settings;

function usage() {
  echo "Usage:\n";
  echo "  seeddms-indexer [-h] [-v] [--config <file>]\n";
  echo "\n";
  echo "Description:\n";
  echo "  This program recreates the full text index of SeedDMS.\n";
  echo "\n";
  echo "Options:\n";
  echo "  -h, --help: print usage information and exit.\n";
  echo "  -v, --version: print version and exit.\n";
  echo "  --config: set alternative config file.\n";
}

$version = "0.0.1";
$shortoptions = "hv";
$longoptions = array('help', 'version', 'config:');
if (false === ($options = getopt($shortoptions, $longoptions))) {
  usage();
  exit(0);
}

/* Print help and exit */
if (isset($options['h']) || isset($options['help'])) {
  usage();
  exit(0);
}

/* Print version and exit */
if (isset($options['v']) || isset($options['verѕion'])) {
  echo $version . "\n";
  exit(0);
}

/* Set alternative config file */
if (isset($options['config'])) {
  $settings = Settings::getInstance($options['config']);
}
else {
  $settings = Settings::getInstance();
}

if (isset($settings->_extraPath)) {
  ini_set('include_path', $settings->_extraPath . PATH_SEPARATOR . ini_get('include_path'));
}

function tree($folder, $indent = '') {
  global $index, $dms, $settings;
  echo $indent . "D " . $folder->getName() . "\n";
  $subfolders = $folder->getSubFolders();
  foreach ($subfolders as $subfolder) {
    tree($subfolder, $indent . '  ');
  }
  $documents = $folder->getDocuments();
  foreach ($documents as $document) {
    echo $indent . "  " . $document->getId() . ":" . $document->getName() . "\n";
    if (!($hits = $index->find('document_id:' . $document->getId()))) {
      $index->addDocument(new IndexedDocument($dms,
        $document,
        $settings->_converters ? $settings->_converters : null));
    }
    else {
      $hit = $hits[0];
      $created = (int) $hit->getDocument()->getFieldValue('created');
      if ($created >= $document->getDate()) {
        echo $indent . "    Document unchanged\n";
      }
      else {
        if ($index->delete($hit->id)) {
          $index->addDocument(new IndexedDocument($dms,
            $document,
            $settings->_converters ? $settings->_converters : null));
        }
      }
    }
  }
}

$db = \BakeryDMS\Core\Persistency\DatabaseAccessFactory::createDatabaseAccess(
  $settings->_dbDriver,
  $settings->_dbHostname,
  $settings->_dbUser,
  $settings->_dbPass,
  $settings->_dbDatabase);
$db->connect() or die ("Could not connect to db-server \"" . $settings->_dbHostname . "\"");

$dms = new DMS($db, $settings->_contentDir . $settings->_contentOffsetDir);
if (!$dms->checkVersion()) {
  echo "Database update needed.";
  exit;
}

$dms->setRootFolderID($settings->_rootFolderID);

$index = Zend_Search_Lucene::create($settings->_luceneDir);
Indexer::init($settings->_stopWordsFile);

$folder = $dms->getFolder($settings->_rootFolderID);
tree($folder);

$index->commit();
?>
