<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require_once('BakeryDMS/Core.php');
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.ClassUI.php");

if (!$user->isAdmin()) {
  UI::exitError(getMLText("admin_tools"), getMLText("access_denied"));
}

if (!isset($_GET["userid"])) {
  UI::exitError(getMLText("admin_tools"), getMLText("unknown_id"));
}

$session->setSu($_GET['userid']);

$session->setSplashMsg(array('type' => 'success', 'msg' => getMLText('splash_substituted_user')));

add_log_line("?userid=" . $_GET["userid"]);
header("Location: ../" . (isset($settings->_siteDefaultPage) && strlen($settings->_siteDefaultPage) > 0 ? $settings->_siteDefaultPage : "out/out.ViewFolder.php?folderid=" . $settings->_rootFolderID));

?>
