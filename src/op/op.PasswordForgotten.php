<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require_once('BakeryDMS/Core.php');
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.ClassUI.php");

include $settings->_rootDir . "languages/" . $settings->_language . "/lang.inc";

function _printMessage($heading, $message) {

  UI::htmlStartPage($heading, "password");
  UI::globalBanner();
  UI::pageNavigation($heading);
  UI::contentContainer($message . "<p><a href=\"../out/out.Login.php\">" . getMLText("login") . "</a></p>\n");
  UI::htmlEndPage();
  return;
}

if (isset($_POST["email"])) {
  $email = $_POST["email"];
}
if (isset($_POST["login"])) {
  $login = $_POST["login"];
}

if (empty($email) || empty($login)) {
  UI::exitError(getMLText("password_forgotten"), getMLText("no_email_or_login"));
}

$user = $dms->getUserByLogin($login, $email);
if ($user) {
  if ($hash = $dms->createPasswordRequest($user)) {
    $emailobj = new SeedDMS_Email();
    $subject = "###SITENAME###: " . getMLText("password_forgotten_email_subject");
    $message = str_replace('###HASH###', $hash, getMLText("password_forgotten_email_body"));

    $emailobj->sendPassword('', $user, $subject, $message);
  }
}

header('Location: ../out/out.PasswordSend.php');
exit;
?>
