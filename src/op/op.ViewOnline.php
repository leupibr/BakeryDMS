<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use BakeryDMS\Core\AccessMode;

require_once('BakeryDMS/Core.php');
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.ClassUI.php");

$documentid = $_GET["documentid"];

if (!isset($documentid) || !is_numeric($documentid) || intval($documentid) < 1) {
  UI::exitError(getMLText("document_title", array("documentname" => getMLText("invalid_doc_id"))),
    getMLText("invalid_doc_id"));
}

$document = $dms->getDocument($documentid);

if (!is_object($document)) {
  UI::exitError(getMLText("document_title", array("documentname" => getMLText("invalid_doc_id"))),
    getMLText("invalid_doc_id"));
}

if ($document->getAccessMode($user) < AccessMode::READ) {
  UI::exitError(getMLText("document_title", array("documentname" => $document->getName())), getMLText("access_denied"));
}

if (isset($_GET["version"])) {
  $version = $_GET["version"];

  if (!is_numeric($version)) {
    UI::exitError(getMLText("document_title", array("documentname" => $document->getName())),
      getMLText("invalid_version"));
  }

  if (intval($version) < 1) {
    $content = $document->getLatestContent();
  } else {
    $content = $document->getContentByVersion($version);
  }

  if (!is_object($content)) {
    UI::exitError(getMLText("document_title", array("documentname" => $document->getName())),
      getMLText("invalid_version"));
  }

  if (isset($settings->_viewOnlineFileTypes) && is_array($settings->_viewOnlineFileTypes) && in_array(strtolower($content->getFileType()),
      $settings->_viewOnlineFileTypes)
  ) {
    header("Content-Type: " . $content->getMimeType());
  }
  header("Content-Disposition: filename=\"" . $document->getName() . $content->getFileType()) . "\"";
  header("Content-Length: " . filesize($dms->contentDir . $content->getPath()));
  header("Expires: 0");
  header("Cache-Control: no-cache, must-revalidate");
  header("Pragma: no-cache");

  readfile($dms->contentDir . $content->getPath());
} elseif (isset($_GET["file"])) {
  $fileid = $_GET["file"];

  if (!is_numeric($fileid) || intval($fileid) < 1) {
    UI::exitError(getMLText("document_title", array("documentname" => $document->getName())),
      getMLText("invalid_version"));
  }

  $file = $document->getDocumentFile($fileid);

  if (!is_object($file)) {
    UI::exitError(getMLText("document_title", array("documentname" => $document->getName())),
      getMLText("invalid_file_id"));
  }

  if (isset($settings->_viewOnlineFileTypes) && is_array($settings->_viewOnlineFileTypes) && in_array(strtolower($file->getFileType()),
      $settings->_viewOnlineFileTypes)
  ) {
    header("Content-Type: " . $file->getMimeType());
  }
  header("Content-Disposition: filename=\"" . $file->getOriginalFileName()) . "\"";
  header("Content-Length: " . filesize($dms->contentDir . $file->getPath()));
  header("Expires: 0");
  header("Cache-Control: no-cache, must-revalidate");
  header("Pragma: no-cache");

  readfile($dms->contentDir . $file->getPath());
}

add_log_line();
exit;
?>
