<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use BakeryDMS\Core\AccessMode;
use BakeryDMS\Core\Folder;

require_once('BakeryDMS/Core.php');
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.ClassUI.php");

/* Check if the form data comes for a trusted request */
if (!checkFormKey('addsubfolder')) {
  UI::exitError(getMLText("folder_title", array("foldername" => getMLText("invalid_request_token"))),
    getMLText("invalid_request_token"));
}

if (!isset($_POST["folderid"]) || !is_numeric($_POST["folderid"]) || intval($_POST["folderid"]) < 1) {
  UI::exitError(getMLText("folder_title", array("foldername" => getMLText("invalid_folder_id"))),
    getMLText("invalid_folder_id"));
}

/** @var int $folderid */
$folderid = $_POST["folderid"];

/** @var Folder $folder */
$folder = $dms->getFolder($folderid);

if (!is_object($folder)) {
  UI::exitError(getMLText("folder_title", array("foldername" => getMLText("invalid_folder_id"))),
    getMLText("invalid_folder_id"));
}

$folderPathHTML = getFolderPathHTML($folder, true);

if ($folder->getAccessMode($user) < AccessMode::READWRITE) {
  UI::exitError(getMLText("folder_title", array("foldername" => $folder->getName())), getMLText("access_denied"));
}

$sequence = $_POST["sequence"];
$sequence = str_replace(',', '.', $_POST["sequence"]);

if (!is_numeric($sequence)) {
  UI::exitError(getMLText("folder_title", array("foldername" => $folder->getName())), getMLText("invalid_sequence"));
}

$name = $_POST["name"];
$comment = $_POST["comment"];
if (isset($_POST["attributes"])) {
  $attributes = $_POST["attributes"];
} else {
  $attributes = array();
}
foreach ($attributes as $attrdefid => $attribute) {
  $attrdef = $dms->getAttributeDefinition($attrdefid);
  if ($attribute) {
    if ($attrdef->getRegex()) {
      if (!preg_match($attrdef->getRegex(), $attribute)) {
        UI::exitError(getMLText("folder_title", array("foldername" => $document->getName())),
          getMLText("attr_no_regex_match"));
      }
    }
  }
}

$subFolder = $folder->addSubFolder($name, $comment, getNewFolderOwner($user), $sequence, $attributes);

if (is_object($subFolder)) {
  // Send notification to subscribers.
  if ($notifier) {
    $notifyList = $folder->getNotifyList();

    $subject = "new_subfolder_email_subject";
    $message = "new_subfolder_email_body";
    $params = array();
    $params['name'] = $subFolder->getName();
    $params['folder_name'] = $folder->getName();
    $params['folder_path'] = $folder->getFolderPathPlain();
    $params['username'] = $user->getFullName();
    $params['comment'] = $comment;
    $params['url'] = "http" . ((isset($_SERVER['HTTPS']) && (strcmp($_SERVER['HTTPS'],
            'off') != 0)) ? "s" : "") . "://" . $_SERVER['HTTP_HOST'] . $settings->_httpRoot . "out/out.ViewFolder.php?folderid=" . $subFolder->getID();
    $params['sitename'] = $settings->_siteName;
    $params['http_root'] = $settings->_httpRoot;
    $notifier->toList($user, $notifyList["users"], $subject, $message, $params);
    foreach ($notifyList["groups"] as $grp) {
      $notifier->toGroup($user, $grp, $subject, $message, $params);
    }
  }

} else {
  UI::exitError(getMLText("folder_title", array("foldername" => $folder->getName())), getMLText("error_occured"));
}

add_log_line("?name=" . $name . "&folderid=" . $folderid);

header("Location:../out/out.ViewFolder.php?folderid=" . $folderid . "&showtree=" . $_POST["showtree"]);

?>
