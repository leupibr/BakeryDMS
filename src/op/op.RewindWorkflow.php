<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use BakeryDMS\Extension\Events\EventType;
use BakeryDMS\Extension\Events\WorkflowEventType;
use BakeryDMS\Extension\ExtensionManager;

require_once('BakeryDMS/Core.php');
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.ClassUI.php");

if (!$user->isAdmin()) {
  UI::exitError(t("admin_tools"), t("access_denied"));
}

/* Check if the form data comes for a trusted request */
if (!checkFormKey('rewindworkflow')) {
  UI::exitError(s(t("document_title"), ["documentname" => t("invalid_request_token")]), t("invalid_request_token"));
}

if (!isset($_POST["documentid"]) || !is_numeric($_POST["documentid"]) || intval($_POST["documentid"]) < 1) {
  UI::exitError(s(t("document_title"), ["documentname" => t("invalid_doc_id")]), t("invalid_doc_id"));
}

$document = $dms->getDocument($_POST["documentid"]);
if (!is_object($document)) {
  UI::exitError(s(t("document_title"), ["documentname" => t("invalid_doc_id")]), t("invalid_doc_id"));
}

if (!isset($_POST["version"]) || !is_numeric($_POST["version"]) || intval($_POST["version"]) < 1) {
  UI::exitError(s(t("document_title"), ["documentname" => $document->getName()]), t("invalid_version"));
}

$version = $document->getContentByVersion((int) $_POST["version"]);
if (!is_object($version)) {
  UI::exitError(s(t("document_title"), ["documentname" => $document->getName()]), t("invalid_version"));
}

$workflow = $version->getWorkflow();
if (!is_object($workflow)) {
  UI::exitError(s(t("document_title"), ["documentname" => htmlspecialchars($document->getName())]),
    t("document_has_no_workflow"));
}

ExtensionManager::getInstance()
  ->getDmsEventFacade()
  ->fireWorkflowEvent(EventType::PreEvent, WorkflowEventType::Rewind, $document);

if ($version->rewindWorkflow()) {
  if ($notifier) {
    $nl = $document->getNotifyList();
    $folder = $document->getFolder();

    $subject = "rewind_workflow_email_subject";
    $message = "rewind_workflow_email_body";
    $params = array();
    $params['name'] = $document->getName();
    $params['version'] = $version->getVersion();
    $params['workflow'] = $workflow->getName();
    $params['folder_path'] = $folder->getFolderPathPlain();
    $params['username'] = $user->getFullName();
    $params['sitename'] = $settings->_siteName;
    $params['http_root'] = $settings->_httpRoot;
    $params['url'] = "http" . ((isset($_SERVER['HTTPS']) && (strcmp($_SERVER['HTTPS'],
            'off') != 0)) ? "s" : "") . "://" . $_SERVER['HTTP_HOST'] . $settings->_httpRoot . "out/out.ViewDocument.php?documentid=" . $document->getID();
    // Send notification to subscribers.
    $notifier->toList($user, $nl["users"], $subject, $message, $params);
    foreach ($nl["groups"] as $grp) {
      $notifier->toGroup($user, $grp, $subject, $message, $params);
    }
  }

  ExtensionManager::getInstance()
    ->getDmsEventFacade()
    ->fireWorkflowEvent(EventType::PostEvent, WorkflowEventType::Rewind, $document);
}

add_log_line("?documentid=" . $document->getID() . "&version" . (int) $_POST["version"]);

header("Location:../out/out.ViewDocument.php?documentid=" . $document->getID());
?>
