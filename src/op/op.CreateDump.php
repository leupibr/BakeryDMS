<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use BakeryDMS\Utils\File;

require_once('BakeryDMS/Core.php');
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.ClassUI.php");

if (!$user->isAdmin()) {
  UI::exitError(getMLText("admin_tools"), getMLText("access_denied"));
}

$dump_name = $settings->_contentDir . time() . ".sql";

$h = fopen($dump_name, "w");

if (is_bool($h) && !$h) {
  UI::exitError(getMLText("admin_tools"), getMLText("error_occured"));
}

$tables = $db->TableList('TABLES');

foreach ($tables as $table) {

  $query = "SELECT * FROM " . $table;
  $records = $db->getResultArray($query);

  fwrite($h, "\n-- TABLE: " . $table . "--\n\n");

  foreach ($records as $record) {

    $values = "";
    $i = 1;
    foreach ($record as $column) {
      if (is_numeric($column)) {
        $values .= $column;
      } else {
        $values .= "'" . $column . "'";
      }

      if ($i < (count($record))) {
        $values .= ",";
      }
      $i++;
    }

    fwrite($h, "INSERT INTO " . $table . " VALUES (" . $values . ");\n");
  }
}

fclose($h);

if (File::gzcompressfile($dump_name, 9)) {
  unlink($dump_name);
} else {
  UI::exitError(getMLText("admin_tools"), getMLText("error_occured"));
}

add_log_line();

header("Location:../out/out.BackupTools.php");

?>
