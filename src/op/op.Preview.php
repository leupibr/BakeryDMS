<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use BakeryDMS\Core\AccessMode;
use BakeryDMS\Preview\Previewer;

require_once('BakeryDMS/Core.php');
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.ClassUI.php");

$documentid = $_GET["documentid"];
if (!isset($documentid) || !is_numeric($documentid) || intval($documentid) < 1) {
  exit;
}

$document = $dms->getDocument($documentid);
if (!is_object($document)) {
  exit;
}

if ($document->getAccessMode($user) < AccessMode::READ) {
  exit;
}

if (isset($_GET['version'])) {
  $version = $_GET["version"];
  if (!is_numeric($version)) {
    exit;
  }
  if (intval($version) < 1) {
    $object = $document->getLatestContent();
  } else {
    $object = $document->getContentByVersion($version);
  }
} elseif (isset($_GET['file'])) {
  $file = $_GET['file'];
  if (!is_numeric($file) || intval($file) < 1) {
    exit;
  }
  $object = $document->getDocumentFile($file);
} else {
  exit;
}

if (!is_object($object)) {
  exit;
}

if (!empty($_GET["width"])) {
  $previewer = new Previewer($settings->_cacheDir, $_GET["width"]);
} else {
  $previewer = new Previewer($settings->_cacheDir);
}
if (!$previewer->hasPreview($object)) {
  $previewer->createPreview($object);
}
header('Content-Type: image/png');
$previewer->getPreview($object);

?>
