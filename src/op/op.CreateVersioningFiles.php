<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require_once('BakeryDMS/Core.php');
include("../inc/inc.LogInit.php");
include("../inc/inc.Language.php");
include("../inc/inc.ClassUI.php");


if (!$user->isAdmin()) {
  UI::exitError(getMLText("admin_tools"), getMLText("access_denied"));
}

function createVersionigFiles($folder) {
  global $settings;

  $documents = $folder->getDocuments();
  foreach ($documents as $document) {
    if (!createVersionigFile($document)) {
      return false;
    }
  }

  $subFolders = $folder->getSubFolders();

  foreach ($subFolders as $folder) {
    if (!createVersionigFiles($folder)) {
      return false;
    }
  }

  return true;
}

if (!isset($_GET["targetid"]) || !is_numeric($_GET["targetid"]) || intval($_GET["targetid"]) < 1) {
  UI::exitError(getMLText("admin_tools"), getMLText("invalid_folder_id"));
}
$folderid = $_GET["targetid"];
$folder = $dms->getFolder($folderid);

if (!is_object($folder)) {
  UI::exitError(getMLText("admin_tools"), getMLText("invalid_folder_id"));
}

if (!createVersionigFiles($folder)) {
  UI::exitError(getMLText("admin_tools"), getMLText("error_occured"));
}

add_log_line();

header("Location:../out/out.BackupTools.php");

?>
