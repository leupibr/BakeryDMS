<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/**
 * This file links all necessary functionality for BakeryDMS in common.
 */
use BakeryDMS\Core\DMS;
use BakeryDMS\Core\Initializer;
use BakeryDMS\Core\Persistency\DatabaseAccessFactory;
use BakeryDMS\Extension\ExtensionManager;
use BakeryDMS\I18n\TranslationHandler;
use BakeryDMS\Security\Authenticator;
use BakeryDMS\Settings;

/* Gets an implementation of the missing 'strptime'-Function on Windows (if the system was running on a Windows server). */
require_once('inc/inc.DateUtils.php');

/* TODO: get rid of this includes */
require_once("inc/inc.Utils.php");
require_once("inc/inc.ClassEmail.php");
require_once("inc/inc.ClassSession.php");

/** Include Lucene classes from Zend-Engine. */
@include_once('Zend/Search/Lucene.php');
@include_once('Zend/Search/Lucene/Analysis/TokenFilter/StopWords.php');

//// Helper methods
/** auto-loader for class files */
spl_autoload_register(function ($class) {
  $classPieces = explode("\\", $class);
  require_once(implode("/", $classPieces) . '.php');
});

//// Initialize DMS
$settings = Settings::getInstance();
(new Initializer($settings))->initialize();

$db = DatabaseAccessFactory::createDatabaseAccess(
  $settings->_dbDriver,
  $settings->_dbHostname,
  $settings->_dbUser,
  $settings->_dbPass,
  $settings->_dbDatabase);
$db->connect() or die ("Could not connect to db-server \"" . $settings->_dbHostname . "\"");

$dms = new DMS($db, $settings->_contentDir . $settings->_contentOffsetDir);
$dms->checkVersion() or die("Database update needed.");

$dms->setRootFolderID($settings->_rootFolderID);
$dms->setMaxDirID($settings->_maxDirID);
$dms->setEnableConverting($settings->_enableConverting);
$dms->setViewOnlineFileTypes($settings->_viewOnlineFileTypes);

// setup authenticator
$authenticator = new Authenticator($db, $dms);
if (!$authenticator->isNonAuthScreen()) {
  $authenticator->authenticate();
  $session = $authenticator->getSession();
  $user = $authenticator->getUser();
  $dms->setCurrentUser($user);

  $notifier = null;
  if (Settings::getInstance()->_enableEmail) {
    $notifier = new SeedDMS_Email();
    $notifier->setSender($user);
  }

  $languageFile = Settings::getInstance()->_rootDir . "view/" . $authenticator->getSessionData("theme") . "/languages/" . $authenticator->getSessionData("language") . "/lang.inc";
  if (file_exists($languageFile)) {
    /** @noinspection PhpIncludeInspection */
    include $languageFile;
  }
}

//// Initialize extensions
$extensionManager = ExtensionManager::getInstance();
$extensionManager->setCoreDms($dms);
$extensionManager->loadExtensions();

//// Internationalization
/**
 * Translates the given key to the configured language.
 *
 * @param $key string The key of the item to translate.
 *
 * @return string The translated key.
 */
function t($key) {
  return TranslationHandler::getInstance()->translate($key);
}

/**
 * Substitutes the wildcards in a text with the given parameters.
 *
 * @param $text string The text with the items to substitute.
 * @param $parameters array The items to replace as key-value pairs.
 *
 * @return string The substituted string.
 */
function s($text, $parameters) {
  return TranslationHandler::getInstance()->substitute($text, $parameters);
}

//// Debugging
/**
 * Useful debug function that wraps a pre around the print_r
 *
 * @param $object object The object to print.
 */
function print_p($object) {
  echo "<pre>";
  print_r($object);
  echo "</pre>";
}
