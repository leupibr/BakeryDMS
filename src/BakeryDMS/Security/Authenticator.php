<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Security;

use BakeryDMS\Core\DMS;
use BakeryDMS\Core\Persistency\IDatabaseAccess;
use BakeryDMS\Core\User;
use BakeryDMS\Settings;

class Authenticator {
  /** @var IDatabaseAccess */
  private $db;

  /** @var DMS */
  private $dms;

  /** @var User */
  private $user;

  /**
   * Initializes a new instance of the {@link Authenticator} class.
   *
   * @param IDatabaseAccess $db
   */
  public function __construct(IDatabaseAccess &$db, DMS &$dms) {
    $this->db = $db;
    $this->dms = $dms;

    $this->session = new \SeedDMS_Session($this->db);
    $this->sessionData = $this->session->load($this->getSessionId());
    $this->user = null;
  }

  /**
   * Authenticates the user and performs a few checks.
   */
  public function authenticate() {
    $this->checkSession();
    $this->updateAccess();
    $this->checkUser();
    $this->checkPasswordChange();
    $this->updateSessionCookie();
  }

  /**
   * Gets the current user.
   * @return User|null The current user.
   */
  public function getUser() {
    if (empty($this->user)) {
      /** @var User $user */
      $this->user = $this->dms->getUser($this->sessionData["userID"]);
      if ($this->user->isAdmin()) {
        if ($this->sessionData["su"]) {
          $this->user = $this->dms->getUser($this->sessionData["su"]);
        }
      }
    }

    return $this->user;
  }

  /**
   * Getter for the session data.
   *
   * @param $key string The items session key.
   *
   * @return mixed
   */
  public function getSessionData($key) {
    return $this->sessionData[$key];
  }

  /**
   * Gets the current session.
   * @return \SeedDMS_Session The current session.
   */
  public function getSession() {
    return $this->session;
  }

  /**
   * Gets the current referrer.
   * @return string
   */
  private function getReferrer() {
    $referrer = $_SERVER["REQUEST_URI"];
    if (!strncmp("/op", $referrer, 3)) {
      $referrer = "";
    }
    else {
      $referrer = urlencode($referrer);
    }
    return $referrer;
  }

  /**
   * Gets the session id.
   * @return string
   */
  private function getSessionId() {
    return @$_COOKIE["mydms_session"];
  }

  /**
   * Checks the session for validity. In case of invalidity, it will redirect to the login page.
   */
  private function checkSession() {
    if (!isset($_COOKIE["mydms_session"])) {
      $this->gotoLogin();
    }

    if (empty($this->sessionData)) {
      $this->deleteSessionCookie();
      $this->gotoLogin();
    }
  }

  /**
   * Updates the access data.
   */
  private function updateAccess() {
    $this->session->updateAccess($this->getSessionId());
  }

  /**
   * Check if user is valid. In case of invalidity, it will redirect to the login page.
   */
  private function checkUser() {
    if (!is_object($this->getUser())) {
      $this->deleteSessionCookie();
      $this->gotoLogin();
    }
  }

  /**
   * Deletes the session cookie.
   */
  private function deleteSessionCookie() {
    setcookie("mydms_session", $this->getSessionId(), time() - 3600, Settings::getInstance()->_httpRoot);
  }

  /**
   * Redirects the user to the login page.
   */
  private function gotoLogin() {
    header("Location: " . Settings::getInstance()->_httpRoot . "out/out.Login.php?referuri=" . $this->getReferrer());
    die();
  }

  /**
   * Updates the session cookie.
   */
  private function updateSessionCookie() {
    if (Settings::getInstance()->_cookieLifetime) {
      $lifetime = time() + intval(Settings::getInstance()->_cookieLifetime);
      /* Turn off http only cookies if jump loader is enabled */
      setcookie("mydms_session",
        $this->getSessionId(),
        $lifetime,
        Settings::getInstance()->_httpRoot,
        null,
        null,
        !Settings::getInstance()->_enableLargeFileUpload);
    }
  }

  /**
   * Check if password needs to be changed because it expired. If it needs
   * to be changed redirect to out/out.ForcePasswordChange.php. Do this
   * check only if password expiration is turned on, we are not on the
   * page to change the password or the page that changes the password, and
   * it is not admin
   */
  private function checkPasswordChange() {
    if ($this->getUser()->isAdmin()) {
      return;
    }
    if (Settings::getInstance()->_passwordExpiration <= 0) {
      return;
    }
    if (basename($_SERVER['SCRIPT_NAME']) == 'out.ForcePasswordChange.php' || basename($_SERVER['SCRIPT_NAME']) == 'op.EditUserData.php') {
      return;
    }

    $pwdexp = $this->getUser()->getPwdExpiration();
    if (substr($pwdexp, 0, 10) == '0000-00-00') {
      return;
    }

    $pwdexpts = strtotime($pwdexp);
    if ($pwdexpts > 0 && $pwdexpts < time()) {
      header("Location: ../out/out.ForcePasswordChange.php");
      die();
    }
  }

  /**
   * @return bool
   */
  public function isNonAuthScreen() {
    // command line tools
    if(!isset($_SERVER["REQUEST_URI"])) {
      return true;
    }

    // login screen
    if (strpos($_SERVER["REQUEST_URI"], "Login") !== false) {
      return true;
    }
    // password forgotten screen
    if (strpos($_SERVER["REQUEST_URI"], "PasswordForgotten") !== false) {
      return true;
    }
    return false;
  }

}