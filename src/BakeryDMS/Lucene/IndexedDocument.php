<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Lucene;

/**
 *  Class for managing an indexed document.
 */
class IndexedDocument extends \Zend_Search_Lucene_Document {
  /**
   * Constructor. Creates our indexable document and adds all
   * necessary fields to it using the passed in document
   */
  public function __construct($dms, $document, $convcmd = null, $nocontent = false) {
    $_convcmd = array(
      'application/pdf' => 'pdftotext -enc UTF-8 -nopgbrk %s - |sed -e \'s/ [a-zA-Z0-9.]\{1\} / /g\' -e \'s/[0-9.]//g\'',
      'application/msword' => 'catdoc %s',
      'application/vnd.ms-excel' => 'ssconvert -T Gnumeric_stf:stf_csv -S %s fd://1',
      'audio/mp3' => "id3 -l -R %s | egrep '(Title|Artist|Album)' | sed 's/^[^:]*: //g'",
      'audio/mpeg' => "id3 -l -R %s | egrep '(Title|Artist|Album)' | sed 's/^[^:]*: //g'",
      'text/plain' => 'cat %s',
    );
    if ($convcmd) {
      $_convcmd = $convcmd;
    }

    $version = $document->getLatestContent();
    $this->addField(\Zend_Search_Lucene_Field::Keyword('document_id', $document->getID()));
    if ($version) {
      $this->addField(\Zend_Search_Lucene_Field::Keyword('mimetype', $version->getMimeType()));
      $this->addField(\Zend_Search_Lucene_Field::Keyword('origfilename', $version->getOriginalFileName(), 'utf-8'));
      if (!$nocontent) {
        $this->addField(\Zend_Search_Lucene_Field::UnIndexed('created', $version->getDate()));
      }
      if ($attributes = $version->getAttributes()) {
        foreach ($attributes as $attribute) {
          $attrdef = $attribute->getAttributeDefinition();
          if ($attrdef->getValueSet() != '') {
            $this->addField(\Zend_Search_Lucene_Field::Keyword('attr_' . str_replace(' ', '_', $attrdef->getName()),
              $attribute->getValue(),
              'utf-8'));
          }
          else {
            $this->addField(\Zend_Search_Lucene_Field::Text('attr_' . str_replace(' ', '_', $attrdef->getName()),
              $attribute->getValue(),
              'utf-8'));
          }
        }
      }
    }
    $this->addField(\Zend_Search_Lucene_Field::Text('title', $document->getName(), 'utf-8'));
    if ($categories = $document->getCategories()) {
      $names = array();
      foreach ($categories as $cat) {
        $names[] = $cat->getName();
      }
      $this->addField(\Zend_Search_Lucene_Field::Text('category', implode(' ', $names), 'utf-8'));
    }
    if ($attributes = $document->getAttributes()) {
      foreach ($attributes as $attribute) {
        $attrdef = $attribute->getAttributeDefinition();
        if ($attrdef->getValueSet() != '') {
          $this->addField(\Zend_Search_Lucene_Field::Keyword('attr_' . str_replace(' ', '_', $attrdef->getName()),
            $attribute->getValue(),
            'utf-8'));
        }
        else {
          $this->addField(\Zend_Search_Lucene_Field::Text('attr_' . str_replace(' ', '_', $attrdef->getName()),
            $attribute->getValue(),
            'utf-8'));
        }
      }
    }

    $owner = $document->getOwner();
    $this->addField(\Zend_Search_Lucene_Field::Text('owner', $owner->getLogin(), 'utf-8'));
    if ($keywords = $document->getKeywords()) {
      $this->addField(\Zend_Search_Lucene_Field::Text('keywords', $keywords, 'utf-8'));
    }
    if ($comment = $document->getComment()) {
      $this->addField(\Zend_Search_Lucene_Field::Text('comment', $comment, 'utf-8'));
    }
    if ($version && !$nocontent) {
      $path = $dms->contentDir . $version->getPath();
      $content = '';
      $fp = null;
      $mimetype = $version->getMimeType();
      if (isset($_convcmd[$mimetype])) {
        $cmd = sprintf($_convcmd[$mimetype], $path);
        $fp = popen($cmd, 'r');
        if ($fp) {
          $content = '';
          while (!feof($fp)) {
            $content .= fread($fp, 2048);
          }
          pclose($fp);
        }
        if ($content) {
          $this->addField(\Zend_Search_Lucene_Field::UnStored('content', $content, 'utf-8'));
        }
      }
    }
  }
}