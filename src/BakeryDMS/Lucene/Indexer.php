<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Lucene;

use Exception;

/**
 * Class for managing a lucene index.
 */
class Indexer {
  /**
   * @var string $indexname name of lucene index
   * @access protected
   */
  protected $indexname;

  static function open($luceneDir) {
    try {
      $index = \Zend_Search_Lucene::open($luceneDir);
      return ($index);
    } catch (Exception $e) {
      return null;
    }
  }

  function create($luceneDir) {
    $index = \Zend_Search_Lucene::create($luceneDir);
    return ($index);
  }

  /**
   * Do some initialization
   *
   */
  function init($stopWordsFile = '') {
    $analyzer = new \Zend_Search_Lucene_Analysis_Analyzer_Common_Utf8_CaseInsensitive();
    if ($stopWordsFile && file_exists($stopWordsFile)) {
      $stopWordsFilter = new \Zend_Search_Lucene_Analysis_TokenFilter_StopWords();
      $stopWordsFilter->loadFromFile($stopWordsFile);
      $analyzer->addFilter($stopWordsFilter);
    }

    \Zend_Search_Lucene_Analysis_Analyzer::setDefault($analyzer);
  }
}