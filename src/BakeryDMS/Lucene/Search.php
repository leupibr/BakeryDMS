<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Lucene;

/**
 * Class for searching in a lucene index.
 */
class Search {
  /**
   * @var object $index lucene index
   * @access protected
   */
  protected $index;

  /**
   * Create a new instance of the search
   *
   * @param object $index lucene index
   * @return object instance of BakeryDMS\Lucene\Search
   */
  function __construct($index) {
    $this->index = $index;
    $this->version = '1.1.5';
    if ($this->version[0] == '@') {
      $this->version = '3.0.0';
    }
  }

  /**
   * Search in index
   *
   * @param object $index lucene index
   * @return object instance of BakeryDMS\Lucene\Search
   */
  function search($term, $owner, $status = '', $categories = array(), $fields = array()) {
    $querystr = '';
    if ($fields) {
    }
    else {
      if ($term) {
        $querystr .= trim($term);
      }
    }
    if ($owner) {
      if ($querystr) {
        $querystr .= ' && ';
      }
      $querystr .= 'owner:' . $owner;
    }
    if ($categories) {
      if ($querystr) {
        $querystr .= ' && ';
      }
      $querystr .= '(category:"';
      $querystr .= implode('" || category:"', $categories);
      $querystr .= '")';
    }
    try {
      $query = \Zend_Search_Lucene_Search_QueryParser::parse($querystr);
      try {
        $hits = $this->index->find($query);
        $recs = array();
        foreach ($hits as $hit) {
          $recs[] = array('id' => $hit->id, 'document_id' => $hit->document_id);
        }
        return $recs;
      } catch (\Zend_Search_Lucene_Exception $e) {
        return false;
      }
    } catch (\Zend_Search_Lucene_Search_QueryParserException $e) {
      return false;
    }
  }

}