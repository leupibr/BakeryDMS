<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Extension;

use BakeryDMS\Extension\Events\IDmsEventRegistry;

/**
 * Interface IExtension. This interface is a representation of a existing extension.
 * It allows BakeryDMS to interact with this subsystem.
 */
interface IExtension {
  /**
   * Sets the DMS facade to the extension.
   *
   * @param IDms $dms The dms facade.
   */
  function setDmsFacade(IDms &$dms);

  /**
   * Sets the DMS event registry.
   *
   * @param IDmsEventRegistry $dmsEventRegistry The event registry
   */
  function setEventFacade(IDmsEventRegistry &$dmsEventRegistry);

  /**
   * Gets the operations that can be called at this extension.
   *
   * @return IOperation[] The operations.
   */
  function getOperations();

  /**
   * Gets the views that are available in this extension.
   *
   * @return IView[] The views.
   */
  function getViews();

  /**
   * Gets the menu items for the global navigation.
   *
   * @return IAction[] Array of menu items for the global navigation.
   */
  function getGlobalNavigationItems();

  /**
   * Gets the menu items for the admin panel.
   *
   * @return IAction[] Array of menu items for the admin panel.
   */
  function getAdminItems();

  /**
   * Gets the folder actions for the current context.
   *
   * @return IAction[] Action points for the folders action bar.
   */
  function getFolderActions();

  /**
   * Gets the document action for the current context.
   *
   * @return IAction[] Action points for the documents action bar.
   */
  function getDocumentActions();

  /**
   * Gets the actions for the my documents view.
   *
   * @return IAction[] Action points for the documents action bar.
   */
  function getMyDocumentActions();

  /**
   * Gets the actions for the my account view.
   *
   * @return IAction[] Action points for the documents action bar.
   */
  function getMyAccountActions();

  /**
   * Gets the actions for the calendar view.
   *
   * @return IAction[] Action points for the documents action bar.
   */
  function getCalendarActions();
}
