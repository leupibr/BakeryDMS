<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Extension\Events;


class EventFacade implements IDmsEventRegistry {
  //// The registries.
  private $documentEventRegistry;
  private $attachmentEventRegistry;
  private $documentLinkEventRegistry;
  private $eventEventRegistry;
  private $workflowEventRegistry;
  private $folderEventRegistry;
  private $categoryEventRegistry;
  private $clipboardEventRegistry;
  private $notificationEventRegistry;
  private $searchEventRegistry;
  private $userActionEventRegistry;
  private $authorizationEventRegistry;

  /**
   * Initializes a new instance of the {@link EventFacade} class.
   */
  public function __construct() {
    $this->documentEventRegistry = new EventRegistry();
    $this->attachmentEventRegistry = new EventRegistry();
    $this->documentLinkEventRegistry = new EventRegistry();
    $this->eventEventRegistry = new EventRegistry();
    $this->workflowEventRegistry = new EventRegistry();
    $this->folderEventRegistry = new EventRegistry();
    $this->categoryEventRegistry = new EventRegistry();
    $this->clipboardEventRegistry = new EventRegistry();
    $this->notificationEventRegistry = new EventRegistry();
    $this->searchEventRegistry = new EventRegistry();
    $this->userActionEventRegistry = new EventRegistry();
    $this->authorizationEventRegistry = new EventRegistry();
  }

  /**
   * @param IEventHandler $handler
   */
  function registerDocumentEventHandler(IEventHandler $handler) {
    $this->documentEventRegistry->registerEventHandler($handler);
  }

  function unregisterDocumentEventHandler(IEventHandler $handler) {
    $this->documentEventRegistry->unregisterEventHandler($handler);
  }

  function fireDocumentEvent($type, $subtype, $data) {
    $this->documentEventRegistry->fireEvent($type, $subtype, $data);
  }


  /**
   * @param IEventHandler $handler
   * @todo Firing event not yet implemented.
   */
  function registerAttachmentEventHandler(IEventHandler $handler) {
    $this->attachmentEventRegistry->registerEventHandler($handler);
  }

  function unregisterAttachmentEventHandler(IEventHandler $handler) {
    $this->attachmentEventRegistry->unregisterEventHandler($handler);
  }

  /**
   * @param IEventHandler $handler
   * @todo Firing event not yet implemented.
   */
  function registerDocumentLinkEventHandler(IEventHandler $handler) {
    $this->documentLinkEventRegistry->registerEventHandler($handler);
  }

  function unregisterDocumentLinkEventHandler(IEventHandler $handler) {
    $this->documentLinkEventRegistry->unregisterEventHandler($handler);
  }

  /**
   * @param IEventHandler $handler
   * @todo Firing event not yet implemented.
   */
  function registerEventEventHandler(IEventHandler $handler) {
    $this->eventEventRegistry->registerEventHandler($handler);
  }

  function unregisterEventEventHandler(IEventHandler $handler) {
    $this->eventEventRegistry->unregisterEventHandler($handler);
  }

  /**
   * @param IEventHandler $handler
   */
  function registerWorkflowEventHandler(IEventHandler $handler) {
    $this->workflowEventRegistry->registerEventHandler($handler);
  }

  function unregisterWorkflowEventHandler(IEventHandler $handler) {
    $this->workflowEventRegistry->unregisterEventHandler($handler);
  }

  function fireWorkflowEvent($type, $subtype, $data) {
    $this->workflowEventRegistry->fireEvent($type, $subtype, $data);
  }

  /**
   * @param IEventHandler $handler
   * @todo Firing event not yet implemented.
   */
  function registerFolderEventHandler(IEventHandler $handler) {
    $this->folderEventRegistry->registerEventHandler($handler);
  }

  function unregisterFolderEventHandler(IEventHandler $handler) {
    $this->folderEventRegistry->unregisterEventHandler($handler);
  }

  /**
   * @param IEventHandler $handler
   * @todo Firing event not yet implemented.
   */
  function registerCategoryEventHandler(IEventHandler $handler) {
    $this->categoryEventRegistry->registerEventHandler($handler);
  }

  function unregisterCategoryEventHandler(IEventHandler $handler) {
    $this->categoryEventRegistry->unregisterEventHandler($handler);
  }

  /**
   * @param IEventHandler $handler
   * @todo Firing event not yet implemented.
   */
  function registerClipboardEventHandler(IEventHandler $handler) {
    $this->clipboardEventRegistry->registerEventHandler($handler);
  }

  function unregisterClipboardEventHandler(IEventHandler $handler) {
    $this->clipboardEventRegistry->unregisterEventHandler($handler);
  }

  /**
   * @param IEventHandler $handler
   * @todo Firing event not yet implemented.
   */
  function registerNotificationEventHandler(IEventHandler $handler) {
    $this->notificationEventRegistry->registerEventHandler($handler);
  }

  function unregisterNotificationEventHandler(IEventHandler $handler) {
    $this->notificationEventRegistry->unregisterEventHandler($handler);
  }

  /**
   * @param IEventHandler $handler
   * @todo Firing event not yet implemented.
   */
  function registerSearchEventHandler(IEventHandler $handler) {
    $this->searchEventRegistry->registerEventHandler($handler);
  }

  function unregisterSearchEventHandler(IEventHandler $handler) {
    $this->searchEventRegistry->unregisterEventHandler($handler);
  }

  /**
   * @param IEventHandler $handler
   * @todo Firing event not yet implemented.
   */
  function registerUserActionEventHandler(IEventHandler $handler) {
    $this->userActionEventRegistry->registerEventHandler($handler);
  }

  function unregisterUserActionEventHandler(IEventHandler $handler) {
    $this->userActionEventRegistry->unregisterEventHandler($handler);
  }

  /**
   * @param IEventHandler $handler
   * @todo Firing event not yet implemented.
   */
  function registerAuthorizationEventHandler(IEventHandler $handler) {
    $this->authorizationEventRegistry->registerEventHandler($handler);
  }

  function unregisterAuthorizationEventHandler(IEventHandler $handler) {
    $this->authorizationEventRegistry->unregisterEventHandler($handler);
  }
}
