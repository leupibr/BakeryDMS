<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Extension\Events;

interface IDmsEventRegistry {
  function registerDocumentEventHandler(IEventHandler $handler);
  function unregisterDocumentEventHandler(IEventHandler $handler);
  function registerAttachmentEventHandler(IEventHandler $handler);
  function unregisterAttachmentEventHandler(IEventHandler $handler);
  function registerDocumentLinkEventHandler(IEventHandler $handler);
  function unregisterDocumentLinkEventHandler(IEventHandler $handler);
  function registerEventEventHandler(IEventHandler $handler);
  function unregisterEventEventHandler(IEventHandler $handler);
  function registerWorkflowEventHandler(IEventHandler $handler);
  function unregisterWorkflowEventHandler(IEventHandler $handler);
  function registerFolderEventHandler(IEventHandler $handler);
  function unregisterFolderEventHandler(IEventHandler $handler);
  function registerCategoryEventHandler(IEventHandler $handler);
  function unregisterCategoryEventHandler(IEventHandler $handler);
  function registerClipboardEventHandler(IEventHandler $handler);
  function unregisterClipboardEventHandler(IEventHandler $handler);
  function registerNotificationEventHandler(IEventHandler $handler);
  function unregisterNotificationEventHandler(IEventHandler $handler);
  function registerSearchEventHandler(IEventHandler $handler);
  function unregisterSearchEventHandler(IEventHandler $handler);
  function registerUserActionEventHandler(IEventHandler $handler);
  function unregisterUserActionEventHandler(IEventHandler $handler);
  function registerAuthorizationEventHandler(IEventHandler $handler);
  function unregisterAuthorizationEventHandler(IEventHandler $handler);
}