<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Extension\Events;


class EventRegistry {
  /**
   * @var IEventHandler[] The registered event handler.
   */
  private $registry;

  /**
   * Initializes a new instance of the {@link EventRegistry} class.
   */
  public function __construct() {
    $this->registry = array();
  }

  /**
   * Registers a handler on the registry. The action will be ignored if the handler is already registered.
   *
   * @param IEventHandler $handler The handler to register.
   */
  public function registerEventHandler(IEventHandler &$handler) {
    if (!$this->isRegistered($handler)) {
      $this->registry[spl_object_hash($handler)] = $handler;
    }
  }

  /**
   * Removes a handler from the registry. The action will be ignored if the handler is not registered.
   *
   * @param IEventHandler $handler The handler to unregister.
   */
  public function unregisterEventHandler(IEventHandler &$handler) {
    if ($this->isRegistered($handler)) {
      unset($this->registry[spl_object_hash($handler)]);
    }
  }

  /**
   * @param int $type Enum of type {@link EventType}.
   * @param int $subType Enum of type {@link IEventSubType}
   * @param object $data Data to pass.
   */
  public function fireEvent($type, $subType, $data) {
    foreach ($this->registry as $id => $handler) {
      $handler->OnEvent($type, $subType, $data);
    }
  }

  /**
   * Checks whether the handler is registered or not.
   *
   * @param IEventHandler $handler The handler to check
   *
   * @return bool <c>true</c> if registered, otherwise false.
   */
  public function isRegistered(IEventHandler &$handler) {
    return array_key_exists(spl_object_hash($handler), $this->registry);
  }
}