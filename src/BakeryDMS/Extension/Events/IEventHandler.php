<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Extension\Events;

/**
 * Interface IEventHandler.
 *
 * @package BakeryDMS\Extension\Events
 */
interface IEventHandler {
  /**
   * If the event happens.
   *
   * @param int $type Type of the event. See {@link EventType}.
   * @param int $subType Subtype of the event. See {@link IEventSubType}
   * @param $data object The event data.
   */
  public function OnEvent($type, $subType, $data);
}