<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Extension\Events;

// TODO: document this enumeration

class DocumentEventType implements IEventSubType {
  const Add = 0;
  const Edit = 1;
  const Move = 2;
  const Remove = 3;
  const Lock = 4;
  const Unlock = 5;
  const Update = 6; // INFO this has a wizard in front! (UpdateDocument, UpdateDocument2)
  const RemoveVersion = 7;
  const EditAttributes = 8;
  const EditComment = 9;
  const ModifyAccess = 10; // op.DocumentAccess.php | "subactions": setowner, delaccess, inherit, setdefault, editaccess, addaccess, notinherit
  const Approve = 11;
}