<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Extension;

/**
 * Interface IAction. A action point that can be visualized on the menu/action bars.
 *
 * @package BakeryDMS\Extension
 */
interface IAction {
  /**
   * Gets the translation key for the text.
   *
   * @return string The translation key.
   */
  function getText();

  /**
   * Gets the icon that should be shown.
   *
   * @return string The icon for the action.
   */
  function getIcon();

  /**
   * Gets the url of the action.
   *
   * @return string The url of the action.
   */
  function getUrl();
}