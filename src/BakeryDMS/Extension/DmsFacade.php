<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Extension;

use BakeryDMS\Core\DMS;
use BakeryDMS\Core\Document;
use BakeryDMS\Core\Folder;
use BakeryDMS\Core\Group;
use BakeryDMS\Core\Persistency\IDatabaseAccess;
use BakeryDMS\Core\User;
use BakeryDMS\Settings;

/**
 * Class DmsFacade.
 *
 * @package BakeryDMS\Extension
 */
class DmsFacade implements IDms {
  /** @var DMS */
  private $dms;

  /**
   * Initializes a new instance of the {@link DmsFacade} class.
   */
  public function __construct() {
  }

  public function setCoreDms(DMS &$dms) {
    $this->dms = $dms;
  }

  /**
   * Gets the BakeryDMS settings.
   *
   * @return Settings The settings.
   */
  function getSettings() {
    return Settings::getInstance();
  }

  /**
   * Gets access to the BakeryDMS database.
   *
   * @return IDatabaseAccess The database access handler.
   */
  function getDatabaseAccess() {
    return $this->dms->getDB();
  }


  /**
   * Builds the path of the current view in an extension context.
   *
   * @param $extensionName string The name of the current extension.
   * @param $viewName string The name of the current view.
   *
   * @return string The path of this view.
   */
  function buildViewPath($extensionName, $viewName) {
    return $this->buildPath($extensionName, $viewName, "view");
  }

  /**
   * Builds the url of the current view in an extension context.
   *
   * @param $extensionName string The name of the current extension.
   * @param $viewName string The name of the current view.
   *
   * @return string The path of this view.
   */
  function buildViewLink($extensionName, $viewName) {
    return $this->buildLink($extensionName, $viewName, "view");
  }

  /**
   * Builds the path of the current operation in an extension context.
   *
   * @param $extensionName string The name of the current extension.
   * @param $operationName string The name of the current view.
   *
   * @return string The path of this view.
   */
  function buildOperationPath($extensionName, $operationName) {
    return $this->buildPath($extensionName, $operationName, "op");
  }

  /**
   * Builds the url of the current operation in an extension context.
   *
   * @param $extensionName string The name of the current extension.
   * @param $operationName string The name of the current view.
   *
   * @return string The path of this view.
   */
  function buildOperationLink($extensionName, $operationName) {
    return $this->buildLink($extensionName, $operationName, "op");
  }

  /**
   * Builds the path within an extension for a given name (e.g. an operation or a view) that is declared by its type.
   *
   * @param $extensionName string The name of the current extension.
   * @param $name string The name of the type.
   * @param $type string The type.
   *
   * @return string The according path.
   */
  private function buildPath($extensionName, $name, $type) {
    return Settings::getInstance()->_httpRoot . "extension/" . $extensionName . "/" . $type . "/" . $name;
  }

  /**
   * Builds an url within an extension for a given name (e.g. an operation or a view) that is declared by its type.
   *
   * @param $extensionName string The name of the current extension.
   * @param $name string The name of the type.
   * @param $type string The type.
   *
   * @return string The url.
   */
  private function buildLink($extensionName, $name, $type) {
    $path = parse_url($_SERVER['HTTP_REFERER']);
    return $path['scheme'] . "://" . $path['host'] . $this->buildPath($extensionName, $name, $type);
  }

  /**
   * Returns the current (logged-in) user.
   *
   * @return User
   */
  function getCurrentUser() {
    return $this->dms->getCurrentUser();
  }

  /**
   * Return list of all users
   *
   * @return  User[] or false
   */
  function getAllUsers() {
    return $this->dms->getAllUsers();
  }

  /**
   * Return a user by its id
   *
   * This function retrieves a user from the database by its id.
   *
   * @param int $id internal id of user
   *
   * @return User instance of {@link BakeryDMS\Core\User} or false
   */
  function getUser($id) {
    return $this->dms->getUser($id);
  }

  /**
   * Returns the current folder the user sees.
   *
   * @return Folder
   */
  function getCurrentFolder() {
    if (isset($_GET['folderid'])) {
      return $this->dms->getFolder($_GET['folderid']);
    }

    if (isset($_GET['documentid'])) {
      return $this->dms->getDocument($_GET['documentid'])->getFolder();
    }

    return $this->dms->getRootFolder();
  }

  /**
   * Returns the folder by its id.
   *
   * @param $folderId int The id of the folder.
   *
   * @return Folder
   */
  function getFolder($folderId) {
    return $this->dms->getFolder($folderId);
  }

  /**
   * Returns the current document the user sees.
   *
   * @return Document
   */
  function getCurrentDocument() {
    if (isset($_GET['documentid'])) {
      return $this->dms->getDocument($_GET['documentid']);

    }
    return null;
  }

  /**
   * Return a document by its id.
   *
   * @param int $documentId The id id of the document.
   *
   * @return Document instance of {@link BakeryDMS\Core\Document} or false
   */
  function getDocument($documentId) {
    return $this->dms->getDocument($documentId);
  }

  /**
   * Returns the groups of the DMS.
   *
   * @return Group[]
   */
  function getGroups() {
    return $this->dms->getAllGroups();
  }

  /**
   * Returns the a group of the DMS given by its id.
   *
   * @return Group
   */
  function getGroup($id) {
    return $this->dms->getGroup($id);
  }
}