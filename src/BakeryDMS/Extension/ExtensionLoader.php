<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Extension;

use BakeryDMS\Extension\Events\IDmsEventRegistry;
use BakeryDMS\Settings;

/**
 * Class ExtensionLoader. This loader loads all the extensions of from a given folder.
 */
class ExtensionLoader {
  /** @var string The path to the extension directory. */
  private $extensionDirectory;

  /** @var IDmsEventRegistry The event registry. */
  private $dmsEventFacade;

  /** @var IDms The DMS facade. */
  private $dmsFacade;

  /**
   * Initializes a new instance of the {@link ExtensionLoader} class.
   *
   * @param IDms $dmsFacade The facade of the dms.
   * @param IDmsEventRegistry $dmsEventFacade The registry for events.
   */
  public function __construct(IDms &$dmsFacade, IDmsEventRegistry &$dmsEventFacade) {
    // TODO: make configurable.
    $this->extensionDirectory = Settings::getInstance()->_rootDir . "extensions/";

    $this->dmsFacade = $dmsFacade;
    $this->dmsEventFacade = $dmsEventFacade;
  }

  /**
   * Loads the extensions from the extension directory.
   *
   * @return IExtension[]
   */
  public function &loadExtensions() {
    $archives = $this->getArchives();

    /** @var IExtension[] $extensions */
    $extensions = [];
    foreach ($archives as $phar) {
      /** @noinspection PhpIncludeInspection */
      require_once($this->extensionDirectory . $phar);

      // get the name from the path.
      $pathArray = explode("/", $phar);
      $name = substr(end($pathArray), 0, -5);

      $this->callExtensionInstaller($name);
      $this->callExtensionUpdater($name);
      $this->instantiateExtension($name, $extensions);
    }
    return $extensions;
  }

  /**
   * Reads all archives from a directory.
   *
   * @return string[] The archives
   */
  private function getArchives() {
    if (file_exists($this->extensionDirectory)) {
      /** @var string[] $archives */
      return array_diff(scandir($this->extensionDirectory), array('..', '.'));
    }
    return array();
  }

  /**
   * Calls the installer of the extension.
   *
   * @param string $name The name of the extension
   */
  private function callExtensionInstaller($name) {
    if (function_exists($name . "_Install")) {
      /** @var IExtensionInstaller $installer */
      $installer = call_user_func($name . "_Install");
      if (!is_null($installer)) {
        $installer->setDmsFacade($this->dmsFacade);
        $installer->install();
      }
    }
  }

  /**
   * Calls the updater of the extension.
   *
   * @param string $name The name of the extension
   */
  private function callExtensionUpdater($name) {
    if (function_exists($name . "_Update")) {
      /** @var IExtensionUpdater $updater */
      $updater = call_user_func($name . "_Update");
      if (!is_null($updater)) {
        $updater->setDmsFacade($this->dmsFacade);
        $updater->update();
      }
    }
  }

  /**
   * Creates an instance of the extension and adds it to the given collection.
   *
   * @param string $name Name of the extension
   * @param IExtension[] $extensions The collection of the extensions
   */
  private function instantiateExtension($name, &$extensions) {
    /** @var IExtension $extension */
    $extension = call_user_func($name . "_Stub");
    if (!is_null($extension)) {
      $extension->setDmsFacade($this->dmsFacade);
      $extension->setEventFacade($this->dmsEventFacade);
      $extensions[$name] = $extension;
    }
  }
}