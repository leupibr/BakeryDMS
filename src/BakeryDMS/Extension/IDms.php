<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Extension;

use BakeryDMS\Core\Document;
use BakeryDMS\Core\Folder;
use BakeryDMS\Core\Group;
use BakeryDMS\Core\Persistency\IDatabaseAccess;
use BakeryDMS\Core\User;
use BakeryDMS\Settings;

/**
 * Interface IDms. This interface is a complete abstraction of all core functions of BakeryDMS and allows
 * a extension to get core model data out from BakeryDMS.
 *
 * @package BakeryDMS\Extension
 */
interface IDms {
  /**
   * Gets the BakeryDMS settings.
   *
   * @return Settings The settings.
   */
  function getSettings();

  /**
   * Gets access to the BakeryDMS database.
   *
   * @return IDatabaseAccess The database access handler.
   */
  function getDatabaseAccess();

  /**
   * Builds the path of the current view in an extension context.
   *
   * @param $extensionName string The name of the current extension.
   * @param $viewName string The name of the current view.
   *
   * @return string The path of this view.
   */
  function buildViewPath($extensionName, $viewName);

  /**
   * Builds the path of the current operation in an extension context.
   *
   * @param $extensionName string The name of the current extension.
   * @param $operationName string The name of the current view.
   *
   * @return string The path of this view.
   */
  function buildOperationPath($extensionName, $operationName);

  /**
   * Returns the current (logged-in) user.
   *
   * @return User
   */
  function getCurrentUser();

  /**
   * Return list of all users
   *
   * @return  User[] or false
   */
  function getAllUsers();

  /**
   * Return a user by its id
   *
   * This function retrieves a user from the database by its id.
   *
   * @param int $id internal id of user
   *
   * @return User instance of {@link BakeryDMS\Core\User} or false
   */
  function getUser($id);

  /**
   * Returns the current folder the user sees.
   *
   * @return Folder
   */
  function getCurrentFolder();

  /**
   * Returns the folder by its id.
   *
   * @param $folderId int The id of the folder.
   *
   * @return Folder
   */
  function getFolder($folderId);

  /**
   * Returns the current document the user sees.
   *
   * @return Document
   */
  function getCurrentDocument();

  /**
   * Return a document by its id.
   *
   * @param int $documentId The id id of the document.
   *
   * @return Document instance of {@link BakeryDMS\Core\Document} or false
   */
  function getDocument($documentId);

  /**
   * Builds the url of the current view in an extension context.
   *
   * @param $extensionName string The name of the current extension.
   * @param $viewName string The name of the current view.
   *
   * @return string The path of this view.
   */
  function buildViewLink($extensionName, $viewName);

  /**
   * Builds the url of the current operation in an extension context.
   *
   * @param $extensionName string The name of the current extension.
   * @param $operationName string The name of the current view.
   *
   * @return string The path of this view.
   */
  function buildOperationLink($extensionName, $operationName);

  /**
   * Returns the groups of the DMS.
   *
   * @return Group[]
   */
  function getGroups();

  /**
   * Returns the a group of the DMS given by its id.
   *
   * @return Group
   */
  function getGroup($id);
}