<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Extension;

use BakeryDMS\Core\DMS;
use BakeryDMS\Extension\Events\EventFacade;

/**
 * Class ExtensionManager. Manages the extensions and can be used as single access point to address all extensions.
 *
 * @package BakeryDMS\Extension
 */
class ExtensionManager {

  /**
   * @var ExtensionManager The singleton instance.
   */
  private static $instance;

  /**
   * @var IExtension[] The extensions.
   */
  private $extensions;

  /**
   * @var DmsFacade The facade to the dms.
   */
  private $dmsFacade;

  /**
   * @var EventFacade The facade to the event registry.
   */
  private $dmsEventFacade;

  /**
   * Initializes a new instance of the {@link ExtensionManager} class.
   */
  private function __construct() {
    $this->dmsFacade = new DmsFacade();
    $this->dmsEventFacade = new EventFacade();
  }

  /**
   * Returns the singleton instance.
   *
   * @return ExtensionManager
   */
  public static function getInstance() {
    if (empty(ExtensionManager::$instance)) {
      ExtensionManager::$instance = new ExtensionManager();
    }
    return ExtensionManager::$instance;
  }

  public function loadExtensions() {
    $this->extensions = (new ExtensionLoader($this->dmsFacade, $this->dmsEventFacade))->loadExtensions();
  }

  /**
   * Get the names of the extensions.
   *
   * @return string[] The extension names.
   */
  public function getExtensionNames() {
    return array_keys($this->extensions);
  }

  /**
   * Returns all global navigation items of the extensions.
   *
   * @return IAction[] The global navigation actions.
   */
  public function getGlobalNavigationItems() {
    $navigationItems = array();
    foreach ($this->extensions as $extension) {
      $navigationItems = array_merge($navigationItems, $extension->getGlobalNavigationItems());
    }
    return $navigationItems;
  }

  /**
   * Returns all the administrative items.
   *
   * @return IAction[] The admin items.
   */
  public function getAdminItems() {
    $adminItems = array();
    foreach ($this->extensions as $extension) {
      $adminItems = array_merge($adminItems, $extension->getAdminItems());
    }
    return $adminItems;
  }

  public function getFolderItems() {
    $items = [];
    foreach ($this->extensions as $extension) {
      $items = array_merge($items, $extension->getFolderActions());
    }
    return $items;
  }

  /**
   * Returns the view of a given extension.
   *
   * @param string $extensionName The name of the extension.
   * @param string $viewName The name of the view.
   *
   * @return IView The view if it exists, otherwise <c>null</c>.
   */
  public function getView($extensionName, $viewName) {
    if (array_key_exists($extensionName, $this->extensions)) {
      $extension = $this->extensions[$extensionName];
      foreach ($extension->getViews() as $view) {
        if ($view->getName() == $viewName) {
          return $view;
        }
      }
    }
    return null;
  }

  /**
   * Returns the operation of a given extension.
   *
   * @param string $extensionName The name of the extension.
   * @param string $operationName The name of the operation.
   *
   * @return IOperation The operation if it exists, otherwise <c>null</c>.
   */
  public function getOperation($extensionName, $operationName) {
    if (array_key_exists($extensionName, $this->extensions)) {
      $extension = $this->extensions[$extensionName];
      foreach ($extension->getOperations() as $operation) {
        if ($operation->getName() == $operationName) {
          return $operation;
        }
      }
    }
    return null;
  }

  public function setCoreDms(DMS &$dms) {
    $this->dmsFacade->setCoreDms($dms);
  }

  /**
   * @return EventFacade
   */
  public function getDmsEventFacade() {
    return $this->dmsEventFacade;
  }

}