# TODO: add hooks in system for:
* op/op.AddDocument.php
* op/op.AddDocumentLink.php
* op/op.AddEvent.php
* op/op.AddFile.php
* op/op.AddFile2.php
* op/op.AddMultiDocument.php
* op/op.AddSubFolder.php
* op/op.AddToClipboard.php
* op/op.AddTransitionToWorkflow.php
* op/op.Ajax.php
* op/op.ApproveDocument.php
* op/op.AttributeMgr.php
* op/op.Categories.php
* op/op.ChangePassword.php
* op/op.ClearClipboard.php
* op/op.CreateDump.php
* op/op.CreateFolderArchive.php
* op/op.CreateStatusIndex.php
* op/op.CreateSubFolderIndex.php
* op/op.CreateVersioningFiles.php
* op/op.DefaultKeywords.php
* op/op.DocumentAccess.php
* op/op.DocumentNotify.php
* op/op.Download.php
* op/op.EditAttributes.php
* op/op.EditComment.php
* op/op.EditDocument.php
* op/op.EditEvent.php
* op/op.EditFolder.php
* op/op.EditUserData.php
* op/op.FolderAccess.php
* op/op.FolderNotify.php
* op/op.GroupMgr.php
* op/op.GroupView.php
* op/op.LockDocument.php
* op/op.Login.php
* op/op.Logout.php
* op/op.ManageNotify.php
* op/op.MoveClipboard.php
* op/op.MoveDocument.php
* op/op.MoveFolder.php
* op/op.OverrideContentStatus.php
* op/op.PasswordForgotten.php
* op/op.Preview.php
* op/op.RemoveArchive.php
* op/op.RemoveDocument.php
* op/op.RemoveDocumentFile.php
* op/op.RemoveDocumentLink.php
* op/op.RemoveDump.php
* op/op.RemoveEvent.php
* op/op.RemoveFolder.php
* op/op.RemoveFolderFiles.php
* op/op.RemoveFromClipboard.php
* op/op.RemoveLog.php
* op/op.RemoveTransitionFromWorkflow.php
* op/op.RemoveVersion.php
* op/op.RemoveWorkflow.php
* op/op.RemoveWorkflowAction.php
* op/op.RemoveWorkflowFromDocument.php
* op/op.RemoveWorkflowState.php
* op/op.ResetSu.php
* op/op.ReturnFromSubWorkflow.php
* op/op.ReviewDocument.php
* op/op.RunSubWorkflow.php
* op/op.Search.php
* op/op.SearchFulltext.php
* op/op.SetExpires.php
* op/op.SetLanguage.php
* op/op.SetReviewersApprovers.php
* op/op.Settings.php
* op/op.SetWorkflow.php
* op/op.SubstituteUser.php
* op/op.UnlockDocument.php
* op/op.UpdateDocument.php
* op/op.UpdateDocument2.php
* op/op.UserDefaultKeywords.php
* op/op.UsrMgr.php
* op/op.ViewOnline.php
* op/op.WorkflowActionsMgr.php
* op/op.WorkflowMgr.php
* op/op.WorkflowStatesMgr.php

# CHECK: check this... maybe call add document multiple times.
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.AddMultiDocument.php

# CHECK: management operations, maybe not necessary
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.CreateStatusIndex.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.CreateFolderArchive.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.RemoveArchive.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.CreateVersioningFiles.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.CreateSubFolderIndex.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.AddTransitionToWorkflow.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.RemoveWorkflow.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.RemoveWorkflowAction.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.RemoveWorkflowFromDocument.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.RemoveWorkflowState.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.RemoveDump.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.WorkflowActionsMgr.php  | addworkflowaction, removeworkflowaction editworkflowaction
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.WorkflowMgr.php | addworkflow, removeworkflow, editworkflow
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.WorkflowStatesMgr.php |  addworkflowstate, removeworkflowstate, editworkflowstate
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.AttributeMgr.php | addattrdef, removeattrdef, editattrdef
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.CreateDump.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.DefaultKeywords.php | addcategory, removecategory, editcategory

# CHECK: whats this?
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.Ajax.php

# TODO: implement add to interface
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.Download.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.FolderNotify.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.GroupMgr.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.GroupView.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.ManageNotify.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.OverrideContentStatus.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.PasswordForgotten.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.Preview.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.RemoveFolderFiles.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.RemoveLog.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.RemoveTransitionFromWorkflow.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.ResetSu.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.ReturnFromSubWorkflow.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.ReviewDocument.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.RunSubWorkflow.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.SetExpires.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.SetLanguage.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.SetReviewersApprovers.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.Settings.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.SetWorkflow.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.SubstituteUser.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.UserDefaultKeywords.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.UsrMgr.php
* /Users/leupibr/Documents/leupigit/ch_tsb_seeddms/op/op.ViewOnline.php
