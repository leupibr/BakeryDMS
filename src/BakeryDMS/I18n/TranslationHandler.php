<?php


namespace BakeryDMS\I18n;

use BakeryDMS\Extension\ExtensionManager;
use BakeryDMS\Settings;

class TranslationHandler {

  /** @var TranslationHandler The translation handler */
  private static $instance;

  /** @var Settings The dms settings. */
  private $settings;

  /** @var  string The current language. */
  private $language;

  /** @var array The core translations. */
  private $coreTranslations;

  /** @var array[array] The translations of the extensions. */
  private $extensionTranslations;

  /**
   * TranslationHandler constructor.
   */
  public function __construct() {
    $this->settings = Settings::getInstance();
    $this->setLanguage();

    $this->loadTranslations();
  }

  /**
   * Returns the singleton instance of the {@see TranslationHandler}.
   * @return TranslationHandler The translation handler.
   */
  public static function getInstance() {
    if (!isset(TranslationHandler::$instance)) {
      TranslationHandler::$instance = new TranslationHandler();
    }
    return TranslationHandler::$instance;
  }

  /**
   * Translates the given key to the configured language.
   *
   * @param $key string The key of the string to translate.
   *
   * @return string The translated key.
   */
  public function translate($key) {
    if (array_key_exists($key, $this->coreTranslations)) {
      return $this->coreTranslations[$key];
    }

    foreach ($this->extensionTranslations as $translationSet) {
      if (array_key_exists($key, $translationSet)) {
        return $translationSet[$key];
      }
    }

    return "**" . $key . "**";
  }

  /**
   * Substitutes the wildcards in a text with the given parameters.
   *
   * @param $text string The text with the items to substitute.
   * @param $parameters array The items to replace as key-value pairs.
   *
   * @return string The substituted string.
   */
  public function substitute($text, $parameters) {
    $result = $text;
    foreach ($parameters as $key => $value) {
      $result = str_replace("[" . $key . "]", $value, $result);
    }
    return $result;
  }

  /**
   * Loads the translations.
   */
  private function loadTranslations() {
    $coreFile = $this->settings->_rootDir . "languages/" . $this->language . "/" . "core.json";
    $this->coreTranslations = json_decode(file_get_contents($coreFile), true);

    $this->extensionTranslations = [];
    $extensions = ExtensionManager::getInstance()->getExtensionNames();
    foreach ($extensions as $extension) {
      $pharName = $this->settings->_rootDir . "extensions/" . $extension . ".phar";
      $pharPath = "languages/" . $this->language . "/translation.json";
      $phar = new \Phar($pharName, 0, $extension . ".phar");
      if (isset($phar[$pharPath])) {
        $this->extensionTranslations[$extension] =
          json_decode(file_get_contents("phar://" . $pharName . "/" . $pharPath), true);
      }
    }
  }

  /**
   * Sets the language.
   */
  private function setLanguage() {
    /** @var $session \SeedDMS_Session */
    global $session;

    if (!empty($session)) {
      $this->language = $session->getLanguage();
    }
    else {
      $this->language = $this->settings->_language;
    }
  }
}

