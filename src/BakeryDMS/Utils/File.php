<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Utils;

/**
 * Class to represent a user in the document management system.
 */
class File {
  static function renameFile($old, $new) {
    return @rename($old, $new);
  }

  static function removeFile($file) {
    return @unlink($file);
  }

  static function copyFile($source, $target) {
    return @copy($source, $target);
  }

  static function moveFile($source, $target) {
    if (!@copyFile($source, $target)) {
      return false;
    }
    return @removeFile($source);
  }

  static function fileSize($file) {
    if (!$a = fopen($file, 'r')) {
      return false;
    }
    fseek($a, 0, SEEK_END);
    $filesize = ftell($a);
    fclose($a);
    return $filesize;
  }

  static function format_filesize($size,
                                  $sizes = array('Bytes', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB')) {
    if ($size == 0) {
      return ('0 Bytes');
    }
    return (round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $sizes[$i]);
  }

  static function parse_filesize($str) {
    preg_replace('/\s\s+/', ' ', $str);
    if (strtoupper(substr($str, -1)) == 'B') {
      $value = (int) substr($str, 0, -2);
      $unit = substr($str, -2, 1);
    }
    else {
      $value = (int) substr($str, 0, -1);
      $unit = substr($str, -1);
    }
    switch (strtoupper($unit)) {
      case 'G':
        return $value * 1024 * 1024 * 1024;
        break;
      case 'M':
        return $value * 1024 * 1024;
        break;
      case 'K':
        return $value * 1024;
        break;
      default;
        return $value;
        break;
    }
  }

  static function checksum($file) {
    return md5_file($file);
  }

  static function renameDir($old, $new) {
    return @rename($old, $new);
  }

  static function makeDir($path) {

    if (!is_dir($path)) {
      $res = @mkdir($path, 0777, true);
      if (!$res) {
        return false;
      }
    }

    return true;
  }

  static function removeDir($path) {
    $handle = @opendir($path);
    while ($entry = @readdir($handle)) {
      if ($entry == ".." || $entry == ".") {
        continue;
      }
      else if (is_dir($path . $entry)) {
        if (!self::removeDir($path . $entry . "/")) {
          return false;
        }
      }
      else {
        if (!@unlink($path . $entry)) {
          return false;
        }
      }
    }
    @closedir($handle);
    return @rmdir($path);
  }

  static function copyDir($sourcePath, $targetPath) {
    if (mkdir($targetPath, 0777)) {
      $handle = @opendir($sourcePath);
      while ($entry = @readdir($handle)) {
        if ($entry == ".." || $entry == ".") {
          continue;
        }
        else if (is_dir($sourcePath . $entry)) {
          if (!self::copyDir($sourcePath . $entry . "/", $targetPath . $entry . "/")) {
            return false;
          }
        }
        else {
          if (!@copy($sourcePath . $entry, $targetPath . $entry)) {
            return false;
          }
        }
      }
      @closedir($handle);
    }
    else {
      return false;
    }

    return true;
  }

  static function moveDir($sourcePath, $targetPath) {
    if (!copyDir($sourcePath, $targetPath)) {
      return false;
    }
    return removeDir($sourcePath);
  }

  // code by Kioob (php.net manual)
  static function gzcompressfile($source, $level = false) {
    $dest = $source . '.gz';
    $mode = 'wb' . $level;
    $error = false;
    if ($fp_out = @gzopen($dest, $mode)) {
      if ($fp_in = @fopen($source, 'rb')) {
        while (!feof($fp_in)) {
          @gzwrite($fp_out, fread($fp_in, 1024 * 512));
        }
        @fclose($fp_in);
      }
      else {
        $error = true;
      }
      @gzclose($fp_out);
    }
    else {
      $error = true;
    }

    if ($error) {
      return false;
    }
    else {
      return $dest;
    }
  }
}