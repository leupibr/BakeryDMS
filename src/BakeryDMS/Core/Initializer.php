<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;


use BakeryDMS\Settings;

class Initializer {
  /**
   * @var Settings The settings for initializing.
   */
  private $settings;

  /**
   * Initializes a new instance of the {@link Initializer} class.
   * Initializer constructor.
   */
  public function __construct(Settings &$settings) {
    $this->settings = $settings;
  }

  /**
   * Initializes the system.
   */
  public function initialize() {
    $this->checkInstallation();
    $this->initExtraPath();
    $this->initExecutionTime();
    $this->stripSlashes();
  }

  /**
   * Checks the installation and the installation file.
   */
  private function checkInstallation() {
    if (!defined('SEEDDMS_INSTALL') && file_exists(dirname($this->settings->_configFilePath) . '/ENABLE_INSTALL_TOOL')) {
      die('SeedDMS won\'t run unless your remove the file ENABLE_INSTALL_TOOL from your configuration directory.');
    }
  }

  /**
   * Initializes the extra path.
   */
  private function initExtraPath() {
    if (isset($this->settings->_extraPath)) {
      ini_set('include_path', $this->settings->_extraPath . PATH_SEPARATOR . ini_get('include_path'));
    }
  }

  /**
   * Initializes the execution time.
   */
  private function initExecutionTime() {
    if (isset($this->settings->_maxExecutionTime)) {
      ini_set('max_execution_time', $this->settings->_maxExecutionTime);
    }
  }

  /**
   * Strips the slashes from the parameters.
   */
  private function stripSlashes() {
    if (get_magic_quotes_gpc()) {
      $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
      while (list($key, $val) = each($process)) {
        foreach ($val as $k => $v) {
          unset($process[$key][$k]);
          if (is_array($v)) {
            $process[$key][stripslashes($k)] = $v;
            $process[] = &$process[$key][stripslashes($k)];
          }
          else {
            $process[$key][stripslashes($k)] = stripslashes($v);
          }
        }
      }
      unset($process);
    }
  }
}