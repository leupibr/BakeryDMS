<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

class DocumentState {
  const DRAFT_REV = 0; // The different states a document can be in.
  const DRAFT_APP = 1; //Document is in approval state. A document is in approval state when it needs to be approved by a user or group.
  const RELEASED = 2; // Document is released. A document is in release state either when it needs no review or approval after uploaded or has been reviewed and/or approved..
  const IN_WORKFLOW = 3; // Document is in workflow. A document is in workflow if a workflow has been started and has not reached a final state.
  const REJECTED = -1; //  Document was rejected. A document is in rejected state when the review failed or approval was not given.
  const OBSOLETE = -2; //  Document is obsolete. A document can be obsoleted once it was released.
  const EXPIRED = -3; // Document is expired. A document expires when the expiration date is reached
}