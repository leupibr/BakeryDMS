<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

/**
 * Class to represent a link between two document.
 *
 * Document links are to establish a reference from one document to
 * another document. The owner of the document link may not be the same
 * as the owner of one of the documents.
 * Use {@link BakeryDMS\Core\Document::addDocumentLink()} to add a reference
 * to another document.
 */
class DocumentLink {
  /** @var int internal id of document link. */
  protected $id;

  /** @var \BakeryDMS\Core\Document reference to document this link belongs to. */
  private $document;

  /** @var \BakeryDMS\Core\Document reference to target document this link points to. */
  private $target;

  /** @var int id of user who is the owner of this link. */
  private $userID;

  /** @var int 1 if this link is public, or 0 if is only visible to the owner. */
  private $isPublic;

  /**
   * Initializes a new instance of the {@link DocumentLink} class.
   *
   * @param $id int The internal id of the link.
   * @param $document \BakeryDMS\Core\Document The document the link belongs to.
   * @param $target \BakeryDMS\Core\Document The document where this link points to.
   * @param $userID int The id of the user.
   * @param $isPublic int 1 if this link is public, otherwise 0.
   */
  function __construct($id, $document, $target, $userID, $isPublic) {
    $this->id = $id;
    $this->document = $document;
    $this->target = $target;
    $this->userID = $userID;
    $this->isPublic = $isPublic;
  }

  function getID() {
    return $this->id;
  }

  function getDocument() {
    return $this->document;
  }

  function getTarget() {
    return $this->target;
  }

  function getUser() {
    if (!isset($this->_user)) {
      $this->_user = $this->document->_dms->getUser($this->userID);
    }
    return $this->_user;
  }

  function isPublic() {
    return $this->isPublic;
  }
}