<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core\Persistency;


/**
 * Class to represent the database access for the document management
 * This class uses PDO for the actual database access.
 *
 * @see http://php.net/manual/en/book.pdo.php for further information.
 */
interface IDatabaseAccess {
  /**
   * Return list of all database tables
   *
   * This function is used to retrieve a list of database tables for backup
   *
   * @return array list of table names
   */
  function TableList();

  /**
   * Connect to database
   *
   * @return boolean true if connection could be established, otherwise false
   */
  function connect();

  /**
   * Make sure a database connection exisits
   *
   * This function checks for a database connection. If it does not exists
   * it will reconnect.
   *
   * @return boolean true if connection is established, otherwise false
   */
  function ensureConnected();

  /**
   * Sanitize String used in database operations
   *
   * @param string text
   * @return string sanitized string
   */
  function qstr($text);

  /**
   * Execute SQL query and return result
   *
   * Call this function only with sql query which return data records.
   *
   * @param string $queryStr sql query
   * @return array/boolean data if query could be executed otherwise false
   */
  function getResultArray($queryStr);

  /**
   * Execute SQL query and return result as an object array.
   *
   * Call this function only with sql query which return data records.
   *
   * @param string $queryStr sql query
   * @param string $className name of the created class
   * @return array data if query could be executed otherwise empty array
   */
  function fetchAllObject($queryStr, $className = "stdClass");

  /**
   * Execute SQL query
   *
   * Call this function only with sql query which do not return data records.
   *
   * @param string $queryStr sql query
   * @param boolean $silent not used anymore. This was used when this method
   *        still issued an error message
   * @return boolean true if query could be executed otherwise false
   */
  function getResult($queryStr, $silent = false);

  function startTransaction();

  function rollbackTransaction();

  function commitTransaction();

  /**
   * Return the id of the last instert record
   *
   * @return integer id used in last autoincrement
   */
  function getInsertID();

  function getErrorMsg();

  function getErrorNo();

  /**
   * Create various temporary tables to speed up and simplify sql queries
   */
  function createTemporaryTable($tableName, $override = false);

  /**
   * Return sql statement for extracting the date part from a field
   * containing a unix timestamp
   *
   * @param string $fieldname name of field containing the timestamp
   * @return string sql code
   */
  function getDateExtract($fieldname, $format = '%Y-%m-%d');
}