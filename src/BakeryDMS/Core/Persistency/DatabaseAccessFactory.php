<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core\Persistency;

define('USE_PDO', 1);

class DatabaseAccessFactory {
  /**
   * Factory method for the correct database access class.
   *
   * @param $driver string The database type e.g. mysql, sqlite
   * @param $hostname string Host of database server
   * @param $user string Name of user having access to database
   * @param $password string Password of user
   * @param $database string|bool Name of database or false if default database should be used.
   *
   * @return IDatabaseAccess The database.
   *   access instance.
   */
  static function createDatabaseAccess($driver, $hostname, $user, $password, $database = false) {
    if (defined('USE_PDO')) {
      return new PdoDatabaseAccess($driver, $hostname, $user, $password, $database);
    }
    else {
      return new AdoDatabaseAccess($driver, $hostname, $user, $password, $database);
    }

  }
}