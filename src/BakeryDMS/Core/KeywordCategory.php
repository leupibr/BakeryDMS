<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

/**
 * Class to represent a keyword category in the document management system.
 */
class KeywordCategory {
  /**
   * @var int id id of keyword category
   */
  private $id;

  /**
   * @var int id of user who is the owner
   */
  private $ownerID;

  /**
   * @var User The owner of the category.
   */
  private $_owner;

  /**
   * @var string Name name of category
   */
  private $name;

  /**
   * @var \BakeryDMS\Core\DMS reference to dms this category belongs to
   */
  private $dms;

  /**
   * Initializes a new instance of the {@link KeywordCategory} class.
   *
   * @param $id int Id of the keyword category.
   * @param $ownerID int Owner of the keyword category.
   * @param $name string Name of the keyword category.
   */
  function __construct($id, $ownerID, $name) {
    $this->id = $id;
    $this->name = $name;
    $this->ownerID = $ownerID;
    $this->dms = null;
  }

  function setDMS($dms) {
    $this->dms = $dms;
  }

  function getID() {
    return $this->id;
  }

  function getName() {
    return $this->name;
  }

  function getOwner() {
    if (!isset($this->_owner)) {
      $this->_owner = $this->dms->getUser($this->ownerID);
    }
    return $this->_owner;
  }

  function setName($newName) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblKeywordCategories SET name = " . $db->qstr($newName) . " WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->name = $newName;
    return true;
  }

  function setOwner($user) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblKeywordCategories SET owner = " . $user->getID() . " WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->ownerID = $user->getID();
    $this->_owner = $user;
    return true;
  }

  function getKeywordLists() {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblKeywords WHERE category = " . $this->id . " ORDER BY `keywords`";
    return $db->getResultArray($queryStr);
  }

  function editKeywordList($listID, $keywords) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblKeywords SET keywords = " . $db->qstr($keywords) . " WHERE id = $listID";
    return $db->getResult($queryStr);
  }

  function addKeywordList($keywords) {
    $db = $this->dms->getDB();

    $queryStr = "INSERT INTO tblKeywords (category, keywords) VALUES (" . $this->id . ", " . $db->qstr($keywords) . ")";
    return $db->getResult($queryStr);
  }

  function removeKeywordList($listID) {
    $db = $this->dms->getDB();

    $queryStr = "DELETE FROM tblKeywords WHERE id = $listID";
    return $db->getResult($queryStr);
  }

  function remove() {
    $db = $this->dms->getDB();

    $db->startTransaction();
    $queryStr = "DELETE FROM tblKeywords WHERE category = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $queryStr = "DELETE FROM tblKeywordCategories WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $db->commitTransaction();
    return true;
  }
}

?>
