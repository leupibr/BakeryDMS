<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

/**
 * Class to represent a generic object in the document management system.
 * This is the base class for generic objects in SeedDMS.
 */
class Object {
  /**
   * @var integer unique id of object
   */
  protected $_id;

  /**
   * @var array list of attributes
   */
  protected $_attributes;

  /**
   * @var object back reference to document management system
   */
  public $_dms;

  function __construct($id) {
    $this->_id = $id;
    $this->_dms = null;
  }

  /**
   * Set dms this object belongs to.
   *
   * Each object needs a reference to the dms it belongs to. It will be
   * set when the object is created.
   * The dms has a references to the currently logged in user
   * and the database connection.
   *
   * @param object $dms reference to dms
   */
  function setDMS($dms) {
    $this->_dms = $dms;
  }

  /**
   * Return the internal id of the document
   *
   * @return integer id of document
   */
  function getID() {
    return $this->_id;
  }

  /**
   * Returns all attributes set for the object
   *
   * @return  Attribute[] list of attributes
   */
  function getAttributes() {
    if (!$this->_attributes) {
      $db = $this->_dms->getDB();

      switch (get_class($this)) {
        case Document::class:
          $queryStr = "SELECT * FROM tblDocumentAttributes WHERE document = " . $this->_id . " ORDER BY `id`";
          break;
        case DocumentContent::class:
          $queryStr = "SELECT * FROM tblDocumentContentAttributes WHERE content = " . $this->_id . " ORDER BY `id`";
          break;
        case Folder::class:
          $queryStr = "SELECT * FROM tblFolderAttributes WHERE folder = " . $this->_id . " ORDER BY `id`";
          break;
        default:
          return false;
      }
      $resArr = $db->getResultArray($queryStr);
      if (is_bool($resArr) && !$resArr) {
        return false;
      }

      $this->_attributes = array();

      foreach ($resArr as $row) {
        $attrdef = $this->_dms->getAttributeDefinition($row['attrdef']);
        $attr = new Attribute($row["id"], $this, $attrdef, $row["value"]);
        $attr->setDMS($this->_dms);
        $this->_attributes[$attrdef->getId()] = $attr;
      }
    }
    return $this->_attributes;

  }

  /**
   * Returns an attribute of the object for the given attribute definition
   *
   * @return array|string value of attritbute or false. The value is an array
   * if the attribute is defined as multi value
   */
  function getAttributeValue($attrdef) {
    if (!$this->_attributes) {
      $this->getAttributes();
    }

    if (isset($this->_attributes[$attrdef->getId()])) {
      $value = $this->_attributes[$attrdef->getId()]->getValue();
      if ($attrdef->getMultipleValues()) {
        $sep = substr($value, 0, 1);
        return (explode($sep, substr($value, 1)));
      }
      else {
        return $value;
      }
    }
    else {
      return false;
    }

  }

  /**
   * Set an attribute of the object for the given attribute definition
   *
   * @param object $attrdef definition of attribute
   * @param array|string $value value of attribute, for multiple values this
   * must be an array
   * @return boolean true if operation was successful, otherwise false
   */
  function setAttributeValue($attrdef, $value) {
    $db = $this->_dms->getDB();
    if (!$this->_attributes) {
      $this->getAttributes();
    }
    if ($attrdef->getMultipleValues() && is_array($value)) {
      $sep = substr($attrdef->getValueSet(), 0, 1);
      $value = $sep . implode($sep, $value);
    }
    if (!isset($this->_attributes[$attrdef->getId()])) {
      switch (get_class($this)) {
        case Document::class:
          $queryStr = "INSERT INTO tblDocumentAttributes (document, attrdef, value) VALUES (" . $this->_id . ", " . $attrdef->getId() . ", " . $db->qstr($value) . ")";
          break;
        case DocumentContent::class:
          $queryStr = "INSERT INTO tblDocumentContentAttributes (content, attrdef, value) VALUES (" . $this->_id . ", " . $attrdef->getId() . ", " . $db->qstr($value) . ")";
          break;
        case Folder::class:
          $queryStr = "INSERT INTO tblFolderAttributes (folder, attrdef, value) VALUES (" . $this->_id . ", " . $attrdef->getId() . ", " . $db->qstr($value) . ")";
          break;
        default:
          return false;
      }
      $res = $db->getResult($queryStr);
      if (!$res) {
        return false;
      }

      $attr = new Attribute($db->getInsertID(), $this, $attrdef, $value);
      $attr->setDMS($this->_dms);
      $this->_attributes[$attrdef->getId()] = $attr;
      return true;
    }

    $this->_attributes[$attrdef->getId()]->setValue($value);

    return true;
  }

  /**
   * Remove an attribute of the object for the given attribute definition
   *
   * @return boolean true if operation was successful, otherwise false
   */
  function removeAttribute($attrdef) {
    $db = $this->_dms->getDB();
    if (!$this->_attributes) {
      $this->getAttributes();
    }
    if (isset($this->_attributes[$attrdef->getId()])) {
      switch (get_class($this)) {
        case Document::class:
          $queryStr = "DELETE FROM tblDocumentAttributes WHERE document=" . $this->_id . " AND attrdef=" . $attrdef->getId();
          break;
        case DocumentContent::class:
          $queryStr = "DELETE FROM tblDocumentContentAttributes WHERE content=" . $this->_id . " AND attrdef=" . $attrdef->getId();
          break;
        case Folder::class:
          $queryStr = "DELETE FROM tblFolderAttributes WHERE folder=" . $this->_id . " AND attrdef=" . $attrdef->getId();
          break;
        default:
          return false;
      }
      $res = $db->getResult($queryStr);
      if (!$res) {
        return false;
      }

      unset($this->_attributes[$attrdef->getId()]);
    }
    return true;
  }
}

?>
