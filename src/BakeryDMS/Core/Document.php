<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/*
 * Document is in review state. A document is in review state when
 * it needs to be reviewed by a user or group.
 */
namespace BakeryDMS\Core;

use BakeryDMS\Utils\File;

/**
 * Class to represent a document in the document management system.
 *
 * A document in SeedDMS is similar to files in a regular file system.
 * Documents may have any number of content elements
 * ({@link BakeryDMS\Core\DocumentContent}). These content elements are often
 * called versions ordered in a timely manner. The most recent content element
 * is the current version.
 *
 * Documents can be linked to other documents and can have attached files.
 * The document content can be anything that can be stored in a regular
 * file.
 */
class Document extends Object {
  /**
   * @var string name of document
   */
  protected $_name;

  /**
   * @var string comment of document
   */
  protected $_comment;

  /**
   * @var integer unix timestamp of creation date
   */
  protected $_date;

  /**
   * @var integer id of user who is the owner
   */
  protected $_ownerID;

  /**
   * @var integer id of folder this document belongs to
   */
  protected $_folderID;

  /**
   * @var integer timestamp of expiration date
   */
  protected $_expires;

  /**
   * @var boolean true if access is inherited, otherwise false
   */
  protected $_inheritAccess;

  /**
   * @var integer default access if access rights are not inherited
   */
  protected $_defaultAccess;

  /**
   * @var array list of notifications for users and groups
   */
  protected $_readAccessList;

  /**
   * @var array list of notifications for users and groups
   */
  public $_notifyList;

  /**
   * @var boolean true if document is locked, otherwise false
   */
  protected $_locked;

  /**
   * @var string list of keywords
   */
  protected $_keywords;

  /**
   * @var array list of categories
   */
  protected $_categories;

  /**
   * @var integer position of document within the parent folder
   */
  protected $_sequence;

  function __construct($id,
                       $name,
                       $comment,
                       $date,
                       $expires,
                       $ownerID,
                       $folderID,
                       $inheritAccess,
                       $defaultAccess,
                       $locked,
                       $keywords,
                       $sequence) {
    parent::__construct($id);
    $this->_name = $name;
    $this->_comment = $comment;
    $this->_date = $date;
    $this->_expires = $expires;
    $this->_ownerID = $ownerID;
    $this->_folderID = $folderID;
    $this->_inheritAccess = $inheritAccess;
    $this->_defaultAccess = $defaultAccess;
    $this->_locked = ($locked == null || $locked == '' ? -1 : $locked);
    $this->_keywords = $keywords;
    $this->_sequence = $sequence;
    $this->_categories = array();
    $this->_notifyList = array();
  }

  /*
	 * Return the directory of the document in the file system relativ
	 * to the contentDir
	 *
	 * @return string directory of document
	 */
  function getDir() {
    if ($this->_dms->maxDirID) {
      $dirid = (int)(($this->_id - 1) / $this->_dms->maxDirID) + 1;
      return $dirid . "/" . $this->_id . "/";
    }
    else {
      return $this->_id . "/";
    }
  }

  /*
	 * Return the name of the document
	 *
	 * @return string name of document
	 */
  function getName() {
    return $this->_name;
  }

  /*
	 * Set the name of the document
	 *
	 * @param $newName string new name of document
	 */
  function setName($newName) {
    $db = $this->_dms->getDB();

    $queryStr = "UPDATE tblDocuments SET name = " . $db->qstr($newName) . " WHERE id = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->_name = $newName;
    return true;
  }

  /*
	 * Return the comment of the document
	 *
	 * @return string comment of document
	 */
  function getComment() {
    return $this->_comment;
  }

  /*
	 * Set the comment of the document
	 *
	 * @param $newComment string new comment of document
	 */
  function setComment($newComment) {
    $db = $this->_dms->getDB();

    $queryStr = "UPDATE tblDocuments SET comment = " . $db->qstr($newComment) . " WHERE id = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->_comment = $newComment;
    return true;
  }

  function getKeywords() {
    return $this->_keywords;
  }

  function setKeywords($newKeywords) {
    $db = $this->_dms->getDB();

    $queryStr = "UPDATE tblDocuments SET keywords = " . $db->qstr($newKeywords) . " WHERE id = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->_keywords = $newKeywords;
    return true;
  }

  /**
   * Retrieve a list of all categories this document belongs to
   *
   * @return array list of category objects
   */
  function getCategories() {
    $db = $this->_dms->getDB();

    if (!$this->_categories) {
      $queryStr = "SELECT * FROM tblCategory WHERE id IN (SELECT categoryID FROM tblDocumentCategory WHERE documentID = " . $this->_id . ")";
      $resArr = $db->getResultArray($queryStr);
      if (is_bool($resArr) && !$resArr) {
        return false;
      }

      foreach ($resArr as $row) {
        $cat = new DocumentCategory($row['id'], $row['name']);
        $cat->setDMS($this->_dms);
        $this->_categories[] = $cat;
      }
    }
    return $this->_categories;
  }

  /**
   * Set a list of categories for the document
   * This function will delete currently assigned categories and sets new
   * categories.
   *
   * @param array $newCategories list of category objects
   */
  function setCategories($newCategories) {
    $db = $this->_dms->getDB();

    $db->startTransaction();
    $queryStr = "DELETE FROM tblDocumentCategory WHERE documentID = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    foreach ($newCategories as $cat) {
      $queryStr = "INSERT INTO tblDocumentCategory (categoryID, documentID) VALUES (" . $cat->getId() . ", " . $this->_id . ")";
      if (!$db->getResult($queryStr)) {
        $db->rollbackTransaction();
        return false;
      }
    }

    $db->commitTransaction();
    $this->_categories = $newCategories;
    return true;
  }

  /**
   * Return creation date of the document
   *
   * @return integer unix timestamp of creation date
   */
  function getDate() {
    return $this->_date;
  }

  /**
   * Return the parent folder of the document
   *
   * @return Folder parent folder
   */
  function getFolder() {
    if (!isset($this->_folder)) {
      $this->_folder = $this->_dms->getFolder($this->_folderID);
    }
    return $this->_folder;
  }

  /**
   * Set folder of a document
   *
   * This function basically moves a document from a folder to another
   * folder.
   *
   * @param object $newFolder
   * @return boolean false in case of an error, otherwise true
   */
  function setFolder($newFolder) {
    $db = $this->_dms->getDB();

    $queryStr = "UPDATE tblDocuments SET folder = " . $newFolder->getID() . " WHERE id = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      return false;
    }
    $this->_folderID = $newFolder->getID();
    $this->_folder = $newFolder;

    // Make sure that the folder search path is also updated.
    $path = $newFolder->getPath();
    $flist = "";
    foreach ($path as $f) {
      $flist .= ":" . $f->getID();
    }
    if (strlen($flist) > 1) {
      $flist .= ":";
    }
    $queryStr = "UPDATE tblDocuments SET folderList = '" . $flist . "' WHERE id = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    return true;
  }

  /**
   * Return owner of document
   *
   * @return User owner of document.
   */
  function getOwner() {
    if (!isset($this->_owner)) {
      $this->_owner = $this->_dms->getUser($this->_ownerID);
    }
    return $this->_owner;
  }

  /**
   * Set owner of a document
   *
   * @param object $newOwner new owner
   * @return boolean true if successful otherwise false
   */
  function setOwner($newOwner) {
    $db = $this->_dms->getDB();

    $queryStr = "UPDATE tblDocuments SET owner = " . $newOwner->getID() . " WHERE id = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->_ownerID = $newOwner->getID();
    $this->_owner = $newOwner;
    return true;
  }

  function getDefaultAccess() {
    if ($this->inheritsAccess()) {
      $res = $this->getFolder();
      if (!$res) {
        return false;
      }
      return $this->_folder->getDefaultAccess();
    }
    return $this->_defaultAccess;
  }

  function setDefaultAccess($mode) {
    $db = $this->_dms->getDB();

    $queryStr = "UPDATE tblDocuments SET defaultAccess = " . (int)$mode . " WHERE id = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->_defaultAccess = $mode;

    // If any of the notification subscribers no longer have read access,
    // remove their subscription.
    if (isset($this->_notifyList["users"])) {
      foreach ($this->_notifyList["users"] as $u) {
        if ($this->getAccessMode($u) < AccessMode::READ) {
          $this->removeNotify($u->getID(), true);
        }
      }
    }
    if (isset($this->_notifyList["groups"])) {
      foreach ($this->_notifyList["groups"] as $g) {
        if ($this->getGroupAccessMode($g) < AccessMode::READ) {
          $this->removeNotify($g->getID(), false);
        }
      }
    }

    return true;
  }

  function inheritsAccess() {
    return $this->_inheritAccess;
  }

  /**
   * Set inherited access mode
   * Setting inherited access mode will set or unset the internal flag which
   * controls if the access mode is inherited from the parent folder or not.
   * It will not modify the
   * access control list for the current object. It will remove all
   * notifications of users which do not even have read access anymore
   * after setting or unsetting inherited access.
   *
   * @param boolean $inheritAccess set to true for setting and false for
   *        unsetting inherited access mode
   * @return boolean true if operation was successful otherwise false
   */
  function setInheritAccess($inheritAccess) {
    $db = $this->_dms->getDB();

    $queryStr = "UPDATE tblDocuments SET inheritAccess = " . ($inheritAccess ? "1" : "0") . " WHERE id = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->_inheritAccess = ($inheritAccess ? "1" : "0");

    // If any of the notification subscribers no longer have read access,
    // remove their subscription.
    if (isset($this->_notifyList["users"])) {
      foreach ($this->_notifyList["users"] as $u) {
        if ($this->getAccessMode($u) < AccessMode::READ) {
          $this->removeNotify($u->getID(), true);
        }
      }
    }
    if (isset($this->_notifyList["groups"])) {
      foreach ($this->_notifyList["groups"] as $g) {
        if ($this->getGroupAccessMode($g) < AccessMode::READ) {
          $this->removeNotify($g->getID(), false);
        }
      }
    }

    return true;
  }

  /**
   * Check if document expires
   *
   * @return boolean true if document has expiration date set, otherwise false
   */
  function expires() {
    if (intval($this->_expires) == 0) {
      return false;
    }
    else {
      return true;
    }
  }

  /**
   * Get expiration time of document
   *
   * @return integer/boolean expiration date as unix timestamp or false
   */
  function getExpires() {
    if (intval($this->_expires) == 0) {
      return false;
    }
    else {
      return $this->_expires;
    }
  }

  /**
   * Set expiration date as unix timestamp
   *
   * @param integer unix timestamp of expiration date
   */
  function setExpires($expires) {
    $db = $this->_dms->getDB();

    $expires = (!$expires) ? 0 : $expires;

    if ($expires == $this->_expires) {
      // No change is necessary.
      return true;
    }

    $queryStr = "UPDATE tblDocuments SET expires = " . (int)$expires . " WHERE id = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->_expires = $expires;
    return true;
  }

  /**
   * Check if the document has expired
   *
   * @return boolean true if document has expired otherwise false
   */
  function hasExpired() {
    if (intval($this->_expires) == 0) {
      return false;
    }
    if (time() > $this->_expires + 24 * 60 * 60) {
      return true;
    }
    return false;
  }

  /**
   * Check if the document has expired and set the status accordingly
   * It will also recalculate the status if the current status is
   * set to BakeryDMS\Core\DocumentState::EXPIRED but the document isn't actually expired.
   * The method will update the document status log database table
   * if needed.
   * FIXME: some left over reviewers/approvers are in the way if
   * no workflow is set and traditional workflow mode is on. In that
   * case the status is set to BakeryDMS\Core\DocumentState::DRAFT_REV or BakeryDMS\Core\DocumentState::DRAFT_APP
   *
   * @return boolean true if status has changed
   */
  function verifyLastestContentExpriry() {
    $lc = $this->getLatestContent();
    if ($lc) {
      $st = $lc->getStatus();

      if (($st["status"] == DocumentState::DRAFT_REV || $st["status"] == DocumentState::DRAFT_APP || $st["status"] == DocumentState::IN_WORKFLOW || $st["status"] == DocumentState::RELEASED) && $this->hasExpired()) {
        return $lc->setStatus(DocumentState::EXPIRED, "", $this->getOwner());
      }
      elseif ($st["status"] == DocumentState::EXPIRED && !$this->hasExpired()) {
        $lc->verifyStatus(true, $this->getOwner());
        return true;
      }
    }
    return false;
  }

  /**
   * Check if document is locked
   *
   * @return boolean true if locked otherwise false
   */
  function isLocked() {
    return $this->_locked != -1;
  }

  /**
   * Lock or unlock document
   *
   * @param $falseOrUser user object for locking or false for unlocking
   * @return boolean true if operation was successful otherwise false
   */
  function setLocked($falseOrUser) {
    $db = $this->_dms->getDB();

    $lockUserID = -1;
    if (is_bool($falseOrUser) && !$falseOrUser) {
      $queryStr = "DELETE FROM tblDocumentLocks WHERE document = " . $this->_id;
    }
    else {
      if (is_object($falseOrUser)) {
        $queryStr = "INSERT INTO tblDocumentLocks (document, userID) VALUES (" . $this->_id . ", " . $falseOrUser->getID() . ")";
        $lockUserID = $falseOrUser->getID();
      }
      else {
        return false;
      }
    }
    if (!$db->getResult($queryStr)) {
      return false;
    }
    unset($this->_lockingUser);
    $this->_locked = $lockUserID;
    return true;
  }

  /**
   * Get the user currently locking the document
   *
   * @return object user have a lock
   */
  function getLockingUser() {
    if (!$this->isLocked()) {
      return false;
    }

    if (!isset($this->_lockingUser)) {
      $this->_lockingUser = $this->_dms->getUser($this->_locked);
    }
    return $this->_lockingUser;
  }

  function getSequence() {
    return $this->_sequence;
  }

  function setSequence($seq) {
    $db = $this->_dms->getDB();

    $queryStr = "UPDATE tblDocuments SET sequence = " . $seq . " WHERE id = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->_sequence = $seq;
    return true;
  }

  /**
   * Delete all entries for this document from the access control list
   *
   * @return boolean true if operation was successful otherwise false
   */
  function clearAccessList() {
    $db = $this->_dms->getDB();

    $queryStr = "DELETE FROM tblACLs WHERE targetType = " . TargetType::DOCUMENT . " AND target = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    unset($this->_accessList);
    return true;
  }

  /**
   * Returns a list of access privileges
   *
   * If the document inherits the access privileges from the parent folder
   * those will be returned.
   * $mode and $op can be set to restrict the list of returned access
   * privileges. If $mode is set to BakeryDMS\Core\AccessMode::ANY no restriction will apply
   * regardless of the value of $op. The returned array contains a list
   * of {@link SeedDMS_Core_UserAccess} and
   * {@link SeedDMS_Core_GroupAccess} objects. Even if the document
   * has no access list the returned array contains the two elements
   * 'users' and 'groups' which are than empty. The methode returns false
   * if the function fails.
   *
   * @param integer $mode access mode (defaults to BakeryDMS\Core\AccessMode::ANY)
   * @param integer $op operation (defaults to BakeryDMS\Core\Operator::EQ)
   * @return array multi dimensional array
   */
  function getAccessList($mode = AccessMode::ANY, $op = Operator::EQ) {
    $db = $this->_dms->getDB();

    if ($this->inheritsAccess()) {
      $res = $this->getFolder();
      if (!$res) {
        return false;
      }
      return $this->_folder->getAccessList($mode, $op);
    }

    if (!isset($this->_accessList[$mode])) {
      if ($op != Operator::GTEQ && $op != Operator::LTEQ && $op != Operator::EQ) {
        return false;
      }
      $modeStr = "";
      if ($mode != AccessMode::ANY) {
        $modeStr = " AND mode" . $op . (int)$mode;
      }
      $queryStr = "SELECT * FROM tblACLs WHERE targetType = " . TargetType::DOCUMENT .
        " AND target = " . $this->_id . $modeStr . " ORDER BY targetType";
      $resArr = $db->getResultArray($queryStr);
      if (is_bool($resArr) && !$resArr) {
        return false;
      }

      $this->_accessList[$mode] = array("groups" => array(), "users" => array());
      foreach ($resArr as $row) {
        if ($row["userID"] != -1) {
          $this->_accessList[$mode]["users"][] =
            new \BakeryDMS\Core\UserAccess($this->_dms->getUser($row["userID"]), $row["mode"]);
        }
        else {
          $this->_accessList[$mode]["groups"][] =
            new \BakeryDMS\Core\GroupAccess($this->_dms->getGroup($row["groupID"]), $row["mode"]);
        }
      }
    }

    return $this->_accessList[$mode];
  }

  /**
   * Add access right to folder
   * This function may change in the future. Instead of passing the a flag
   * and a user/group id a user or group object will be expected.
   *
   * @param integer $mode access mode
   * @param integer $userOrGroupID id of user or group
   * @param integer $isUser set to 1 if $userOrGroupID is the id of a
   *        user
   */
  function addAccess($mode, $userOrGroupID, $isUser) {
    $db = $this->_dms->getDB();

    $userOrGroup = ($isUser) ? "userID" : "groupID";

    $queryStr = "INSERT INTO tblACLs (target, targetType, " . $userOrGroup . ", mode) VALUES
					(" . $this->_id . ", " . TargetType::DOCUMENT . ", " . (int)$userOrGroupID . ", " . (int)$mode . ")";
    if (!$db->getResult($queryStr)) {
      return false;
    }

    unset($this->_accessList);

    // Update the notify list, if necessary.
    if ($mode == AccessMode::NONE) {
      $this->removeNotify($userOrGroupID, $isUser);
    }

    return true;
  }

  /**
   * Change access right of document
   * This function may change in the future. Instead of passing the a flag
   * and a user/group id a user or group object will be expected.
   *
   * @param integer $newMode access mode
   * @param integer $userOrGroupID id of user or group
   * @param integer $isUser set to 1 if $userOrGroupID is the id of a
   *        user
   */
  function changeAccess($newMode, $userOrGroupID, $isUser) {
    $db = $this->_dms->getDB();

    $userOrGroup = ($isUser) ? "userID" : "groupID";

    $queryStr = "UPDATE tblACLs SET mode = " . (int)$newMode . " WHERE targetType = " . TargetType::DOCUMENT . " AND target = " . $this->_id . " AND " . $userOrGroup . " = " . (int)$userOrGroupID;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    unset($this->_accessList);

    // Update the notify list, if necessary.
    if ($newMode == AccessMode::NONE) {
      $this->removeNotify($userOrGroupID, $isUser);
    }

    return true;
  }

  /**
   * Remove access rights for a user or group
   *
   * @param integer $userOrGroupID ID of user or group
   * @param boolean $isUser true if $userOrGroupID is a user id, false if it
   *        is a group id.
   * @return boolean true on success, otherwise false
   */
  function removeAccess($userOrGroupID, $isUser) {
    $db = $this->_dms->getDB();

    $userOrGroup = ($isUser) ? "userID" : "groupID";

    $queryStr = "DELETE FROM tblACLs WHERE targetType = " . TargetType::DOCUMENT . " AND target = " . $this->_id . " AND " . $userOrGroup . " = " . (int)$userOrGroupID;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    unset($this->_accessList);

    // Update the notify list, if the user looses access rights.
    $mode = ($isUser ? $this->getAccessMode($this->_dms->getUser($userOrGroupID)) : $this->getGroupAccessMode($this->_dms->getGroup($userOrGroupID)));
    if ($mode == AccessMode::NONE) {
      $this->removeNotify($userOrGroupID, $isUser);
    }

    return true;
  }

  /**
   * Returns the greatest access privilege for a given user
   *
   * This function returns the access mode for a given user. An administrator
   * and the owner of the folder has unrestricted access. A guest user has
   * read only access or no access if access rights are further limited
   * by access control lists. All other users have access rights according
   * to the access control lists or the default access. This function will
   * recursive check for access rights of parent folders if access rights
   * are inherited.
   *
   * The function searches the access control list for entries of
   * user $user. If it finds more than one entry it will return the
   * one allowing the greatest privileges, but user rights will always
   * precede group rights. If there is no entry in the
   * access control list, it will return the default access mode.
   * The function takes inherited access rights into account.
   * For a list of possible access rights see {@link AccessMode}.
   *
   * @param $user User the user to retrieve the acces mode for.
   * @return integer access mode
   */
  function getAccessMode($user) {
    if (!$user) {
      return AccessMode::NONE;
    }

    /* Administrators have unrestricted access */
    if ($user->isAdmin()) {
      return AccessMode::ALL;
    }

    /* The owner of the document has unrestricted access */
    if ($user->getID() == $this->_ownerID) {
      return AccessMode::ALL;
    }

    /* The guest users do not have more than read access */
    if ($user->isGuest()) {
      $mode = $this->getDefaultAccess();
      if ($mode >= AccessMode::READ) {
        return AccessMode::READ;
      }
      else {
        return AccessMode::NONE;
      }
    }

    /* Check ACLs */
    $accessList = $this->getAccessList();
    if (!$accessList) {
      return false;
    }

    foreach ($accessList["users"] as $userAccess) {
      if ($userAccess->getUserID() == $user->getID()) {
        return $userAccess->getMode();
      }
    }
    /* Get the highest right defined by a group */
    $result = 0;
    foreach ($accessList["groups"] as $groupAccess) {
      if ($user->isMemberOfGroup($groupAccess->getGroup())) {
        if ($groupAccess->getMode() > $result) {
          $result = $groupAccess->getMode();
        }
//					return $groupAccess->getMode();
      }
    }
    if ($result) {
      return $result;
    }
    $result = $this->getDefaultAccess();
    return $result;
  }

  /**
   * Returns the greatest access privilege for a given group
   *
   * This function searches the access control list for entries of
   * group $group. If it finds more than one entry it will return the
   * one allowing the greatest privileges. If there is no entry in the
   * access control list, it will return the default access mode.
   * The function takes inherited access rights into account.
   * For a list of possible access rights see {@link AccessMode}
   *
   * @param $group Group instance of a group
   * @return integer access mode
   */
  function getGroupAccessMode($group) {
    $highestPrivileged = AccessMode::NONE;

    //ACLs durchforsten
    $foundInACL = false;
    $accessList = $this->getAccessList();
    if (!$accessList) {
      return false;
    }

    foreach ($accessList["groups"] as $groupAccess) {
      if ($groupAccess->getGroupID() == $group->getID()) {
        $foundInACL = true;
        if ($groupAccess->getMode() > $highestPrivileged) {
          $highestPrivileged = $groupAccess->getMode();
        }
        if ($highestPrivileged == AccessMode::ALL) // max access right -> skip the rest
        {
          return $highestPrivileged;
        }
      }
    }

    if ($foundInACL) {
      return $highestPrivileged;
    }

    //Standard-Berechtigung verwenden
    return $this->getDefaultAccess();
  }

  /**
   * Returns a list of all notifications
   *
   * The returned list has two elements called 'users' and 'groups'. Each one
   * is an array itself containing objects of class {@link BakeryDMS\Core\User} and
   * BakeryDMS\Core\Group.
   *
   * @param integer $type type of notification (not yet used)
   * @return array list of notifications
   */
  function getNotifyList($type = 0) {
    if (empty($this->_notifyList)) {
      $db = $this->_dms->getDB();

      $documentNotificationQuery = "SELECT * FROM tblNotify WHERE targetType = " . TargetType::DOCUMENT . " AND target = " . $this->_id;
      $folderNotificationQuery = "SELECT * FROM tblNotify WHERE targetType = " . TargetType::FOLDER. " AND target = " . $this->getFolder()->getID();
      $documentNotifications = $db->getResultArray($documentNotificationQuery);
      $folderNotifications = $db->getResultArray($folderNotificationQuery);

      $this->_notifyList = array("groups" => array(), "users" => array());
      if (empty($documentNotifications) && empty($folderNotifications)) {
        return $this->_notifyList;
      }

      $notificationList = array_merge($documentNotifications, $folderNotifications);

      foreach ($notificationList as $notificationItem) {
        if ($notificationItem["userID"] != -1) {
          $user = $this->_dms->getUser($notificationItem["userID"]);
          $this->_notifyList["users"][$user->getID()] = $user;
          //array_push($this->_notifyList["users"], $this->_dms->getUser($notificationItem["userID"]));
        }
        else
        {
          $group= $this->_dms->getGroup($notificationItem["groupID"]);
          $this->_notifyList["groups"][$group->getID()] = $group;
          //array_push($this->_notifyList["groups"], $this->_dms->getGroup($notificationItem["groupID"]));
        }
      }
    }
    return $this->_notifyList;
  }

  /**
   * Add a user/group to the notification list
   * This function does not check if the currently logged in user
   * is allowed to add a notification. This must be checked by the calling
   * application.
   *
   * @param $userOrGroupID integer id of user or group to add
   * @param $isUser integer 1 if $userOrGroupID is a user,
   *                0 if $userOrGroupID is a group
   * @return integer  0: Update successful.
   *                 -1: Invalid User/Group ID.
   *                 -2: Target User / Group does not have read access.
   *                 -3: User is already subscribed.
   *                 -4: Database / internal error.
   */
  function addNotify($userOrGroupID, $isUser) {
    $db = $this->_dms->getDB();

    $userOrGroup = ($isUser ? "userID" : "groupID");

    /* Verify that user / group exists. */
    $obj = ($isUser ? $this->_dms->getUser($userOrGroupID) : $this->_dms->getGroup($userOrGroupID));
    if (!is_object($obj)) {
      return -1;
    }

    /* Verify that the requesting user has permission to add the target to
		 * the notification system.
		 */
    /*
		 * The calling application should enforce the policy on who is allowed
		 * to add someone to the notification system. If is shall remain here
		 * the currently logged in user should be passed to this function
		 *
		GLOBAL $user;
		if ($user->isGuest()) {
			return -2;
		}
		if (!$user->isAdmin()) {
			if ($isUser) {
				if ($user->getID() != $obj->getID()) {
					return -2;
				}
			}
			else {
				if (!$obj->isMember($user)) {
					return -2;
				}
			}
		}
		 */

    /* Verify that target user / group has read access to the document. */
    if ($isUser) {
      // Users are straightforward to check.
      if ($this->getAccessMode($obj) < AccessMode::READ) {
        return -2;
      }
    }
    else {
      // Groups are a little more complex.
      if ($this->getDefaultAccess() >= AccessMode::READ) {
        // If the default access is at least READ-ONLY, then just make sure
        // that the current group has not been explicitly excluded.
        $acl = $this->getAccessList(AccessMode::NONE, Operator::EQ);
        $found = false;
        foreach ($acl["groups"] as $group) {
          if ($group->getGroupID() == $userOrGroupID) {
            $found = true;
            break;
          }
        }
        if ($found) {
          return -2;
        }
      }
      else {
        // The default access is restricted. Make sure that the group has
        // been explicitly allocated access to the document.
        $acl = $this->getAccessList(AccessMode::READ, Operator::GTEQ);
        if (is_bool($acl)) {
          return -4;
        }
        $found = false;
        foreach ($acl["groups"] as $group) {
          if ($group->getGroupID() == $userOrGroupID) {
            $found = true;
            break;
          }
        }
        if (!$found) {
          return -2;
        }
      }
    }
    /* Check to see if user/group is already on the list. */
    $queryStr = "SELECT * FROM `tblNotify` WHERE `tblNotify`.`target` = '" . $this->_id . "' " .
      "AND `tblNotify`.`targetType` = '" . TargetType::DOCUMENT . "' " .
      "AND `tblNotify`.`" . $userOrGroup . "` = '" . (int)$userOrGroupID . "'";
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr)) {
      return -4;
    }
    if (count($resArr) > 0) {
      return -3;
    }

    $queryStr = "INSERT INTO tblNotify (target, targetType, " . $userOrGroup . ") VALUES (" . $this->_id . ", " . TargetType::DOCUMENT . ", " . (int)$userOrGroupID . ")";
    if (!$db->getResult($queryStr)) {
      return -4;
    }

    unset($this->_notifyList);
    return 0;
  }

  /**
   * Remove a user or group from the notification list
   * This function does not check if the currently logged in user
   * is allowed to remove a notification. This must be checked by the calling
   * application.
   *
   * @param $userOrGroupID id of user or group
   * @param $isUser boolean true if a user is passed in $userOrGroupID, false
   *        if a group is passed in $userOrGroupID
   * @param $type type of notification (0 will delete all) Not used yet!
   * @return integer 0 if operation was succesful
   *                 -1 if the userid/groupid is invalid
   *                 -3 if the user/group is already subscribed
   *                 -4 in case of an internal database error
   */
  function removeNotify($userOrGroupID, $isUser, $type = 0) {
    $db = $this->_dms->getDB();

    /* Verify that user / group exists. */
    $obj = ($isUser ? $this->_dms->getUser($userOrGroupID) : $this->_dms->getGroup($userOrGroupID));
    if (!is_object($obj)) {
      return -1;
    }

    $userOrGroup = ($isUser) ? "userID" : "groupID";

    /* Verify that the requesting user has permission to add the target to
		 * the notification system.
		 */
    /*
		 * The calling application should enforce the policy on who is allowed
		 * to add someone to the notification system. If is shall remain here
		 * the currently logged in user should be passed to this function
		 *
		GLOBAL $user;
		if ($user->isGuest()) {
			return -2;
		}
		if (!$user->isAdmin()) {
			if ($isUser) {
				if ($user->getID() != $obj->getID()) {
					return -2;
				}
			}
			else {
				if (!$obj->isMember($user)) {
					return -2;
				}
			}
		}
		 */

    /* Check to see if the target is in the database. */
    $queryStr = "SELECT * FROM `tblNotify` WHERE `tblNotify`.`target` = '" . $this->_id . "' " .
      "AND `tblNotify`.`targetType` = '" . TargetType::DOCUMENT . "' " .
      "AND `tblNotify`.`" . $userOrGroup . "` = '" . (int)$userOrGroupID . "'";
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr)) {
      return -4;
    }
    if (count($resArr) == 0) {
      return -3;
    }

    $queryStr = "DELETE FROM tblNotify WHERE target = " . $this->_id . " AND targetType = " . TargetType::DOCUMENT . " AND " . $userOrGroup . " = " . (int)$userOrGroupID;
    /* If type is given then delete only those notifications */
    if ($type) {
      $queryStr .= " AND `type` = " . (int)$type;
    }
    if (!$db->getResult($queryStr)) {
      return -4;
    }

    unset($this->_notifyList);
    return 0;
  }

  /**
   * Add content to a document
   *
   * Each document may have any number of content elements attached to it.
   * Each content element has a version number. Newer versions (greater
   * version number) replace older versions.
   *
   * @param string $comment comment
   * @param object $user user who shall be the owner of this content
   * @param string $tmpFile file containing the actuall content
   * @param string $orgFileName original file name
   * @param string $mimeType MimeType of the content
   * @param array $reviewers list of reviewers
   * @param array $approvers list of approvers
   * @param integer $version version number of content or 0 if next higher version shall be used.
   * @param array $attributes list of version attributes. The element key
   *        must be the id of the attribute definition.
   * @return bool/array false in case of an error or a result set
   */
  function addContent($comment,
                      $user,
                      $tmpFile,
                      $orgFileName,
                      $fileType,
                      $mimeType,
                      $reviewers = array(),
                      $approvers = array(),
                      $version = 0,
                      $attributes = array(),
                      $workflow = null) {
    $db = $this->_dms->getDB();

    // the doc path is id/version.filetype
    $dir = $this->getDir();

    $date = time();

    /* The version field in table tblDocumentContent used to be auto
		 * increment but that requires the field to be primary as well if
		 * innodb is used. That's why the version is now determined here.
		 */
    if ((int)$version < 0) {
      $version = 0;
    }
    elseif ((int)$version < 1) {
      $queryStr = "SELECT MAX(version) AS m FROM tblDocumentContent WHERE document = " . $this->_id;
      $resArr = $db->getResultArray($queryStr);
      if (is_bool($resArr) && !$res) {
        return false;
      }

      $version = $resArr[0]['m'] + 1;
    }

    $filesize = File::fileSize($tmpFile);
    $checksum = File::checksum($tmpFile);

    $db->startTransaction();
    $queryStr = "INSERT INTO tblDocumentContent (document, version, comment, date, createdBy, dir, orgFileName, fileType, mimeType, fileSize, checksum) VALUES " .
      "(" . $this->_id . ", " . (int)$version . "," . $db->qstr($comment) . ", " . $date . ", " . $user->getID() . ", " . $db->qstr($dir) . ", " . $db->qstr($orgFileName) . ", " . $db->qstr($fileType) . ", " . $db->qstr($mimeType) . ", " . $filesize . ", " . $db->qstr($checksum) . ")";
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $contentID = $db->getInsertID();

    // copy file
    if (!File::makeDir($this->_dms->contentDir . $dir)) {
      $db->rollbackTransaction();
      return false;
    }
    if (!File::copyFile($tmpFile, $this->_dms->contentDir . $dir . $version . $fileType)) {
      $db->rollbackTransaction();
      return false;
    }

    unset($this->_content);
    unset($this->_latestContent);
    $content = new \BakeryDMS\Core\DocumentContent($contentID,
      $this,
      $version,
      $comment,
      $date,
      $user->getID(),
      $dir,
      $orgFileName,
      $fileType,
      $mimeType,
      $filesize,
      $checksum);
    if ($workflow) {
      $content->setWorkflow($workflow, $user);
    }
    $docResultSet = new \BakeryDMS\Core\AddContentResultSet($content);

    if ($attributes) {
      foreach ($attributes as $attrdefid => $attribute) {
        if (trim($attribute)) {
          if (!$content->setAttributeValue($this->_dms->getAttributeDefinition($attrdefid), $attribute)) {
            $this->removeContent($content);
            $db->rollbackTransaction();
            return false;
          }
        }
      }
    }

    // TODO - verify
    if ($this->_dms->enableConverting
      && in_array($docResultSet->getContent()->getFileType(),
        array_keys($this->_dms->convertFileTypes))
    ) {
      $docResultSet->getContent()->convert();
    } // Even if if fails, do not return false

    $queryStr = "INSERT INTO `tblDocumentStatus` (`documentID`, `version`) " .
      "VALUES (" . $this->_id . ", " . (int)$version . ")";
    if (!$db->getResult($queryStr)) {
      $this->removeContent($content);
      $db->rollbackTransaction();
      return false;
    }

    $statusID = $db->getInsertID();

    // Add reviewers into the database. Reviewers must review the document
    // and submit comments, if appropriate. Reviewers can also recommend that
    // a document be rejected.
    $pendingReview = false;
    $reviewRes = array();
    foreach (array("i", "g") as $i) {
      if (isset($reviewers[$i])) {
        foreach ($reviewers[$i] as $reviewerID) {
          $reviewer = ($i == "i" ? $this->_dms->getUser($reviewerID) : $this->_dms->getGroup($reviewerID));
          $res = ($i == "i" ? $docResultSet->getContent()->addIndReviewer($reviewer,
            $user,
            true) : $docResultSet->getContent()->addGrpReviewer($reviewer, $user, true));
          $docResultSet->addReviewer($reviewer, $i, $res);
          // If no error is returned, or if the error is just due to email
          // failure, mark the state as "pending review".
          if ($res == 0 || $res = -3 || $res = -4) {
            $pendingReview = true;
          }
        }
      }
    }
    // Add approvers to the database. Approvers must also review the document
    // and make a recommendation on its release as an approved version.
    $pendingApproval = false;
    $approveRes = array();
    foreach (array("i", "g") as $i) {
      if (isset($approvers[$i])) {
        foreach ($approvers[$i] as $approverID) {
          $approver = ($i == "i" ? $this->_dms->getUser($approverID) : $this->_dms->getGroup($approverID));
          $res = ($i == "i" ? $docResultSet->getContent()->addIndApprover($approver,
            $user,
            true) : $docResultSet->getContent()->addGrpApprover($approver, $user, !$pendingReview));
          $docResultSet->addApprover($approver, $i, $res);
          if ($res == 0 || $res = -3 || $res = -4) {
            $pendingApproval = true;
          }
        }
      }
    }

    // If there are no reviewers or approvers, the document is automatically
    // promoted to the released state.
    if ($pendingReview) {
      $status = DocumentState::DRAFT_REV;
      $comment = "";
    }
    elseif ($pendingApproval) {
      $status = DocumentState::DRAFT_APP;
      $comment = "";
    }
    elseif ($workflow) {
      $status = DocumentState::IN_WORKFLOW;
      $comment = ", workflow: " . $workflow->getName();
    }
    else {
      $status = DocumentState::RELEASED;
      $comment = "";
    }
    $queryStr = "INSERT INTO `tblDocumentStatusLog` (`statusID`, `status`, `comment`, `date`, `userID`) " .
      "VALUES ('" . $statusID . "', '" . $status . "', 'New document content submitted" . $comment . "', CURRENT_TIMESTAMP, '" . $user->getID() . "')";
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $docResultSet->setStatus($status, $comment, $user);

    $db->commitTransaction();
    return $docResultSet;
  }

  /**
   * Return all content elements of a document
   *
   * This functions returns an array of content elements ordered by version
   *
   * @return array list of objects of class BakeryDMS\Core\DocumentContent
   */
  function getContent() {
    $db = $this->_dms->getDB();

    if (!isset($this->_content)) {
      $queryStr = "SELECT * FROM tblDocumentContent WHERE document = " . $this->_id . " ORDER BY version";
      $resArr = $db->getResultArray($queryStr);
      if (is_bool($resArr) && !$res) {
        return false;
      }

      $this->_content = array();
      foreach ($resArr as $row) {
        array_push($this->_content,
          new \BakeryDMS\Core\DocumentContent($row["id"],
            $this,
            $row["version"],
            $row["comment"],
            $row["date"],
            $row["createdBy"],
            $row["dir"],
            $row["orgFileName"],
            $row["fileType"],
            $row["mimeType"],
            $row['fileSize'],
            $row['checksum']));
      }
    }

    return $this->_content;
  }

  /**
   * Return the content element of a document with a given version number
   *
   * @param integer $version version number of content element
   * @return object object of class BakeryDMS\Core\DocumentContent
   */
  function getContentByVersion($version) {
    if (!is_numeric($version)) {
      return false;
    }

    if (isset($this->_content)) {
      foreach ($this->_content as $revision) {
        if ($revision->getVersion() == $version) {
          return $revision;
        }
      }
      return false;
    }

    $db = $this->_dms->getDB();
    $queryStr = "SELECT * FROM tblDocumentContent WHERE document = " . $this->_id . " AND version = " . (int)$version;
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && !$res) {
      return false;
    }
    if (count($resArr) != 1) {
      return false;
    }

    $resArr = $resArr[0];
    return new \BakeryDMS\Core\DocumentContent($resArr["id"],
      $this,
      $resArr["version"],
      $resArr["comment"],
      $resArr["date"],
      $resArr["createdBy"],
      $resArr["dir"],
      $resArr["orgFileName"],
      $resArr["fileType"],
      $resArr["mimeType"],
      $resArr['fileSize'],
      $resArr['checksum']);
  }

  function getLatestContent() {
    if (!isset($this->_latestContent)) {
      $db = $this->_dms->getDB();
      $queryStr = "SELECT * FROM tblDocumentContent WHERE document = " . $this->_id . " ORDER BY version DESC LIMIT 0,1";
      $resArr = $db->getResultArray($queryStr);
      if (is_bool($resArr) && !$resArr) {
        return false;
      }
      if (count($resArr) != 1) {
        return false;
      }

      $resArr = $resArr[0];
      $this->_latestContent = new \BakeryDMS\Core\DocumentContent($resArr["id"],
        $this,
        $resArr["version"],
        $resArr["comment"],
        $resArr["date"],
        $resArr["createdBy"],
        $resArr["dir"],
        $resArr["orgFileName"],
        $resArr["fileType"],
        $resArr["mimeType"],
        $resArr['fileSize'],
        $resArr['checksum']);
    }
    return $this->_latestContent;
  }

  function removeContent($version) {
    $db = $this->_dms->getDB();

    $emailList = array();
    $emailList[] = $version->_userID;

    if (file_exists($this->_dms->contentDir . $version->getPath())) {
      if (!File::removeFile($this->_dms->contentDir . $version->getPath())) {
        return false;
      }
    }

    $db->startTransaction();

    $status = $version->getStatus();
    $stID = $status["statusID"];

    $queryStr = "DELETE FROM tblDocumentContent WHERE `document` = " . $this->getID() . " AND `version` = " . $version->_version;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $queryStr = "DELETE FROM tblDocumentContentAttributes WHERE content = " . $version->getId();
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $queryStr = "DELETE FROM `tblDocumentStatusLog` WHERE `statusID` = '" . $stID . "'";
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $queryStr = "DELETE FROM `tblDocumentStatus` WHERE `documentID` = '" . $this->getID() . "' AND `version` = '" . $version->_version . "'";
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $status = $version->getReviewStatus();
    $stList = "";
    foreach ($status as $st) {
      $stList .= (strlen($stList) == 0 ? "" : ", ") . "'" . $st["reviewID"] . "'";
      if ($st["status"] == 0 && !in_array($st["required"], $emailList)) {
        $emailList[] = $st["required"];
      }
    }

    if (strlen($stList) > 0) {
      $queryStr = "DELETE FROM `tblDocumentReviewLog` WHERE `tblDocumentReviewLog`.`reviewID` IN (" . $stList . ")";
      if (!$db->getResult($queryStr)) {
        $db->rollbackTransaction();
        return false;
      }
    }
    $queryStr = "DELETE FROM `tblDocumentReviewers` WHERE `documentID` = '" . $this->getID() . "' AND `version` = '" . $version->_version . "'";
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }
    $status = $version->getApprovalStatus();
    $stList = "";
    foreach ($status as $st) {
      $stList .= (strlen($stList) == 0 ? "" : ", ") . "'" . $st["approveID"] . "'";
      if ($st["status"] == 0 && !in_array($st["required"], $emailList)) {
        $emailList[] = $st["required"];
      }
    }
    if (strlen($stList) > 0) {
      $queryStr = "DELETE FROM `tblDocumentApproveLog` WHERE `tblDocumentApproveLog`.`approveID` IN (" . $stList . ")";
      if (!$db->getResult($queryStr)) {
        $db->rollbackTransaction();
        return false;
      }
    }
    $queryStr = "DELETE FROM `tblDocumentApprovers` WHERE `documentID` = '" . $this->getID() . "' AND `version` = '" . $version->_version . "'";
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $queryStr = "DELETE FROM `tblWorkflowDocumentContent` WHERE `document` = '" . $this->getID() . "' AND `version` = '" . $version->_version . "'";
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $db->commitTransaction();
    return true;
  }

  /**
   * Return a certain document link
   *
   * @param integer $linkID id of link
   * @return object instance of DocumentLink or false in case of
   *         an error.
   */
  function getDocumentLink($linkID) {
    $db = $this->_dms->getDB();

    if (!is_numeric($linkID)) {
      return false;
    }

    $queryStr = "SELECT * FROM tblDocumentLinks WHERE document = " . $this->_id . " AND id = " . (int)$linkID;
    $resArr = $db->getResultArray($queryStr);
    if ((is_bool($resArr) && !$resArr) || count($resArr) == 0) {
      return false;
    }

    $resArr = $resArr[0];
    $document = $this->_dms->getDocument($resArr["document"]);
    $target = $this->_dms->getDocument($resArr["target"]);
    return new \BakeryDMS\Core\DocumentLink($resArr["id"], $document, $target, $resArr["userID"], $resArr["public"]);
  }

  /**
   * Return all document links
   *
   * The list may contain all links to other documents, even those which
   * may not be visible by certain users, unless you pass appropriate
   * parameters to filter out public links and those created by
   * the given user. The application may call
   * BakeryDMS\Core\DMS::filterDocumentLinks() afterwards.
   *
   * @param boolean $publiconly return on publically visible links
   * @param object $user return also private links of this user
   * @return \BakeryDMS\Core\DocumentLink[] The document links.
   */
  function getDocumentLinks($publiconly = false, $user = null) {
    if (!isset($this->_documentLinks)) {
      $db = $this->_dms->getDB();

      $queryStr = "SELECT * FROM tblDocumentLinks WHERE document = " . $this->_id;
      $tmp = array();
      if ($publiconly) {
        $tmp[] = "public=1";
      }
      if ($user) {
        $tmp[] = "userID=" . $user->getID();
      }
      if ($tmp) {
        $queryStr .= " AND (" . implode(" OR ", $tmp) . ")";
      }

      $resArr = $db->getResultArray($queryStr);
      if (is_bool($resArr) && !$resArr) {
        return false;
      }
      $this->_documentLinks = array();

      foreach ($resArr as $row) {
        $target = $this->_dms->getDocument($row["target"]);
        array_push($this->_documentLinks,
          new \BakeryDMS\Core\DocumentLink($row["id"], $this, $target, $row["userID"], $row["public"]));
      }
    }
    return $this->_documentLinks;
  }

  /**
   * Return all document having a link on this document
   *
   * The list contains all documents which have a link to the current
   * document. The list contains even those documents which
   * may not be accessible by the user, unless you pass appropriate
   * parameters to filter out public links and those created by
   * the given user.
   * This functions is basically the reverse of
   * BakeryDMS\Core\Document::getDocumentLinks()
   *
   * The application may call
   * BakeryDMS\Core\DMS::filterDocumentLinks() afterwards.
   *
   * @param boolean $publiconly return on publically visible links
   * @param object $user return also private links of this user
   * @return DocumentLink[] document links.
   */
  function getReverseDocumentLinks($publiconly = false, $user = null) {
    $db = $this->_dms->getDB();

    $queryStr = "SELECT * FROM tblDocumentLinks WHERE target = " . $this->_id;
    $tmp = array();
    if ($publiconly) {
      $tmp[] = "public=1";
    }
    if ($user) {
      $tmp[] = "userID=" . $user->getID();
    }
    if ($tmp) {
      $queryStr .= " AND (" . implode(" OR ", $tmp) . ")";
    }

    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    $links = array();
    foreach ($resArr as $row) {
      $document = $this->_dms->getDocument($row["document"]);
      array_push($links,
        new \BakeryDMS\Core\DocumentLink($row["id"], $document, $this, $row["userID"], $row["public"]));
    }

    return $links;
  }

  function addDocumentLink($targetID, $userID, $public) {
    $db = $this->_dms->getDB();

    $public = ($public) ? "1" : "0";

    $queryStr = "INSERT INTO tblDocumentLinks(document, target, userID, public) VALUES (" . $this->_id . ", " . (int)$targetID . ", " . (int)$userID . ", " . (int)$public . ")";
    if (!$db->getResult($queryStr)) {
      return false;
    }

    unset($this->_documentLinks);
    return true;
  }

  function removeDocumentLink($linkID) {
    $db = $this->_dms->getDB();

    if (!is_numeric($linkID)) {
      return false;
    }

    $queryStr = "DELETE FROM tblDocumentLinks WHERE document = " . $this->_id . " AND id = " . (int)$linkID;
    if (!$db->getResult($queryStr)) {
      return false;
    }
    unset ($this->_documentLinks);
    return true;
  }

  function getDocumentFile($ID) {
    $db = $this->_dms->getDB();

    if (!is_numeric($ID)) {
      return false;
    }

    $queryStr = "SELECT * FROM tblDocumentFiles WHERE document = " . $this->_id . " AND id = " . (int)$ID;
    $resArr = $db->getResultArray($queryStr);
    if ((is_bool($resArr) && !$resArr) || count($resArr) == 0) {
      return false;
    }

    $resArr = $resArr[0];
    return new \BakeryDMS\Core\DocumentFile($resArr["id"],
      $this,
      $resArr["userID"],
      $resArr["comment"],
      $resArr["date"],
      $resArr["dir"],
      $resArr["fileType"],
      $resArr["mimeType"],
      $resArr["orgFileName"],
      $resArr["name"]);
  }

  function getDocumentFiles() {
    if (!isset($this->_documentFiles)) {
      $db = $this->_dms->getDB();

      $queryStr = "SELECT * FROM tblDocumentFiles WHERE document = " . $this->_id . " ORDER BY `date` DESC";
      $resArr = $db->getResultArray($queryStr);
      if (is_bool($resArr) && !$resArr) {
        return false;
      }

      $this->_documentFiles = array();

      foreach ($resArr as $row) {
        array_push($this->_documentFiles,
          new \BakeryDMS\Core\DocumentFile($row["id"],
            $this,
            $row["userID"],
            $row["comment"],
            $row["date"],
            $row["dir"],
            $row["fileType"],
            $row["mimeType"],
            $row["orgFileName"],
            $row["name"]));
      }
    }
    return $this->_documentFiles;
  }

  function addDocumentFile($name, $comment, $user, $tmpFile, $orgFileName, $fileType, $mimeType) {
    $db = $this->_dms->getDB();

    $dir = $this->getDir();

    $queryStr = "INSERT INTO tblDocumentFiles (comment, date, dir, document, fileType, mimeType, orgFileName, userID, name) VALUES " .
      "(" . $db->qstr($comment) . ", '" . time() . "', " . $db->qstr($dir) . ", " . $this->_id . ", " . $db->qstr($fileType) . ", " . $db->qstr($mimeType) . ", " . $db->qstr($orgFileName) . "," . $user->getID() . "," . $db->qstr($name) . ")";
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $id = $db->getInsertID();

    $file = $this->getDocumentFile($id);
    if (is_bool($file) && !$file) {
      return false;
    }

    // copy file
    if (!File::makeDir($this->_dms->contentDir . $dir)) {
      return false;
    }
    if (!File::copyFile($tmpFile, $this->_dms->contentDir . $file->getPath())) {
      return false;
    }

    return true;
  }

  function removeDocumentFile($ID) {
    $db = $this->_dms->getDB();

    if (!is_numeric($ID)) {
      return false;
    }

    $file = $this->getDocumentFile($ID);
    if (is_bool($file) && !$file) {
      return false;
    }

    if (file_exists($this->_dms->contentDir . $file->getPath())) {
      if (!File::removeFile($this->_dms->contentDir . $file->getPath())) {
        return false;
      }
    }

    $name = $file->getName();
    $comment = $file->getcomment();

    $queryStr = "DELETE FROM tblDocumentFiles WHERE document = " . $this->getID() . " AND id = " . (int)$ID;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    unset ($this->_documentFiles);

    return true;
  }

  /**
   * Remove a document completly
   *
   * This methods calls the callback 'onPreRemoveDocument' before removing
   * the document. The current document will be passed as the second
   * parameter to the callback function. After successful deletion the
   * 'onPostRemoveDocument' callback will be used. The current document id
   * will be passed as the second parameter. If onPreRemoveDocument fails
   * the whole function will fail and the document will not be deleted.
   * The return value of 'onPostRemoveDocument' will be disregarded.
   *
   * @return boolean true on success, otherwise false
   */
  function remove() {
    $db = $this->_dms->getDB();

    /* Check if 'onPreRemoveDocument' callback is set */
    if (isset($this->_dms->callbacks['onPreRemoveDocument'])) {
      $callback = $this->_dms->callbacks['onPreRemoveDocument'];
      if (!call_user_func($callback[0], $callback[1], $this)) {
        return false;
      }
    }

    $res = $this->getContent();
    if (is_bool($res) && !$res) {
      return false;
    }

    $db->startTransaction();

    // FIXME: call a new function removeContent instead
    foreach ($this->_content as $version) {
      if (!$this->removeContent($version)) {
        $db->rollbackTransaction();
        return false;
      }
    }

    // remove document file
    $res = $this->getDocumentFiles();
    if (is_bool($res) && !$res) {
      $db->rollbackTransaction();
      return false;
    }

    foreach ($res as $documentfile) {
      if (!$this->removeDocumentFile($documentfile->getId())) {
        $db->rollbackTransaction();
        return false;
      }
    }

    // TODO: versioning file?

    if (file_exists($this->_dms->contentDir . $this->getDir())) {
      if (!File::removeDir($this->_dms->contentDir . $this->getDir())) {
        $db->rollbackTransaction();
        return false;
      }
    }

    $queryStr = "DELETE FROM tblDocuments WHERE id = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }
    $queryStr = "DELETE FROM tblDocumentAttributes WHERE document = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }
    $queryStr = "DELETE FROM tblACLs WHERE target = " . $this->_id . " AND targetType = " . TargetType::DOCUMENT;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }
    $queryStr = "DELETE FROM tblDocumentLinks WHERE document = " . $this->_id . " OR target = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }
    $queryStr = "DELETE FROM tblDocumentLocks WHERE document = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }
    $queryStr = "DELETE FROM tblDocumentFiles WHERE document = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }
    $queryStr = "DELETE FROM tblDocumentCategory WHERE documentID = " . $this->_id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    // Delete the notification list.
    $queryStr = "DELETE FROM tblNotify WHERE target = " . $this->_id . " AND targetType = " . TargetType::DOCUMENT;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $db->commitTransaction();

    /* Check if 'onPostRemoveDocument' callback is set */
    if (isset($this->_dms->callbacks['onPostRemoveDocument'])) {
      $callback = $this->_dms->callbacks['onPostRemoveDocument'];
      if (!call_user_func($callback[0], $callback[1], $this->_id)) {
      }
    }

    return true;
  }

  /**
   * Get List of users and groups which have read access on the document
   * The list will not include any guest users,
   * administrators and the owner of the folder unless $listadmin resp.
   * $listowner is set to true.
   *
   * This function is deprecated. Use
   * {@see BakeryDMS\Core\Document::getReadAccessList()} instead.
   */
  function getApproversList() {
    return $this->getReadAccessList(0, 0);
  }

  /**
   * Returns a list of groups and users with read access on the document
   *
   * @param boolean $listadmin if set to true any admin will be listed too
   * @param boolean $listowner if set to true the owner will be listed too
   *
   * @return array list of users and groups
   */
  function getReadAccessList($listadmin = 0, $listowner = 0) {
    $db = $this->_dms->getDB();

    if (!isset($this->_readAccessList)) {
      $this->_readAccessList = array("groups" => array(), "users" => array());
      $userIDs = "";
      $groupIDs = "";
      $defAccess = $this->getDefaultAccess();

      if ($defAccess < AccessMode::READ) {
        // Get the list of all users and groups that are listed in the ACL as
        // having read access to the document.
        $tmpList = $this->getAccessList(AccessMode::READ, Operator::GTEQ);
      }
      else {
        // Get the list of all users and groups that DO NOT have read access
        // to the document.
        $tmpList = $this->getAccessList(AccessMode::NONE, Operator::LTEQ);
      }
      foreach ($tmpList["groups"] as $groupAccess) {
        $groupIDs .= (strlen($groupIDs) == 0 ? "" : ", ") . $groupAccess->getGroupID();
      }
      foreach ($tmpList["users"] as $userAccess) {
        $user = $userAccess->getUser();
        if (!$listadmin && $user->isAdmin()) {
          continue;
        }
        if (!$listowner && $user->getID() == $this->_ownerID) {
          continue;
        }
        if ($user->isGuest()) {
          continue;
        }
        $userIDs .= (strlen($userIDs) == 0 ? "" : ", ") . $userAccess->getUserID();
      }

      // Construct a query against the users table to identify those users
      // that have read access to this document, either directly through an
      // ACL entry, by virtue of ownership or by having administrative rights
      // on the database.
      $queryStr = "";
      /* If default access is less then read, $userIDs and $groupIDs contains
			 * a list of user with read access
			 */
      if ($defAccess < AccessMode::READ) {
        if (strlen($groupIDs) > 0) {
          $queryStr = "SELECT `tblUsers`.* FROM `tblUsers` " .
            "LEFT JOIN `tblGroupMembers` ON `tblGroupMembers`.`userID`=`tblUsers`.`id` " .
            "WHERE `tblGroupMembers`.`groupID` IN (" . $groupIDs . ") " .
            "AND `tblUsers`.`role` != " . User::role_guest . " UNION ";
        }
        $queryStr .=
          "SELECT `tblUsers`.* FROM `tblUsers` " .
          "WHERE (`tblUsers`.`role` != " . User::role_guest . ") " .
          "AND ((`tblUsers`.`id` = " . $this->_ownerID . ") " .
          "OR (`tblUsers`.`role` = " . User::role_admin . ")" .
          (strlen($userIDs) == 0 ? "" : " OR (`tblUsers`.`id` IN (" . $userIDs . "))") .
          ") ORDER BY `login`";
      } /* If default access is equal or greate then read, $userIDs and
			 * $groupIDs contains a list of user without read access
			 */
      else {
        if (strlen($groupIDs) > 0) {
          $queryStr = "SELECT `tblUsers`.* FROM `tblUsers` " .
            "LEFT JOIN `tblGroupMembers` ON `tblGroupMembers`.`userID`=`tblUsers`.`id` " .
            "WHERE `tblGroupMembers`.`groupID` NOT IN (" . $groupIDs . ")" .
            "AND `tblUsers`.`role` != " . User::role_guest . " " .
            (strlen($userIDs) == 0 ? "" : " AND (`tblUsers`.`id` NOT IN (" . $userIDs . "))") . " UNION ";
        }
        $queryStr .=
          "SELECT `tblUsers`.* FROM `tblUsers` " .
          "WHERE (`tblUsers`.`id` = " . $this->_ownerID . ") " .
          "OR (`tblUsers`.`role` = " . User::role_admin . ") " .
          "UNION " .
          "SELECT `tblUsers`.* FROM `tblUsers` " .
          "WHERE `tblUsers`.`role` != " . User::role_guest . " " .
          (strlen($userIDs) == 0 ? "" : " AND (`tblUsers`.`id` NOT IN (" . $userIDs . "))") .
          " ORDER BY `login`";
      }
      $resArr = $db->getResultArray($queryStr);
      if (!is_bool($resArr)) {
        foreach ($resArr as $row) {
          $user = $this->_dms->getUser($row['id']);
          if (!$listadmin && $user->isAdmin()) {
            continue;
          }
          if (!$listowner && $user->getID() == $this->_ownerID) {
            continue;
          }
          $this->_readAccessList["users"][] = $user;
        }
      }

      // Assemble the list of groups that have read access to the document.
      $queryStr = "";
      if ($defAccess < AccessMode::READ) {
        if (strlen($groupIDs) > 0) {
          $queryStr = "SELECT `tblGroups`.* FROM `tblGroups` " .
            "WHERE `tblGroups`.`id` IN (" . $groupIDs . ")";
        }
      }
      else {
        if (strlen($groupIDs) > 0) {
          $queryStr = "SELECT `tblGroups`.* FROM `tblGroups` " .
            "WHERE `tblGroups`.`id` NOT IN (" . $groupIDs . ")";
        }
        else {
          $queryStr = "SELECT `tblGroups`.* FROM `tblGroups`";
        }
      }
      if (strlen($queryStr) > 0) {
        $resArr = $db->getResultArray($queryStr);
        if (!is_bool($resArr)) {
          foreach ($resArr as $row) {
            $group = $this->_dms->getGroup($row["id"]);
            $this->_readAccessList["groups"][] = $group;
          }
        }
      }
    }
    return $this->_readAccessList;
  }

  /**
   * Get the internally used folderList which stores the ids of folders from
   * the root folder to the parent folder.
   *
   * @return string column separated list of folder ids
   */
  function getFolderList() {
    $db = $this->_dms->getDB();

    $queryStr = "SELECT folderList FROM tblDocuments WHERE id = " . $this->_id;
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    return $resArr[0]['folderList'];
  }

  /**
   * Checks the internal data of the document and repairs it.
   * Currently, this function only repairs an incorrect folderList
   *
   * @return boolean true on success, otherwise false
   */
  function repair() {
    $db = $this->_dms->getDB();

    $curfolderlist = $this->getFolderList();

    // calculate the folderList of the folder
    $parent = $this->getFolder();
    $pathPrefix = "";
    $path = $parent->getPath();
    foreach ($path as $f) {
      $pathPrefix .= ":" . $f->getID();
    }
    if (strlen($pathPrefix) > 1) {
      $pathPrefix .= ":";
    }
    if ($curfolderlist != $pathPrefix) {
      $queryStr = "UPDATE tblDocuments SET folderList='" . $pathPrefix . "' WHERE id = " . $this->_id;
      $res = $db->getResult($queryStr);
      if (!$res) {
        return false;
      }
    }
    return true;
  }

  /**
   * Calculate the disk space including all versions of the document
   *
   * This is done by using the internal database field storing the
   * filesize of a document version.
   *
   * @return integer total disk space in Bytes
   */
  function getUsedDiskSpace() {
    $db = $this->_dms->getDB();

    $queryStr = "SELECT SUM(filesize) sum FROM tblDocumentContent WHERE document = " . $this->_id;
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    return $resArr[0]['sum'];
  }

}


?>
