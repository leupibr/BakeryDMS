<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

/**
 * Class to represent an attribute in the document management system.
 *
 * Attributes are key/value pairs which can be attached to documents,
 * folders and document content. The number of attributes is unlimited.
 * Each attribute has a value and is related to an attribute definition,
 * which holds the name and other information about the attribute.
 *
 * @see AttributeDefinition
 */
class Attribute {
  /** @var integer id of attribute. */
  private $id;

  /** @var \BakeryDMS\Core\Object folder, document or document content this attribute belongs to. */
  private $object;

  /** @var AttributeDefinition Definition of this attribute. */
  private $definition;

  /** @var string value of this attribute. */
  private $value;

  /** @var \BakeryDMS\Core\DMS reference to the dms instance this attribute belongs to. */
  private $dms;

  /**
   * Initializes a new instance of the {@link Attribute} class.
   *
   * @param int $id internal id of attribute
   * @param \BakeryDMS\Core\Object $obj object this attribute is attached to
   * @param AttributeDefinition $definition reference to the attribute definition
   * @param string $value value of the attribute
   */
  function __construct($id, $obj, $definition, $value) {
    $this->id = $id;
    $this->object = $obj;
    $this->definition = $definition;
    $this->value = $value;
    $this->dms = null;
  }

  /**
   * Sets the reference to the dms.
   *
   * @param \BakeryDMS\Core\DMS $dms The dms to set.
   */
  function setDMS($dms) {
    $this->dms = $dms;
  }

  /**
   * Gets the internal id of the attribute.
   *
   * @return int The id.
   */
  function getID() {
    return $this->id;
  }

  /**
   * Gets the value of the attribute.
   *
   * @return string The value
   */
  function getValue() {
    return $this->value;
  }

  /**
   * Set a value of an attribute.
   * The attribute is deleted completely if the value is the empty string.
   *
   * @param string $value value to be set.
   * @return boolean true if operation was successful, otherwise false.
   */
  function setValue($value) {
    if (trim($value) === '') {
      return $this->deleteAttribute();
    }
    else {
      return $this->updateAttributeValue($value);
    }
  }

  /**
   * Gets the attribute definition.
   *
   * @return AttributeDefinition The attribute definition.
   */
  function getAttributeDefinition() {
    return $this->definition;
  }

  /**
   * Deletes a attribute completely.
   * @return bool true if successful, false otherwise.
   */
  private function deleteAttribute() {
    switch (get_class($this->object)) {
      case Document::class:
        $queryStr = "DELETE FROM tblDocumentAttributes WHERE `document` = " . $this->object->getID() . " AND `attrdef` = " . $this->definition->getId();
        break;

      case DocumentContent::class:
        $queryStr = "DELETE FROM tblDocumentContentAttributes WHERE `content` = " . $this->object->getID() . " AND `attrdef` = " . $this->definition->getId();
        break;

      case Folder::class:
        $queryStr = "DELETE FROM tblFolderAttributes WHERE `folder` = " . $this->object->getID() . " AND `attrdef` = " . $this->definition->getId();
        break;

      default:
        return false;
    }

    $db = $this->dms->getDB();
    if (!$db->getResult($queryStr)) {
      return false;
    }
    return true;
  }

  /**
   * Sets the value of this attribute.
   *
   * @param string $value The value to set.
   * @return bool true if successful, false otherwise.
   */
  private function updateAttributeValue($value) {
    $db = $this->dms->getDB();
    switch (get_class($this->object)) {
      case Document::class:
        $queryStr = "UPDATE tblDocumentAttributes SET value = " . $db->qstr($value) . " WHERE `document` = " . $this->object->getID() . " AND `attrdef` = " . $this->definition->getId();
        break;

      case DocumentContent::class:
        $queryStr = "UPDATE tblDocumentContentAttributes SET value = " . $db->qstr($value) . " WHERE `content` = " . $this->object->getID() . " AND `attrdef` = " . $this->definition->getId();
        break;

      case Folder::class:
        $queryStr = "UPDATE tblFolderAttributes SET value = " . $db->qstr($value) . " WHERE `folder` = " . $this->object->getID() . " AND `attrdef` = " . $this->definition->getId();
        break;

      default:
        return false;
    }

    if (!$db->getResult($queryStr)) {
      return false;
    }
    $this->value = $value;
    return true;
  }
}