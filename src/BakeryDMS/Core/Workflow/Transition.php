<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core\Workflow;

use BakeryDMS\Core\Group;
use BakeryDMS\Core\User;

/**
 * Class to represent a workflow transition in the document management system.
 */
class Transition {

  /**
   * @var int id of workflow transition.
   */
  private $id;

  /**
   * @var Workflow workflow this transition belongs to.
   */
  private $workflow;

  /**
   * @var State State of the workflow transition.
   */
  private $state;

  /**
   * @var State Next state of the workflow transition.
   */
  private $nextState;

  /**
   * @var Action Action of the workflow transition.
   */
  private $action;

  /**
   * @var int maximum of seconds allowed until this transition must be triggered
   */
  private $maxTime;

  /**
   * @var User[] List of users allowed to trigger this transaction
   */
  private $users;

  /**
   * @var Group[] List of groups allowed to trigger this transaction
   */
  private $groups;

  /**
   * @var \BakeryDMS\Core\DMS reference to the dms instance this attribute belongs to
   */
  private $dms;

  /**
   * Initializes a new instance of the {@link Transition} class.
   *
   * @param $id int The id of the transition.
   * @param $workflow State The workflow of the transition.
   * @param $state State The source state.
   * @param $action Action The workflow action of the transition.
   * @param $nextstate State The target state.
   * @param $maxtime int The maximum number of seconds allowed until this transition must be triggered
   */
  function __construct($id, $workflow, $state, $action, $nextstate, $maxtime) {
    $this->id = $id;
    $this->workflow = $workflow;
    $this->state = $state;
    $this->action = $action;
    $this->nextState = $nextstate;
    $this->maxTime = $maxtime;
    $this->dms = null;
  }

  function setDMS($dms) {
    $this->dms = $dms;
  }

  function getID() {
    return $this->id;
  }

  function getWorkflow() {
    return $this->workflow;
  }

  function setWorkflow($newWorkflow) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblWorkflowTransitions SET workflow = " . $newWorkflow->getID() . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->workflow = $newWorkflow;
    return true;
  }

  function getState() {
    return $this->state;
  }

  function setState($newState) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblWorkflowTransitions SET state = " . $newState->getID() . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->state = $newState;
    return true;
  }

  function getNextState() {
    return $this->nextState;
  }

  function setNextState($newNextState) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblWorkflowTransitions SET nextstate = " . $newNextState->getID() . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->nextState = $newNextState;
    return true;
  }

  function getAction() {
    return $this->action;
  }

  function setAction($newAction) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblWorkflowTransitions SET action = " . $newAction->getID() . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->action = $newAction;
    return true;
  }

  function getMaxTime() {
    return $this->maxTime;
  }

  function setMaxTime($maxtime) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblWorkflowTransitions SET maxtime = " . intval($maxtime) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->maxTime = $maxtime;
    return true;
  }

  /**
   * Get all users allowed to trigger this transition
   *
   * @return array list of users
   */
  function getUsers() {
    $db = $this->dms->getDB();

    if ($this->users) {
      return $this->users;
    }

    $queryStr = "SELECT * FROM tblWorkflowTransitionUsers WHERE transition=" . $this->id;
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    $users = array();
    for ($i = 0; $i < count($resArr); $i++) {
      $user = new TransitionUser($resArr[$i]['id'],
        $this,
        $this->dms->getUser($resArr[$i]['userid']));
      $user->setDMS($this->dms);
      $users[$i] = $user;
    }

    $this->users = $users;

    return $this->users;
  }

  /**
   * Get all users allowed to trigger this transition
   *
   * @return array list of users
   */
  function getGroups() {
    $db = $this->dms->getDB();

    if ($this->groups) {
      return $this->groups;
    }

    $queryStr = "SELECT * FROM tblWorkflowTransitionGroups WHERE transition=" . $this->id;
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    $groups = array();
    for ($i = 0; $i < count($resArr); $i++) {
      $group = new TransitionGroup($resArr[$i]['id'],
        $this,
        $this->dms->getGroup($resArr[$i]['groupid']),
        $resArr[$i]['minusers']);
      $group->setDMS($this->dms);
      $groups[$i] = $group;
    }

    $this->groups = $groups;

    return $this->groups;
  }

  /**
   * Remove the workflow transition
   *
   * @return boolean true on success or false in case of an error
   *         false is also returned if the workflow action is currently in use
   */
  function remove() {
    $db = $this->dms->getDB();

    $db->startTransaction();

    // Delete workflow transition itself
    $queryStr = "DELETE FROM tblWorkflowTransitions WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $db->commitTransaction();

    return true;
  }

}