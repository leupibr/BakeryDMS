<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA./

namespace BakeryDMS\Core\Workflow;

use BakeryDMS\Core\Group;

/**
 * Class to represent a group allowed to trigger a workflow transition.
 */
class TransitionGroup {
  /**
   * @var int id of workflow transition
   */
  private $id;

  /**
   * @var Transition reference to the transition this group belongs to
   */
  private $transition;

  /**
   * @var int number of users that must trigger the transition
   */
  private $numOfUsers;

  /**
   * @var Group group of users
   */
  private $group;

  /**
   * @var \BakeryDMS\Core\DMS reference to the dms instance this attribute belongs to
   */
  private $dms;

  function __construct($id, $transition, $group, $numOfUsers) {
    $this->id = $id;
    $this->transition = $transition;
    $this->group = $group;
    $this->numOfUsers = $numOfUsers;
  }

  function setDMS($dms) {
    $this->dms = $dms;
  }

  /**
   * Get the transtion itself
   *
   * @return object group
   */
  function getTransition() {
    return $this->transition;
  }

  /**
   * Get the group whose user are allowed to trigger the transition
   *
   * @return object group
   */
  function getGroup() {
    return $this->group;
  }

  /**
   * Returns the number of users of this group needed to trigger the transition
   *
   * @return integer number of users
   */
  function getNumOfUsers() {
    return $this->numOfUsers;
  }

}