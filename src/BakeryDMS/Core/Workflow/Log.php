<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core\Workflow;
use BakeryDMS\Core\Document;
use BakeryDMS\Core\User;

/**
 * Class to represent a group allowed to trigger a workflow transition.
 */
class Log {
  /**
   * @var int id of workflow log
   */
  private $id;

  /**
   * @var Document document this log entry belongs to
   */
  private $document;

  /**
   * @var int version of document this log entry belongs to
   */
  private $version;

  /**
   * @var Workflow workflow
   */
  private $workflow;

  /**
   * @var User user initiating this log entry
   */
  private $user;

  /**
   * @var Transition transition
   */
  private $transition;

  /**
   * @var string date
   */
  private $date;

  /**
   * @var string comment
   */
  private $comment;

  /**
   * @var \BakeryDMS\Core\DMS reference to the dms instance this attribute belongs to
   */
  private $dms;

  function __construct($id, $document, $version, $workflow, $user, $transition, $date, $comment) {
    $this->id = $id;
    $this->document = $document;
    $this->version = $version;
    $this->workflow = $workflow;
    $this->user = $user;
    $this->transition = $transition;
    $this->date = $date;
    $this->comment = $comment;
    $this->dms = null;
  }

  function setDMS($dms) {
    $this->dms = $dms;
  }

  /**
   * @return Transition
   */
  function getTransition() {
    return $this->transition;
  }

  function getUser() {
    return $this->user;
  }

  function getComment() {
    return $this->comment;
  }

  function getDate() {
    return $this->date;
  }

}

?>
