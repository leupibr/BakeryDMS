<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core\Workflow;

/**
 * Class to represent a workflow state in the document management system.
 */
class State {
  /**
   * @var int id of workflow state
   */
  private $id;

  /**
   * @var string Nof the workflow state
   */
  private $name;

  /**
   * @var int maximum of seconds allowed in this state
   */
  private $maxTime;

  /**
   * @var int maximum of seconds allowed in this state
   */
  private $preconditionFunction;

  /**
   * @var object document status when this state is reached
   */
  private $documentStatus;

  /**
   * @var \BakeryDMS\Core\DMS reference to the dms instance this attribute belongs to
   */
  private $dms;

  function __construct($id, $name, $maxtime, $precondfunc, $documentstatus) {
    $this->id = $id;
    $this->name = $name;
    $this->maxTime = $maxtime;
    $this->preconditionFunction = $precondfunc;
    $this->documentStatus = $documentstatus;
    $this->dms = null;
  }

  function setDMS($dms) {
    $this->dms = $dms;
  }

  function getID() {
    return $this->id;
  }

  function getName() {
    return $this->name;
  }

  function setName($newName) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblWorkflowStates SET name = " . $db->qstr($newName) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->name = $newName;
    return true;
  }

  function getMaxTime() {
    return $this->maxTime;
  }

  function setMaxTime($maxtime) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblWorkflowStates SET maxtime = " . intval($maxtime) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->maxTime = $maxtime;
    return true;
  }

  function getPreCondFunc() {
    return $this->preconditionFunction;
  }

  function setPreCondFunc($precondfunc) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblWorkflowStates SET precondfunc = " . $db->qstr($precondfunc) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->maxTime = $maxtime;
    return true;
  }

  /**
   * Get the document status which is set when this state is reached
   *
   * The document status uses the define states S_REJECTED and S_RELEASED
   * Only those two states will update the document status
   *
   * @return integer document status
   */
  function getDocumentStatus() {
    return $this->documentStatus;
  }

  function setDocumentStatus($docstatus) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblWorkflowStates SET documentstatus = " . intval($docstatus) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->documentStatus = $docstatus;
    return true;
  }

  /**
   * Check if workflow state is currently used by any workflow transition
   *
   * @return boolean true if workflow is used, otherwise false
   */
  function isUsed() {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblWorkflowTransitions WHERE state=" . $this->id . " OR nextstate=" . $this->id;
    $resArr = $db->getResultArray($queryStr);
    if (is_array($resArr) && count($resArr) == 0) {
      return false;
    }
    return true;
  }

  /**
   * Remove the workflow state
   *
   * @return boolean true on success or false in case of an error
   *         false is also returned if the workflow state is currently in use
   */
  function remove() {
    $db = $this->dms->getDB();

    if ($this->isUsed()) {
      return false;
    }

    $db->startTransaction();

    // Delete workflow state itself
    $queryStr = "DELETE FROM tblWorkflowStates WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $db->commitTransaction();

    return true;
  }

}