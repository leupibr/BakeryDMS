<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core\Workflow;

/**
 * Class to represent a workflow action in the document management system.
 */
class Action {
  /**
   * @var int id of workflow action
   */
  private $id;

  /**
   * @var string name of the workflow action
   */
  private $name;

  /**
   * @var \BakeryDMS\Core\DMS reference to the dms instance this attribute belongs to
   */
  private $dms;

  /**
   * Initializes a new instance of the {@link Action} class.
   *
   * @param $id int The id of the action.
   * @param $name string The name of the action.
   */
  function __construct($id, $name) {
    $this->id = $id;
    $this->name = $name;
    $this->dms = null;
  }

  function setDMS($dms) {
    $this->dms = $dms;
  }

  function getID() {
    return $this->id;
  }

  function getName() {
    return $this->name;
  }

  function setName($newName) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblWorkflowActions SET name = " . $db->qstr($newName) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->name = $newName;
    return true;
  }

  /**
   * Check if workflow action is currently used by any workflow transition
   *
   * @return boolean true if workflow action is used, otherwise false
   */
  function isUsed() {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblWorkflowTransitions WHERE action=" . $this->id;
    $resArr = $db->getResultArray($queryStr);
    if (is_array($resArr) && count($resArr) == 0) {
      return false;
    }
    return true;
  }

  /**
   * Remove the workflow action
   *
   * @return boolean true on success or false in case of an error
   *         false is also returned if the workflow action is currently in use
   */
  function remove() {
    $db = $this->dms->getDB();

    if ($this->isUsed()) {
      return false;
    }

    $db->startTransaction();

    // Delete workflow state itself
    $queryStr = "DELETE FROM tblWorkflowActions WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $db->commitTransaction();

    return true;
  }

}