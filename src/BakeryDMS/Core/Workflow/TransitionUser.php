<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA./

namespace BakeryDMS\Core\Workflow;

use BakeryDMS\Core\User;

/**
 * Class to represent a user allowed to trigger a workflow transition.
 */
class TransitionUser {
  /**
   * @var int id of workflow transition
   */
  private $id;

  /**
   * @var Transition reference to the transition this user belongs to
   */
  private $transition;

  /**
   * @var User user allowed to trigger a transition
   */
  private $user;

  /**
   * @var \BakeryDMS\Core\DMS reference to the dms instance this attribute belongs to
   */
  var $dms;

  function __construct($id, $transition, $user) {
    $this->id = $id;
    $this->transition = $transition;
    $this->user = $user;
  }

  function setDMS($dms) {
    $this->dms = $dms;
  }

  /**
   * Get the transition itself
   *
   * @return Transition group
   */
  function getTransition() {
    return $this->transition;
  }

  /**
   * Get the user who is allowed to trigger the transition
   *
   * @return object user
   */
  function getUser() {
    return $this->user;
  }
}