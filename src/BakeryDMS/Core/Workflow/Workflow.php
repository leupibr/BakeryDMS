<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core\Workflow;

/**
 * Class to represent an workflow in the document management system.
 */
class Workflow {
  /**
   * @var int id of workflow
   */
  private $id;

  /**
   * @var string name of the workflow
   */
  private $name;

  /**
   * @var \BakeryDMS\Core\Workflow\State Initial state of the workflow
   */
  private $initialState;

  /**
   * @var Transition[] The transitions of this workflow.
   */
  private $transitions;

  /**
   * @var \BakeryDMS\Core\DMS reference to the dms instance this attribute belongs to
   */
  private $dms;

  /**
   * Initializes a new instance of the {@link Workflow} class.
   *
   * @param $id int The id of the workflow.
   * @param $name string The name of the workflow.
   * @param $initialState \BakeryDMS\Core\Workflow\State The initial state of the workflow.
   */
  function __construct($id, $name, $initialState) {
    $this->id = $id;
    $this->name = $name;
    $this->initialState = $initialState;
    $this->transitions = null;
    $this->dms = null;
  }

  function setDMS($dms) {
    $this->dms = $dms;
  }

  function getID() {
    return $this->id;
  }

  function getName() {
    return $this->name;
  }

  function setName($newName) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblWorkflows SET name = " . $db->qstr($newName) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->name = $newName;
    return true;
  }

  function getInitState() {
    return $this->initialState;
  }

  function setInitState($state) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblWorkflows SET initstate = " . $state->getID() . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->initialState = $state;
    return true;
  }

  function getTransitions() {
    $db = $this->dms->getDB();

    if ($this->transitions) {
      return $this->transitions;
    }

    $queryStr = "SELECT * FROM tblWorkflowTransitions WHERE workflow=" . $this->id;
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    $transitions = array();
    for ($i = 0; $i < count($resArr); $i++) {
      $transition = new Transition($resArr[$i]["id"],
        $this,
        $this->dms->getWorkflowState($resArr[$i]["state"]),
        $this->dms->getWorkflowAction($resArr[$i]["action"]),
        $this->dms->getWorkflowState($resArr[$i]["nextstate"]),
        $resArr[$i]["maxtime"]);
      $transition->setDMS($this->dms);
      $transitions[$resArr[$i]["id"]] = $transition;
    }

    $this->transitions = $transitions;

    return $this->transitions;
  }

  function getStates() {
    $db = $this->dms->getDB();

    if (!$this->transitions) {
      $this->getTransitions();
    }

    $states = array();
    foreach ($this->transitions as $transition) {
      if (!isset($states[$transition->getState()->getID()])) {
        $states[$transition->getState()->getID()] = $transition->getState();
      }
      if (!isset($states[$transition->getNextState()->getID()])) {
        $states[$transition->getNextState()->getID()] = $transition->getNextState();
      }
    }

    return $states;
  }

  /**
   * Get the transition by its id
   *
   * @param integer $id id of transition
   * @param object transition
   */
  function getTransition($id) {
    $db = $this->dms->getDB();

    if (!$this->transitions) {
      $this->getTransitions();
    }

    if ($this->transitions[$id]) {
      return $this->transitions[$id];
    }

    return false;
  }

  /**
   * Get the transitions that can be triggered while being in the given state
   *
   * @param object $state current workflow state
   *
   * @return Transition[]
   */
  function getNextTransitions($state) {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblWorkflowTransitions WHERE workflow=" . $this->id . " AND state=" . $state->getID();
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return [];
    }

    $result = [];
    foreach ($resArr as $transitionData) {
      $transition = new Transition($transitionData["id"], $this,
        $this->dms->getWorkflowState($transitionData["state"]),
        $this->dms->getWorkflowAction($transitionData["action"]),
        $this->dms->getWorkflowState($transitionData["nextstate"]),
        $transitionData["maxtime"]);
      $transition->setDMS($this->dms);
      $result[] = $transition;
    }

    return $result;
  }

  /**
   * Get the transitions that lead to the given state
   *
   * @param object $state current workflow state
   * @param array list of transitions
   */
  function getPreviousTransitions($state) {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblWorkflowTransitions WHERE workflow=" . $this->id . " AND nextstate=" . $state->getID();
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    $wkftransitions = array();
    for ($i = 0; $i < count($resArr); $i++) {
      $wkftransition = new Transition($resArr[$i]["id"],
        $this,
        $this->dms->getWorkflowState($resArr[$i]["state"]),
        $this->dms->getWorkflowAction($resArr[$i]["action"]),
        $this->dms->getWorkflowState($resArr[$i]["nextstate"]),
        $resArr[$i]["maxtime"]);
      $wkftransition->setDMS($this->dms);
      $wkftransitions[$i] = $wkftransition;
    }

    return $wkftransitions;
  }

  /**
   * Get all transitions from one state into another state
   *
   * @param object $state state to start from
   * @param object $nextstate state after transition
   * @param array list of transitions
   */
  function getTransitionsByStates($state, $nextstate) {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblWorkflowTransitions WHERE workflow=" . $this->id . " AND state=" . $state->getID() . " AND nextstate=" . $nextstate->getID();
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    $wkftransitions = array();
    for ($i = 0; $i < count($resArr); $i++) {
      $wkftransition = new Transition($resArr[$i]["id"],
        $this,
        $this->dms->getWorkflowState($resArr[$i]["state"]),
        $this->dms->getWorkflowAction($resArr[$i]["action"]),
        $this->dms->getWorkflowState($resArr[$i]["nextstate"]),
        $resArr[$i]["maxtime"]);
      $wkftransition->setDMS($this->dms);
      $wkftransitions[$i] = $wkftransition;
    }

    return $wkftransitions;
  }

  /**
   * Remove a transition from a workflow
   * Deprecated! Use BakeryDMS\Core\Workflow\Transition::remove() instead.
   *
   * @param object $transition
   *
   * @return boolean true if no error occurred, otherwise false
   */
  function removeTransition($transition) {
    return $transition->remove();
  }

  /**
   * Add new transition to workflow
   *
   * @param object $state
   * @param object $action
   * @param object $nextstate
   * @param array $users
   * @param array $groups
   *
   * @return object instance of new transition
   */
  function addTransition($state, $action, $nextstate, $users, $groups) {
    $db = $this->dms->getDB();

    $db->startTransaction();
    $queryStr = "INSERT INTO tblWorkflowTransitions (workflow, state, action, nextstate) VALUES (" . $this->id . ", " . $state->getID() . ", " . $action->getID() . ", " . $nextstate->getID() . ")";
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $transition = $this->getTransition($db->getInsertID());

    foreach ($users as $user) {
      $queryStr = "INSERT INTO tblWorkflowTransitionUsers (transition, userid) VALUES (" . $transition->getID() . ", " . $user->getID() . ")";
      if (!$db->getResult($queryStr)) {
        $db->rollbackTransaction();
        return false;
      }
    }

    foreach ($groups as $group) {
      $queryStr = "INSERT INTO tblWorkflowTransitionGroups (transition, groupid, minusers) VALUES (" . $transition->getID() . ", " . $group->getID() . ", 1)";
      if (!$db->getResult($queryStr)) {
        $db->rollbackTransaction();
        return false;
      }
    }

    $db->commitTransaction();
    return $transition;
  }

  /**
   * Check if workflow is currently used by any document
   *
   * @return boolean true if workflow is used, otherwise false
   */
  function isUsed() {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblWorkflowDocumentContent WHERE workflow=" . $this->id;
    $resArr = $db->getResultArray($queryStr);
    if (is_array($resArr) && count($resArr) == 0) {
      return false;
    }
    return true;
  }

  /**
   * Remove the workflow and all its transitions
   * Do not remove actions and states of the workflow
   *
   * @return boolean true on success or false in case of an error
   *         false is also returned if the workflow is currently in use
   */
  function remove() {
    $db = $this->dms->getDB();

    if ($this->isUsed()) {
      return false;
    }

    $db->startTransaction();

    $queryStr = "DELETE FROM tblWorkflowTransitions WHERE workflow = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $queryStr = "DELETE FROM tblWorkflowMandatoryWorkflow WHERE workflow = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    // Delete workflow itself
    $queryStr = "DELETE FROM tblWorkflows WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $db->commitTransaction();

    return true;
  }

}