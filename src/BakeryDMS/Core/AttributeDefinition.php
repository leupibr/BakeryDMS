<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

/**
 * Class to represent an attribute definition in the document management system.
 *
 * Attribute definitions specify the name, type, object type, minimum and
 * maximum values and a value set. The object type determines the object
 * an attribute may be attached to. If the object type is set to object_all
 * the attribute can be used for documents, document content and folders.
 *
 * The type of an attribute specifies the scalar data type.
 *
 * Attributes for which multiple values are allowed must have the
 * multiple flag set to true and specify a value set. A value set
 * is a string consisting of n separated values. The separator is the
 * first char of the value set. A possible value could be '|REV-A|REV-B'
 * If multiple values are allowed, then minValues and maxValues may
 * restrict the allowed number of values.
 */
class AttributeDefinition {
  /** @var integer id of attribute definition */
  private $id;

  /** @var string name of attribute definition */
  private $name;

  /**
   * @var int object type of attribute definition. This can be one of
   * {@link type_int|type_float|type_string|type_boolean}
   */
  private $type;

  /**
   * @var int type of attribute definition. This can be one of
   * {@link objtype_all|objtype_folder|objtype_document|objtype_documentcontent}
   */
  private $objectType;

  /** @var boolean whether an attribute can have multiple values. */
  private $multipleValueAllowed;

  /** @var integer minimum values of an attribute. */
  private $minValues;

  /** @var integer maximum values of an attribute. */
  private $maxValues;

  /** @var string list of possible values of an attribute. */
  private $valueSet;

  /** @var string regular expression the value must match. */
  private $regexPattern;

  /** @var \BakeryDMS\Core\DMS reference to the dms instance this attribute definition belongs to. */
  private $dms;

  /*
   * Possible scalar data types of an attribute
   */
  const type_int = '1';
  const type_float = '2';
  const type_string = '3';
  const type_boolean = '4';

  /*
   * The object type for which a attribute may be used
   */
  const objtype_all = '0';
  const objtype_folder = '1';
  const objtype_document = '2';
  const objtype_documentcontent = '3';

  /**
   * Initializes a new instance of the {@link AttributeDefinition} class.
   *
   * @param integer $id internal id of attribute definition
   * @param string $name name of attribute
   * @param int $objectType type of object for which this attribute definition
   *        may be used.
   * @param integer $type scalar type of attribute
   * @param boolean $multiple set to true if multiple values are allowed
   * @param integer $minValues minimum number of values
   * @param integer $maxValues maximum number of values
   * @param string $valueSet separated list of allowed values, the first char
   *        is taken as the separator
   * @param string $regex The regex for checking the input.
   */
  function __construct($id, $name, $objectType, $type, $multiple, $minValues, $maxValues, $valueSet, $regex) {
    $this->id = $id;
    $this->name = $name;
    $this->type = $type;
    $this->objectType = $objectType;
    $this->multipleValueAllowed = $multiple;
    $this->minValues = $minValues;
    $this->maxValues = $maxValues;
    $this->valueSet = $valueSet;
    $this->_separator = '';
    $this->regexPattern = $regex;
    $this->dms = null;
  }

  /**
   * Set reference to dms.
   *
   * @param \BakeryDMS\Core\DMS $dms
   */
  function setDMS($dms) {
    $this->dms = $dms;
  }

  function getID() {
    return $this->id;
  }

  function getName() {
    return $this->name;
  }

  function setName($name) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblAttributeDefinitions SET name =" . $db->qstr($name) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->name = $name;
    return true;
  }

  function getObjType() {
    return $this->objectType;
  }

  function setObjType($objectType) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblAttributeDefinitions SET objtype =" . intval($objectType) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->objectType = $objectType;
    return true;
  }

  function getType() {
    return $this->type;
  }

  function setType($type) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblAttributeDefinitions SET type =" . intval($type) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->type = $type;
    return true;
  }

  function getMultipleValues() {
    return $this->multipleValueAllowed;
  }

  function setMultipleValues($mv) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblAttributeDefinitions SET multiple =" . intval($mv) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->multipleValueAllowed = $mv;
    return true;
  }

  function getMinValues() {
    return $this->minValues;
  }

  function setMinValues($minValues) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblAttributeDefinitions SET minvalues =" . intval($minValues) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->minValues = $minValues;
    return true;
  }

  function getMaxValues() {
    return $this->maxValues;
  }

  function setMaxValues($maxValues) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblAttributeDefinitions SET maxvalues =" . intval($maxValues) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->maxValues = $maxValues;
    return true;
  }

  /**
   * Get the value set as saved in the database
   *
   * @return string value set
   */
  function getValueSet() {
    return $this->valueSet;
  }

  /**
   * Get the whole value set as an array
   *
   * @return array values of value set or false if the value set has
   *         less than 2 chars
   */
  function getValueSetAsArray() {
    if (strlen($this->valueSet) > 1) {
      return explode($this->valueSet[0], substr($this->valueSet, 1));
    }
    else {
      return false;
    }
  }

  /**
   * Get the n'th value of a value set
   *
   * @param integer $ind
   * @return string n'th value of value set or false if the index is
   *         out of range or the value set has less than 2 chars
   */
  function getValueSetValue($ind) {
    if (strlen($this->valueSet) > 1) {
      $tmp = explode($this->valueSet[0], substr($this->valueSet, 1));
      if (isset($tmp[$ind])) {
        return $tmp[$ind];
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }

  /**
   * Set the value set
   *
   * A value set is a list of values allowed for an attribute. The values
   * are separated by a char which must also be the first char of the
   * value set string.
   *
   * @param string $valueSet
   * @return boolean true if value set could be set, otherwise false
   */
  function setValueSet($valueSet) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblAttributeDefinitions SET valueset =" . $db->qstr($valueSet) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->valueSet = $valueSet;
    $this->_separator = substr($valueSet, 0, 1);
    return true;
  }

  /**
   * Get the regular expression as saved in the database
   *
   * @return string regular expression
   */
  function getRegex() {
    return $this->regexPattern;
  }

  /**
   * Set the regular expression
   *
   * A value of the attribute must match this regular expression.
   *
   * @param string $regex
   * @return boolean true if regex could be set, otherwise false
   */
  function setRegex($regex) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblAttributeDefinitions SET regex =" . $db->qstr($regex) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->regexPattern = $regex;
    return true;
  }

  /**
   * Check if the attribute definition is used
   *
   * Checks all documents, folders and document content whether at least
   * one of them referenceѕ this attribute definition
   *
   * @return boolean true if attribute definition is used, otherwise false
   */
  function isUsed() {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblDocumentAttributes WHERE attrdef=" . $this->id;
    $resArr = $db->getResultArray($queryStr);
    if (is_array($resArr) && count($resArr) == 0) {
      $queryStr = "SELECT * FROM tblFolderAttributes WHERE attrdef=" . $this->id;
      $resArr = $db->getResultArray($queryStr);
      if (is_array($resArr) && count($resArr) == 0) {
        $queryStr = "SELECT * FROM tblDocumentContentAttributes WHERE attrdef=" . $this->id;
        $resArr = $db->getResultArray($queryStr);
        if (is_array($resArr) && count($resArr) == 0) {

          return false;
        }
      }
    }
    return true;
  }

  /**
   * Return a list of documents, folders, document contents where this
   * attribute definition is used
   *
   * @param integer $limit return not more the n objects of each type
   * @return boolean true if attribute definition is used, otherwise false
   */
  function getStatistics($limit = 0) {
    $db = $this->dms->getDB();

    $result = array('docs' => array(), 'folders' => array(), 'contents' => array());
    if ($this->objectType == AttributeDefinition::objtype_all ||
      $this->objectType == AttributeDefinition::objtype_document
    ) {
      $queryStr = "SELECT * FROM tblDocumentAttributes WHERE attrdef=" . $this->id;
      if ($limit) {
        $queryStr .= " limit " . (int) $limit;
      }
      $resArr = $db->getResultArray($queryStr);
      if ($resArr) {
        foreach ($resArr as $rec) {
          if ($doc = $this->dms->getDocument($rec['document'])) {
            $result['docs'][] = $doc;
          }
        }
      }
      $queryStr = "SELECT count(*) c, value FROM tblDocumentAttributes WHERE attrdef=" . $this->id . " GROUP BY value ORDER BY c DESC";
      $resArr = $db->getResultArray($queryStr);
      if ($resArr) {
        $result['frequencies'] = $resArr;
      }
    }

    if ($this->objectType == AttributeDefinition::objtype_all ||
      $this->objectType == AttributeDefinition::objtype_folder
    ) {
      $queryStr = "SELECT * FROM tblFolderAttributes WHERE attrdef=" . $this->id;
      if ($limit) {
        $queryStr .= " limit " . (int) $limit;
      }
      $resArr = $db->getResultArray($queryStr);
      if ($resArr) {
        foreach ($resArr as $rec) {
          if ($folder = $this->dms->getFolder($rec['folder'])) {
            $result['folders'][] = $folder;
          }
        }
      }
    }

    if ($this->objectType == AttributeDefinition::objtype_all ||
      $this->objectType == AttributeDefinition::objtype_documentcontent
    ) {
      $queryStr = "SELECT * FROM tblDocumentContentAttributes WHERE attrdef=" . $this->id;
      if ($limit) {
        $queryStr .= " limit " . (int) $limit;
      }
      $resArr = $db->getResultArray($queryStr);
      if ($resArr) {
        foreach ($resArr as $rec) {
          if ($content = $this->dms->getDocumentContent($rec['content'])) {
            $result['contents'][] = $content;
          }
        }
      }
    }

    return $result;
  }

  /**
   * Remove the attribute definition
   * Removal is only executed when the definition is not used anymore.
   *
   * @return boolean true on success or false in case of an error
   */
  function remove() {
    $db = $this->dms->getDB();

    if ($this->isUsed()) {
      return false;
    }

    // Delete user itself
    $queryStr = "DELETE FROM tblAttributeDefinitions WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    return true;
  }

  /**
   * Get all documents and folder by a given attribute value
   *
   * @param string $attributeValue value of attribute
   * @param integer $limit limit number of documents/folders
   * @return array array containing list of documents and folders
   */
  public function getObjects($attributeValue, $limit) {
    $db = $this->dms->getDB();

    $result = array('docs' => array(), 'folders' => array(), 'contents' => array());
    if ($this->objectType == AttributeDefinition::objtype_all ||
      $this->objectType == AttributeDefinition::objtype_document
    ) {
      $queryStr = "SELECT * FROM tblDocumentAttributes WHERE attrdef=" . $this->id . " AND value=" . $db->qstr($attributeValue);
      if ($limit) {
        $queryStr .= " limit " . (int) $limit;
      }
      $resArr = $db->getResultArray($queryStr);
      if ($resArr) {
        foreach ($resArr as $rec) {
          if ($doc = $this->dms->getDocument($rec['document'])) {
            $result['docs'][] = $doc;
          }
        }
      }
    }

    if ($this->objectType == AttributeDefinition::objtype_all ||
      $this->objectType == AttributeDefinition::objtype_folder
    ) {
      $queryStr = "SELECT * FROM tblFolderAttributes WHERE attrdef=" . $this->id . " AND value=" . $db->qstr($attributeValue);
      if ($limit) {
        $queryStr .= " limit " . (int) $limit;
      }
      $resArr = $db->getResultArray($queryStr);
      if ($resArr) {
        foreach ($resArr as $rec) {
          if ($folder = $this->dms->getFolder($rec['folder'])) {
            $result['folders'][] = $folder;
          }
        }
      }
    }

    return $result;
  }
}

?>
