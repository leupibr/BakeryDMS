<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

/**
 * Used to indicate that a search should return all
 * results in the ACL table. See {@link BakeryDMS\Core\Folder::getAccessList()}
 */
class AccessMode {
  const ANY = -1;
  const NONE = 1; // No rights at all
  const READ = 2; // Read access only
  const READWRITE = 3; // Read and write access only
  const ALL = 4; // Unrestricted access


  /**
   * @param int $mode The access mode by its integer value.
   * @return string
   */
  public static function getText($mode) {
    switch ($mode) {
      case AccessMode::NONE:
        return t("access_mode_none");
        break;
      case AccessMode::READ:
        return t("access_mode_read");
        break;
      case AccessMode::READWRITE:
        return t("access_mode_readwrite");
        break;
      case AccessMode::ALL:
        return t("access_mode_all");
        break;
    }
  }
}