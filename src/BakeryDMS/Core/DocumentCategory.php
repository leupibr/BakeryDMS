<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;
/**
 * Class to represent a document category in the document management system
 */
class DocumentCategory {
  /**
   * @var int id of document category
   */
  private $id;

  /**
   * @var string name of category
   */
  private $name;

  /**
   * @var object reference to dms this category belongs to
   */
  private $dms;

  /**
   * Initializes a new instance of the {@link DocumentCategory} class.
   *
   * @param $id int The id of the category.
   * @param $name string The name of the category.
   */
  function __construct($id, $name) {
    $this->id = $id;
    $this->name = $name;
    $this->dms = null;
  }

  /**
   * Sets the link to the dms.
   * @param $dms \BakeryDMS\Core\DMS The dms link.
   */
  function setDMS($dms) {
    $this->dms = $dms;
  }

  /**
   * @return int Gets the id.
   */
  function getID() {
    return $this->id;
  }

  /**
   * @return string Gets the name.
   */
  function getName() {
    return $this->name;
  }

  /**
   * Sets the name.
   * @param $newName string The new name to set.
   * @return bool true if successful, false otherwise.
   */
  function setName($newName) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblCategory SET name = " . $db->qstr($newName) . " WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->name = $newName;
    return true;
  }

  /**
   * Checks if the category is in use.
   * @return bool True if is in use, false otherwise.
   */
  function isUsed() {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblDocumentCategory WHERE categoryID=" . $this->id;
    $resArr = $db->getResultArray($queryStr);
    if (is_array($resArr) && count($resArr) == 0) {
      return false;
    }
    return true;
  }

  /**
   * Get a array of categories.
   * @return array The categories.
   */
  function getCategories() {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblCategory";
    return $db->getResultArray($queryStr);
  }

  function addCategory($keywords) {
    $db = $this->dms->getDB();

    $queryStr = "INSERT INTO tblCategory (category) VALUES (" . $db->qstr($keywords) . ")";
    return $db->getResult($queryStr);
  }

  function remove() {
    $db = $this->dms->getDB();

    $queryStr = "DELETE FROM tblCategory WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    return true;
  }

  function getDocumentsByCategory() {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblDocumentCategory WHERE categoryID=" . $this->id;
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    $documents = array();
    foreach ($resArr as $row) {
      array_push($documents, $this->dms->getDocument($row["id"]));
    }
    return $documents;
  }

}

?>
