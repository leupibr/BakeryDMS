<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

use BakeryDMS\Core\Workflow\Log;
use BakeryDMS\Core\Workflow\State;
use BakeryDMS\Core\Workflow\Workflow;
use BakeryDMS\Utils\File;

/**
 * Class to represent content of a document.
 *
 * Each document has content attached to it, often called a 'version' of the
 * document. The document content represents a file on the disk with some
 * meta data stored in the database. A document content has a version number
 * which is incremented with each replacement of the old content. Old versions
 * are kept unless they are explicitly deleted by
 * {@link BakeryDMS\Core\Document::removeContent()}.
 */
class DocumentContent extends Object {
  /**
   * Recalculate the status of a document
   * The methods checks the review and approval status and sets the
   * status of the document accordingly.
   * If status is BakeryDMS\Core\DocumentState::RELEASED and version has workflow set status
   * to BakeryDMS\Core\DocumentState::IN_WORKFLOW
   * If status is BakeryDMS\Core\DocumentState::RELEASED and there are reviewers set status BakeryDMS\Core\DocumentState::DRAFT_REV
   * If status is BakeryDMS\Core\DocumentState::RELEASED or BakeryDMS\Core\DocumentState::DRAFT_REV and there are approvers set
   * status BakeryDMS\Core\DocumentState::DRAFT_APP
   * If status is draft and there are no approver and no reviewers set
   * status to BakeryDMS\Core\DocumentState::RELEASED
   * The status of a document with the current status BakeryDMS\Core\DocumentState::OBSOLETE, BakeryDMS\Core\DocumentState::REJECTED,
   * or BakeryDMS\Core\DocumentState::EXPIRED will not be changed unless the parameter
   * $ignorecurrentstatus is set to true.
   *
   * @param boolean $ignorecurrentstatus ignore the current status and
   *        recalculate a new status in any case
   * @param object $user the user initiating this method
   */
  function verifyStatus($ignorecurrentstatus = false, $user = null) {

    unset($this->_status);
    $st = $this->getStatus();

    if (!$ignorecurrentstatus && ($st["status"] == DocumentState::OBSOLETE || $st["status"] == DocumentState::REJECTED || $st["status"] == DocumentState::EXPIRED)) {
      return;
    }

    $pendingReview = false;
    unset($this->_reviewStatus);  // force to be reloaded from DB
    $reviewStatus = $this->getReviewStatus();
    if (is_array($reviewStatus) && count($reviewStatus) > 0) {
      foreach ($reviewStatus as $r) {
        if ($r["status"] == 0) {
          $pendingReview = true;
          break;
        }
      }
    }
    $pendingApproval = false;
    unset($this->_approvalStatus);  // force to be reloaded from DB
    $approvalStatus = $this->getApprovalStatus();
    if (is_array($approvalStatus) && count($approvalStatus) > 0) {
      foreach ($approvalStatus as $a) {
        if ($a["status"] == 0) {
          $pendingApproval = true;
          break;
        }
      }
    }

    unset($this->_workflow); // force to be reloaded from DB
    if ($this->getWorkflow()) {
      $this->setStatus(DocumentState::IN_WORKFLOW, "", $user);
    }
    elseif ($pendingReview) {
      $this->setStatus(DocumentState::DRAFT_REV, "", $user);
    }
    elseif ($pendingApproval) {
      $this->setStatus(DocumentState::DRAFT_APP, "", $user);
    }
    else {
      $this->setStatus(DocumentState::RELEASED, "", $user);
    }
  }

  function __construct($id,
                       $document,
                       $version,
                       $comment,
                       $date,
                       $userID,
                       $dir,
                       $orgFileName,
                       $fileType,
                       $mimeType,
                       $fileSize = 0,
                       $checksum = '') {
    parent::__construct($id);
    $this->_document = $document;
    $this->_version = (int) $version;
    $this->_comment = $comment;
    $this->_date = $date;
    $this->_userID = (int) $userID;
    $this->_dir = $dir;
    $this->_orgFileName = $orgFileName;
    $this->_fileType = $fileType;
    $this->_mimeType = $mimeType;
    $this->_dms = $document->_dms;
    if (!$fileSize) {
      $this->_fileSize = File::fileSize($this->_dms->contentDir . $this->getPath());
    }
    else {
      $this->_fileSize = $fileSize;
    }
    $this->_checksum = $checksum;
    $this->_workflow = null;
    $this->_workflowState = null;
  }

  function getVersion() {
    return $this->_version;
  }

  function getComment() {
    return $this->_comment;
  }

  function getDate() {
    return $this->_date;
  }

  function getOriginalFileName() {
    return $this->_orgFileName;
  }

  function getFileType() {
    return $this->_fileType;
  }

  function getFileName() {
    return $this->_version . $this->_fileType;
  }

  function getDir() {
    return $this->_dir;
  }

  function getMimeType() {
    return $this->_mimeType;
  }

  function getDocument() {
    return $this->_document;
  }

  function getUser() {
    if (!isset($this->_user)) {
      $this->_user = $this->_document->_dms->getUser($this->_userID);
    }
    return $this->_user;
  }

  function getPath() {
    return $this->_document->getDir() . $this->_version . $this->_fileType;
  }

  function setDate($date = false) {
    $db = $this->_document->_dms->getDB();

    if (!$date) {
      $date = time();
    }

    $queryStr = "UPDATE tblDocumentContent SET date = " . (int) $date . " WHERE `document` = " . $this->_document->getID() . " AND `version` = " . $this->_version;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->_date = $date;

    return true;
  }

  function getFileSize() {
    return $this->_fileSize;
  }

  /**
   * Set file size by reading the file
   */
  function setFileSize() {
    $filesize = File::fileSize($this->_dms->contentDir . $this->_document->getDir() . $this->getFileName());
    if ($filesize === false) {
      return false;
    }

    $db = $this->_document->_dms->getDB();
    $queryStr = "UPDATE tblDocumentContent SET fileSize = " . $filesize . " WHERE `document` = " . $this->_document->getID() . " AND `version` = " . $this->_version;
    if (!$db->getResult($queryStr)) {
      return false;
    }
    $this->_fileSize = $filesize;

    return true;
  }

  function getChecksum() {
    return $this->_checksum;
  }

  /**
   * Set checksum by reading the file
   */
  function setChecksum() {
    $checksum = File::checksum($this->_dms->contentDir . $this->_document->getDir() . $this->getFileName());
    if ($checksum === false) {
      return false;
    }

    $db = $this->_document->_dms->getDB();
    $queryStr = "UPDATE tblDocumentContent SET checksum = " . $db->qstr($checksum) . " WHERE `document` = " . $this->_document->getID() . " AND `version` = " . $this->_version;
    if (!$db->getResult($queryStr)) {
      return false;
    }
    $this->_checksum = $checksum;

    return true;
  }

  function setComment($newComment) {
    $db = $this->_document->_dms->getDB();

    $queryStr = "UPDATE tblDocumentContent SET comment = " . $db->qstr($newComment) . " WHERE `document` = " . $this->_document->getID() . " AND `version` = " . $this->_version;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->_comment = $newComment;

    return true;
  }

  /**
   * This function is deprecated
   */
  function convert() {
    if (file_exists($this->_document->_dms->contentDir . $this->_document->getID() . '/' . "index.html")) {
      return true;
    }

    if (!in_array($this->_fileType, array_keys($this->_document->_dms->convertFileTypes))) {
      return false;
    }

    $source = $this->_document->_dms->contentDir . $this->_document->getID() . '/' . $this->getFileName();
    $target = $this->_document->_dms->contentDir . $this->_document->getID() . '/' . "index.html";

    $command = $this->_document->_dms->convertFileTypes[$this->_fileType];
    $command = str_replace("{SOURCE}", "\"$source\"", $command);
    $command = str_replace("{TARGET}", "\"$target\"", $command);

    $output = array();
    $res = 0;
    exec($command, $output, $res);

    if ($res != 0) {
      print (implode("\n", $output));
      return false;
    }
    return true;
  }

  /**
   * FIXME: this function should not be part of the DMS. It lies in the duty
   * of the application whether a file can be viewed online or not.
   */
  function viewOnline() {
    if (!isset($this->_document->_dms->_viewOnlineFileTypes) || !is_array($this->_document->_dms->_viewOnlineFileTypes)) {
      return false;
    }

    if (in_array(strtolower($this->_fileType), $this->_document->_dms->_viewOnlineFileTypes)) {
      return true;
    }

    if ($this->_document->_dms->enableConverting && in_array($this->_fileType,
        array_keys($this->_document->_dms->convertFileTypes))
    ) {
      if ($this->wasConverted()) {
        return true;
      }
    }

    return false;
  }

  function wasConverted() {
    return file_exists($this->_document->_dms->contentDir . $this->_document->getID() . '/' . "index.html");
  }

  /**
   * This function is deprecated
   */
  function getURL() {
    if (!$this->viewOnline()) {
      return false;
    }

    if (in_array(strtolower($this->_fileType), $this->_document->_dms->_viewOnlineFileTypes)) {
      return "/" . $this->_document->getID() . "/" . $this->_version . "/" . $this->getOriginalFileName();
    }
    else {
      return "/" . $this->_document->getID() . "/" . $this->_version . "/index.html";
    }
  }

  /**
   * Get the latest status of the content
   *
   * The status of the content reflects its current review, approval or workflow
   * state. A status can be a negative or positive number or 0. A negative
   * numbers indicate a missing approval, review or an obsolete content.
   * Positive numbers indicate some kind of approval or workflow being
   * active, but not necessarily a release.
   * BakeryDMS\Core\DocumentState::DRAFT_REV, 0
   * BakeryDMS\Core\DocumentState::DRAFT_APP, 1
   * BakeryDMS\Core\DocumentState::RELEASED, 2
   * BakeryDMS\Core\DocumentState::IN_WORKFLOW, 3
   * BakeryDMS\Core\DocumentState::REJECTED, -1
   * BakeryDMS\Core\DocumentState::OBSOLETE, -2
   * BakeryDMS\Core\DocumentState::EXPIRED, -3
   * When a content is inserted and does not need approval nor review,
   * then its status is set to BakeryDMS\Core\DocumentState::RELEASED immediately. Any change of
   * the status is monitored in the table tblDocumentStatusLog. This
   * function will always return the latest entry for the content.
   */
  function getStatus($limit = 1) {
    $db = $this->_document->_dms->getDB();

    if (!is_numeric($limit)) {
      return false;
    }

    // Retrieve the current overall status of the content represented by
    // this object.
    if (!isset($this->_status)) {
      $queryStr =
        "SELECT `tblDocumentStatus`.*, `tblDocumentStatusLog`.`status`, " .
        "`tblDocumentStatusLog`.`comment`, `tblDocumentStatusLog`.`date`, " .
        "`tblDocumentStatusLog`.`userID` " .
        "FROM `tblDocumentStatus` " .
        "LEFT JOIN `tblDocumentStatusLog` USING (`statusID`) " .
        "WHERE `tblDocumentStatus`.`documentID` = '" . $this->_document->getID() . "' " .
        "AND `tblDocumentStatus`.`version` = '" . $this->_version . "' " .
        "ORDER BY `tblDocumentStatusLog`.`statusLogID` DESC LIMIT " . (int) $limit;

      $res = $db->getResultArray($queryStr);
      if (is_bool($res) && !$res) {
        return false;
      }
      if (count($res) != 1) {
        return false;
      }
      $this->_status = $res[0];
    }
    return $this->_status;
  }

  /**
   * Get current and former states of the document content
   *
   * @param integer $limit if not set all log entries will be returned
   * @return array list of status changes
   */
  function getStatusLog($limit = 0) {
    $db = $this->_document->_dms->getDB();

    if (!is_numeric($limit)) {
      return false;
    }

    $queryStr =
      "SELECT `tblDocumentStatus`.*, `tblDocumentStatusLog`.`status`, " .
      "`tblDocumentStatusLog`.`comment`, `tblDocumentStatusLog`.`date`, " .
      "`tblDocumentStatusLog`.`userID` " .
      "FROM `tblDocumentStatus` " .
      "LEFT JOIN `tblDocumentStatusLog` USING (`statusID`) " .
      "WHERE `tblDocumentStatus`.`documentID` = '" . $this->_document->getID() . "' " .
      "AND `tblDocumentStatus`.`version` = '" . $this->_version . "' " .
      "ORDER BY `tblDocumentStatusLog`.`statusLogID` DESC ";
    if ($limit) {
      $queryStr .= "LIMIT " . (int) $limit;
    }

    $res = $db->getResultArray($queryStr);
    if (is_bool($res) && !$res) {
      return false;
    }

    return $res;
  }

  /**
   * Set the status of the content
   * Setting the status means to add another entry into the table
   * tblDocumentStatusLog. The method returns also false if the status
   * is already set on the value passed to the method.
   *
   * @param integer $status new status of content
   * @param string $comment comment for this status change
   * @param object $updateUser user initiating the status change
   * @return boolean true on success, otherwise false
   */
  function setStatus($status, $comment, $updateUser) {
    $db = $this->_document->_dms->getDB();

    if (!is_numeric($status)) {
      return false;
    }

    /* return an error if $updateuser is not set */
    if (!$updateUser) {
      return false;
    }

    // If the supplied value lies outside of the accepted range, return an
    // error.
    if ($status < -3 || $status > 3) {
      return false;
    }

    // Retrieve the current overall status of the content represented by
    // this object, if it hasn't been done already.
    if (!isset($this->_status)) {
      $this->getStatus();
    }
    if (!@$this->_status || $this->_status["status"] == $status) {
      return false;
    }
    $queryStr = "INSERT INTO `tblDocumentStatusLog` (`statusID`, `status`, `comment`, `date`, `userID`) " .
      "VALUES ('" . $this->_status["statusID"] . "', '" . (int) $status . "', " . $db->qstr($comment) . ", CURRENT_TIMESTAMP, '" . $updateUser->getID() . "')";
    $res = $db->getResult($queryStr);
    if (is_bool($res) && !$res) {
      return false;
    }

    return true;
  }

  /**
   * Returns the access mode similar to a document
   * There is no real access mode for document content, so this is more
   * like a virtual access mode, derived from the status or workflow
   * of the document content. The idea is to return an access mode
   * BakeryDMS\Core\AccessMode::NONE if the user is still in a workflow or under review/approval.
   * In such a case only those user involved in the workflow/review/approval
   * process should be allowed to see the document. This method could
   * be called by any function that returns the content e.g. getLatestContent()
   * It may as well be used by BakeryDMS\Core\Document::getAccessMode() to
   * prevent access on the whole document if there is just one version.
   * The return value is planed to be either BakeryDMS\Core\AccessMode::NONE or BakeryDMS\Core\AccessMode::READ.
   *
   * @param object $user
   * @return integer mode
   */
  function getAccessMode($u) {
    if (!$this->_workflow) {
      $this->getWorkflow();
    }

    if ($this->_workflow) {
      if (!$this->_workflowState) {
        $this->getWorkflowState();
      }
      $transitions = $this->_workflow->getNextTransitions($this->_workflowState);
      foreach ($transitions as $transition) {
        if ($this->triggerWorkflowTransitionIsAllowed($u, $transition)) {
          return M::READ;
        }
      }
      return M::NONE;
    }

    return M::READ;
  }

  /**
   * Get the current review status of the document content
   * The review status is a list of reviews and its current status
   *
   * @param integer $limit the number of recent status changes per reviewer
   * @return array list of review status
   */
  function getReviewStatus($limit = 1) {
    $db = $this->_document->_dms->getDB();

    if (!is_numeric($limit)) {
      return false;
    }

    // Retrieve the current status of each assigned reviewer for the content
    // represented by this object.
    // FIXME: caching was turned off to make list of review log in ViewDocument
    // possible
    if (1 || !isset($this->_reviewStatus)) {
      /* First get a list of all reviews for this document content */
      $queryStr =
        "SELECT reviewID FROM tblDocumentReviewers WHERE `version`='" . $this->_version
        . "' AND `documentID` = '" . $this->_document->getID() . "' ";
      $recs = $db->getResultArray($queryStr);
      if (is_bool($recs) && !$recs) {
        return false;
      }
      $this->_reviewStatus = array();
      if ($recs) {
        foreach ($recs as $rec) {
          $queryStr =
            "SELECT `tblDocumentReviewers`.*, `tblDocumentReviewLog`.`reviewLogID`, `tblDocumentReviewLog`.`status`, " .
            "`tblDocumentReviewLog`.`comment`, `tblDocumentReviewLog`.`date`, " .
            "`tblDocumentReviewLog`.`userID`, `tblUsers`.`fullName`, `tblGroups`.`name` AS `groupName` " .
            "FROM `tblDocumentReviewers` " .
            "LEFT JOIN `tblDocumentReviewLog` USING (`reviewID`) " .
            "LEFT JOIN `tblUsers` on `tblUsers`.`id` = `tblDocumentReviewers`.`required`" .
            "LEFT JOIN `tblGroups` on `tblGroups`.`id` = `tblDocumentReviewers`.`required`" .
            "WHERE `tblDocumentReviewers`.`reviewID` = '" . $rec['reviewID'] . "' " .
            "ORDER BY `tblDocumentReviewLog`.`reviewLogID` DESC LIMIT " . (int) $limit;

          $res = $db->getResultArray($queryStr);
          if (is_bool($res) && !$res) {
            unset($this->_reviewStatus);
            return false;
          }
          $this->_reviewStatus = array_merge($this->_reviewStatus, $res);
        }
      }
    }
    return $this->_reviewStatus;
  }

  /**
   * Get the current approval status of the document content
   * The approval status is a list of approvals and its current status
   *
   * @param integer $limit the number of recent status changes per approver
   * @return array list of approval status
   */
  function getApprovalStatus($limit = 1) {
    $db = $this->_document->_dms->getDB();

    if (!is_numeric($limit)) {
      return false;
    }

    // Retrieve the current status of each assigned approver for the content
    // represented by this object.
    // FIXME: caching was turned off to make list of approval log in ViewDocument
    // possible
    if (1 || !isset($this->_approvalStatus)) {
      /* First get a list of all approvals for this document content */
      $queryStr =
        "SELECT approveID FROM tblDocumentApprovers WHERE `version`='" . $this->_version
        . "' AND `documentID` = '" . $this->_document->getID() . "' ";
      $recs = $db->getResultArray($queryStr);
      if (is_bool($recs) && !$recs) {
        return false;
      }
      $this->_approvalStatus = array();
      if ($recs) {
        foreach ($recs as $rec) {
          $queryStr =
            "SELECT `tblDocumentApprovers`.*, `tblDocumentApproveLog`.`status`, " .
            "`tblDocumentApproveLog`.`comment`, `tblDocumentApproveLog`.`date`, " .
            "`tblDocumentApproveLog`.`userID`, `tblUsers`.`fullName`, `tblGroups`.`name` AS `groupName` " .
            "FROM `tblDocumentApprovers` " .
            "LEFT JOIN `tblDocumentApproveLog` USING (`approveID`) " .
            "LEFT JOIN `tblUsers` on `tblUsers`.`id` = `tblDocumentApprovers`.`required` " .
            "LEFT JOIN `tblGroups` on `tblGroups`.`id` = `tblDocumentApprovers`.`required`" .
            "WHERE `tblDocumentApprovers`.`approveID` = '" . $rec['approveID'] . "' " .
            "ORDER BY `tblDocumentApproveLog`.`approveLogId` DESC LIMIT " . (int) $limit;

          $res = $db->getResultArray($queryStr);
          if (is_bool($res) && !$res) {
            unset($this->_approvalStatus);
            return false;
          }
          $this->_approvalStatus = array_merge($this->_approvalStatus, $res);
        }
      }
    }
    return $this->_approvalStatus;
  }

  function addIndReviewer($user, $requestUser, $listadmin = false) {
    $db = $this->_document->_dms->getDB();

    $userID = $user->getID();

    // Get the list of users and groups with read access to this document.
    if ($this->_document->getAccessMode($user) < M::READ) {
      return -2;
    }

    // Check to see if the user has already been added to the review list.
    $reviewStatus = $user->getReviewStatus($this->_document->getID(), $this->_version);
    if (is_bool($reviewStatus) && !$reviewStatus) {
      return -1;
    }
    $indstatus = false;
    if (count($reviewStatus["indstatus"]) > 0) {
      $indstatus = array_pop($reviewStatus["indstatus"]);
      if ($indstatus["status"] != -2) {
        // User is already on the list of reviewers; return an error.
        return -3;
      }
    }

    // Add the user into the review database.
    if (!$indstatus || ($indstatus && $indstatus["status"] != -2)) {
      $queryStr = "INSERT INTO `tblDocumentReviewers` (`documentID`, `version`, `type`, `required`) " .
        "VALUES ('" . $this->_document->getID() . "', '" . $this->_version . "', '0', '" . $userID . "')";
      $res = $db->getResult($queryStr);
      if (is_bool($res) && !$res) {
        return -1;
      }
      $reviewID = $db->getInsertID();
    }
    else {
      $reviewID = isset($indstatus["reviewID"]) ? $$indstatus["reviewID"] : NULL;
    }

    $queryStr = "INSERT INTO `tblDocumentReviewLog` (`reviewID`, `status`, `comment`, `date`, `userID`) " .
      "VALUES ('" . $reviewID . "', '0', '', CURRENT_TIMESTAMP, '" . $requestUser->getID() . "')";
    $res = $db->getResult($queryStr);
    if (is_bool($res) && !$res) {
      return -1;
    }

    // Add reviewer to event notification table.
    //$this->_document->addNotify($userID, true);

    return 0;
  }

  function addGrpReviewer($group, $requestUser) {
    $db = $this->_document->_dms->getDB();

    $groupID = $group->getID();

    // Get the list of users and groups with read access to this document.
    if (!isset($this->_readAccessList)) {
      // TODO: error checking.
      $this->_readAccessList = $this->_document->getReadAccessList();
    }
    $approved = false;
    foreach ($this->_readAccessList["groups"] as $appGroup) {
      if ($groupID == $appGroup->getID()) {
        $approved = true;
        break;
      }
    }
    if (!$approved) {
      return -2;
    }

    // Check to see if the group has already been added to the review list.
    $reviewStatus = $group->getReviewStatus($this->_document->getID(), $this->_version);
    if (is_bool($reviewStatus) && !$reviewStatus) {
      return -1;
    }
    if (count($reviewStatus) > 0 && $reviewStatus[0]["status"] != -2) {
      // Group is already on the list of reviewers; return an error.
      return -3;
    }

    // Add the group into the review database.
    if (!isset($reviewStatus[0]["status"]) || (isset($reviewStatus[0]["status"]) && $reviewStatus[0]["status"] != -2)) {
      $queryStr = "INSERT INTO `tblDocumentReviewers` (`documentID`, `version`, `type`, `required`) " .
        "VALUES ('" . $this->_document->getID() . "', '" . $this->_version . "', '1', '" . $groupID . "')";
      $res = $db->getResult($queryStr);
      if (is_bool($res) && !$res) {
        return -1;
      }
      $reviewID = $db->getInsertID();
    }
    else {
      $reviewID = isset($reviewStatus[0]["reviewID"]) ? $reviewStatus[0]["reviewID"] : NULL;
    }

    $queryStr = "INSERT INTO `tblDocumentReviewLog` (`reviewID`, `status`, `comment`, `date`, `userID`) " .
      "VALUES ('" . $reviewID . "', '0', '', CURRENT_TIMESTAMP, '" . $requestUser->getID() . "')";
    $res = $db->getResult($queryStr);
    if (is_bool($res) && !$res) {
      return -1;
    }

    // Add reviewer to event notification table.
    //$this->_document->addNotify($groupID, false);

    return 0;
  }

  /**
   * Add a review to the document content
   *
   * This method will add an entry to the table tblDocumentReviewLog.
   * It will first check if the user is ment to review the document version.
   * It not the return value is -3.
   * Next it will check if the users has been removed from the list of
   * reviewers. In that case -4 will be returned.
   * If the given review status has been set by the user before, it cannot
   * be set again and 0 will be returned. Іf the review could be succesfully
   * added, the review log id will be returned.
   *
   * @see SeedDMS_Core_DocumentContent::setApprovalByInd()
   * @param object $user user doing the review
   * @param object $requestUser user asking for the review, this is mostly
   * the user currently logged in.
   * @param integer $status status of review
   * @param string $comment comment for review
   * @return integer new review log id
   */
  function setReviewByInd($user, $requestUser, $status, $comment) {
    $db = $this->_document->_dms->getDB();

    // Check to see if the user can be removed from the review list.
    $reviewStatus = $user->getReviewStatus($this->_document->getID(), $this->_version);
    if (is_bool($reviewStatus) && !$reviewStatus) {
      return -1;
    }
    if (count($reviewStatus["indstatus"]) == 0) {
      // User is not assigned to review this document. No action required.
      // Return an error.
      return -3;
    }
    $indstatus = array_pop($reviewStatus["indstatus"]);
    if ($indstatus["status"] == -2) {
      // User has been deleted from reviewers
      return -4;
    }
    // Check if the status is really different from the current status
    if ($indstatus["status"] == $status) {
      return 0;
    }

    $queryStr = "INSERT INTO `tblDocumentReviewLog` (`reviewID`, `status`,
  	  `comment`, `date`, `userID`) " .
      "VALUES ('" . $indstatus["reviewID"] . "', '" .
      (int) $status . "', " . $db->qstr($comment) . ", CURRENT_TIMESTAMP, '" .
      $requestUser->getID() . "')";
    $res = $db->getResult($queryStr);
    if (is_bool($res) && !$res) {
      return -1;
    }
    else {
      $reviewLogID = $db->getInsertID();
      return $reviewLogID;
    }
  }

  /**
   * Add a review to the document content
   *
   * This method is similar to
   * {@see BakeryDMS\Core\DocumentContent::setReviewByInd()} but adds a review
   * for a group instead of a user.
   *
   * @param object $group group doing the review
   * @param object $requestUser user asking for the review, this is mostly
   * the user currently logged in.
   * @param integer $status status of review
   * @param string $comment comment for review
   * @return integer new review log id
   */
  function setReviewByGrp($group, $requestUser, $status, $comment) {
    $db = $this->_document->_dms->getDB();

    // Check to see if the user can be removed from the review list.
    $reviewStatus = $group->getReviewStatus($this->_document->getID(), $this->_version);
    if (is_bool($reviewStatus) && !$reviewStatus) {
      return -1;
    }
    if (count($reviewStatus) == 0) {
      // User is not assigned to review this document. No action required.
      // Return an error.
      return -3;
    }
    if ($reviewStatus[0]["status"] == -2) {
      // Group has been deleted from reviewers
      return -4;
    }

    // Check if the status is really different from the current status
    if ($reviewStatus[0]["status"] == $status) {
      return 0;
    }

    $queryStr = "INSERT INTO `tblDocumentReviewLog` (`reviewID`, `status`,
  	  `comment`, `date`, `userID`) " .
      "VALUES ('" . $reviewStatus[0]["reviewID"] . "', '" .
      (int) $status . "', " . $db->qstr($comment) . ", CURRENT_TIMESTAMP, '" .
      $requestUser->getID() . "')";
    $res = $db->getResult($queryStr);
    if (is_bool($res) && !$res) {
      return -1;
    }
    else {
      $reviewLogID = $db->getInsertID();
      return $reviewLogID;
    }
  }

  function addIndApprover($user, $requestUser, $listadmin = false) {
    $db = $this->_document->_dms->getDB();

    $userID = $user->getID();

    // Get the list of users and groups with read access to this document.
    if ($this->_document->getAccessMode($user) < M::READ) {
      return -2;
    }
    /*
		$readAccessList = $this->_document->getReadAccessList($listadmin);
		$approved = false;
		foreach ($readAccessList["users"] as $appUser) {
			if ($userID == $appUser->getID()) {
				$approved = true;
				break;
			}
		}
		if (!$approved) {
			return -2;
		}
		*/

    // Check to see if the user has already been added to the approvers list.
    $approvalStatus = $user->getApprovalStatus($this->_document->getID(), $this->_version);
    if (is_bool($approvalStatus) && !$approvalStatus) {
      return -1;
    }
    $indstatus = false;
    if (count($approvalStatus["indstatus"]) > 0) {
      $indstatus = array_pop($approvalStatus["indstatus"]);
      if ($indstatus["status"] != -2) {
        // User is already on the list of approverss; return an error.
        return -3;
      }
    }

    if (!$indstatus || (isset($indstatus["status"]) && $indstatus["status"] != -2)) {
      // Add the user into the approvers database.
      $queryStr = "INSERT INTO `tblDocumentApprovers` (`documentID`, `version`, `type`, `required`) " .
        "VALUES ('" . $this->_document->getID() . "', '" . $this->_version . "', '0', '" . $userID . "')";
      $res = $db->getResult($queryStr);
      if (is_bool($res) && !$res) {
        return -1;
      }
      $approveID = $db->getInsertID();
    }
    else {
      $approveID = isset($indstatus["approveID"]) ? $indstatus["approveID"] : NULL;
    }

    $queryStr = "INSERT INTO `tblDocumentApproveLog` (`approveID`, `status`, `comment`, `date`, `userID`) " .
      "VALUES ('" . $approveID . "', '0', '', CURRENT_TIMESTAMP, '" . $requestUser->getID() . "')";
    $res = $db->getResult($queryStr);
    if (is_bool($res) && !$res) {
      return -1;
    }

    $approveLogID = $db->getInsertID();
    return $approveLogID;
  }

  function addGrpApprover($group, $requestUser) {
    $db = $this->_document->_dms->getDB();

    $groupID = $group->getID();

    // Get the list of users and groups with read access to this document.
    if (!isset($this->_readAccessList)) {
      // TODO: error checking.
      $this->_readAccessList = $this->_document->getReadAccessList();
    }
    $approved = false;
    foreach ($this->_readAccessList["groups"] as $appGroup) {
      if ($groupID == $appGroup->getID()) {
        $approved = true;
        break;
      }
    }
    if (!$approved) {
      return -2;
    }

    // Check to see if the group has already been added to the approver list.
    $approvalStatus = $group->getApprovalStatus($this->_document->getID(), $this->_version);
    if (is_bool($approvalStatus) && !$approvalStatus) {
      return -1;
    }
    if (count($approvalStatus) > 0 && $approvalStatus[0]["status"] != -2) {
      // Group is already on the list of approvers; return an error.
      return -3;
    }

    // Add the group into the approver database.
    if (!isset($approvalStatus[0]["status"]) || (isset($approvalStatus[0]["status"]) && $approvalStatus[0]["status"] != -2)) {
      $queryStr = "INSERT INTO `tblDocumentApprovers` (`documentID`, `version`, `type`, `required`) " .
        "VALUES ('" . $this->_document->getID() . "', '" . $this->_version . "', '1', '" . $groupID . "')";
      $res = $db->getResult($queryStr);
      if (is_bool($res) && !$res) {
        return -1;
      }
      $approveID = $db->getInsertID();
    }
    else {
      $approveID = isset($approvalStatus[0]["approveID"]) ? $approvalStatus[0]["approveID"] : NULL;
    }

    $queryStr = "INSERT INTO `tblDocumentApproveLog` (`approveID`, `status`, `comment`, `date`, `userID`) " .
      "VALUES ('" . $approveID . "', '0', '', CURRENT_TIMESTAMP, '" . $requestUser->getID() . "')";
    $res = $db->getResult($queryStr);
    if (is_bool($res) && !$res) {
      return -1;
    }

    // Add approver to event notification table.
    //$this->_document->addNotify($groupID, false);

    $approveLogID = $db->getInsertID();
    return $approveLogID;
  }

  /**
   * Sets approval status of a document content for a user
   *
   * This function can be used to approve or reject a document content, or
   * to reset its approval state. In most cases this function will be
   * called by an user, but  an admin may set the approval for
   * somebody else.
   * It is first checked if the user is in the list of approvers at all.
   * Then it is check if the approval status is already -2. In both cases
   * the function returns with an error.
   *
   * @see SeedDMS_Core_DocumentContent::setReviewByInd()
   * @param object $user user in charge for doing the approval
   * @param object $requestUser user actually calling this function
   * @param integer $status the status of the approval, possible values are
   *        0=unprocessed (maybe used to reset a status)
   *        1=approved,
   *       -1=rejected,
   *       -2=user is deleted (use {link
   *       BakeryDMS\Core\DocumentContent::delIndApprover} instead)
   * @param string $comment approval comment
   * @return integer 0 on success, < 0 in case of an error
   */
  function setApprovalByInd($user, $requestUser, $status, $comment) {
    $db = $this->_document->_dms->getDB();

    // Check to see if the user can be removed from the approval list.
    $approvalStatus = $user->getApprovalStatus($this->_document->getID(), $this->_version);
    if (is_bool($approvalStatus) && !$approvalStatus) {
      return -1;
    }
    if (count($approvalStatus["indstatus"]) == 0) {
      // User is not assigned to approve this document. No action required.
      // Return an error.
      return -3;
    }
    $indstatus = array_pop($approvalStatus["indstatus"]);
    if ($indstatus["status"] == -2) {
      // User has been deleted from approvers
      return -4;
    }
    // Check if the status is really different from the current status
    if ($indstatus["status"] == $status) {
      return 0;
    }

    $queryStr = "INSERT INTO `tblDocumentApproveLog` (`approveID`, `status`,
  	  `comment`, `date`, `userID`) " .
      "VALUES ('" . $indstatus["approveID"] . "', '" .
      (int) $status . "', " . $db->qstr($comment) . ", CURRENT_TIMESTAMP, '" .
      $requestUser->getID() . "')";
    $res = $db->getResult($queryStr);
    if (is_bool($res) && !$res) {
      return -1;
    }
    else {
      return 0;
    }
  }

  /**
   * Sets approval status of a document content for a group
   * The functions behaves like
   * {link BakeryDMS\Core\DocumentContent::setApprovalByInd} but does it for
   * group instead of a user
   */
  function setApprovalByGrp($group, $requestUser, $status, $comment) {
    $db = $this->_document->_dms->getDB();

    // Check to see if the user can be removed from the approval list.
    $approvalStatus = $group->getApprovalStatus($this->_document->getID(), $this->_version);
    if (is_bool($approvalStatus) && !$approvalStatus) {
      return -1;
    }
    if (count($approvalStatus) == 0) {
      // User is not assigned to approve this document. No action required.
      // Return an error.
      return -3;
    }
    if ($approvalStatus[0]["status"] == -2) {
      // Group has been deleted from approvers
      return -4;
    }

    // Check if the status is really different from the current status
    if ($approvalStatus[0]["status"] == $status) {
      return 0;
    }

    $queryStr = "INSERT INTO `tblDocumentApproveLog` (`approveID`, `status`,
  	  `comment`, `date`, `userID`) " .
      "VALUES ('" . $approvalStatus[0]["approveID"] . "', '" .
      (int) $status . "', " . $db->qstr($comment) . ", CURRENT_TIMESTAMP, '" .
      $requestUser->getID() . "')";
    $res = $db->getResult($queryStr);
    if (is_bool($res) && !$res) {
      return -1;
    }
    else {
      return 0;
    }
  }

  function delIndReviewer($user, $requestUser) {
    $db = $this->_document->_dms->getDB();

    $userID = $user->getID();

    // Check to see if the user can be removed from the review list.
    $reviewStatus = $user->getReviewStatus($this->_document->getID(), $this->_version);
    if (is_bool($reviewStatus) && !$reviewStatus) {
      return -1;
    }
    if (count($reviewStatus["indstatus"]) == 0) {
      // User is not assigned to review this document. No action required.
      // Return an error.
      return -3;
    }
    $indstatus = array_pop($reviewStatus["indstatus"]);
    if ($indstatus["status"] != 0) {
      // User has already submitted a review or has already been deleted;
      // return an error.
      return -3;
    }

    $queryStr = "INSERT INTO `tblDocumentReviewLog` (`reviewID`, `status`, `comment`, `date`, `userID`) " .
      "VALUES ('" . $indstatus["reviewID"] . "', '-2', '', CURRENT_TIMESTAMP, '" . $requestUser->getID() . "')";
    $res = $db->getResult($queryStr);
    if (is_bool($res) && !$res) {
      return -1;
    }

    return 0;
  }

  function delGrpReviewer($group, $requestUser) {
    $db = $this->_document->_dms->getDB();

    $groupID = $group->getID();

    // Check to see if the user can be removed from the review list.
    $reviewStatus = $group->getReviewStatus($this->_document->getID(), $this->_version);
    if (is_bool($reviewStatus) && !$reviewStatus) {
      return -1;
    }
    if (count($reviewStatus) == 0) {
      // User is not assigned to review this document. No action required.
      // Return an error.
      return -3;
    }
    if ($reviewStatus[0]["status"] != 0) {
      // User has already submitted a review or has already been deleted;
      // return an error.
      return -3;
    }

    $queryStr = "INSERT INTO `tblDocumentReviewLog` (`reviewID`, `status`, `comment`, `date`, `userID`) " .
      "VALUES ('" . $reviewStatus[0]["reviewID"] . "', '-2', '', CURRENT_TIMESTAMP, '" . $requestUser->getID() . "')";
    $res = $db->getResult($queryStr);
    if (is_bool($res) && !$res) {
      return -1;
    }

    return 0;
  }

  function delIndApprover($user, $requestUser) {
    $db = $this->_document->_dms->getDB();

    $userID = $user->getID();

    // Check to see if the user can be removed from the approval list.
    $approvalStatus = $user->getApprovalStatus($this->_document->getID(), $this->_version);
    if (is_bool($approvalStatus) && !$approvalStatus) {
      return -1;
    }
    if (count($approvalStatus["indstatus"]) == 0) {
      // User is not assigned to approve this document. No action required.
      // Return an error.
      return -3;
    }
    $indstatus = array_pop($approvalStatus["indstatus"]);
    if ($indstatus["status"] != 0) {
      // User has already submitted an approval or has already been deleted;
      // return an error.
      return -3;
    }

    $queryStr = "INSERT INTO `tblDocumentApproveLog` (`approveID`, `status`, `comment`, `date`, `userID`) " .
      "VALUES ('" . $indstatus["approveID"] . "', '-2', '', CURRENT_TIMESTAMP, '" . $requestUser->getID() . "')";
    $res = $db->getResult($queryStr);
    if (is_bool($res) && !$res) {
      return -1;
    }

    return 0;
  }

  function delGrpApprover($group, $requestUser) {
    $db = $this->_document->_dms->getDB();

    $groupID = $group->getID();

    // Check to see if the user can be removed from the approver list.
    $approvalStatus = $group->getApprovalStatus($this->_document->getID(), $this->_version);
    if (is_bool($approvalStatus) && !$approvalStatus) {
      return -1;
    }
    if (count($approvalStatus) == 0) {
      // User is not assigned to approve this document. No action required.
      // Return an error.
      return -3;
    }
    if ($approvalStatus[0]["status"] != 0) {
      // User has already submitted an approval or has already been deleted;
      // return an error.
      return -3;
    }

    $queryStr = "INSERT INTO `tblDocumentApproveLog` (`approveID`, `status`, `comment`, `date`, `userID`) " .
      "VALUES ('" . $approvalStatus[0]["approveID"] . "', '-2', '', CURRENT_TIMESTAMP, '" . $requestUser->getID() . "')";
    $res = $db->getResult($queryStr);
    if (is_bool($res) && !$res) {
      return -1;
    }

    return 0;
  }

  /**
   * Set state of workflow assigned to the document content
   *
   * @param object $state
   */
  function setWorkflowState($state) {
    $db = $this->_document->_dms->getDB();

    if ($this->_workflow) {
      $queryStr = "UPDATE tblWorkflowDocumentContent SET state=" . $state->getID() . " WHERE workflow=" . intval($this->_workflow->getID()) . " AND document=" . intval($this->_document->getID()) . " AND version=" . intval($this->_version) . "";
      if (!$db->getResult($queryStr)) {
        return false;
      }
      $this->_workflowState = $state;
      return true;
    }
    return false;
  }

  /**
   * Get state of workflow assigned to the document content
   *
   * @return State/boolean The state or false in case of error, e.g. the version has not a
   *   workflow
   */
  function getWorkflowState() {
    $db = $this->_document->_dms->getDB();

    if (!$this->_workflow) {
      $this->getWorkflow();
    }

    if (!$this->_workflow) {
      return false;
    }

    if (!$this->_workflowState) {
      $queryStr =
        "SELECT b.* FROM tblWorkflowDocumentContent a LEFT JOIN tblWorkflowStates b ON a.state = b.id WHERE workflow=" . intval($this->_workflow->getID())
        . " AND a.version='" . $this->_version
        . "' AND a.document = '" . $this->_document->getID() . "' ";
      $recs = $db->getResultArray($queryStr);
      if (is_bool($recs) && !$recs) {
        return false;
      }
      $this->_workflowState = new State($recs[0]['id'],
        $recs[0]['name'],
        $recs[0]['maxtime'],
        $recs[0]['precondfunc'],
        $recs[0]['documentstatus']);
      $this->_workflowState->setDMS($this->_document->_dms);
    }
    return $this->_workflowState;
  }

  /**
   * Assign a workflow to a document
   *
   * @param object $workflow
   */
  function setWorkflow($workflow, $user) {
    $db = $this->_document->_dms->getDB();

    $this->getWorkflow();
    if ($workflow && is_object($workflow)) {
      $db->startTransaction();
      $initstate = $workflow->getInitState();
      $queryStr = "INSERT INTO tblWorkflowDocumentContent (workflow, document, version, state, date) VALUES (" . $workflow->getID() . ", " . $this->_document->getID() . ", " . $this->_version . ", " . $initstate->getID() . ", CURRENT_TIMESTAMP)";
      if (!$db->getResult($queryStr)) {
        $db->rollbackTransaction();
        return false;
      }
      $this->_workflow = $workflow;
      if (!$this->setStatus(DocumentState::IN_WORKFLOW, "Added workflow '" . $workflow->getName() . "'", $user)) {
        $db->rollbackTransaction();
        return false;
      }
      $db->commitTransaction();
      return true;
    }
    return true;
  }

  /**
   * Get workflow assigned to the document content
   *
   * The method returns the last workflow if one was assigned.
   * If a the document version is in a sub workflow, it will have
   * a never date and therefore will be found first.
   *
   * @return boolean|Workflow an object of class {@link BakeryDMS\Core\Workflow\Workflow}
   *         or false in case of error, e.g. the version has not a workflow
   */
  function getWorkflow() {
    $db = $this->_document->_dms->getDB();

    if (!isset($this->_workflow)) {
      $queryStr =
        "SELECT b.* FROM tblWorkflowDocumentContent a LEFT JOIN tblWorkflows b ON a.workflow = b.id WHERE a.`version`='" . $this->_version
        . "' AND a.`document` = '" . $this->_document->getID() . "' "
        . " ORDER BY date DESC LIMIT 1";
      $recs = $db->getResultArray($queryStr);
      if (is_bool($recs) && !$recs) {
        return false;
      }
      if (!$recs) {
        return false;
      }
      $this->_workflow = new Workflow($recs[0]['id'],
        $recs[0]['name'],
        $this->_document->_dms->getWorkflowState($recs[0]['initstate']));
      $this->_workflow->setDMS($this->_document->_dms);
    }
    return $this->_workflow;
  }

  /**
   * Restart workflow from its initial state
   *
   * @return boolean true if workflow could be restarted
   *         or false in case of error
   */
  function rewindWorkflow() {
    $db = $this->_document->_dms->getDB();

    $this->getWorkflow();

    if (!isset($this->_workflow)) {
      return true;
    }

    $db->startTransaction();
    $queryStr = "DELETE FROM tblWorkflowLog WHERE `document` = " . $this->_document->getID() . " AND `version` = " . $this->_version . " AND `workflow` = " . $this->_workflow->getID();
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $this->setWorkflowState($this->_workflow->getInitState());
    $db->commitTransaction();

    return true;
  }

  /**
   * Remove workflow
   *
   * Fully removing a workflow including entries in the workflow log is
   * only allowed if the workflow is still its initial state.
   * At a later point of time only unlinking the document from the
   * workflow is allowed. It will keep any log entries.
   * A workflow is unlinked from a document when enterNextState()
   * succeeds.
   *
   * @param object $user user doing initiating the removal
   * @param boolean $unlink if true, just unlink the workflow from the
   *        document but do not remove the workflow log. The $unlink
   *        flag has been added to detach the workflow from the document
   *        when it has reached a valid end state
   * (see BakeryDMS\Core\DocumentContent::enterNextState())
   * @return boolean true if workflow could be removed
   *         or false in case of error
   */
  function removeWorkflow($user, $unlink = false) {
    $db = $this->_document->_dms->getDB();

    $this->getWorkflow();

    if (!isset($this->_workflow)) {
      return true;
    }

    if (DMS::checkIfEqual($this->_workflow->getInitState(),
        $this->getWorkflowState()) || $unlink == true
    ) {
      $db->startTransaction();
      $queryStr =
        "DELETE FROM tblWorkflowDocumentContent WHERE "
        . "`version`='" . $this->_version . "' "
        . " AND `document` = '" . $this->_document->getID() . "' "
        . " AND `workflow` = '" . $this->_workflow->getID() . "' ";
      if (!$db->getResult($queryStr)) {
        $db->rollbackTransaction();
        return false;
      }
      if (!$unlink) {
        $queryStr =
          "DELETE FROM tblWorkflowLog WHERE "
          . "`version`='" . $this->_version . "' "
          . " AND `document` = '" . $this->_document->getID() . "' "
          . " AND `workflow` = '" . $this->_workflow->getID() . "' ";
        if (!$db->getResult($queryStr)) {
          $db->rollbackTransaction();
          return false;
        }
      }
      $this->_workflow = null;
      $this->_workflowState = null;
      $this->verifyStatus(false, $user);
      $db->commitTransaction();
    }

    return true;
  }

  /**
   * Run a sub workflow
   *
   * @return bool|Workflow
   */
  function getParentWorkflow() {
    $db = $this->_document->_dms->getDB();

    /* document content must be in a workflow */
    $this->getWorkflow();
    if (!$this->_workflow) {
      return false;
    }

    $queryStr =
      "SELECT * FROM tblWorkflowDocumentContent WHERE "
      . "`version`='" . $this->_version . "' "
      . " AND `document` = '" . $this->_document->getID() . "' "
      . " AND `workflow` = '" . $this->_workflow->getID() . "' ";
    $recs = $db->getResultArray($queryStr);
    if (is_bool($recs) && !$recs) {
      return false;
    }
    if (!$recs) {
      return false;
    }

    if ($recs[0]['parentworkflow']) {
      return $this->_document->_dms->getWorkflow($recs[0]['parentworkflow']);
    }

    return false;
  }

  /**
   * Run a sub workflow
   *
   * @param object $subworkflow
   */
  function runSubWorkflow($subworkflow) {
    $db = $this->_document->_dms->getDB();

    /* document content must be in a workflow */
    $this->getWorkflow();
    if (!$this->_workflow) {
      return false;
    }

    /* The current workflow state must match the sub workflows initial state */
    if ($subworkflow->getInitState()->getID() != $this->_workflowState->getID()) {
      return false;
    }

    if ($subworkflow) {
      $initstate = $subworkflow->getInitState();
      $queryStr = "INSERT INTO tblWorkflowDocumentContent (parentworkflow, workflow, document, version, state, date) VALUES (" . $this->_workflow->getID() . ", " . $subworkflow->getID() . ", " . $this->_document->getID() . ", " . $this->_version . ", " . $initstate->getID() . ", CURRENT_TIMESTAMP)";
      if (!$db->getResult($queryStr)) {
        return false;
      }
      $this->_workflow = $subworkflow;
      return true;
    }
    return true;
  }

  /**
   * Return from sub workflow to parent workflow.
   * The method will trigger the given transition
   *
   * FIXME: Needs much better checking if this is allowed
   *
   * @param object $user intiating the return
   * @param object $transtion to trigger
   * @param string $comment for the transition trigger
   */
  function returnFromSubWorkflow($user, $transition = null, $comment = '') {
    $db = $this->_document->_dms->getDB();

    /* document content must be in a workflow */
    $this->getWorkflow();
    if (!$this->_workflow) {
      return false;
    }

    if (isset($this->_workflow)) {
      $db->startTransaction();

      $queryStr =
        "SELECT * FROM tblWorkflowDocumentContent WHERE workflow=" . intval($this->_workflow->getID())
        . " AND `version`='" . $this->_version
        . "' AND `document` = '" . $this->_document->getID() . "' ";
      $recs = $db->getResultArray($queryStr);
      if (is_bool($recs) && !$recs) {
        $db->rollbackTransaction();
        return false;
      }
      if (!$recs) {
        $db->rollbackTransaction();
        return false;
      }

      $queryStr = "DELETE FROM `tblWorkflowDocumentContent` WHERE `workflow` =" . intval($this->_workflow->getID()) . " AND `document` = '" . $this->_document->getID() . "' AND `version` = '" . $this->_version . "'";
      if (!$db->getResult($queryStr)) {
        $db->rollbackTransaction();
        return false;
      }

      $this->_workflow = $this->_document->_dms->getWorkflow($recs[0]['parentworkflow']);
      $this->_workflow->setDMS($this->_document->_dms);

      if ($transition) {
        echo "Trigger transition";
        if (false === $this->triggerWorkflowTransition($user, $transition, $comment)) {
          $db->rollbackTransaction();
          return false;
        }
      }

      $db->commitTransaction();
    }
    return $this->_workflow;
  }

  /**
   * Check if the user is allowed to trigger the transition
   * A user is allowed if either the user itself or
   * a group of which the user is a member of is registered for
   * triggering a transition. This method does not change the workflow
   * state of the document content.
   *
   * @param object $user
   * @return boolean true if user may trigger transaction
   */
  function triggerWorkflowTransitionIsAllowed($user, $transition) {
    $db = $this->_document->_dms->getDB();

    if (!$this->_workflow) {
      $this->getWorkflow();
    }

    if (!$this->_workflow) {
      return false;
    }

    if (!$this->_workflowState) {
      $this->getWorkflowState();
    }

    /* Check if the user has already triggered the transition */
    $queryStr =
      "SELECT * FROM tblWorkflowLog WHERE `version`='" . $this->_version . "' AND `document` = '" . $this->_document->getID() . "' AND `workflow` = " . $this->_workflow->getID() . " AND userid = " . $user->getID();
    $queryStr .= " AND `transition` = " . $transition->getID();
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    if (count($resArr)) {
      return false;
    }

    /* Get all transition users allowed to trigger the transition */
    $transusers = $transition->getUsers();
    if ($transusers) {
      foreach ($transusers as $transuser) {
        if ($user->getID() == $transuser->getUser()->getID()) {
          return true;
        }
      }
    }

    /* Get all transition groups whose members are allowed to trigger
		 * the transition */
    $transgroups = $transition->getGroups();
    if ($transgroups) {
      foreach ($transgroups as $transgroup) {
        $group = $transgroup->getGroup();
        if ($group->isMember($user)) {
          return true;
        }
      }
    }

    return false;
  }

  /**
   * Check if all conditions are met to change the workflow state
   * of a document content (run the transition).
   * The conditions are met if all explicitly set users and a sufficient
   * number of users of the groups have acknowledged the content.
   *
   * @return boolean true if transaction maybe executed
   */
  function executeWorkflowTransitionIsAllowed($transition) {
    if (!$this->_workflow) {
      $this->getWorkflow();
    }

    if (!$this->_workflow) {
      return false;
    }

    if (!$this->_workflowState) {
      $this->getWorkflowState();
    }

    /* Get the Log of transition triggers */
    $entries = $this->getWorkflowLog($transition);
    if (!$entries) {
      return false;
    }

    /* Get all transition users allowed to trigger the transition
		 * $allowedusers is a list of all users allowed to trigger the
		 * transition
		 */
    $transusers = $transition->getUsers();
    $allowedusers = array();
    foreach ($transusers as $transuser) {
      $a = $transuser->getUser();
      $allowedusers[$a->getID()] = $a;
    }

    /* Get all transition groups whose members are allowed to trigger
		 * the transition */
    $transgroups = $transition->getGroups();
    foreach ($entries as $entry) {
      $loguser = $entry->getUser();
      /* Unset each allowed user if it was found in the log */
      if (isset($allowedusers[$loguser->getID()])) {
        unset($allowedusers[$loguser->getID()]);
      }
      /* Also check groups if required. Count the group membership of
			 * each user in the log in the array $gg
			 */
      if ($transgroups) {
        $loggroups = $loguser->getGroups();
        foreach ($loggroups as $loggroup) {
          if (!isset($gg[$loggroup->getID()])) {
            $gg[$loggroup->getID()] = 1;
          }
          else {
            $gg[$loggroup->getID()]++;
          }
        }
      }
    }
    /* If there are allowed users left, then there some users still
		 * need to trigger the transition.
		 */
    if ($allowedusers) {
      return false;
    }

    if ($transgroups) {
      foreach ($transgroups as $transgroup) {
        $group = $transgroup->getGroup();
        $minusers = $transgroup->getNumOfUsers();
        if (!isset($gg[$group->getID()])) {
          return false;
        }
        if ($gg[$group->getID()] < $minusers) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Trigger transition
   *
   * This method will be deprecated
   *
   * The method will first check if the user is allowed to trigger the
   * transition. If the user is allowed, an entry in the workflow log
   * will be added, which is later used to check if the transition
   * can actually be processed. The method will finally call
   * executeWorkflowTransitionIsAllowed() which checks all log entries
   * and does the transitions post function if all users and groups have
   * triggered the transition. Finally enterNextState() is called which
   * will try to enter the next state.
   *
   * @param object $user
   * @param object $transition
   * @param string $comment user comment
   * @return boolean/object next state if transition could be triggered and
   *         then next state could be entered,
   *         true if the transition could just be triggered or
   *         false in case of an error
   */
  function triggerWorkflowTransition($user, $transition, $comment = '') {

    $db = $this->_document->_dms->getDB();

    if (!$this->_workflow) {
      $this->getWorkflow();
    }

    if (!$this->_workflow) {
      return false;
    }

    if (!$this->_workflowState) {
      $this->getWorkflowState();
    }

    if (!$this->_workflowState) {
      return false;
    }

    /* Check if the user is allowed to trigger the transition.
		 */
    if (!$this->triggerWorkflowTransitionIsAllowed($user, $transition)) {
      return false;
    }

    $state = $this->_workflowState;
    $queryStr = "INSERT INTO tblWorkflowLog (document, version, workflow, userid, transition, date, comment) VALUES (" . $this->_document->getID() . ", " . $this->_version . ", " . (int) $this->_workflow->getID() . ", " . (int) $user->getID() . ", " . (int) $transition->getID() . ", CURRENT_TIMESTAMP, " . $db->qstr($comment) . ")";
    if (!$db->getResult($queryStr)) {
      return false;
    }

    /* Check if this transition is processed. Run the post function in
		 * that case. A transition is processed when all users and groups
		 * have triggered it.
		 */
    if ($this->executeWorkflowTransitionIsAllowed($transition)) {
      /* run post function of transition */
//			echo "run post function of transition ".$transition->getID()."<br />";
    }


    /* Go into the next state. This will only succeed if the pre condition
		 * function of that states succeeds.
		 */
    $nextstate = $transition->getNextState();
    if ($this->enterNextState($user, $nextstate)) {
      return $nextstate;
    }

    return true;

  }

  /**
   * Enter next state of workflow if possible
   *
   * The method will check if one of the following states in the workflow
   * can be reached.
   * It does it by running
   * the precondition function of that state. The precondition function
   * gets a list of all transitions leading to the state. It will
   * determine, whether the transitions has been triggered and if that
   * is sufficient to enter the next state. If no pre condition function
   * is set, then 1 of n transtions are enough to enter the next state.
   *
   * If moving in the next state is possible and this state has a
   * corresponding document state, then the document state will be
   * updated and the workflow will be detached from the document.
   *
   * @param object $user
   * @param object $nextstate
   * @return boolean true if the state could be reached
   *         false if not
   */
  function enterNextState($user, $nextstate) {

    /* run the pre condition of the next state. If it is not set
			 * the next state will be reached if one of the transitions
			 * leading to the given state can be processed.
			 */
    if ($nextstate->getPreCondFunc() == '') {
      $transitions = $this->_workflow->getPreviousTransitions($nextstate);
      foreach ($transitions as $transition) {
//				echo "transition ".$transition->getID()." led to state ".$nextstate->getName()."<br />";
        if ($this->executeWorkflowTransitionIsAllowed($transition)) {
//					echo "stepping into next state<br />";
          $this->setWorkflowState($nextstate);

          /* Check if the new workflow state has a mapping into a
						 * document state. If yes, set the document state will
						 * be updated and the workflow will be removed from the
						 * document.
						 */
          $docstate = $nextstate->getDocumentStatus();
          if ($docstate == DocumentState::RELEASED || $docstate == DocumentState::REJECTED) {
            $this->setStatus($docstate, "Workflow has ended", $user);
            /* Detach the workflow from the document, but keep the
							 * workflow log
							 */
            $this->removeWorkflow($user, true);
            return true;
          }

          /* make sure the users and groups allowed to trigger the next
						 * transitions are also allowed to read the document
						 */
          $transitions = $this->_workflow->getNextTransitions($nextstate);
          foreach ($transitions as $tran) {
//							echo "checking access for users/groups allowed to trigger transition ".$tran->getID()."<br />";
            $transusers = $tran->getUsers();
            foreach ($transusers as $transuser) {
              $u = $transuser->getUser();
//								echo $u->getFullName()."<br />";
              if ($this->_document->getAccessMode($u) < AccessMode::READ) {
                $this->_document->addAccess(AccessMode::READ, $u->getID(), 1);
//									echo "granted read access<br />";
              }
              else {
//									echo "has already access<br />";
              }
            }
            $transgroups = $tran->getGroups();
            foreach ($transgroups as $transgroup) {
              $g = $transgroup->getGroup();
//								echo $g->getName()."<br />";
              if ($this->_document->getGroupAccessMode($g) < AccessMode::READ) {
                $this->_document->addAccess(AccessMode::READ, $g->getID(), 0);
//									echo "granted read access<br />";
              }
              else {
//									echo "has already access<br />";
              }
            }
          }
          return (true);
        }
        else {
//						echo "transition not ready for process now<br />";
        }
      }
      return false;
    }
    else {
    }

  }

  /**
   * Get the so far logged operations on the document content within the
   * workflow
   *
   * @return array list of operations
   */
  function getWorkflowLog($transition = null) {
    $db = $this->_document->_dms->getDB();

    /*
		if(!$this->_workflow)
			$this->getWorkflow();

		if(!$this->_workflow)
			return false;
*/
    $queryStr =
      "SELECT * FROM tblWorkflowLog WHERE `version`='" . $this->_version . "' AND `document` = '" . $this->_document->getID() . "'"; // AND `workflow` = ". $this->_workflow->getID();
    if ($transition) {
      $queryStr .= " AND `transition` = " . $transition->getID();
    }
    $queryStr .= " ORDER BY `date` DESC";
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    $workflowlogs = array();
    for ($i = 0; $i < count($resArr); $i++) {
      $workflow = $this->_document->_dms->getWorkflow($resArr[$i]["workflow"]);
      $workflowlog = new Log($resArr[$i]["id"],
        $this->_document->_dms->getDocument($resArr[$i]["document"]),
        $resArr[$i]["version"],
        $workflow,
        $this->_document->_dms->getUser($resArr[$i]["userid"]),
        $workflow->getTransition($resArr[$i]["transition"]),
        $resArr[$i]["date"],
        $resArr[$i]["comment"]);
      $workflowlog->setDMS($this);
      $workflowlogs[$i] = $workflowlog;
    }

    return $workflowlogs;
  }

  /**
   * Get the latest logged transition for the document content within the
   * workflow
   *
   * @return array list of operations
   */
  function getLastWorkflowTransition() {
    $db = $this->_document->_dms->getDB();

    if (!$this->_workflow) {
      $this->getWorkflow();
    }

    if (!$this->_workflow) {
      return false;
    }

    $queryStr =
      "SELECT * FROM tblWorkflowLog WHERE `version`='" . $this->_version . "' AND `document` = '" . $this->_document->getID() . "' AND `workflow` = " . $this->_workflow->getID();
    $queryStr .= " ORDER BY `id` DESC LIMIT 1";
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    $workflowlogs = array();
    $i = 0;
    $workflowlog = new Log($resArr[$i]["id"],
      $this->_document->_dms->getDocument($resArr[$i]["document"]),
      $resArr[$i]["version"],
      $this->_workflow,
      $this->_document->_dms->getUser($resArr[$i]["userid"]),
      $this->_workflow->getTransition($resArr[$i]["transition"]),
      $resArr[$i]["date"],
      $resArr[$i]["comment"]);
    $workflowlog->setDMS($this);

    return $workflowlog;
  }

  /**
   * Check if the document content needs an action by a user
   *
   * This method will return true if document content is in a transition
   * which can be triggered by the given user.
   *
   * @param User $user
   * @return boolean true is action is needed
   */
  function needsWorkflowAction($user) {
    $needwkflaction = false;
    if ($this->_workflow) {
      if (!$this->_workflowState) {
        $this->getWorkflowState();
      }
      $workflowstate = $this->_workflowState;
      $transitions = $this->_workflow->getNextTransitions($workflowstate);
      foreach ($transitions as $transition) {
        if ($this->triggerWorkflowTransitionIsAllowed($user, $transition)) {
          $needwkflaction = true;
        }
      }
    }
    return $needwkflaction;
  }

}