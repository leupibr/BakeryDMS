<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

/**
 * Class to represent a user group in the document management system.
 */
class Group {
  /**
   * @var int The id of the user group
   */
  protected $id;

  /**
   * @var string The name of the user group
   */
  protected $_name;

  /**
   * @var \BakeryDMS\Core\DMS Back reference to DMS this user group belongs to
   */
  protected $_dms;

  /**
   * Initializes a new instance of the {@link } class.
   *
   * @param $id int The id of the group.
   * @param $name string The name of the group.
   * @param $comment string Comments to the group.
   */
  function __construct($id, $name, $comment) {
    $this->id = $id;
    $this->_name = $name;
    $this->_comment = $comment;
    $this->_dms = null;
  }

  function setDMS($dms) {
    $this->_dms = $dms;
  }

  function getID() {
    return $this->id;
  }

  function getName() {
    return $this->_name;
  }

  function setName($newName) {
    $db = $this->_dms->getDB();

    $queryStr = "UPDATE tblGroups SET name = " . $db->qstr($newName) . " WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->_name = $newName;
    return true;
  }

  function getComment() {
    return $this->_comment;
  }

  function setComment($newComment) {
    $db = $this->_dms->getDB();

    $queryStr = "UPDATE tblGroups SET comment = " . $db->qstr($newComment) . " WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->_comment = $newComment;
    return true;
  }

  /**
   * @return User[]|bool
   */
  function getUsers() {
    $db = $this->_dms->getDB();

    if (!isset($this->_users)) {
      $queryStr = "SELECT `tblUsers`.* FROM `tblUsers` " .
        "LEFT JOIN `tblGroupMembers` ON `tblGroupMembers`.`userID`=`tblUsers`.`id` " .
        "WHERE `tblGroupMembers`.`groupID` = '" . $this->id . "'";
      $resArr = $db->getResultArray($queryStr);
      if (is_bool($resArr) && $resArr == false) {
        return false;
      }

      $this->_users = array();

      foreach ($resArr as $row) {
        $user = new User($row["id"],
          $row["login"],
          $row["pwd"],
          $row["fullName"],
          $row["email"],
          $row["language"],
          $row["theme"],
          $row["comment"],
          $row["role"],
          $row['hidden']);
        array_push($this->_users, $user);
      }
    }
    return $this->_users;
  }

  function getManagers() {
    $db = $this->_dms->getDB();

    $queryStr = "SELECT `tblUsers`.* FROM `tblUsers` " .
      "LEFT JOIN `tblGroupMembers` ON `tblGroupMembers`.`userID`=`tblUsers`.`id` " .
      "WHERE `tblGroupMembers`.`groupID` = '" . $this->id . "' AND tblGroupMembers.manager = 1";
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    $managers = array();

    foreach ($resArr as $row) {
      $user = new User($row["id"],
        $row["login"],
        $row["pwd"],
        $row["fullName"],
        $row["email"],
        $row["language"],
        $row["theme"],
        $row["comment"],
        $row["role"],
        $row['hidden']);
      array_push($managers, $user);
    }
    return $managers;
  }

  function addUser($user, $asManager = false) {
    $db = $this->_dms->getDB();

    $queryStr = "INSERT INTO tblGroupMembers (groupID, userID, manager) VALUES (" . $this->id . ", " . $user->getID() . ", " . ($asManager ? "1" : "0") . " )";
    $res = $db->getResult($queryStr);

    if ($res) {
      return false;
    }

    unset($this->_users);
    return true;
  }

  function removeUser($user) {
    $db = $this->_dms->getDB();

    $queryStr = "DELETE FROM tblGroupMembers WHERE groupID = " . $this->id . " AND userID = " . $user->getID();
    $res = $db->getResult($queryStr);

    if ($res) {
      return false;
    }
    unset($this->_users);
    return true;
  }

  // $asManager=false: verify if user is in group
  // $asManager=true : verify if user is in group as manager
  function isMember($user, $asManager = false) {
    if (isset($this->_users) && !$asManager) {
      foreach ($this->_users as $usr) {
        if ($usr->getID() == $user->getID()) {
          return true;
        }
      }
      return false;
    }

    $db = $this->_dms->getDB();
    if ($asManager) {
      $queryStr = "SELECT * FROM tblGroupMembers WHERE groupID = " . $this->id . " AND userID = " . $user->getID() . " AND manager = 1";
    }
    else {
      $queryStr = "SELECT * FROM tblGroupMembers WHERE groupID = " . $this->id . " AND userID = " . $user->getID();
    }

    $resArr = $db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) != 1) {
      return false;
    }

    return true;
  }

  function toggleManager($user) {
    $db = $this->_dms->getDB();

    if (!$this->isMember($user)) {
      return false;
    }

    if ($this->isMember($user, true)) {
      $queryStr = "UPDATE tblGroupMembers SET manager = 0 WHERE groupID = " . $this->id . " AND userID = " . $user->getID();
    }
    else {
      $queryStr = "UPDATE tblGroupMembers SET manager = 1 WHERE groupID = " . $this->id . " AND userID = " . $user->getID();
    }

    if (!$db->getResult($queryStr)) {
      return false;
    }
    return true;
  }

  /**
   * Delete user group
   * This function deletes the user group and all it references, like access
   * control lists, notifications, as a child of other groups, etc.
   *
   * @param object $user the user doing the removal (needed for entry in
   *        review log.
   * @return boolean true on success or false in case of an error
   */
  function remove($user) {
    $db = $this->_dms->getDB();

    $db->startTransaction();

    $queryStr = "DELETE FROM tblGroupMembers WHERE groupID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }
    $queryStr = "DELETE FROM tblACLs WHERE groupID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }
    $queryStr = "DELETE FROM tblNotify WHERE groupID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }
    $queryStr = "DELETE FROM tblMandatoryReviewers WHERE reviewerGroupID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }
    $queryStr = "DELETE FROM tblMandatoryApprovers WHERE approverGroupID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }
    $queryStr = "DELETE FROM tblWorkflowTransitionGroups WHERE groupid = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }
    $queryStr = "DELETE FROM tblGroups WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    // TODO : update document status if reviewer/approver has been deleted

    $reviewStatus = $this->getReviewStatus();
    foreach ($reviewStatus as $r) {
      $queryStr = "INSERT INTO `tblDocumentReviewLog` (`reviewID`, `status`, `comment`, `date`, `userID`) " .
        "VALUES ('" . $r["reviewID"] . "', '-2', 'Review group removed from process', CURRENT_TIMESTAMP, '" . $user->getID() . "')";
      $res = $db->getResult($queryStr);
      if (!$res) {
        $db->rollbackTransaction();
        return false;
      }
    }

    $approvalStatus = $this->getApprovalStatus();
    foreach ($approvalStatus as $a) {
      $queryStr = "INSERT INTO `tblDocumentApproveLog` (`approveID`, `status`, `comment`, `date`, `userID`) " .
        "VALUES ('" . $a["approveID"] . "', '-2', 'Approval group removed from process', CURRENT_TIMESTAMP, '" . $user->getID() . "')";
      $res = $db->getResult($queryStr);
      if (!$res) {
        $db->rollbackTransaction();
        return false;
      }
    }

    $db->commitTransaction();

    return true;
  }

  function getReviewStatus($documentID = null, $version = null) {
    $db = $this->_dms->getDB();

    if (!$db->createTemporaryTable("ttreviewid")) {
      return false;
    }

    $status = array();

    // See if the group is assigned as a reviewer.
    $queryStr = "SELECT `tblDocumentReviewers`.*, `tblDocumentReviewLog`.`status`, " .
      "`tblDocumentReviewLog`.`comment`, `tblDocumentReviewLog`.`date`, " .
      "`tblDocumentReviewLog`.`userID` " .
      "FROM `tblDocumentReviewers` " .
      "LEFT JOIN `tblDocumentReviewLog` USING (`reviewID`) " .
      "LEFT JOIN `ttreviewid` on `ttreviewid`.`maxLogID` = `tblDocumentReviewLog`.`reviewLogID` " .
      "WHERE `ttreviewid`.`maxLogID`=`tblDocumentReviewLog`.`reviewLogID` " .
      ($documentID == null ? "" : "AND `tblDocumentReviewers`.`documentID` = '" . (int) $documentID . "' ") .
      ($version == null ? "" : "AND `tblDocumentReviewers`.`version` = '" . (int) $version . "' ") .
      "AND `tblDocumentReviewers`.`type`='1' " .
      "AND `tblDocumentReviewers`.`required`='" . $this->id . "' ";
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) > 0) {
      foreach ($resArr as $res) {
        $status[] = $res;
      }
    }
    return $status;
  }

  function getApprovalStatus($documentID = null, $version = null) {
    $db = $this->_dms->getDB();

    if (!$db->createTemporaryTable("ttapproveid")) {
      return false;
    }

    $status = array();

    // See if the group is assigned as an approver.
    $queryStr = "SELECT `tblDocumentApprovers`.*, `tblDocumentApproveLog`.`status`, " .
      "`tblDocumentApproveLog`.`comment`, `tblDocumentApproveLog`.`date`, " .
      "`tblDocumentApproveLog`.`userID` " .
      "FROM `tblDocumentApprovers` " .
      "LEFT JOIN `tblDocumentApproveLog` USING (`approveID`) " .
      "LEFT JOIN `ttapproveid` on `ttapproveid`.`maxLogID` = `tblDocumentApproveLog`.`approveLogID` " .
      "WHERE `ttapproveid`.`maxLogID`=`tblDocumentApproveLog`.`approveLogID` " .
      ($documentID == null ? "" : "AND `tblDocumentApprovers`.`documentID` = '" . (int) $documentID . "' ") .
      ($version == null ? "" : "AND `tblDocumentApprovers`.`version` = '" . (int) $version . "' ") .
      "AND `tblDocumentApprovers`.`type`='1' " .
      "AND `tblDocumentApprovers`.`required`='" . $this->id . "' ";
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) > 0) {
      foreach ($resArr as $res) {
        $status[] = $res;
      }
    }

    return $status;
  }
}

?>
