<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

/**
 * Class to represent a group access right.
 * This class cannot be used to modify access rights.
 */
class GroupAccess {
  /** @var \BakeryDMS\Core\Group The group of this access item. */
  private $_group;
  /** @var int The mode of this access item. */
  private $_mode;

  /**
   * Initializes a new instance of the {@link GroupAccess} class.
   *
   * @param $group \BakeryDMS\Core\Group The group of the access item.
   * @param int $mode The access rights.
   */
  function __construct($group, $mode) {
    $this->_group = $group;
    $this->_mode = $mode;
  }

  /**
   * Returns the group id.
   *
   * @return int The group id.
   */
  function getGroupID() {
    return $this->_group->getID();
  }

  /**
   * Returns the mode.
   *
   * @return int The mode.
   */
  function getMode() {
    return $this->_mode;
  }

  /**
   * Returns the group of this access item.
   *
   * @return \BakeryDMS\Core\Group The group.
   */
  function getGroup() {
    return $this->_group;
  }
}