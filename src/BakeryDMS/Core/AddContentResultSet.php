<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

/**
 * Class to represent a list of document contents.
 *
 * Perhaps not the cleanest object ever devised, it exists to encapsulate all
 * of the data generated during the addition of new content to the database.
 * The object stores a copy of the new BakeryDMS\Core\DocumentContent object, the newly assigned
 * reviewers and approvers and the status.
 */
class AddContentResultSet {
  protected $_indReviewers;
  protected $_grpReviewers;
  protected $_indApprovers;
  protected $_grpApprovers;
  protected $_content;
  protected $_status;

  function __construct($content) {
    $this->_content = $content;
    $this->_indReviewers = null;
    $this->_grpReviewers = null;
    $this->_indApprovers = null;
    $this->_grpApprovers = null;
    $this->_status = null;
  }

  function addReviewer($reviewer, $type, $status) {
    if (!is_object($reviewer) || (strcasecmp($type, "i") && strcasecmp($type, "g")) && !is_integer($status)) {
      return false;
    }
    if (!strcasecmp($type, "i")) {
      if (strcasecmp(get_class($reviewer), User::class)) {
        return false;
      }
      if ($this->_indReviewers == null) {
        $this->_indReviewers = array();
      }
      $this->_indReviewers[$status][] = $reviewer;
    }
    if (!strcasecmp($type, "g")) {
      if (strcasecmp(get_class($reviewer), Group::class)) {
        return false;
      }
      if ($this->_grpReviewers == null) {
        $this->_grpReviewers = array();
      }
      $this->_grpReviewers[$status][] = $reviewer;
    }
    return true;
  }

  function addApprover($approver, $type, $status) {
    if (!is_object($approver) || (strcasecmp($type, "i") && strcasecmp($type, "g")) && !is_integer($status)) {
      return false;
    }
    if (!strcasecmp($type, "i")) {
      if (strcasecmp(get_class($approver), User::class)) {
        return false;
      }
      if ($this->_indApprovers == null) {
        $this->_indApprovers = array();
      }
      $this->_indApprovers[$status][] = $approver;
    }
    if (!strcasecmp($type, "g")) {
      if (strcasecmp(get_class($approver), Group::class)) {
        return false;
      }
      if ($this->_grpApprovers == null) {
        $this->_grpApprovers = array();
      }
      $this->_grpApprovers[$status][] = $approver;
    }
    return true;
  }

  function setStatus($status) {
    if (!is_integer($status)) {
      return false;
    }
    if ($status < -3 || $status > 2) {
      return false;
    }
    $this->_status = $status;
    return true;
  }

  function getStatus() {
    return $this->_status;
  }

  function getContent() {
    return $this->_content;
  }

  function getReviewers($type) {
    if (strcasecmp($type, "i") && strcasecmp($type, "g")) {
      return false;
    }
    if (!strcasecmp($type, "i")) {
      return ($this->_indReviewers == null ? array() : $this->_indReviewers);
    }
    else {
      return ($this->_grpReviewers == null ? array() : $this->_grpReviewers);
    }
  }

  function getApprovers($type) {
    if (strcasecmp($type, "i") && strcasecmp($type, "g")) {
      return false;
    }
    if (!strcasecmp($type, "i")) {
      return ($this->_indApprovers == null ? array() : $this->_indApprovers);
    }
    else {
      return ($this->_grpApprovers == null ? array() : $this->_grpApprovers);
    }
  }
}