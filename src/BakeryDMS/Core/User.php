<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

use BakeryDMS\Core\Workflow\Workflow;

/**
 * Class to represent a user in the document management system.
 */
class User {
  /**
   * @var int id of user
   */
  private $id;

  /**
   * @var string login name of user
   */
  private $login;

  /**
   * @var string password of user as saved in database (md5)
   */
  private $password;

  /**
   * @var string date when password expires
   */
  private $passwordExpiration;

  /**
   * @var string full human readable name of user
   */
  private $fullName;

  /**
   * @var string email address of user
   */
  private $email;

  /**
   * @var string prefered language of user
   *      possible values are 'English', 'German', 'Chinese_ZH_TW', 'Czech'
   *      'Francais', 'Hungarian', 'Italian', 'Portuguese_BR', 'Slovak',
   *      'Spanish'
   */
  private $language;

  /**
   * @var string preselected theme of user
   */
  private $theme;

  /**
   * @var string comment of user
   */
  private $comment;

  /**
   * @var string role of user. Can be one of BakeryDMS\Core\User::role_user,
   *      BakeryDMS\Core\User::role_admin, BakeryDMS\Core\User::role_guest
   */
  private $role;

  /**
   * @var boolean true if user shall be hidden
   */
  private $isHidden;

  /**
   * @var boolean true if user is disabled
   */
  private $isDisabled;

  /**
   * @var int number of login failures
   */
  private $loginFailures;

  /**
   * @var object reference to the dms instance this user belongs to
   */
  private $dms;

  const role_user = '0';
  const role_admin = '1';
  const role_guest = '2';

  function __construct($id,
                       $login,
                       $pwd,
                       $fullName,
                       $email,
                       $language,
                       $theme,
                       $comment,
                       $role,
                       $isHidden = 0,
                       $isDisabled = 0,
                       $pwdExpiration = '0000-00-00 00:00:00',
                       $loginFailures = 0,
                       $quota = 0) {
    $this->id = $id;
    $this->login = $login;
    $this->password = $pwd;
    $this->fullName = $fullName;
    $this->email = $email;
    $this->language = $language;
    $this->theme = $theme;
    $this->comment = $comment;
    $this->role = $role;
    $this->isHidden = $isHidden;
    $this->isDisabled = $isDisabled;
    $this->passwordExpiration = $pwdExpiration;
    $this->loginFailures = $loginFailures;
    $this->quota = $quota;
    $this->dms = null;
  }

  function setDMS($dms) {
    $this->dms = $dms;
  }

  function getID() {
    return $this->id;
  }

  function getLogin() {
    return $this->login;
  }

  function setLogin($newLogin) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblUsers SET login =" . $db->qstr($newLogin) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->login = $newLogin;
    return true;
  }

  function getFullName() {
    return $this->fullName;
  }

  function setFullName($newFullName) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblUsers SET fullname = " . $db->qstr($newFullName) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->fullName = $newFullName;
    return true;
  }

  function getPwd() {
    return $this->password;
  }

  function setPwd($newPwd) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblUsers SET pwd =" . $db->qstr($newPwd) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->password = $newPwd;
    return true;
  }

  function getPwdExpiration() {
    return $this->passwordExpiration;
  }

  function setPwdExpiration($newPwdExpiration) {
    $db = $this->dms->getDB();

    if (trim($newPwdExpiration) == '') {
      $newPwdExpiration = '0000-00-00 00:00:00';
    }
    $queryStr = "UPDATE tblUsers SET pwdExpiration =" . $db->qstr($newPwdExpiration) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->passwordExpiration = $newPwdExpiration;
    return true;
  }

  function getEmail() {
    return $this->email;
  }

  function setEmail($newEmail) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblUsers SET email =" . $db->qstr($newEmail) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->email = $newEmail;
    return true;
  }

  function getLanguage() {
    return $this->language;
  }

  function setLanguage($newLanguage) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblUsers SET language =" . $db->qstr($newLanguage) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->language = $newLanguage;
    return true;
  }

  function getTheme() {
    return $this->theme;
  }

  function setTheme($newTheme) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblUsers SET theme =" . $db->qstr($newTheme) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->theme = $newTheme;
    return true;
  }

  function getComment() {
    return $this->comment;
  }

  function setComment($newComment) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblUsers SET comment =" . $db->qstr($newComment) . " WHERE id = " . $this->id;
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    $this->comment = $newComment;
    return true;
  }

  function getRole() {
    return $this->role;
  }

  function setRole($newrole) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblUsers SET role = " . $newrole . " WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->role = $newrole;
    return true;
  }

  function isAdmin() {
    return ($this->role == User::role_admin);
  }

  function setAdmin($isAdmin) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblUsers SET role = " . User::role_admin . " WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->role = User::role_admin;
    return true;
  }

  function isGuest() {
    return ($this->role == User::role_guest);
  }

  function setGuest($isGuest) {
    $db = $this->dms->getDB();

    $queryStr = "UPDATE tblUsers SET role = " . User::role_guest . " WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->role = User::role_guest;
    return true;
  }

  function isHidden() {
    return $this->isHidden;
  }

  function setHidden($isHidden) {
    $db = $this->dms->getDB();

    $isHidden = ($isHidden) ? "1" : "0";
    $queryStr = "UPDATE tblUsers SET hidden = " . $isHidden . " WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->isHidden = $isHidden;
    return true;
  }

  function isDisabled() {
    return $this->isDisabled;
  }

  function setDisabled($isDisabled) {
    $db = $this->dms->getDB();

    $isDisabled = ($isDisabled) ? "1" : "0";
    $queryStr = "UPDATE tblUsers SET disabled = " . $isDisabled . " WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->isDisabled = $isDisabled;
    return true;
  }

  function addLoginFailure() {
    $db = $this->dms->getDB();

    $this->loginFailures++;
    $queryStr = "UPDATE tblUsers SET loginfailures = " . $this->loginFailures . " WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    return $this->loginFailures;
  }

  function clearLoginFailures() {
    $db = $this->dms->getDB();

    $this->loginFailures = 0;
    $queryStr = "UPDATE tblUsers SET loginfailures = " . $this->loginFailures . " WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    return true;
  }

  /**
   * Calculate the disk space for all documents owned by the user
   *
   * This is done by using the internal database field storing the
   * filesize of a document version.
   *
   * @return integer total disk space in Bytes
   */
  function getUsedDiskSpace() {
    $db = $this->dms->getDB();

    $queryStr = "SELECT SUM(filesize) sum FROM tblDocumentContent a LEFT JOIN tblDocuments b ON a.document=b.id WHERE b.owner = " . $this->id;
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    return $resArr[0]['sum'];
  }

  function getQuota() {
    return $this->quota;
  }

  function setQuota($quota) {
    $db = $this->dms->getDB();

    $quota = intval($quota);
    $queryStr = "UPDATE tblUsers SET quota = " . $quota . " WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->quota = $quota;
    return true;
  }

  /**
   * Remove the user and also remove all its keywords, notifies, etc.
   * Do not remove folders and documents of the user, but assign them
   * to a different user.
   *
   * @param object $user the user doing the removal (needed for entry in
   *        review and approve log).
   * @param object $assignToUser the user who is new owner of folders and
   *        documents which previously were owned by the delete user.
   * @return boolean true on success or false in case of an error
   */
  function remove($user, $assignToUser = null) {
    $db = $this->dms->getDB();

    /* Records like folders and documents that formely have belonged to
     * the user will assign to another user. If no such user is set,
     * the function now returns false and will not use the admin user
     * anymore.
     */
    if (!$assignToUser) {
      return;
    }
    $assignTo = $assignToUser->getID();

    $db->startTransaction();

    // delete private keyword lists
    $queryStr = "SELECT tblKeywords.id FROM tblKeywords, tblKeywordCategories WHERE tblKeywords.category = tblKeywordCategories.id AND tblKeywordCategories.owner = " . $this->id;
    $resultArr = $db->getResultArray($queryStr);
    if (count($resultArr) > 0) {
      $queryStr = "DELETE FROM tblKeywords WHERE ";
      for ($i = 0; $i < count($resultArr); $i++) {
        $queryStr .= "id = " . $resultArr[$i]["id"];
        if ($i + 1 < count($resultArr)) {
          $queryStr .= " OR ";
        }
      }
      if (!$db->getResult($queryStr)) {
        $db->rollbackTransaction();
        return false;
      }
    }

    $queryStr = "DELETE FROM tblKeywordCategories WHERE owner = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    //Benachrichtigungen entfernen
    $queryStr = "DELETE FROM tblNotify WHERE userID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    /* Assign documents of the removed user to the given user */
    $queryStr = "UPDATE tblFolders SET owner = " . $assignTo . " WHERE owner = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $queryStr = "UPDATE tblDocuments SET owner = " . $assignTo . " WHERE owner = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $queryStr = "UPDATE tblDocumentContent SET createdBy = " . $assignTo . " WHERE createdBy = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    // Remove private links on documents ...
    $queryStr = "DELETE FROM tblDocumentLinks WHERE userID = " . $this->id . " AND public = 0";
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    // ... but keep public links
    $queryStr = "UPDATE tblDocumentLinks SET userID = " . $assignTo . " WHERE userID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    // set administrator for deleted user's attachments
    $queryStr = "UPDATE tblDocumentFiles SET userID = " . $assignTo . " WHERE userID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    //Evtl. von diesem Benutzer gelockte Dokumente werden freigegeben
    $queryStr = "DELETE FROM tblDocumentLocks WHERE userID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    // Delete user from all groups
    $queryStr = "DELETE FROM tblGroupMembers WHERE userID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    // User aus allen ACLs streichen
    $queryStr = "DELETE FROM tblACLs WHERE userID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    // Delete image of user
    $queryStr = "DELETE FROM tblUserImages WHERE userID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    // Delete entries in password history
    $queryStr = "DELETE FROM tblUserPasswordHistory WHERE userID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    // Delete entries in password request
    $queryStr = "DELETE FROM tblUserPasswordRequest WHERE userID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    // mandatory review/approve
    $queryStr = "DELETE FROM tblMandatoryReviewers WHERE reviewerUserID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $queryStr = "DELETE FROM tblMandatoryApprovers WHERE approverUserID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $queryStr = "DELETE FROM tblMandatoryReviewers WHERE userID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $queryStr = "DELETE FROM tblMandatoryApprovers WHERE userID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $queryStr = "DELETE FROM tblWorkflowMandatoryWorkflow WHERE userid = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    $queryStr = "DELETE FROM tblWorkflowTransitionUsers WHERE userid = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    // set administrator for deleted user's events
    $queryStr = "UPDATE tblEvents SET userID = " . $assignTo . " WHERE userID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    // Delete user itself
    $queryStr = "DELETE FROM tblUsers WHERE id = " . $this->id;
    if (!$db->getResult($queryStr)) {
      $db->rollbackTransaction();
      return false;
    }

    // TODO : update document status if reviewer/approver has been deleted
    // "DELETE FROM tblDocumentApproveLog WHERE userID = " . $this->_id;
    // "DELETE FROM tblDocumentReviewLog WHERE userID = " . $this->_id;


    $reviewStatus = $this->getReviewStatus();
    foreach ($reviewStatus["indstatus"] as $ri) {
      $queryStr = "INSERT INTO `tblDocumentReviewLog` (`reviewID`, `status`, `comment`, `date`, `userID`) " .
        "VALUES ('" . $ri["reviewID"] . "', '-2', 'Reviewer removed from process', CURRENT_TIMESTAMP, '" . $user->getID() . "')";
      $res = $db->getResult($queryStr);
      if (!$res) {
        $db->rollbackTransaction();
        return false;
      }
    }

    $approvalStatus = $this->getApprovalStatus();
    foreach ($approvalStatus["indstatus"] as $ai) {
      $queryStr = "INSERT INTO `tblDocumentApproveLog` (`approveID`, `status`, `comment`, `date`, `userID`) " .
        "VALUES ('" . $ai["approveID"] . "', '-2', 'Approver removed from process', CURRENT_TIMESTAMP, '" . $user->getID() . "')";
      $res = $db->getResult($queryStr);
      if (!$res) {
        $db->rollbackTransaction();
        return false;
      }
    }

    $db->commitTransaction();
    return true;
  }

  /**
   * Make the user a member of a group
   * This function uses {@link SeedDMS_Group::addUser} but checks before if
   * the user is already a member of the group.
   *
   * @param object $group group to be the member of
   * @return boolean true on success or false in case of an error or the user
   *        is already a member of the group
   */
  function joinGroup($group) {
    if ($group->isMember($this)) {
      return false;
    }

    if (!$group->addUser($this)) {
      return false;
    }

    unset($this->_groups);
    return true;
  }

  /**
   * Removes the user from a group
   * This function uses {@link SeedDMS_Group::removeUser} but checks before if
   * the user is a member of the group at all.
   *
   * @param object $group group to leave
   * @return boolean true on success or false in case of an error or the user
   *        is not a member of the group
   */
  function leaveGroup($group) {
    if (!$group->isMember($this)) {
      return false;
    }

    if (!$group->removeUser($this)) {
      return false;
    }

    unset($this->_groups);
    return true;
  }

  /**
   * Get all groups the user is a member of
   *
   * @return array list of groups
   */
  function getGroups() {
    $db = $this->dms->getDB();

    if (!isset($this->_groups)) {
      $queryStr = "SELECT `tblGroups`.*, `tblGroupMembers`.`userID` FROM `tblGroups` " .
        "LEFT JOIN `tblGroupMembers` ON `tblGroups`.`id` = `tblGroupMembers`.`groupID` " .
        "WHERE `tblGroupMembers`.`userID`='" . $this->id . "'";
      $resArr = $db->getResultArray($queryStr);
      if (is_bool($resArr) && $resArr == false) {
        return false;
      }

      $this->_groups = array();
      foreach ($resArr as $row) {
        $group = new Group($row["id"], $row["name"], $row["comment"]);
        $group->setDMS($this->dms);
        array_push($this->_groups, $group);
      }
    }
    return $this->_groups;
  }

  /**
   * Checks if user is member of a given group
   *
   * @param object $group
   * @return boolean true if user is member of the given group otherwise false
   */
  function isMemberOfGroup($group) {
    return $group->isMember($this);
  }

  /**
   * Check if user has an image in its profile
   *
   * @return boolean true if user has a picture of itself
   */
  function hasImage() {
    if (!isset($this->_hasImage)) {
      $db = $this->dms->getDB();

      $queryStr = "SELECT COUNT(*) AS num FROM tblUserImages WHERE userID = " . $this->id;
      $resArr = $db->getResultArray($queryStr);
      if ($resArr === false) {
        return false;
      }

      if ($resArr[0]["num"] == 0) {
        $this->_hasImage = false;
      }
      else {
        $this->_hasImage = true;
      }
    }

    return $this->_hasImage;
  }

  /**
   * Get the image from the users profile
   *
   * @return array image data
   */
  function getImage() {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblUserImages WHERE userID = " . $this->id;
    $resArr = $db->getResultArray($queryStr);
    if ($resArr === false) {
      return false;
    }

    if ($resArr) {
      $resArr = $resArr[0];
    }
    return $resArr;
  }

  function setImage($tmpfile, $mimeType) {
    $db = $this->dms->getDB();

    $fp = fopen($tmpfile, "rb");
    if (!$fp) {
      return false;
    }
    $content = fread($fp, filesize($tmpfile));
    fclose($fp);

    if ($this->hasImage()) {
      $queryStr = "UPDATE tblUserImages SET image = '" . base64_encode($content) . "', mimeType = " . $db->qstr($mimeType) . " WHERE userID = " . $this->id;
    }
    else {
      $queryStr = "INSERT INTO tblUserImages (userID, image, mimeType) VALUES (" . $this->id . ", '" . base64_encode($content) . "', " . $db->qstr($mimeType) . ")";
    }
    if (!$db->getResult($queryStr)) {
      return false;
    }

    $this->_hasImage = true;
    return true;
  }

  /**
   * Returns all documents of a given user
   *
   * @param object $user
   * @return array list of documents
   */
  function getDocuments() {
    $db = $this->dms->getDB();

    $queryStr = "SELECT `tblDocuments`.*, `tblDocumentLocks`.`userID` as `lockUser` " .
      "FROM `tblDocuments` " .
      "LEFT JOIN `tblDocumentLocks` ON `tblDocuments`.`id`=`tblDocumentLocks`.`document` " .
      "WHERE `tblDocuments`.`owner` = " . $this->id . " ORDER BY `sequence`";

    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    $documents = array();
    foreach ($resArr as $row) {
      $document = new Document($row["id"],
        $row["name"],
        $row["comment"],
        $row["date"],
        $row["expires"],
        $row["owner"],
        $row["folder"],
        $row["inheritAccess"],
        $row["defaultAccess"],
        $row["lockUser"],
        $row["keywords"],
        $row["sequence"]);
      $document->setDMS($this->dms);
      $documents[] = $document;
    }
    return $documents;
  }

  /**
   * Returns all documents locked by a given user
   * FIXME: Not full implemented. Do not use, because it still requires the
   * temporary tables!
   *
   * @param object $user
   * @return array list of documents
   */
  function getDocumentsLocked() {
    $db = $this->dms->getDB();

    $queryStr = "SELECT `tblDocuments`.* " .
      "FROM `tblDocumentLocks` LEFT JOIN `tblDocuments` ON `tblDocuments`.`id` = `tblDocumentLocks`.`document` " .
      "WHERE `tblDocumentLocks`.`userID` = '" . $this->id . "' " .
      "ORDER BY `id` DESC";

    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    $documents = array();
    foreach ($resArr as $row) {
      $document = new Document($row["id"],
        $row["name"],
        $row["comment"],
        $row["date"],
        $row["expires"],
        $row["owner"],
        $row["folder"],
        $row["inheritAccess"],
        $row["defaultAccess"],
        $row["lockUser"],
        $row["keywords"],
        $row["sequence"]);
      $document->setDMS($this->dms);
      $documents[] = $document;
    }
    return $documents;
  }

  /**
   * Get a list of reviews
   * This function returns a list of all reviews seperated by individual
   * and group reviews. If the document id
   * is passed, then only this document will be checked for approvals. The
   * same is true for the version of a document which limits the list
   * further.
   *
   * For a detaile description of the result array see
   * {link BakeryDMS\Core\User::getApprovalStatus} which does the same for
   * approvals.
   *
   * @param int $documentID optional document id for which to retrieve the
   *        reviews
   * @param int $version optional version of the document
   * @return array list of all reviews
   */
  function getReviewStatus($documentID = null, $version = null) {
    $db = $this->dms->getDB();

    /*
        if (!$db->createTemporaryTable("ttreviewid")) {
          return false;
        }
    */
    $status = array("indstatus" => array(), "grpstatus" => array());

    // See if the user is assigned as an individual reviewer.
    $queryStr = "SELECT `tblDocumentReviewers`.*, `tblDocumentReviewLog`.`status`, " .
      "`tblDocumentReviewLog`.`comment`, `tblDocumentReviewLog`.`date`, " .
      "`tblDocumentReviewLog`.`userID` " .
      "FROM `tblDocumentReviewers` " .
      "LEFT JOIN `tblDocumentReviewLog` USING (`reviewID`) " .
      "WHERE `tblDocumentReviewers`.`type`='0' " .
      ($documentID == null ? "" : "AND `tblDocumentReviewers`.`documentID` = '" . (int) $documentID . "' ") .
      ($version == null ? "" : "AND `tblDocumentReviewers`.`version` = '" . (int) $version . "' ") .
      "AND `tblDocumentReviewers`.`required`='" . $this->id . "' " .
      "ORDER BY `tblDocumentReviewLog`.`reviewLogID` DESC";
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr === false) {
      return false;
    }
    if (count($resArr) > 0) {
      foreach ($resArr as $res) {
        if (isset($status["indstatus"][$res['documentID']])) {
          if ($status["indstatus"][$res['documentID']]['date'] < $res['date']) {
            $status["indstatus"][$res['documentID']] = $res;
          }
        }
        else {
          $status["indstatus"][$res['documentID']] = $res;
        }
      }
    }

    // See if the user is the member of a group that has been assigned to
    // review the document version.
    $queryStr = "SELECT `tblDocumentReviewers`.*, `tblDocumentReviewLog`.`status`, " .
      "`tblDocumentReviewLog`.`comment`, `tblDocumentReviewLog`.`date`, " .
      "`tblDocumentReviewLog`.`userID` " .
      "FROM `tblDocumentReviewers` " .
      "LEFT JOIN `tblDocumentReviewLog` USING (`reviewID`) " .
      "LEFT JOIN `tblGroupMembers` ON `tblGroupMembers`.`groupID` = `tblDocumentReviewers`.`required` " .
      "WHERE `tblDocumentReviewers`.`type`='1' " .
      ($documentID == null ? "" : "AND `tblDocumentReviewers`.`documentID` = '" . (int) $documentID . "' ") .
      ($version == null ? "" : "AND `tblDocumentReviewers`.`version` = '" . (int) $version . "' ") .
      "AND `tblGroupMembers`.`userID`='" . $this->id . "' " .
      "ORDER BY `tblDocumentReviewLog`.`reviewLogID` DESC";
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr === false) {
      return false;
    }
    if (count($resArr) > 0) {
      foreach ($resArr as $res) {
        if (isset($status["grpstatus"][$res['documentID']])) {
          if ($status["grpstatus"][$res['documentID']]['date'] < $res['date']) {
            $status["grpstatus"][$res['documentID']] = $res;
          }
        }
        else {
          $status["grpstatus"][$res['documentID']] = $res;
        }
      }
    }
    return $status;
  }

  /**
   * Get a list of approvals
   * This function returns a list of all approvals seperated by individual
   * and group approvals. If the document id
   * is passed, then only this document will be checked for approvals. The
   * same is true for the version of a document which limits the list
   * further.
   *
   * The result array has two elements:
   * - indstatus: which contains the approvals by individuals (users)
   * - grpstatus: which contains the approvals by groups
   *
   * Each element is itself an array of approvals with the following elements:
   * - approveID: unique id of approval
   * - documentID: id of document, that needs to be approved
   * - version: version of document, that needs to be approved
   * - type: 0 for individual approval, 1 for group approval
   * - required: id of user who is required to do the approval
   * - status: 0 not approved, ....
   * - comment: comment given during approval
   * - date: date of approval
   * - userID: id of user who has done the approval
   *
   * @param int $documentID optional document id for which to retrieve the
   *        approvals
   * @param int $version optional version of the document
   * @return array list of all approvals
   */
  function getApprovalStatus($documentID = null, $version = null) {
    $db = $this->dms->getDB();

    /*
        if (!$db->createTemporaryTable("ttapproveid")) {
          return false;
        }
    */
    $status = array("indstatus" => array(), "grpstatus" => array());

    // See if the user is assigned as an individual approver.
    /*
    $queryStr = "SELECT `tblDocumentApprovers`.*, `tblDocumentApproveLog`.`status`, ".
      "`tblDocumentApproveLog`.`comment`, `tblDocumentApproveLog`.`date`, ".
      "`tblDocumentApproveLog`.`userID` ".
      "FROM `tblDocumentApprovers` ".
      "LEFT JOIN `tblDocumentApproveLog` USING (`approveID`) ".
      "LEFT JOIN `ttapproveid` on `ttapproveid`.`maxLogID` = `tblDocumentApproveLog`.`approveLogID` ".
      "WHERE `ttapproveid`.`maxLogID`=`tblDocumentApproveLog`.`approveLogID` ".
      ($documentID==null ? "" : "AND `tblDocumentApprovers`.`documentID` = '". $documentID ."' ").
      ($version==null ? "" : "AND `tblDocumentApprovers`.`version` = '". $version ."' ").
      "AND `tblDocumentApprovers`.`type`='0' ".
      "AND `tblDocumentApprovers`.`required`='". $this->_id ."' ";
*/
    $queryStr =
      "SELECT `tblDocumentApprovers`.*, `tblDocumentApproveLog`.`status`, " .
      "`tblDocumentApproveLog`.`comment`, `tblDocumentApproveLog`.`date`, " .
      "`tblDocumentApproveLog`.`userID` " .
      "FROM `tblDocumentApprovers` " .
      "LEFT JOIN `tblDocumentApproveLog` USING (`approveID`) " .
      "WHERE `tblDocumentApprovers`.`type`='0' " .
      ($documentID == null ? "" : "AND `tblDocumentApprovers`.`documentID` = '" . (int) $documentID . "' ") .
      ($version == null ? "" : "AND `tblDocumentApprovers`.`version` = '" . (int) $version . "' ") .
      "AND `tblDocumentApprovers`.`required`='" . $this->id . "' " .
      "ORDER BY `tblDocumentApproveLog`.`approveLogID` DESC";

    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) > 0) {
      foreach ($resArr as $res) {
        if (isset($status["indstatus"][$res['documentID']])) {
          if ($status["indstatus"][$res['documentID']]['date'] < $res['date']) {
            $status["indstatus"][$res['documentID']] = $res;
          }
        }
        else {
          $status["indstatus"][$res['documentID']] = $res;
        }
      }
    }

    // See if the user is the member of a group that has been assigned to
    // approve the document version.
    /*
    $queryStr = "SELECT `tblDocumentApprovers`.*, `tblDocumentApproveLog`.`status`, ".
      "`tblDocumentApproveLog`.`comment`, `tblDocumentApproveLog`.`date`, ".
      "`tblDocumentApproveLog`.`userID` ".
      "FROM `tblDocumentApprovers` ".
      "LEFT JOIN `tblDocumentApproveLog` USING (`approveID`) ".
      "LEFT JOIN `tblGroupMembers` ON `tblGroupMembers`.`groupID` = `tblDocumentApprovers`.`required` ".
      "LEFT JOIN `ttapproveid` on `ttapproveid`.`maxLogID` = `tblDocumentApproveLog`.`approveLogID` ".
      "WHERE `ttapproveid`.`maxLogID`=`tblDocumentApproveLog`.`approveLogID` ".
      ($documentID==null ? "" : "AND `tblDocumentApprovers`.`documentID` = '". $documentID ."' ").
      ($version==null ? "" : "AND `tblDocumentApprovers`.`version` = '". $version ."' ").
      "AND `tblDocumentApprovers`.`type`='1' ".
      "AND `tblGroupMembers`.`userID`='". $this->_id ."'";
      */
    $queryStr =
      "SELECT `tblDocumentApprovers`.*, `tblDocumentApproveLog`.`status`, " .
      "`tblDocumentApproveLog`.`comment`, `tblDocumentApproveLog`.`date`, " .
      "`tblDocumentApproveLog`.`userID` " .
      "FROM `tblDocumentApprovers` " .
      "LEFT JOIN `tblDocumentApproveLog` USING (`approveID`) " .
      "LEFT JOIN `tblGroupMembers` ON `tblGroupMembers`.`groupID` = `tblDocumentApprovers`.`required` " .
      "WHERE `tblDocumentApprovers`.`type`='1' " .
      ($documentID == null ? "" : "AND `tblDocumentApprovers`.`documentID` = '" . (int) $documentID . "' ") .
      ($version == null ? "" : "AND `tblDocumentApprovers`.`version` = '" . (int) $version . "' ") .
      "AND `tblGroupMembers`.`userID`='" . $this->id . "' " .
      "ORDER BY `tblDocumentApproveLog`.`approveLogID` DESC";
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) > 0) {
      foreach ($resArr as $res) {
        if (isset($status["grpstatus"][$res['documentID']])) {
          if ($status["grpstatus"][$res['documentID']]['date'] < $res['date']) {
            $status["grpstatus"][$res['documentID']] = $res;
          }
        }
        else {
          $status["grpstatus"][$res['documentID']] = $res;
        }
      }
    }
    return $status;
  }

  /**
   * Get a list of documents with a workflow
   *
   * @param int $documentID optional document id for which to retrieve the
   *        reviews
   * @param int $version optional version of the document
   * @return array list of all workflows
   */
  function getWorkflowStatus($documentID = null, $version = null) {
    $db = $this->dms->getDB();

    $queryStr = 'SELECT d.*, c.userid FROM tblWorkflowTransitions a LEFT JOIN tblWorkflows b ON a.workflow=b.id LEFT JOIN tblWorkflowTransitionUsers c ON a.id=c.transition LEFT JOIN tblWorkflowDocumentContent d ON b.id=d.workflow WHERE d.document IS NOT NULL AND a.state=d.state AND c.userid=' . $this->id;
    if ($documentID) {
      $queryStr .= ' AND d.document=' . (int) $documentID;
      if ($version) {
        $queryStr .= ' AND d.version=' . (int) $version;
      }
    }
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    $result['u'] = array();
    if (count($resArr) > 0) {
      foreach ($resArr as $res) {
        $result['u'][] = $res;
      }
    }

    $queryStr = 'SELECT d.*, c.groupid FROM tblWorkflowTransitions a LEFT JOIN tblWorkflows b ON a.workflow=b.id LEFT JOIN tblWorkflowTransitionGroups c ON a.id=c.transition LEFT JOIN tblWorkflowDocumentContent d ON b.id=d.workflow LEFT JOIN tblGroupMembers e ON c.groupid = e.groupID WHERE d.document IS NOT NULL AND a.state=d.state AND e.userID=' . $this->id;
    if ($documentID) {
      $queryStr .= ' AND d.document=' . (int) $documentID;
      if ($version) {
        $queryStr .= ' AND d.version=' . (int) $version;
      }
    }
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    $result['g'] = array();
    if (count($resArr) > 0) {
      foreach ($resArr as $res) {
        $result['g'][] = $res;
      }
    }
    return $result;
  }

  /**
   * Get a list of mandatory reviewers
   * A user which isn't trusted completely may have assigned mandatory
   * reviewers (both users and groups).
   * Whenever the user inserts a new document the mandatory reviewers are
   * filled in as reviewers.
   *
   * @return array list of arrays with two elements containing the user id
   *         (reviewerUserID) and group id (reviewerGroupID) of the reviewer.
   */
  function getMandatoryReviewers() {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblMandatoryReviewers WHERE userID = " . $this->id;
    $resArr = $db->getResultArray($queryStr);

    return $resArr;
  }

  /**
   * Get a list of mandatory approvers
   * See {link BakeryDMS\Core\User::getMandatoryReviewers}
   *
   * @return array list of arrays with two elements containing the user id
   *         (approverUserID) and group id (approverGroupID) of the approver.
   */
  function getMandatoryApprovers() {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblMandatoryApprovers WHERE userID = " . $this->id;
    $resArr = $db->getResultArray($queryStr);

    return $resArr;
  }

  /**
   * Get the mandatory workflow
   * A user which isn't trusted completely may have assigned mandatory
   * workflow
   * Whenever the user inserts a new document the mandatory workflow is
   * filled in as the workflow.
   *
   * @return Workflow workflow
   */
  function getMandatoryWorkflow() {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblWorkflowMandatoryWorkflow WHERE userid = " . $this->id;
    $resArr = $db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    if (!$resArr) {
      return null;
    }

    $workflow = $this->dms->getWorkflow($resArr[0]['workflow']);
    return $workflow;
  }

  /**
   * Set a mandatory reviewer
   * This function sets a mandatory reviewer if it isn't already set.
   *
   * @param integer $id id of reviewer
   * @param boolean $isgroup true if $id is a group
   * @return boolean true on success, otherwise false
   */
  function setMandatoryReviewer($id, $isgroup = false) {
    $db = $this->dms->getDB();

    if ($isgroup) {

      $queryStr = "SELECT * FROM tblMandatoryReviewers WHERE userID = " . $this->id . " AND reviewerGroupID = " . $id;
      $resArr = $db->getResultArray($queryStr);
      if (count($resArr) != 0) {
        return true;
      }

      $queryStr = "INSERT INTO tblMandatoryReviewers (userID, reviewerGroupID) VALUES (" . $this->id . ", " . $id . ")";
      $resArr = $db->getResult($queryStr);
      if (is_bool($resArr) && !$resArr) {
        return false;
      }

    }
    else {

      $queryStr = "SELECT * FROM tblMandatoryReviewers WHERE userID = " . $this->id . " AND reviewerUserID = " . $id;
      $resArr = $db->getResultArray($queryStr);
      if (count($resArr) != 0) {
        return true;
      }

      $queryStr = "INSERT INTO tblMandatoryReviewers (userID, reviewerUserID) VALUES (" . $this->id . ", " . $id . ")";
      $resArr = $db->getResult($queryStr);
      if (is_bool($resArr) && !$resArr) {
        return false;
      }
    }

  }

  /**
   * Set a mandatory approver
   * This function sets a mandatory approver if it isn't already set.
   *
   * @param integer $id id of approver
   * @param boolean $isgroup true if $id is a group
   * @return boolean true on success, otherwise false
   */
  function setMandatoryApprover($id, $isgroup = false) {
    $db = $this->dms->getDB();

    if ($isgroup) {

      $queryStr = "SELECT * FROM tblMandatoryApprovers WHERE userID = " . $this->id . " AND approverGroupID = " . (int) $id;
      $resArr = $db->getResultArray($queryStr);
      if (count($resArr) != 0) {
        return;
      }

      $queryStr = "INSERT INTO tblMandatoryApprovers (userID, approverGroupID) VALUES (" . $this->id . ", " . $id . ")";
      $resArr = $db->getResult($queryStr);
      if (is_bool($resArr) && !$resArr) {
        return false;
      }

    }
    else {

      $queryStr = "SELECT * FROM tblMandatoryApprovers WHERE userID = " . $this->id . " AND approverUserID = " . (int) $id;
      $resArr = $db->getResultArray($queryStr);
      if (count($resArr) != 0) {
        return;
      }

      $queryStr = "INSERT INTO tblMandatoryApprovers (userID, approverUserID) VALUES (" . $this->id . ", " . $id . ")";
      $resArr = $db->getResult($queryStr);
      if (is_bool($resArr) && !$resArr) {
        return false;
      }
    }
  }

  /**
   * Set a mandatory workflow
   * This function sets a mandatory workflow if it isn't already set.
   *
   * @param object $workflow workflow
   * @return boolean true on success, otherwise false
   */
  function setMandatoryWorkflow($workflow) {
    $db = $this->dms->getDB();

    $queryStr = "SELECT * FROM tblWorkflowMandatoryWorkflow WHERE userid = " . $this->id . " AND workflow = " . (int) $workflow->getID();
    $resArr = $db->getResultArray($queryStr);
    if (count($resArr) != 0) {
      return;
    }

    $queryStr = "INSERT INTO tblWorkflowMandatoryWorkflow (userid, workflow) VALUES (" . $this->id . ", " . $workflow->getID() . ")";
    $resArr = $db->getResult($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }
  }

  /**
   * Deletes all mandatory reviewers
   *
   * @return boolean true on success, otherwise false
   */
  function delMandatoryReviewers() {
    $db = $this->dms->getDB();
    $queryStr = "DELETE FROM tblMandatoryReviewers WHERE userID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }
    return true;
  }

  /**
   * Deletes all mandatory approvers
   *
   * @return boolean true on success, otherwise false
   */
  function delMandatoryApprovers() {
    $db = $this->dms->getDB();

    $queryStr = "DELETE FROM tblMandatoryApprovers WHERE userID = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }
    return true;
  }

  /**
   * Deletes the  mandatory workflow
   *
   * @return boolean true on success, otherwise false
   */
  function delMandatoryWorkflow() {
    $db = $this->dms->getDB();
    $queryStr = "DELETE FROM tblWorkflowMandatoryWorkflow WHERE userid = " . $this->id;
    if (!$db->getResult($queryStr)) {
      return false;
    }
    return true;
  }

}

?>
