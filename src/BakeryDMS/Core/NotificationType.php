<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

class NotificationType {
  const ALL = 0; // Notify on all actions on the folder/document
  const DELETE = 1; // Notify when object has been deleted
  const MOVE = 2; // Notify when object has been moved
  const UPDATE = 3; // Notify when object has been updated (no new version)
  const NEW_VERSION = 4; // Notify when document has new version
  const DELETE_VERSION = 5; // Notify when version of document was deleted
  const ADD_DOCUMENT = 6; // Notify when version of document was deleted
}