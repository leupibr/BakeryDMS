<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

/**
 * Class to represent a user access right.
 * This class cannot be used to modify access rights.
 */
class UserAccess {
  /** @var User The user of this access. */
  private $user;

  /** @var int The mode of this access. */
  private $mode;

  /**
   * Initializes a new instance of the {@link UserAccess} class.
   *
   * @param User $user The user of this access.
   * @param int $mode The access rights.
   */
  function __construct($user, $mode) {
    $this->user = $user;
    $this->mode = $mode;
  }

  /**
   * Gets the user id.
   *
   * @return int Gets the id of the user
   */
  function getUserID() {
    return $this->user->getID();
  }

  /**
   * Gets the access mode.
   *
   * @return int The mode
   */
  function getMode() {
    return $this->mode;
  }

  /**
   * Flag if the user is admin.
   *
   * @return bool true if is admin, otherwise false.
   */
  function isAdmin() {
    return ($this->mode == User::role_admin);
  }

  /**
   * Gets the user of this access item.
   *
   * @return User The user.
   */
  function getUser() {
    return $this->user;
  }
}