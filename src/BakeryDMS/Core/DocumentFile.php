<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

/**
 * Class to represent a file attached to a document.
 *
 * Beside the regular document content arbitrary files can be attached
 * to a document. This is a similar concept as attaching files to emails.
 * The owner of the attached file and the document may not be the same.
 * Use {@link BakeryDMS\Core\Document::addDocumentFile()} to attach a file.
 */
class DocumentFile {
  /** @var int internal id of document file. */
  protected $id;

  /** @var \BakeryDMS\Core\Document reference to document this file belongs to. */
  protected $document;

  /** @var int id of user who is the owner of this link. */
  protected $userID;

  /** @var string comment for the attached file. */
  protected $comment;

  /** @var string date when the file was attached. */
  protected $date;

  /**
   * @var string directory where the file is stored. This is the
   * document id with a proceeding '/'.
   * FIXME: looks like this isn't used anymore. The file path is
   * constructed by getPath()
   */
  protected $directory;

  /** @var string extension of the original file name with a leading '.'. */
  protected $fileType;

  /** @var string mime type of the file. */
  protected $mimeType;

  /** @var string name of the file that was originally uploaded. */
  protected $originFileName;

  /** @var string name of the file as given by the user. */
  protected $name;

  /**
   * Initializes a new instance of the {@link DocumentFile} class.
   *
   * @param $id int The id of the document file.
   * @param $document \BakeryDMS\Core\Document The document file.
   * @param $userID int The id of the user.
   * @param $comment string Comment for the attached file
   * @param $date string Date when the file was attached.
   * @param $dir string Directory where the file is stored.
   * @param $fileType string extension of the original file name with a leading '.'.
   * @param $mimeType string mime type of the file.
   * @param $orgFileName string name of the file that was originally uploaded.
   * @param $name string name of the file as given by the user.
   */
  function __construct($id, $document, $userID, $comment, $date, $dir, $fileType, $mimeType, $orgFileName, $name) {
    $this->id = $id;
    $this->document = $document;
    $this->userID = $userID;
    $this->comment = $comment;
    $this->date = $date;
    $this->directory = $dir;
    $this->fileType = $fileType;
    $this->mimeType = $mimeType;
    $this->originFileName = $orgFileName;
    $this->name = $name;
  }

  function getID() {
    return $this->id;
  }

  function getDocument() {
    return $this->document;
  }

  function getUserID() {
    return $this->userID;
  }

  function getComment() {
    return $this->comment;
  }

  function getDate() {
    return $this->date;
  }

  function getDir() {
    return $this->directory;
  }

  function getFileType() {
    return $this->fileType;
  }

  function getMimeType() {
    return $this->mimeType;
  }

  function getOriginalFileName() {
    return $this->originFileName;
  }

  function getName() {
    return $this->name;
  }

  function getUser() {
    if (!isset($this->_user)) {
      $this->_user = $this->document->_dms->getUser($this->userID);
    }
    return $this->_user;
  }

  function getPath() {
    return $this->document->getDir() . "f" . $this->id . $this->fileType;
  }
}
