<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Core;

use BakeryDMS\Core\Workflow\Action;
use BakeryDMS\Core\Workflow\State;
use BakeryDMS\Core\Workflow\Transition;
use BakeryDMS\Core\Workflow\Workflow;

/**
 * Class to represent the complete document management system.
 * This class is needed to do most of the dms operations. It needs
 * an instance of {@link IDatabaseAccess} to access the
 * underlying database. Many methods are factory functions which create
 * objects representing the entities in the dms, like folders, documents,
 * users, or groups.
 *
 * Each dms has its own database for meta data and a data store for document
 * content. Both must be specified when creating a new instance of this class.
 * All folders and documents are organized in a hierarchy like
 * a regular file system starting with a {@link $rootFolderID}
 *
 * This class does not enforce any access rights on documents and folders
 * by design. It is up to the calling application to use the methods
 * {@link BakeryDMS\Core\Folder::getAccessMode()} and
 * {@link BakeryDMS\Core\Document::getAccessMode()} and interpret them as desired.
 * Though, there are two convenient functions to filter a list of
 * documents/folders for which users have access rights for. See
 * {@link BakeryDMS\Core\DMS::filterAccess()}
 * and {@link BakeryDMS\Core\DMS::filterUsersByAccess()}
 *
 * Though, this class has two methods to set the currently logged in user
 * ({@link setUser} and {@link login}), none of them need to be called, because
 * there is currently no class within the SeedDMS core which needs the logged
 * in user.
 */
class DMS {
  /**
   * @var Persistency\IDatabaseAccess reference to database object.
   */
  protected $db;

  /**
   * @var User $user reference to currently logged in user. This must be an instance of {@link BakeryDMS\Core\User}.
   *   This variable is currently not used. It is set by {@link setUser}.
   */
  private $user;

  /**
   * @var string $contentDir location in the file system where all the document data is located. This should be an
   *   absolute path.
   */
  public $contentDir;

  /**
   * @var integer $rootFolderID ID of root folder
   */
  public $rootFolderID;

  /**
   * @var boolean $enableConverting set to true if conversion of content is desired
   */
  public $enableConverting;

  /**
   * @var array $convertFileTypes list of files types that shall be converted
   */
  public $convertFileTypes;

  /**
   * @var array $viewOnlineFileTypes list of files types that can be viewed online
   */
  public $viewOnlineFileTypes;

  /**
   * @var string $version version of pear package
   */
  public $version;

  /**
   * @var array $callbacks list of methods called when certain operations,
   * like removing a document, are executed. Set a callback with
   * {@link BakeryDMS\Core\DMS::setCallback()}.
   * The key of the array is the internal callback function name. Each
   * array element is an array with two elements: the function name
   * and the parameter passed to the function.
   *
   * Currently implemented callbacks are:
   *
   * onPreRemoveDocument($user_param, $document);
   *   called before deleting a document. If this function returns false
   *   the document will not be deleted.
   *
   * onPostRemoveDocument($user_param, $document_id);
   *   called after the successful deletion of a document.
   */
  public $callbacks;


  /**
   * Checks if two objects are equal by comparing its ID
   *
   * The regular php check done by '==' compares all attributes of
   * two objects, which isn't required. The method will first check
   * if the objects are instances of the same class.
   *
   * @param object $object1
   * @param object $object2
   *
   * @return boolean true if objects are equal, otherwise false
   */
  static function checkIfEqual($object1, $object2) {
    if (get_class($object1) != get_class($object2)) {
      return false;
    }
    if ($object1->getID() != $object2->getID()) {
      return false;
    }
    return true;
  }

  /**
   * Filter objects out which are not accessible in a given mode by a user.
   *
   * @param array $objArr list of objects (either documents or folders)
   * @param object $user user for which access is checked
   * @param integer $minMode minimum access mode required
   *
   * @return array filtered list of objects
   */
  static function filterAccess($objArr, $user, $minMode) {
    if (!is_array($objArr)) {
      return array();
    }
    $newArr = array();
    foreach ($objArr as $obj) {
      if ($obj->getAccessMode($user) >= $minMode) {
        array_push($newArr, $obj);
      }
    }
    return $newArr;
  }

  /**
   * Filter users out which cannot access an object in a given mode.
   *
   * @param object $obj object that shall be accessed
   * @param array $users list of users which are to check for sufficient
   *        access rights
   * @param integer $minMode minimum access right on the object for each user
   *
   * @return array filtered list of users
   */
  static function filterUsersByAccess($obj, $users, $minMode) {
    $newArr = array();
    foreach ($users as $currUser) {
      if ($obj->getAccessMode($currUser) >= $minMode) {
        array_push($newArr, $currUser);
      }
    }
    return $newArr;
  }

  /**
   * Filters the document links.
   *
   * @param $user User user for which access is being checked.
   * @param $links \BakeryDMS\Core\DocumentLink[] list of objects of type.
   *
   * @return array A filtered list of links which are accessible by the given user.
   */
  static function filterDocumentLinks($user, $links) {
    $tmp = array();
    foreach ($links as $link) {
      if ($link->isPublic() || ($link->getUser()->getID() == $user->getID()) || $user->isAdmin()) {
        array_push($tmp, $link);
      }
    }
    return $tmp;
  }

  /**
   * Create a new instance of the dms
   *
   * @param $db Persistency\IDatabaseAccess to access the underlying database
   * @param $contentDir string path in filesystem containing the data store all document contents is stored
   *
   * @return DMS The dms.
   */
  function __construct($db, $contentDir) {
    $this->db = $db;
    if (substr($contentDir, -1) == '/') {
      $this->contentDir = $contentDir;
    }
    else {
      $this->contentDir = $contentDir . '/';
    }
    $this->rootFolderID = 1;
    $this->maxDirID = 0; //31998;
    $this->enableConverting = false;
    $this->convertFileTypes = array();
    $this->version = '4.3.10';
    if ($this->version[0] == '@') {
      $this->version = '4.3.10';
    }
  }

  /**
   * @return Persistency\IDatabaseAccess
   */
  function getDB() {
    return $this->db;
  }

  /**
   * Return the database version
   *
   * @return array array with elements major, minor, subminor, date
   */
  function getDBVersion() {
    $tbllist = $this->db->TableList();
    $tbllist = explode(',', strtolower(join(',', $tbllist)));
    if (!array_search('tblversion', $tbllist)) {
      return false;
    }
    $queryStr = "SELECT * FROM tblVersion ORDER BY major,minor,subminor LIMIT 1";
    $resArr = $this->db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) != 1) {
      return false;
    }
    $resArr = $resArr[0];
    return $resArr;
  }

  /**
   * Check if the version in the database is the same as of this package
   * Only the major and minor version number will be checked.
   *
   * @return boolean returns false if versions do not match, but returns
   *         true if version matches or table tblVersion does not exists.
   */
  function checkVersion() {
    $tbllist = $this->db->TableList();
    $tbllist = explode(',', strtolower(join(',', $tbllist)));
    if (!array_search('tblversion', $tbllist)) {
      return true;
    }
    $queryStr = "SELECT * FROM tblVersion ORDER BY major,minor,subminor LIMIT 1";
    $resArr = $this->db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) != 1) {
      return false;
    }
    $resArr = $resArr[0];
    $ver = explode('.', $this->version);
    if (($resArr['major'] != $ver[0]) || ($resArr['minor'] != $ver[1])) {
      return false;
    }
    return true;
  }

  /**
   * Set id of root folder
   * This function must be called right after creating an instance of
   * {@link BakeryDMS\Core\DMS}
   *
   * @param integer $id id of root folder
   */
  function setRootFolderID($id) {
    $this->rootFolderID = $id;
  }

  /**
   * Set maximum number of subdirectories per directory
   *
   * The value of maxDirID is quite crucial because, all documents are
   * associated with a directory in the filesystem. Consequently, there is
   * maximum number of documents, because depending on the file system
   * the maximum number of subdirectories is limited. Since version 3.3.0 of
   * SeedDMS an additional directory level has been introduced. All documents
   * from 1 to maxDirID-1 will be saved in 1/&lt;docid&gt;, documents from maxDirID
   * to 2*maxDirID-1 are stored in 2/&lt;docid&gt; and so on.
   *
   * This function must be called right after creating an instance of
   * {@link BakeryDMS\Core\DMS}
   *
   * @param integer $id id of root folder
   */
  function setMaxDirID($id) {
    $this->maxDirID = $id;
  }

  /**
   * Gets the root folder.
   *
   * @return Folder return the root folder.
   */
  function getRootFolder() {
    return $this->getFolder($this->rootFolderID);
  }

  function setEnableConverting($enable) {
    $this->enableConverting = $enable;
  }

  function setConvertFileTypes($types) {
    $this->convertFileTypes = $types;
  }

  function setViewOnlineFileTypes($types) {
    $this->viewOnlineFileTypes = $types;
  }

  /**
   * Login as a user
   *
   * Checks if the given credentials are valid and returns a user object.
   * It also sets the property $user for later access on the currently
   * logged in user
   *
   * @param string $username login name of user
   * @param string $password password of user
   *
   * @return object instance of class {@link BakeryDMS\Core\User} or false
   */
  function login($username, $password) {
  }

  /**
   * Set the logged in user.
   * If user authentication was done externally, this function can
   * be used to tell the dms who is currently logged in.
   *
   * @param User $user
   *
   */
  function setCurrentUser($user) {
    $this->user = $user;
  }

  /**
   * @return User
   */
  public function getCurrentUser() {
    return $this->user;
  }

  /**
   * Return a document by its id
   *
   * This function retrieves a document from the database by its id.
   *
   * @param integer $id internal id of document
   *
   * @return Document instance of {@link BakeryDMS\Core\Document} or false
   */
  function getDocument($id) {
    if (!is_numeric($id)) {
      return false;
    }

    $queryStr = "SELECT * FROM tblDocuments WHERE id = " . (int) $id;
    $resArr = $this->db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) != 1) {
      return false;
    }
    $resArr = $resArr[0];

    // New Locking mechanism uses a separate table to track the lock.
    $queryStr = "SELECT * FROM tblDocumentLocks WHERE document = " . (int) $id;
    $lockArr = $this->db->getResultArray($queryStr);
    if ((is_bool($lockArr) && $lockArr == false) || (count($lockArr) == 0)) {
      // Could not find a lock on the selected document.
      $lock = -1;
    }
    else {
      // A lock has been identified for this document.
      $lock = $lockArr[0]["userID"];
    }

    $document = new Document($resArr["id"],
      $resArr["name"],
      $resArr["comment"],
      $resArr["date"],
      $resArr["expires"],
      $resArr["owner"],
      $resArr["folder"],
      $resArr["inheritAccess"],
      $resArr["defaultAccess"],
      $lock,
      $resArr["keywords"],
      $resArr["sequence"]);
    $document->setDMS($this);
    return $document;
  }

  /**
   * Returns all documents of a given user
   *
   * @param object $user
   *
   * @return array list of documents
   */
  function getDocumentsByUser($user) {
    return $user->getDocuments();
  }

  /**
   * Returns all documents locked by a given user
   *
   * @deprecated FIXME: Not full implemented. Do not use, because it still requires the temporary tables!
   *
   * @param object $user
   *
   * @return array list of documents
   */
  function getDocumentsLockedByUser($user) {
    return $user->getDocumentsLocked();
  }

  /**
   * Returns a document by its name
   *
   * This function searches a document by its name and restricts the search
   * to given folder if passed as the second parameter.
   *
   * @param string $name
   * @param object $folder
   *
   * @return object/boolean found document or false
   */
  function getDocumentByName($name, $folder = null) {
    if (!$name) {
      return false;
    }

    $queryStr = "SELECT `tblDocuments`.*, `tblDocumentLocks`.`userID` as `lockUser` " .
      "FROM `tblDocuments` " .
      "LEFT JOIN `tblDocumentLocks` ON `tblDocuments`.`id`=`tblDocumentLocks`.`document` " .
      "WHERE `tblDocuments`.`name` = " . $this->db->qstr($name);
    if ($folder) {
      $queryStr .= " AND `tblDocuments`.`folder` = " . $folder->getID();
    }
    $queryStr .= " LIMIT 1";

    $resArr = $this->db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    if (!$resArr) {
      return false;
    }

    $row = $resArr[0];
    $document = new Document($row["id"],
      $row["name"],
      $row["comment"],
      $row["date"],
      $row["expires"],
      $row["owner"],
      $row["folder"],
      $row["inheritAccess"],
      $row["defaultAccess"],
      $row["lockUser"],
      $row["keywords"],
      $row["sequence"]);
    $document->setDMS($this);
    return $document;
  }

  /**
   * Return a document content by its id
   *
   * This function retrieves a document content from the database by its id.
   *
   * @param integer $id internal id of document content
   *
   * @return object instance of {@link BakeryDMS\Core\DocumentContent} or false
   */
  function getDocumentContent($id) {
    if (!is_numeric($id)) {
      return false;
    }

    $queryStr = "SELECT * FROM tblDocumentContent WHERE id = " . (int) $id;
    $resArr = $this->db->getResultArray($queryStr);
    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) != 1) {
      return false;
    }
    $row = $resArr[0];

    $document = $this->getDocument($row['document']);
    $version = new DocumentContent($row['id'],
      $document,
      $row['version'],
      $row['comment'],
      $row['date'],
      $row['createdBy'],
      $row['dir'],
      $row['orgFileName'],
      $row['fileType'],
      $row['mimeType'],
      $row['fileSize'],
      $row['checksum']);
    return $version;
  }

  function makeTimeStamp($hour, $min, $sec, $year, $month, $day) {
    $thirtyone = array(1, 3, 5, 7, 8, 10, 12);
    $thirty = array(4, 6, 9, 11);

    // Very basic check that the terms are valid. Does not fail for illegal
    // dates such as 31 Feb.
    if (!is_numeric($hour) || !is_numeric($min) || !is_numeric($sec) || !is_numeric($year) || !is_numeric($month) || !is_numeric($day) || $month < 1 || $month > 12 || $day < 1 || $day > 31 || $hour < 0 || $hour > 23 || $min < 0 || $min > 59 || $sec < 0 || $sec > 59) {
      return false;
    }
    $year = (int) $year;
    $month = (int) $month;
    $day = (int) $day;

    if (array_search($month, $thirtyone)) {
      $max = 31;
    }
    else if (array_search($month, $thirty)) {
      $max = 30;
    }
    else {
      $max = (($year % 4 == 0) && ($year % 100 != 0 || $year % 400 == 0)) ? 29 : 28;
    }

    // If the date falls out of bounds, set it to the maximum for the given
    // month. Makes assumption about the user's intention, rather than failing
    // for absolutely everything.
    if ($day > $max) {
      $day = $max;
    }

    return mktime($hour, $min, $sec, $month, $day, $year);
  }

  /*
	 * Search the database for documents
	 *
	 * Note: the creation date will be used to check againts the
	 * date saved with the document
	 * or folder. The modification date will only be used for documents. It
	 * is checked against the creation date of the document content. This
	 * meanѕ that updateѕ of a document will only result in a searchable
	 * modification if a new version is uploaded.
	 *
	 * @param query string seach query with space separated words
	 * @param limit integer number of items in result set
	 * @param offset integer index of first item in result set
	 * @param logicalmode string either AND or OR
	 * @param searchin array() list of fields to search in
	 *        1 = keywords, 2=name, 3=comment, 4=attributes
	 * @param startFolder object search in the folder only (null for root folder)
	 * @param owner object search for documents owned by this user
	 * @param status array list of status
	 * @param creationstartdate array search for documents created after this date
	 * @param creationenddate array search for documents created before this date
	 * @param modificationstartdate array search for documents modified after this date
	 * @param modificationenddate array search for documents modified before this date
	 * @param categories array list of categories the documents must have assigned
	 * @param attributes array list of attributes
	 * @param mode int decide whether to search for documents/folders
	 *        0x1 = documents only
	 *        0x2 = folders only
	 *        0x3 = both
	 * @param expirationstartdate array search for documents expiring after this date
	 * @param expirationenddate array search for documents expiring before this date
	 * @return array containing the elements total and docs
	 */
  function search($query,
                  $limit = 0,
                  $offset = 0,
                  $logicalmode = 'AND',
                  $searchin = array(),
                  $startFolder = null,
                  $owner = null,
                  $status = array(),
                  $creationstartdate = array(),
                  $creationenddate = array(),
                  $modificationstartdate = array(),
                  $modificationenddate = array(),
                  $categories = array(),
                  $attributes = array(),
                  $mode = 0x3,
                  $expirationstartdate = array(),
                  $expirationenddate = array()) {
    // Split the search string into constituent keywords.
    $tkeys = array();
    if (strlen($query) > 0) {
      $tkeys = preg_split("/[\t\r\n ,]+/", $query);
    }

    // if none is checkd search all
    if (count($searchin) == 0) {
      $searchin = array(1, 2, 3, 4);
    }

    /*--------- Do it all over again for folders -------------*/
    $totalFolders = 0;
    if ($mode & 0x2) {
      $searchKey = "";
      $searchFields = array();
      if (in_array(2, $searchin)) {
        $searchFields[] = "`tblFolders`.`name`";
      }
      if (in_array(3, $searchin)) {
        $searchFields[] = "`tblFolders`.`comment`";
      }
      if (in_array(4, $searchin)) {
        $searchFields[] = "`tblFolderAttributes`.`value`";
      }

      if (count($searchFields) > 0) {
        foreach ($tkeys as $key) {
          $key = trim($key);
          if (strlen($key) > 0) {
            $searchKey = (strlen($searchKey) == 0 ? "" : $searchKey . " " . $logicalmode . " ") . "(" . implode(" like " . $this->db->qstr("%" . $key . "%") . " OR ",
                $searchFields) . " like " . $this->db->qstr("%" . $key . "%") . ")";
          }
        }
      }

      // Check to see if the search has been restricted to a particular sub-tree in
      // the folder hierarchy.
      $searchFolder = "";
      if ($startFolder) {
        $searchFolder = "`tblFolders`.`folderList` LIKE '%:" . $startFolder->getID() . ":%'";
      }

      // Check to see if the search has been restricted to a particular
      // document owner.
      $searchOwner = "";
      if ($owner) {
        if (is_array($owner)) {
          $ownerids = array();
          foreach ($owner as $o) {
            $ownerids[] = $o->getID();
          }
          if ($ownerids) {
            $searchOwner = "`tblFolders`.`owner` IN (" . implode(',', $ownerids) . ")";
          }
        }
        else {
          $searchOwner = "`tblFolders`.`owner` = '" . $owner->getId() . "'";
        }
      }

      // Check to see if the search has been restricted to a particular
      // attribute.
      $searchAttributes = array();
      if ($attributes) {
        foreach ($attributes as $attrdefid => $attribute) {
          if ($attribute) {
            $attrdef = $this->getAttributeDefinition($attrdefid);
            if ($attrdef->getObjType() == \BakeryDMS\Core\AttributeDefinition::objtype_folder) {
              if ($attrdef->getValueSet()) {
                $searchAttributes[] = "`tblFolderAttributes`.`attrdef`=" . $attrdefid . " AND `tblFolderAttributes`.`value`='" . $attribute . "'";
              }
              else {
                $searchAttributes[] = "`tblFolderAttributes`.`attrdef`=" . $attrdefid . " AND `tblFolderAttributes`.`value` like '%" . $attribute . "%'";
              }
            }
          }
        }
      }

      // Is the search restricted to documents created between two specific dates?
      $searchCreateDate = "";
      if ($creationstartdate) {
        $startdate = DMS::makeTimeStamp($creationstartdate['hour'],
          $creationstartdate['minute'],
          $creationstartdate['second'],
          $creationstartdate['year'],
          $creationstartdate["month"],
          $creationstartdate["day"]);
        if ($startdate) {
          $searchCreateDate .= "`tblFolders`.`date` >= " . $startdate;
        }
      }
      if ($creationenddate) {
        $stopdate = DMS::makeTimeStamp($creationenddate['hour'],
          $creationstartdate['minute'],
          $creationstartdate['second'],
          $creationenddate["year"],
          $creationenddate["month"],
          $creationenddate["day"]);
        if ($stopdate) {
          if ($startdate) {
            $searchCreateDate .= " AND ";
          }
          $searchCreateDate .= "`tblFolders`.`date` <= " . $stopdate;
        }
      }

      $searchQuery = "FROM `tblFolders` LEFT JOIN `tblFolderAttributes` on `tblFolders`.`id`=`tblFolderAttributes`.`folder` WHERE 1=1";

      if (strlen($searchKey) > 0) {
        $searchQuery .= " AND (" . $searchKey . ")";
      }
      if (strlen($searchFolder) > 0) {
        $searchQuery .= " AND " . $searchFolder;
      }
      if (strlen($searchOwner) > 0) {
        $searchQuery .= " AND (" . $searchOwner . ")";
      }
      if (strlen($searchCreateDate) > 0) {
        $searchQuery .= " AND (" . $searchCreateDate . ")";
      }
      if ($searchAttributes) {
        $searchQuery .= " AND (" . implode(" AND ", $searchAttributes) . ")";
      }

      /* Do not search for folders if not at least a search for a key,
			 * an owner, or creation date is requested.
			 */
      if ($searchKey || $searchOwner || $searchCreateDate || $searchAttributes) {
        // Count the number of rows that the search will produce.
        $resArr = $this->db->getResultArray("SELECT COUNT(*) AS num FROM (SELECT DISTINCT `tblFolders`.id " . $searchQuery . ") a");
        if ($resArr && isset($resArr[0]) && is_numeric($resArr[0]["num"]) && $resArr[0]["num"] > 0) {
          $totalFolders = (integer) $resArr[0]["num"];
        }

        // If there are no results from the count query, then there is no real need
        // to run the full query. TODO: re-structure code to by-pass additional
        // queries when no initial results are found.

        // Only search if the offset is not beyond the number of folders
        if ($totalFolders > $offset) {
          // Prepare the complete search query, including the LIMIT clause.
          $searchQuery = "SELECT DISTINCT `tblFolders`.* " . $searchQuery . " GROUP BY `tblFolders`.`id`";

          if ($limit) {
            $searchQuery .= " LIMIT " . $offset . "," . $limit;
          }

          // Send the complete search query to the database.
          $resArr = $this->db->getResultArray($searchQuery);
        }
        else {
          $resArr = array();
        }

        // ------------------- Ausgabe der Ergebnisse ----------------------------
        $numResults = count($resArr);
        if ($numResults == 0) {
          $folderresult = array('totalFolders' => $totalFolders, 'folders' => array());
        }
        else {
          foreach ($resArr as $folderArr) {
            $folders[] = $this->getFolder($folderArr['id']);
          }
          $folderresult = array('totalFolders' => $totalFolders, 'folders' => $folders);
        }
      }
      else {
        $folderresult = array('totalFolders' => 0, 'folders' => array());
      }
    }
    else {
      $folderresult = array('totalFolders' => 0, 'folders' => array());
    }

    /*--------- Do it all over again for documents -------------*/

    $totalDocs = 0;
    if ($mode & 0x1) {
      $searchKey = "";
      $searchFields = array();
      if (in_array(1, $searchin)) {
        $searchFields[] = "`tblDocuments`.`keywords`";
      }
      if (in_array(2, $searchin)) {
        $searchFields[] = "`tblDocuments`.`name`";
      }
      if (in_array(3, $searchin)) {
        $searchFields[] = "`tblDocuments`.`comment`";
      }
      if (in_array(4, $searchin)) {
        $searchFields[] = "`tblDocumentAttributes`.`value`";
        $searchFields[] = "`tblDocumentContentAttributes`.`value`";
      }


      if (count($searchFields) > 0) {
        foreach ($tkeys as $key) {
          $key = trim($key);
          if (strlen($key) > 0) {
            $searchKey = (strlen($searchKey) == 0 ? "" : $searchKey . " " . $logicalmode . " ") . "(" . implode(" like " . $this->db->qstr("%" . $key . "%") . " OR ",
                $searchFields) . " like " . $this->db->qstr("%" . $key . "%") . ")";
          }
        }
      }

      // Check to see if the search has been restricted to a particular sub-tree in
      // the folder hierarchy.
      $searchFolder = "";
      if ($startFolder) {
        $searchFolder = "`tblDocuments`.`folderList` LIKE '%:" . $startFolder->getID() . ":%'";
      }

      // Check to see if the search has been restricted to a particular
      // document owner.
      $searchOwner = "";
      if ($owner) {
        if (is_array($owner)) {
          $ownerids = array();
          foreach ($owner as $o) {
            $ownerids[] = $o->getID();
          }
          if ($ownerids) {
            $searchOwner = "`tblDocuments`.`owner` IN (" . implode(',', $ownerids) . ")";
          }
        }
        else {
          $searchOwner = "`tblDocuments`.`owner` = '" . $owner->getId() . "'";
        }
      }

      // Check to see if the search has been restricted to a particular
      // document category.
      $searchCategories = "";
      if ($categories) {
        $catids = array();
        foreach ($categories as $category) {
          $catids[] = $category->getId();
        }
        $searchCategories = "`tblDocumentCategory`.`categoryID` in (" . implode(',', $catids) . ")";
      }

      // Check to see if the search has been restricted to a particular
      // attribute.
      $searchAttributes = array();
      if ($attributes) {
        foreach ($attributes as $attrdefid => $attribute) {
          if ($attribute) {
            $attrdef = $this->getAttributeDefinition($attrdefid);
            if ($attrdef->getObjType() == \BakeryDMS\Core\AttributeDefinition::objtype_document) {
              if ($attrdef->getValueSet()) {
                $searchAttributes[] = "`tblDocumentAttributes`.`attrdef`=" . $attrdefid . " AND `tblDocumentAttributes`.`value`='" . $attribute . "'";
              }
              else {
                $searchAttributes[] = "`tblDocumentAttributes`.`attrdef`=" . $attrdefid . " AND `tblDocumentAttributes`.`value` like '%" . $attribute . "%'";
              }
            }
            elseif ($attrdef->getObjType() == \BakeryDMS\Core\AttributeDefinition::objtype_documentcontent) {
              if ($attrdef->getValueSet()) {
                $searchAttributes[] = "`tblDocumentContentAttributes`.`attrdef`=" . $attrdefid . " AND `tblDocumentContentAttributes`.`value`='" . $attribute . "'";
              }
              else {
                $searchAttributes[] = "`tblDocumentContentAttributes`.`attrdef`=" . $attrdefid . " AND `tblDocumentContentAttributes`.`value` like '%" . $attribute . "%'";
              }
            }
          }
        }
      }

      // Is the search restricted to documents created between two specific dates?
      $searchCreateDate = "";
      if ($creationstartdate) {
        $startdate = DMS::makeTimeStamp($creationstartdate['hour'],
          $creationstartdate['minute'],
          $creationstartdate['second'],
          $creationstartdate['year'],
          $creationstartdate["month"],
          $creationstartdate["day"]);
        if ($startdate) {
          $searchCreateDate .= "`tblDocuments`.`date` >= " . $startdate;
        }
      }
      if ($creationenddate) {
        $stopdate = DMS::makeTimeStamp($creationenddate['hour'],
          $creationenddate['minute'],
          $creationenddate['second'],
          $creationenddate["year"],
          $creationenddate["month"],
          $creationenddate["day"]);
        if ($stopdate) {
          if ($searchCreateDate) {
            $searchCreateDate .= " AND ";
          }
          $searchCreateDate .= "`tblDocuments`.`date` <= " . $stopdate;
        }
      }
      if ($modificationstartdate) {
        $startdate = DMS::makeTimeStamp($modificationstartdate['hour'],
          $modificationstartdate['minute'],
          $modificationstartdate['second'],
          $modificationstartdate['year'],
          $modificationstartdate["month"],
          $modificationstartdate["day"]);
        if ($startdate) {
          if ($searchCreateDate) {
            $searchCreateDate .= " AND ";
          }
          $searchCreateDate .= "`tblDocumentContent`.`date` >= " . $startdate;
        }
      }
      if ($modificationenddate) {
        $stopdate = DMS::makeTimeStamp($modificationenddate['hour'],
          $modificationenddate['minute'],
          $modificationenddate['second'],
          $modificationenddate["year"],
          $modificationenddate["month"],
          $modificationenddate["day"]);
        if ($stopdate) {
          if ($searchCreateDate) {
            $searchCreateDate .= " AND ";
          }
          $searchCreateDate .= "`tblDocumentContent`.`date` <= " . $stopdate;
        }
      }
      $searchExpirationDate = '';
      if ($expirationstartdate) {
        $startdate = DMS::makeTimeStamp($expirationstartdate['hour'],
          $expirationstartdate['minute'],
          $expirationstartdate['second'],
          $expirationstartdate['year'],
          $expirationstartdate["month"],
          $expirationstartdate["day"]);
        if ($startdate) {
          if ($searchExpirationDate) {
            $searchExpirationDate .= " AND ";
          }
          $searchExpirationDate .= "`tblDocuments`.`expires` >= " . $startdate;
        }
      }
      if ($expirationenddate) {
        $stopdate = DMS::makeTimeStamp($expirationenddate['hour'],
          $expirationenddate['minute'],
          $expirationenddate['second'],
          $expirationenddate["year"],
          $expirationenddate["month"],
          $expirationenddate["day"]);
        if ($stopdate) {
          if ($searchExpirationDate) {
            $searchExpirationDate .= " AND ";
          }
          $searchExpirationDate .= "`tblDocuments`.`expires` <= " . $stopdate;
        }
      }

      // ---------------------- Suche starten ----------------------------------

      //
      // Construct the SQL query that will be used to search the database.
      //

      if (!$this->db->createTemporaryTable("ttcontentid") || !$this->db->createTemporaryTable("ttstatid")) {
        return false;
      }

      $searchQuery = "FROM `tblDocumentContent` " .
        "LEFT JOIN `tblDocuments` ON `tblDocuments`.`id` = `tblDocumentContent`.`document` " .
        "LEFT JOIN `tblDocumentAttributes` ON `tblDocuments`.`id` = `tblDocumentAttributes`.`document` " .
        "LEFT JOIN `tblDocumentContentAttributes` ON `tblDocumentContent`.`id` = `tblDocumentContentAttributes`.`content` " .
        "LEFT JOIN `tblDocumentStatus` ON `tblDocumentStatus`.`documentID` = `tblDocumentContent`.`document` " .
        "LEFT JOIN `tblDocumentStatusLog` ON `tblDocumentStatusLog`.`statusID` = `tblDocumentStatus`.`statusID` " .
        "LEFT JOIN `ttstatid` ON `ttstatid`.`maxLogID` = `tblDocumentStatusLog`.`statusLogID` " .
        "LEFT JOIN `ttcontentid` ON `ttcontentid`.`maxVersion` = `tblDocumentStatus`.`version` AND `ttcontentid`.`document` = `tblDocumentStatus`.`documentID` " .
        "LEFT JOIN `tblDocumentLocks` ON `tblDocuments`.`id`=`tblDocumentLocks`.`document` " .
        "LEFT JOIN `tblDocumentCategory` ON `tblDocuments`.`id`=`tblDocumentCategory`.`documentID` " .
        "WHERE `ttstatid`.`maxLogID`=`tblDocumentStatusLog`.`statusLogID` " .
        "AND `ttcontentid`.`maxVersion` = `tblDocumentContent`.`version`";

      if (strlen($searchKey) > 0) {
        $searchQuery .= " AND (" . $searchKey . ")";
      }
      if (strlen($searchFolder) > 0) {
        $searchQuery .= " AND " . $searchFolder;
      }
      if (strlen($searchOwner) > 0) {
        $searchQuery .= " AND (" . $searchOwner . ")";
      }
      if (strlen($searchCategories) > 0) {
        $searchQuery .= " AND (" . $searchCategories . ")";
      }
      if (strlen($searchCreateDate) > 0) {
        $searchQuery .= " AND (" . $searchCreateDate . ")";
      }
      if (strlen($searchExpirationDate) > 0) {
        $searchQuery .= " AND (" . $searchExpirationDate . ")";
      }
      if ($searchAttributes) {
        $searchQuery .= " AND (" . implode(" AND ", $searchAttributes) . ")";
      }

      // status
      if ($status) {
        $searchQuery .= " AND `tblDocumentStatusLog`.`status` IN (" . implode(',', $status) . ")";
      }

      if ($searchKey || $searchOwner || $searchCategories || $searchCreateDate || $searchExpirationDate || $searchAttributes || $status) {
        // Count the number of rows that the search will produce.
        $resArr = $this->db->getResultArray("SELECT COUNT(*) AS num FROM (SELECT DISTINCT `tblDocuments`.id " . $searchQuery . ") a");
        if (is_numeric($resArr[0]["num"]) && $resArr[0]["num"] > 0) {
          $totalDocs = (integer) $resArr[0]["num"];
        }

        // If there are no results from the count query, then there is no real need
        // to run the full query. TODO: re-structure code to by-pass additional
        // queries when no initial results are found.

        // Prepare the complete search query, including the LIMIT clause.
        $searchQuery = "SELECT DISTINCT `tblDocuments`.*, " .
          "`tblDocumentContent`.`version`, " .
          "`tblDocumentStatusLog`.`status`, `tblDocumentLocks`.`userID` as `lockUser` " . $searchQuery;

        // calculate the remaining entrїes of the current page
        // If page is not full yet, get remaining entries
        if ($limit) {
          $remain = $limit - count($folderresult['folders']);
          if ($remain) {
            if ($remain == $limit) {
              $offset -= $totalFolders;
            }
            else {
              $offset = 0;
            }
            if ($limit) {
              $searchQuery .= " LIMIT " . $offset . "," . $remain;
            }

            // Send the complete search query to the database.
            $resArr = $this->db->getResultArray($searchQuery);
          }
          else {
            $resArr = array();
          }
        }
        else {
          // Send the complete search query to the database.
          $resArr = $this->db->getResultArray($searchQuery);
        }

        // ------------------- Ausgabe der Ergebnisse ----------------------------
        $numResults = count($resArr);
        if ($numResults == 0) {
          $docresult = array('totalDocs' => $totalDocs, 'docs' => array());
        }
        else {
          foreach ($resArr as $docArr) {
            $docs[] = $this->getDocument($docArr['id']);
          }
          $docresult = array('totalDocs' => $totalDocs, 'docs' => $docs);
        }
      }
      else {
        $docresult = array('totalDocs' => 0, 'docs' => array());
      }
    }
    else {
      $docresult = array('totalDocs' => 0, 'docs' => array());
    }

    if ($limit) {
      $totalPages = (integer) (($totalDocs + $totalFolders) / $limit);
      if ((($totalDocs + $totalFolders) % $limit) > 0) {
        $totalPages++;
      }
    }
    else {
      $totalPages = 1;
    }

    return array_merge($docresult, $folderresult, array('totalPages' => $totalPages));
  }

  /**
   * Return a folder by its id
   *
   * This function retrieves a folder from the database by its id.
   *
   * @param integer $id internal id of folder
   *
   * @return Folder instance of BakeryDMS\Core\Folder or false
   */
  function getFolder($id) {
    if (!is_numeric($id)) {
      return false;
    }

    $queryStr = "SELECT * FROM tblFolders WHERE id = " . (int) $id;
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    else if (count($resArr) != 1) {
      return false;
    }

    $resArr = $resArr[0];
    $folder = new Folder($resArr["id"],
      $resArr["name"],
      $resArr["parent"],
      $resArr["comment"],
      $resArr["date"],
      $resArr["owner"],
      $resArr["inheritAccess"],
      $resArr["defaultAccess"],
      $resArr["sequence"]);
    $folder->setDMS($this);
    return $folder;
  }

  /**
   * Return a folder by its name
   *
   * This function retrieves a folder from the database by its name. The
   * search covers the whole database. If
   * the parameter $folder is not null, it will search for the name
   * only within this parent folder. It will not be done recursively.
   *
   * @param string $name name of the folder
   * @param object $folder parent folder
   *
   * @return object/boolean found folder or false
   */
  function getFolderByName($name, $folder = null) {
    if (!$name) {
      return false;
    }

    $queryStr = "SELECT * FROM tblFolders WHERE name = " . $this->db->qstr($name);
    if ($folder) {
      $queryStr .= " AND `parent` = " . $folder->getID();
    }
    $queryStr .= " LIMIT 1";
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    if (!$resArr) {
      return false;
    }

    $resArr = $resArr[0];
    $folder = new Folder($resArr["id"],
      $resArr["name"],
      $resArr["parent"],
      $resArr["comment"],
      $resArr["date"],
      $resArr["owner"],
      $resArr["inheritAccess"],
      $resArr["defaultAccess"],
      $resArr["sequence"]);
    $folder->setDMS($this);
    return $folder;
  }

  /**
   * Returns a list of folders and error message not linked in the tree
   *
   * This function checks all folders in the database.
   *
   * @return array list of errors
   */
  function checkFolders() {
    $queryStr = "SELECT * FROM tblFolders";
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr === false) {
      return false;
    }

    $cache = array();
    foreach ($resArr as $rec) {
      $cache[$rec['id']] = array('name' => $rec['name'],
        'parent' => $rec['parent'],
        'folderList' => $rec['folderList']);
    }
    $errors = array();
    foreach ($cache as $id => $rec) {
      if (!array_key_exists($rec['parent'], $cache) && $rec['parent'] != 0) {
        $errors[$id] = array('id' => $id,
          'name' => $rec['name'],
          'parent' => $rec['parent'],
          'msg' => 'Missing parent');
      }
      else {
        $tmparr = explode(':', $rec['folderList']);
        array_shift($tmparr);
        if (count($tmparr) != count(array_unique($tmparr))) {
          $errors[$id] = array('id' => $id,
            'name' => $rec['name'],
            'parent' => $rec['parent'],
            'msg' => 'Duplicate entry in folder list (' . $rec['folderList'] . ')');
        }
      }
    }

    return $errors;
  }

  /**
   * Returns a list of documents and error message not linked in the tree
   *
   * This function checks all documents in the database.
   *
   * @return array list of errors
   */
  function checkDocuments() {
    $queryStr = "SELECT * FROM tblFolders";
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr === false) {
      return false;
    }

    $fcache = array();
    foreach ($resArr as $rec) {
      $fcache[$rec['id']] = array('name' => $rec['name'],
        'parent' => $rec['parent'],
        'folderList' => $rec['folderList']);
    }

    $queryStr = "SELECT * FROM tblDocuments";
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr === false) {
      return false;
    }

    $dcache = array();
    foreach ($resArr as $rec) {
      $dcache[$rec['id']] = array('name' => $rec['name'],
        'parent' => $rec['folder'],
        'folderList' => $rec['folderList']);
    }
    $errors = array();
    foreach ($dcache as $id => $rec) {
      if (!array_key_exists($rec['parent'], $fcache) && $rec['parent'] != 0) {
        $errors[$id] = array('id' => $id,
          'name' => $rec['name'],
          'parent' => $rec['parent'],
          'msg' => 'Missing parent');
      }
      else {
        $tmparr = explode(':', $rec['folderList']);
        array_shift($tmparr);
        if (count($tmparr) != count(array_unique($tmparr))) {
          $errors[$id] = array('id' => $id,
            'name' => $rec['name'],
            'parent' => $rec['parent'],
            'msg' => 'Duplicate entry in folder list (' . $rec['folderList'] . '');
        }
      }
    }

    return $errors;
  }

  /**
   * Return a user by its id
   *
   * This function retrieves a user from the database by its id.
   *
   * @param integer $id internal id of user
   *
   * @return User instance of {@link BakeryDMS\Core\User} or false
   */
  function getUser($id) {
    if (!is_numeric($id)) {
      return false;
    }

    $queryStr = "SELECT * FROM tblUsers WHERE id = " . (int) $id;
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) != 1) {
      return false;
    }

    $resArr = $resArr[0];
    $user = new User($resArr["id"],
      $resArr["login"],
      $resArr["pwd"],
      $resArr["fullName"],
      $resArr["email"],
      $resArr["language"],
      $resArr["theme"],
      $resArr["comment"],
      $resArr["role"],
      $resArr["hidden"],
      $resArr["disabled"],
      $resArr["pwdExpiration"],
      $resArr["loginfailures"],
      $resArr["quota"]);
    $user->setDMS($this);
    return $user;
  }

  /**
   * Return a user by its login
   *
   * This function retrieves a user from the database by its login.
   * If the second optional parameter $email is not empty, the user must
   * also have the given email.
   *
   * @param string $login internal login of user
   * @param string $email email of user
   *
   * @return object instance of {@link BakeryDMS\Core\User} or false
   */
  function getUserByLogin($login, $email = '') {
    $queryStr = "SELECT * FROM tblUsers WHERE login = " . $this->db->qstr($login);
    if ($email) {
      $queryStr .= " AND email=" . $this->db->qstr($email);
    }
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) != 1) {
      return false;
    }

    $resArr = $resArr[0];

    $user = new User($resArr["id"],
      $resArr["login"],
      $resArr["pwd"],
      $resArr["fullName"],
      $resArr["email"],
      $resArr["language"],
      $resArr["theme"],
      $resArr["comment"],
      $resArr["role"],
      $resArr["hidden"],
      $resArr["disabled"],
      $resArr["pwdExpiration"],
      $resArr["loginfailures"],
      $resArr["quota"]);
    $user->setDMS($this);
    return $user;
  }

  /**
   * Return a user by its email
   *
   * This function retrieves a user from the database by its email.
   * It is needed when the user requests a new password.
   *
   * @param integer $email email address of user
   *
   * @return object instance of {@link BakeryDMS\Core\User} or false
   */
  function getUserByEmail($email) {
    $queryStr = "SELECT * FROM tblUsers WHERE email = " . $this->db->qstr($email);
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) != 1) {
      return false;
    }

    $resArr = $resArr[0];

    $user = new User($resArr["id"],
      $resArr["login"],
      $resArr["pwd"],
      $resArr["fullName"],
      $resArr["email"],
      $resArr["language"],
      $resArr["theme"],
      $resArr["comment"],
      $resArr["role"],
      $resArr["hidden"],
      $resArr["disabled"],
      $resArr["pwdExpiration"],
      $resArr["loginfailures"],
      $resArr["quota"]);
    $user->setDMS($this);
    return $user;
  }

  /**
   * Return list of all users
   *
   * @return User[]|bool
   */
  function getAllUsers($orderby = '') {
    if ($orderby == 'fullname') {
      $queryStr = "SELECT * FROM tblUsers ORDER BY fullname";
    }
    else {
      $queryStr = "SELECT * FROM tblUsers ORDER BY login";
    }
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    $users = array();

    for ($i = 0; $i < count($resArr); $i++) {
      $user = new User($resArr[$i]["id"],
        $resArr[$i]["login"],
        $resArr[$i]["pwd"],
        $resArr[$i]["fullName"],
        $resArr[$i]["email"],
        (isset($resArr["language"]) ? $resArr["language"] : NULL),
        (isset($resArr["theme"]) ? $resArr["theme"] : NULL),
        $resArr[$i]["comment"],
        $resArr[$i]["role"],
        $resArr[$i]["hidden"],
        $resArr[$i]["disabled"],
        $resArr[$i]["pwdExpiration"],
        $resArr[$i]["loginfailures"],
        $resArr[$i]["quota"]);
      $user->setDMS($this);
      $users[$i] = $user;
    }

    return $users;
  }

  /**
   * Add a new user
   *
   * @param string $login login name
   * @param string $pwd password of new user
   * @param string $email Email of new user
   * @param string $language language of new user
   * @param string $comment comment of new user
   * @param integer $role role of new user (can be 0=normal, 1=admin, 2=guest)
   * @param integer $isHidden hide user in all lists, if this is set login
   *        is still allowed
   * @param integer $isDisabled disable user and prevent login
   *
   * @return object of {@link BakeryDMS\Core\User}
   */
  function addUser($login,
                   $pwd,
                   $fullName,
                   $email,
                   $language,
                   $theme,
                   $comment,
                   $role = '0',
                   $isHidden = 0,
                   $isDisabled = 0,
                   $pwdexpiration = '') {
    $db = $this->db;
    if (is_object($this->getUserByLogin($login))) {
      return false;
    }
    if ($role == '') {
      $role = '0';
    }
    if (trim($pwdexpiration) == '') {
      $pwdexpiration = '0000-00-00 00:00:00';
    }
    $queryStr = "INSERT INTO tblUsers (login, pwd, fullName, email, language, theme, comment, role, hidden, disabled, pwdExpiration) VALUES (" . $db->qstr($login) . ", " . $db->qstr($pwd) . ", " . $db->qstr($fullName) . ", " . $db->qstr($email) . ", '" . $language . "', '" . $theme . "', " . $db->qstr($comment) . ", '" . intval($role) . "', '" . intval($isHidden) . "', '" . intval($isDisabled) . "', " . $db->qstr($pwdexpiration) . ")";
    $res = $this->db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    return $this->getUser($this->db->getInsertID());
  }

  /**
   * Get a group by its id
   *
   * @param integer $id id of group
   *
   * @return Group|boolean group or false if no group was found
   */
  function getGroup($id) {
    if (!is_numeric($id)) {
      return false;
    }

    $queryStr = "SELECT * FROM tblGroups WHERE id = " . (int) $id;
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    else if (count($resArr) != 1) //wenn, dann wohl eher 0 als > 1 ;-)
    {
      return false;
    }

    $resArr = $resArr[0];

    $group = new Group($resArr["id"], $resArr["name"], $resArr["comment"]);
    $group->setDMS($this);
    return $group;
  }

  /**
   * Get a group by its name
   *
   * @param string $name name of group
   * @param bool $caseInsensitive <c>true</c> for case insensitive search of name, <c>false</c> otherwise
   *
   * @return Group|bool group if found, <c>false</c> otherwise
   */
  function getGroupByName($name, $caseInsensitive = true) {
    $queryStr = $caseInsensitive
      ? "SELECT `tblGroups`.* FROM `tblGroups` WHERE `tblGroups`.`name` = " . $this->db->qstr($name)
      : "SELECT `tblGroups`.* FROM `tblGroups` WHERE `tblGroups`.`name` LIKE " . $this->db->qstr($name);
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    else if (count($resArr) != 1) //wenn, dann wohl eher 0 als > 1 ;-)
    {
      return false;
    }

    $resArr = $resArr[0];

    $group = new Group($resArr["id"], $resArr["name"], $resArr["comment"]);
    $group->setDMS($this);
    return $group;
  }

  /**
   * Get a list of all groups.
   *
   * @return Group[] array of groups.
   */
  function getAllGroups() {
    $queryStr = "SELECT * FROM tblGroups ORDER BY name";
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    $groups = array();

    for ($i = 0; $i < count($resArr); $i++) {

      $group = new Group($resArr[$i]["id"], $resArr[$i]["name"], $resArr[$i]["comment"]);
      $group->setDMS($this);
      $groups[$i] = $group;
    }

    return $groups;
  }

  /**
   * Create a new user group
   *
   * @param string $name name of group
   * @param string $comment comment of group
   *
   * @return Group/boolean instance of a group or false in case of an error.
   */
  function addGroup($name, $comment) {
    if (is_object($this->getGroupByName($name))) {
      return false;
    }

    $queryStr = "INSERT INTO tblGroups (name, comment) VALUES (" . $this->db->qstr($name) . ", " . $this->db->qstr($comment) . ")";
    if (!$this->db->getResult($queryStr)) {
      return false;
    }

    return $this->getGroup($this->db->getInsertID());
  }

  function getKeywordCategory($id) {
    if (!is_numeric($id)) {
      return false;
    }

    $queryStr = "SELECT * FROM tblKeywordCategories WHERE id = " . (int) $id;
    $resArr = $this->db->getResultArray($queryStr);
    if ((is_bool($resArr) && !$resArr) || (count($resArr) != 1)) {
      return false;
    }

    $resArr = $resArr[0];
    $cat = new KeywordCategory($resArr["id"], $resArr["owner"], $resArr["name"]);
    $cat->setDMS($this);
    return $cat;
  }

  function getKeywordCategoryByName($name, $userID) {
    $queryStr = "SELECT * FROM tblKeywordCategories WHERE name = " . $this->db->qstr($name) . " AND owner = " . (int) $userID;
    $resArr = $this->db->getResultArray($queryStr);
    if ((is_bool($resArr) && !$resArr) || (count($resArr) != 1)) {
      return false;
    }

    $resArr = $resArr[0];
    $cat = new KeywordCategory($resArr["id"], $resArr["owner"], $resArr["name"]);
    $cat->setDMS($this);
    return $cat;
  }

  function getAllKeywordCategories($userIDs = array()) {
    $queryStr = "SELECT * FROM tblKeywordCategories";
    if ($userIDs) {
      $queryStr .= " WHERE owner in (" . implode(',', $userIDs) . ")";
    }

    $resArr = $this->db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    $categories = array();
    foreach ($resArr as $row) {
      $cat = new KeywordCategory($row["id"], $row["owner"], $row["name"]);
      $cat->setDMS($this);
      array_push($categories, $cat);
    }

    return $categories;
  }

  /**
   * This function should be replaced by getAllKeywordCategories()
   */
  function getAllUserKeywordCategories($userID) {
    $queryStr = "SELECT * FROM tblKeywordCategories";
    if ($userID != -1) {
      $queryStr .= " WHERE owner = " . (int) $userID;
    }

    $resArr = $this->db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    $categories = array();
    foreach ($resArr as $row) {
      $cat = new KeywordCategory($row["id"], $row["owner"], $row["name"]);
      $cat->setDMS($this);
      array_push($categories, $cat);
    }

    return $categories;
  }

  function addKeywordCategory($userID, $name) {
    if (is_object($this->getKeywordCategoryByName($name, $userID))) {
      return false;
    }
    $queryStr = "INSERT INTO tblKeywordCategories (owner, name) VALUES (" . (int) $userID . ", " . $this->db->qstr($name) . ")";
    if (!$this->db->getResult($queryStr)) {
      return false;
    }

    return $this->getKeywordCategory($this->db->getInsertID());
  }

  function getDocumentCategory($id) {
    if (!is_numeric($id)) {
      return false;
    }

    $queryStr = "SELECT * FROM tblCategory WHERE id = " . (int) $id;
    $resArr = $this->db->getResultArray($queryStr);
    if ((is_bool($resArr) && !$resArr) || (count($resArr) != 1)) {
      return false;
    }

    $resArr = $resArr[0];
    $cat = new DocumentCategory($resArr["id"], $resArr["name"]);
    $cat->setDMS($this);
    return $cat;
  }

  function getDocumentCategories() {
    $queryStr = "SELECT * FROM tblCategory";

    $resArr = $this->db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    $categories = array();
    foreach ($resArr as $row) {
      $cat = new DocumentCategory($row["id"], $row["name"]);
      $cat->setDMS($this);
      array_push($categories, $cat);
    }

    return $categories;
  }

  /**
   * Get a category by its name
   *
   * The name of a category is by default unique.
   *
   * @param string $name human readable name of category
   *
   * @return object instance of {@link BakeryDMS\Core\DocumentCategory}
   */
  function getDocumentCategoryByName($name) {
    if (!$name) {
      return false;
    }

    $queryStr = "SELECT * FROM tblCategory WHERE name=" . $this->db->qstr($name);
    $resArr = $this->db->getResultArray($queryStr);
    if (!$resArr) {
      return false;
    }

    $row = $resArr[0];
    $cat = new DocumentCategory($row["id"], $row["name"]);
    $cat->setDMS($this);

    return $cat;
  }

  function addDocumentCategory($name) {
    if (is_object($this->getDocumentCategoryByName($name))) {
      return false;
    }
    $queryStr = "INSERT INTO tblCategory (name) VALUES (" . $this->db->qstr($name) . ")";
    if (!$this->db->getResult($queryStr)) {
      return false;
    }

    return $this->getDocumentCategory($this->db->getInsertID());
  }

  /**
   * Get all notifications for a group
   *
   * @param object $group group for which notifications are to be retrieved
   * @param integer $type type of item (BakeryDMS\Core\TargetType::DOCUMENT or BakeryDMS\Core\TargetType::FOLDER)
   *
   * @return array array of notifications
   */
  function getNotificationsByGroup($group, $type = 0) {
    $queryStr = "SELECT `tblNotify`.* FROM `tblNotify` " .
      "WHERE `tblNotify`.`groupID` = " . $group->getID();
    if ($type) {
      $queryStr .= " AND `tblNotify`.`targetType` = " . (int) $type;
    }

    $resArr = $this->db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    $notifications = array();
    foreach ($resArr as $row) {
      $not = new Notification($row["target"], $row["targetType"], $row["userID"], $row["groupID"]);
      $not->setDMS($this);
      array_push($notifications, $cat);
    }

    return $notifications;
  }

  /**
   * Get all notifications for a user
   *
   * @param object $user user for which notifications are to be retrieved
   * @param integer $type type of item (BakeryDMS\Core\TargetType::DOCUMENT or BakeryDMS\Core\TargetType::FOLDER)
   *
   * @return array array of notifications
   */
  function getNotificationsByUser($user, $type = 0) {
    $queryStr = "SELECT `tblNotify`.* FROM `tblNotify` " .
      "WHERE `tblNotify`.`userID` = " . $user->getID();
    if ($type) {
      $queryStr .= " AND `tblNotify`.`targetType` = " . (int) $type;
    }

    $resArr = $this->db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    $notifications = array();
    foreach ($resArr as $row) {
      $not = new Notification($row["target"], $row["targetType"], $row["userID"], $row["groupID"]);
      $not->setDMS($this);
      array_push($notifications, $cat);
    }

    return $notifications;
  }

  /**
   * Create a token to request a new password.
   * This function will not delete the password but just creates an entry
   * in tblUserRequestPassword indicating a password request.
   *
   * @return string hash value of false in case of an error
   */
  function createPasswordRequest($user) {
    $hash = md5(uniqid(time()));
    $queryStr = "INSERT INTO tblUserPasswordRequest (userID, hash, `date`) VALUES (" . $user->getId() . ", " . $this->db->qstr($hash) . ", CURRENT_TIMESTAMP)";
    $resArr = $this->db->getResult($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }
    return $hash;

  }

  /**
   * Check if hash for a password request is valid.
   * This function searches a previously create password request and
   * returns the user.
   *
   * @param string $hash
   */
  function checkPasswordRequest($hash) {
    /* Get the password request from the database */
    $queryStr = "SELECT * FROM tblUserPasswordRequest WHERE hash=" . $this->db->qstr($hash);
    $resArr = $this->db->getResultArray($queryStr);
    if (is_bool($resArr) && !$resArr) {
      return false;
    }

    if (count($resArr) != 1) {
      return false;
    }
    $resArr = $resArr[0];

    return $this->getUser($resArr['userID']);

  }

  /**
   * Delete a password request
   *
   * @param string $hash
   */
  function deletePasswordRequest($hash) {
    /* Delete the request, so nobody can use it a second time */
    $queryStr = "DELETE FROM tblUserPasswordRequest WHERE hash=" . $this->db->qstr($hash);
    if (!$this->db->getResult($queryStr)) {
      return false;
    }
    return true;
  }

  /**
   * Return a attribute definition by its id
   *
   * This function retrieves a attribute definitionr from the database by
   * its id.
   *
   * @param integer $id internal id of attribute defintion
   *
   * @return object instance of {@link SeedDMS_Core_AttributeDefinition} or false
   */
  function getAttributeDefinition($id) {
    if (!is_numeric($id)) {
      return false;
    }

    $queryStr = "SELECT * FROM tblAttributeDefinitions WHERE id = " . (int) $id;
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) != 1) {
      return false;
    }

    $resArr = $resArr[0];

    $attrdef = new \BakeryDMS\Core\AttributeDefinition($resArr["id"],
      $resArr["name"],
      $resArr["objtype"],
      $resArr["type"],
      $resArr["multiple"],
      $resArr["minvalues"],
      $resArr["maxvalues"],
      $resArr["valueset"],
      $resArr["regex"]);
    $attrdef->setDMS($this);
    return $attrdef;
  }

  /**
   * Return a attribute definition by its name
   *
   * This function retrieves an attribute def. from the database by its name.
   *
   * @param string $name internal name of attribute def.
   *
   * @return object instance of {@link SeedDMS_Core_AttributeDefinition} or false
   */
  function getAttributeDefinitionByName($name) {
    if (!$name) {
      return false;
    }

    $queryStr = "SELECT * FROM tblAttributeDefinitions WHERE name = " . $this->db->qstr($name);
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) != 1) {
      return false;
    }

    $resArr = $resArr[0];

    $attrdef = new \BakeryDMS\Core\AttributeDefinition($resArr["id"],
      $resArr["name"],
      $resArr["objtype"],
      $resArr["type"],
      $resArr["multiple"],
      $resArr["minvalues"],
      $resArr["maxvalues"],
      $resArr["valueset"],
      $resArr["regex"]);
    $attrdef->setDMS($this);
    return $attrdef;
  }

  /**
   * Return list of all attributes definitions
   *
   * @param integer $objtype select those attributes defined for an object type
   *
   * @return array of instances of {@link SeedDMS_Core_AttributeDefinition} or false
   */
  function getAllAttributeDefinitions($objtype = 0) {
    $queryStr = "SELECT * FROM tblAttributeDefinitions";
    if ($objtype) {
      if (is_array($objtype)) {
        $queryStr .= ' WHERE objtype in (\'' . implode("','", $objtype) . '\')';
      }
      else {
        $queryStr .= ' WHERE objtype=' . intval($objtype);
      }
    }
    $queryStr .= ' ORDER BY name';
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    $attrdefs = array();

    for ($i = 0; $i < count($resArr); $i++) {
      $attrdef = new \BakeryDMS\Core\AttributeDefinition($resArr[$i]["id"],
        $resArr[$i]["name"],
        $resArr[$i]["objtype"],
        $resArr[$i]["type"],
        $resArr[$i]["multiple"],
        $resArr[$i]["minvalues"],
        $resArr[$i]["maxvalues"],
        $resArr[$i]["valueset"],
        $resArr[$i]["regex"]);
      $attrdef->setDMS($this);
      $attrdefs[$i] = $attrdef;
    }

    return $attrdefs;
  }

  /**
   * Add a new attribute definition
   *
   * @param string $name name of attribute
   * @param string $type type of attribute
   * @param boolean $multiple set to 1 if attribute has multiple attributes
   * @param integer $minvalues minimum number of values
   * @param integer $maxvalues maximum number of values if multiple is set
   * @param string $valueset list of allowed values (csv format)
   *
   * @return object of {@link BakeryDMS\Core\User}
   */
  function addAttributeDefinition($name,
                                  $objtype,
                                  $type,
                                  $multiple = 0,
                                  $minvalues = 0,
                                  $maxvalues = 1,
                                  $valueset = '',
                                  $regex = '') {
    if (is_object($this->getAttributeDefinitionByName($name))) {
      return false;
    }
    if (!$type) {
      return false;
    }
    $queryStr = "INSERT INTO tblAttributeDefinitions (name, objtype, type, multiple, minvalues, maxvalues, valueset, regex) VALUES (" . $this->db->qstr($name) . ", " . intval($objtype) . ", " . intval($type) . ", " . intval($multiple) . ", " . intval($minvalues) . ", " . intval($maxvalues) . ", " . $this->db->qstr($valueset) . ", " . $this->db->qstr($regex) . ")";
    $res = $this->db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    return $this->getAttributeDefinition($this->db->getInsertID());
  }

  /**
   * Return list of all workflows
   *
   * @return \BakeryDMS\Core\Workflow\Workflow[]|boolean Workflows as array or false on failure.
   */
  function getAllWorkflows() {
    $queryStr = "SELECT * FROM tblWorkflows ORDER BY name";
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    $queryStr = "SELECT * FROM tblWorkflowStates ORDER BY name";
    $ressArr = $this->db->getResultArray($queryStr);

    if (is_bool($ressArr) && $ressArr == false) {
      return false;
    }

    for ($i = 0; $i < count($ressArr); $i++) {
      $wkfstates[$ressArr[$i]["id"]] = new State($ressArr[$i]["id"],
        $ressArr[$i]["name"],
        $ressArr[$i]["maxtime"],
        $ressArr[$i]["precondfunc"],
        $ressArr[$i]["documentstatus"]);
    }

    $workflows = array();
    for ($i = 0; $i < count($resArr); $i++) {
      $workflow = new Workflow($resArr[$i]["id"],
        $resArr[$i]["name"],
        $wkfstates[$resArr[$i]["initstate"]]);
      $workflow->setDMS($this);
      $workflows[$i] = $workflow;
    }

    return $workflows;
  }

  /**
   * Return workflow by its Id
   *
   * @return \BakeryDMS\Core\Workflow\Workflow|bool The workflow or false on failure.
   */
  function getWorkflow($id) {
    $queryStr = "SELECT * FROM tblWorkflows WHERE id=" . intval($id);
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    if (!$resArr) {
      return false;
    }

    $initstate = $this->getWorkflowState($resArr[0]['initstate']);

    $workflow = new Workflow($resArr[0]["id"], $resArr[0]["name"], $initstate);
    $workflow->setDMS($this);

    return $workflow;
  }

  /**
   * Return workflow by its name
   *
   * @return \BakeryDMS\Core\Workflow\Workflow|bool The workflow or false on failure.
   */
  function getWorkflowByName($name) {
    if (!$name) {
      return false;
    }

    $queryStr = "SELECT * FROM tblWorkflows WHERE name=" . $this->db->qstr($name);
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    if (!$resArr) {
      return false;
    }

    $initstate = $this->getWorkflowState($resArr[0]['initstate']);

    $workflow = new Workflow($resArr[0]["id"], $resArr[0]["name"], $initstate);
    $workflow->setDMS($this);

    return $workflow;
  }

  function addWorkflow($name, $initstate) {
    $db = $this->db;
    if (is_object($this->getWorkflowByName($name))) {
      return false;
    }
    $queryStr = "INSERT INTO tblWorkflows (name, initstate) VALUES (" . $db->qstr($name) . ", " . $initstate->getID() . ")";
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    return $this->getWorkflow($db->getInsertID());
  }

  /**
   * Return a workflow state by its id
   *
   * This function retrieves a workflow state from the database by its id.
   *
   * @param integer $id internal id of workflow state
   *
   * @return \BakeryDMS\Core\Workflow\State|bool The state or false on failure:
   */
  function getWorkflowState($id) {
    if (!is_numeric($id)) {
      return false;
    }

    $queryStr = "SELECT * FROM tblWorkflowStates WHERE id = " . (int) $id;
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) != 1) {
      return false;
    }

    $resArr = $resArr[0];

    $state = new State($resArr["id"],
      $resArr["name"],
      $resArr["maxtime"],
      $resArr["precondfunc"],
      $resArr["documentstatus"]);
    $state->setDMS($this);
    return $state;
  }

  /**
   * Return workflow state by its name
   *
   * @param string $name name of workflow state
   *
   * @return \BakeryDMS\Core\Workflow\State|bool The state or false on failure.
   */
  function getWorkflowStateByName($name) {
    if (!$name) {
      return false;
    }

    $queryStr = "SELECT * FROM tblWorkflowStates WHERE name=" . $this->db->qstr($name);
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    if (!$resArr) {
      return false;
    }

    $resArr = $resArr[0];

    $state = new State($resArr["id"],
      $resArr["name"],
      $resArr["maxtime"],
      $resArr["precondfunc"],
      $resArr["documentstatus"]);
    $state->setDMS($this);

    return $state;
  }

  /**
   * Return list of all workflow states
   *
   * @return \BakeryDMS\Core\Workflow\State|bool The state or false on failure.
   */
  function getAllWorkflowStates() {
    $queryStr = "SELECT * FROM tblWorkflowStates ORDER BY name";
    $ressArr = $this->db->getResultArray($queryStr);

    if (is_bool($ressArr) && $ressArr == false) {
      return false;
    }

    $wkfstates = array();
    for ($i = 0; $i < count($ressArr); $i++) {
      $wkfstate = new State($ressArr[$i]["id"],
        $ressArr[$i]["name"],
        $ressArr[$i]["maxtime"],
        $ressArr[$i]["precondfunc"],
        $ressArr[$i]["documentstatus"]);
      $wkfstate->setDMS($this);
      $wkfstates[$i] = $wkfstate;
    }

    return $wkfstates;
  }

  /**
   * Add new workflow state
   *
   * @param string $name name of workflow state
   * @param integer $docstatus document status when this state is reached
   *
   * @return object instance of new workflow state
   */
  function addWorkflowState($name, $docstatus) {
    $db = $this->db;
    if (is_object($this->getWorkflowStateByName($name))) {
      return false;
    }
    $queryStr = "INSERT INTO tblWorkflowStates (name, documentstatus) VALUES (" . $db->qstr($name) . ", " . (int) $docstatus . ")";
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    return $this->getWorkflowState($db->getInsertID());
  }

  /**
   * Return a workflow action by its id
   *
   * This function retrieves a workflow action from the database by its id.
   *
   * @param integer $id internal id of workflow action
   *
   * @return Action|bool Workflow action or false on failure.
   */
  function getWorkflowAction($id) {
    if (!is_numeric($id)) {
      return false;
    }

    $queryStr = "SELECT * FROM tblWorkflowActions WHERE id = " . (int) $id;
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) != 1) {
      return false;
    }

    $resArr = $resArr[0];

    $action = new Action($resArr["id"], $resArr["name"]);
    $action->setDMS($this);
    return $action;
  }

  /**
   * Return a workflow action by its name
   *
   * This function retrieves a workflow action from the database by its name.
   *
   * @param string $name name of workflow action
   *
   * @return Action|bool Workflow action or false on failure.
   */
  function getWorkflowActionByName($name) {
    if (!$name) {
      return false;
    }

    $queryStr = "SELECT * FROM tblWorkflowActions WHERE name = " . $this->db->qstr($name);
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) != 1) {
      return false;
    }

    $resArr = $resArr[0];

    $action = new Action($resArr["id"], $resArr["name"]);
    $action->setDMS($this);
    return $action;
  }

  /**
   * Return list of workflow action
   *
   * @return \BakeryDMS\Core\Workflow\Action[]|bool Workflow actions or false on failure.
   */
  function getAllWorkflowActions() {
    $queryStr = "SELECT * FROM tblWorkflowActions";
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }

    $wkfactions = array();
    for ($i = 0; $i < count($resArr); $i++) {
      $action = new Action($resArr[$i]["id"], $resArr[$i]["name"]);
      $action->setDMS($this);
      $wkfactions[$i] = $action;
    }

    return $wkfactions;
  }

  /**
   * Add new workflow action
   *
   * @param string $name name of workflow action
   *
   * @return object instance new workflow action
   */
  function addWorkflowAction($name) {
    $db = $this->db;
    if (is_object($this->getWorkflowActionByName($name))) {
      return false;
    }
    $queryStr = "INSERT INTO tblWorkflowActions (name) VALUES (" . $db->qstr($name) . ")";
    $res = $db->getResult($queryStr);
    if (!$res) {
      return false;
    }

    return $this->getWorkflowAction($db->getInsertID());
  }

  /**
   * Return a workflow transition by its id
   *
   * This function retrieves a workflow transition from the database by its id.
   *
   * @param integer $id internal id of workflow transition
   *
   * @return \BakeryDMS\Core\Workflow\Transition|bool The transition or false on failure.
   */
  function getWorkflowTransition($id) {
    if (!is_numeric($id)) {
      return false;
    }

    $queryStr = "SELECT * FROM tblWorkflowTransitions WHERE id = " . (int) $id;
    $resArr = $this->db->getResultArray($queryStr);

    if (is_bool($resArr) && $resArr == false) {
      return false;
    }
    if (count($resArr) != 1) {
      return false;
    }

    $resArr = $resArr[0];

    $transition = new Transition($resArr["id"],
      $this->getWorkflow($resArr["workflow"]),
      $this->getWorkflowState($resArr["state"]),
      $this->getWorkflowAction($resArr["action"]),
      $this->getWorkflowState($resArr["nextstate"]),
      $resArr["maxtime"]);
    $transition->setDMS($this);
    return $transition;
  }

  /**
   * Returns document content which is not linked to a document
   *
   * This method is for finding straying document content without
   * a parent document. In normal operation this should not happen
   * but little checks for database consistency and possible errors
   * in the application may have left over document content though
   * the document is gone already.
   */
  function getUnlinkedDocumentContent() {
    $queryStr = "SELECT * FROM tblDocumentContent WHERE document NOT IN (SELECT id FROM tblDocuments)";
    $resArr = $this->db->getResultArray($queryStr);
    if (!$resArr) {
      return false;
    }

    $versions = array();
    foreach ($resArr as $row) {
      $document = new Document($row['document'], '', '', '', '', '', '', '', '', '', '', '');
      $document->setDMS($this);
      $version = new DocumentContent($row['id'],
        $document,
        $row['version'],
        $row['comment'],
        $row['date'],
        $row['createdBy'],
        $row['dir'],
        $row['orgFileName'],
        $row['fileType'],
        $row['mimeType'],
        $row['fileSize'],
        $row['checksum']);
      $versions[] = $version;
    }
    return $versions;

  }

  /**
   * Returns document content which has no file size set
   *
   * This method is for finding document content without a file size
   * set in the database. The file size of a document content was introduced
   * in version 4.0.0 of SeedDMS for implementation of user quotas.
   */
  function getNoFileSizeDocumentContent() {
    $queryStr = "SELECT * FROM tblDocumentContent WHERE fileSize = 0 OR fileSize IS NULL";
    $resArr = $this->db->getResultArray($queryStr);
    if (!$resArr) {
      return false;
    }

    $versions = array();
    foreach ($resArr as $row) {
      $document = new Document($row['document'], '', '', '', '', '', '', '', '', '', '', '');
      $document->setDMS($this);
      $version = new DocumentContent($row['id'],
        $document,
        $row['version'],
        $row['comment'],
        $row['date'],
        $row['createdBy'],
        $row['dir'],
        $row['orgFileName'],
        $row['fileType'],
        $row['mimeType'],
        $row['fileSize'],
        $row['checksum'],
        $row['fileSize'],
        $row['checksum']);
      $versions[] = $version;
    }
    return $versions;

  }

  /**
   * Returns document content which has no checksum set
   *
   * This method is for finding document content without a checksum
   * set in the database. The checksum of a document content was introduced
   * in version 4.0.0 of SeedDMS for finding duplicates.
   */
  function getNoChecksumDocumentContent() {
    $queryStr = "SELECT * FROM tblDocumentContent WHERE checksum = '' OR checksum IS NULL";
    $resArr = $this->db->getResultArray($queryStr);
    if (!$resArr) {
      return false;
    }

    $versions = array();
    foreach ($resArr as $row) {
      $document = new Document($row['document'], '', '', '', '', '', '', '', '', '', '', '');
      $document->setDMS($this);
      $version = new DocumentContent($row['id'],
        $document,
        $row['version'],
        $row['comment'],
        $row['date'],
        $row['createdBy'],
        $row['dir'],
        $row['orgFileName'],
        $row['fileType'],
        $row['mimeType'],
        $row['fileSize'],
        $row['checksum']);
      $versions[] = $version;
    }
    return $versions;

  }

  /**
   * Returns statistical information
   *
   * This method returns all kind of statistical information like
   * documents or used space per user, recent activity, etc.
   *
   * @param string $type type of statistic
   *
   * @return array|bool statistical information.
   */
  function getStatisticalData($type = '') {
    switch ($type) {
      case 'docsperuser':
        $queryStr = "SELECT b.fullname AS `key`, count(owner) AS total FROM tblDocuments a LEFT JOIN tblUsers b ON a.owner=b.id GROUP BY owner";
        $resArr = $this->db->getResultArray($queryStr);
        if (!$resArr) {
          return false;
        }

        return $resArr;
      case 'docspermimetype':
        $queryStr = "SELECT b.mimeType AS `key`, count(mimeType) AS total FROM tblDocuments a LEFT JOIN tblDocumentContent b ON a.id=b.document GROUP BY b.mimeType";
        $resArr = $this->db->getResultArray($queryStr);
        if (!$resArr) {
          return false;
        }

        return $resArr;
      case 'docspercategory':
        $queryStr = "SELECT b.name AS `key`, count(a.categoryID) AS total FROM tblDocumentCategory a LEFT JOIN tblCategory b ON a.categoryID=b.id GROUP BY a.categoryID";
        $resArr = $this->db->getResultArray($queryStr);
        if (!$resArr) {
          return false;
        }

        return $resArr;
      case 'docsperstatus':
        $queryStr = "SELECT b.status AS `key`, count(b.status) AS total FROM (SELECT a.id, max(b.version), max(c.statusLogId) AS maxlog FROM tblDocuments a LEFT JOIN tblDocumentStatus b ON a.id=b.documentid LEFT JOIN tblDocumentStatusLog c ON b.statusid=c.statusid GROUP BY a.id, b.version ORDER BY a.id, b.statusid) a LEFT JOIN tblDocumentStatusLog b ON a.maxlog=b.statusLogId GROUP BY b.status";
        $queryStr = "SELECT b.status AS `key`, count(b.status) AS total FROM (SELECT a.id, max(c.statusLogId) AS maxlog FROM tblDocuments a LEFT JOIN tblDocumentStatus b ON a.id=b.documentid LEFT JOIN tblDocumentStatusLog c ON b.statusid=c.statusid GROUP BY a.id  ORDER BY a.id, b.statusid) a LEFT JOIN tblDocumentStatusLog b ON a.maxlog=b.statusLogId GROUP BY b.status";
        $resArr = $this->db->getResultArray($queryStr);
        if (!$resArr) {
          return false;
        }

        return $resArr;
      case 'docspermonth':
        $queryStr = "SELECT *, count(`key`) AS total FROM (SELECT " . $this->db->getDateExtract("date",
            '%Y-%m') . " AS `key` FROM tblDocuments) a GROUP BY `key` ORDER BY `key`";
        $resArr = $this->db->getResultArray($queryStr);
        if (!$resArr) {
          return false;
        }

        return $resArr;
      case 'docsaccumulated':
        $queryStr = "SELECT *, count(`key`) AS total FROM (SELECT " . $this->db->getDateExtract("date") . " AS `key` FROM tblDocuments) a GROUP BY `key` ORDER BY `key`";
        $resArr = $this->db->getResultArray($queryStr);
        if (!$resArr) {
          return false;
        }

        $sum = 0;
        foreach ($resArr as &$res) {
          $sum += $res['total'];
          /* auxially variable $key is need because sqlite returns
					 * a key '`key`'
					 */
          $res['key'] = mktime(12,
              0,
              0,
              substr($res['key'], 5, 2),
              substr($res['key'], 8, 2),
              substr($res['key'], 0, 4)) * 1000;
          $res['total'] = $sum;
        }
        return $resArr;
      case 'sizeperuser':
        $queryStr = "SELECT c.fullname AS `key`, sum(fileSize) AS total FROM tblDocuments a LEFT JOIN tblDocumentContent b ON a.id=b.document LEFT JOIN tblUsers c ON a.owner=c.id GROUP BY a.owner";
        $resArr = $this->db->getResultArray($queryStr);
        if (!$resArr) {
          return false;
        }

        return $resArr;
      default:
        return array();
    }
  }

  /**
   * Set a callback function
   *
   * @param string $name internal name of callback
   * @param mixed $func function name as expected by {call_user_method}
   * @param mixed $params parameter passed as the first argument to the
   *        callback
   */
  function setCallback($name, $func, $params = null) {
    if ($name && $func) {
      $this->callbacks[$name] = array($func, $params);
    }
  }

}

?>
