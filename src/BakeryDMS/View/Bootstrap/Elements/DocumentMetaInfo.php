<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\View\Bootstrap\Elements;


use BakeryDMS\Core\AccessMode;
use BakeryDMS\Core\Attribute;
use BakeryDMS\Core\Folder;
use BakeryDMS\Core\User;

class DocumentMetaInfo {
  /**
   * @var User
   */
  private $user;
  /**
   * @var Folder
   */
  private $folder;
  /**
   * @var User
   */
  private $owner;

  /**
   * @param $user User
   * @param $folder Folder
   * @param $owner User
   */
  public function __construct($user, $folder, $owner) {
    $this->user = $user;
    $this->folder = $folder;
    $this->owner = $owner;
  }

  /**
   * @param $this ->user
   * @param $this ->folder
   * @param $this ->owner
   */
  public
  function display() {
    echo "<table class=\"table-condensed\">\n";
    if ($this->user->isAdmin()) {
      echo "<tr>";
      echo "<td>" . t("id") . ":</td>\n";
      echo "<td>" . htmlspecialchars($this->folder->getID()) . "</td>\n";
      echo "</tr>";
    }
    echo "<tr>";
    echo "<td>" . t("owner") . ":</td>\n";
    echo "<td><a href=\"mailto:" . htmlspecialchars($this->owner->getEmail()) . "\">" . htmlspecialchars($this->owner->getFullName()) . "</a></td>\n";
    echo "</tr>";
    if ($this->folder->getComment()) {
      echo "<tr>";
      echo "<td>" . t("comment") . ":</td>\n";
      echo "<td>" . htmlspecialchars($this->folder->getComment()) . "</td>\n";
      echo "</tr>";
    }

    if ($this->user->isAdmin()) {
      if ($this->folder->inheritsAccess()) {
        echo "<tr>";
        echo "<td>" . t("access_mode") . ":</td>\n";
        echo "<td>";
        echo t("inherited");
        echo "</tr>";
      }
      else {
        echo "<tr>";
        echo "<td>" . t('default_access') . ":</td>";
        echo "<td>" . AccessMode::getText($this->folder->getDefaultAccess()) . "</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>" . t('access_mode') . ":</td>";
        echo "<td>";
        $this->printAccessList($this->folder);
        echo "</td>";
        echo "</tr>";
      }
    }

    $attributes = $this->folder->getAttributes();
    if ($attributes) {
      foreach ($attributes as $attribute) {
        $attrdef = $attribute->getAttributeDefinition();
        ?>
        <tr>
          <td><?php echo htmlspecialchars($attrdef->getName()); ?>:</td>
          <td><?php echo htmlspecialchars($attribute->getValue()); ?></td>
        </tr>
      <?php
      }
    }
    echo "</table>\n";
  }

  /**
   * @param $obj Folder The folder to print the list from.
   */
  function printAccessList($obj) {
    $accessList = $obj->getAccessList();
    if (count($accessList["users"]) == 0 && count($accessList["groups"]) == 0) {
      return;
    }

    for ($i = 0; $i < count($accessList["groups"]); $i++) {
      $group = $accessList["groups"][$i]->getGroup();
      $accesstext = AccessMode::getText($accessList["groups"][$i]->getMode());
      print $accesstext . ": " . htmlspecialchars($group->getName());
      if ($i + 1 < count($accessList["groups"]) || count($accessList["users"]) > 0) {
        print "<br />";
      }
    }
    for ($i = 0; $i < count($accessList["users"]); $i++) {
      $user = $accessList["users"][$i]->getUser();
      $accesstext = AccessMode::getText($accessList["users"][$i]->getMode());
      print $accesstext . ": " . htmlspecialchars($user->getFullName());
      if ($i + 1 < count($accessList["users"])) {
        print "<br />";
      }
    }
  }


}