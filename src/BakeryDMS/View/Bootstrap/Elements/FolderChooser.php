<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\View\Bootstrap\Elements;

use BakeryDMS\Core\AccessMode;
use BakeryDMS\Settings;

/**
 * Class FolderChooser
 */
class FolderChooser {
  private $formName;
  private $accessMode;
  /**
   * @var int
   */
  private $exclude;
  /**
   * @var bool
   */
  private $default;

  /**
   * @param string $formName The name of the form element.
   * @param int $accessMode The access mode according to {@link AccessMode}.
   * @param int $exclude id of a folder to exclude
   * @param bool $default TODO clarify meaning of this param
   */
  function __construct($formName, $accessMode, $exclude = -1, $default = false) {
    $this->formName = $formName;
    $this->accessMode = $accessMode;
    $this->exclude = $exclude;
    $this->default = $default;
  }

  /**
   * Displays a full functional folderChooser in HTML.
   */
  public function display() {
    echo '<input type="hidden" id="targetid' . $this->formName . '" name="targetid" value="' . (($this->default) ? $this->default->getID() : "") . '">';
    echo '<div class="input-append">';
    echo '<input type="text" id="choosefoldersearch' . $this->formName . '" data-target="targetid' . $this->formName . '" data-provide="typeahead"  name="targetname' . $this->formName . '" value="' . (($this->default) ? htmlspecialchars($this->default->getName()) : "") . '" placeholder="' . getMLText('type_to_search') . '" autocomplete="off" />';
    echo '<a data-target="#folderChooser' . $this->formName . '" href="' . Settings::getInstance()->_httpRoot . 'out/out.FolderChooser.php?form=' . $this->formName . "&mode=" . $this->accessMode . "&exclude=" . $this->exclude . '" role="button" class="btn" data-toggle="modal">' . getMLText("folder") . "…</a>";
    echo "</div>";
    ?>
    <div class="modal hide"
         id="folderChooser<?php echo $this->formName ?>"
         tabindex="-1"
         role="dialog"
         aria-labelledby="folderChooser<?php echo $this->formName ?>Label"
         aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="folderChooser<?php echo $this->formName ?>Label"><?php printMLText("choose_target_folder") ?></h3>
      </div>
      <div class="modal-body">
        <p>Please wait, until document tree is loaded …</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><?php printMLText("close") ?></button>
      </div>
    </div>
    <script language="JavaScript">
      /* Set up a callback which is called when a folder in the tree is selected */
      modalFolderChooser<?php echo $this->formName ?> = $('#folderChooser<?php echo $this->formName ?>');
      function folderSelected<?php echo $this->formName ?>(id, name) {
        $('#targetid<?php echo $this->formName ?>').val(id);
        $('#choosefoldersearch<?php echo $this->formName ?>').val(name);
        modalFolderChooser<?php echo $this->formName ?>.modal('hide');
      }
    </script>
  <?php
  }
}