<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\View\Bootstrap;

use BakeryDMS\Extension\IAction;
use BakeryDMS\Extension\IView;
use SeedDMS_Bootstrap_Style;

class ExtensionView extends SeedDMS_Bootstrap_Style {
  /** @var IView $view */
  private $view;

  /** @var IAction[] $actions */
  private $actions;

  function __construct($params, $theme = 'bootstrap') {
    parent::__construct($params, $theme);
  }

  public function setView(IView &$view) {
    $this->view = $view;
    $this->actions = $view->getActions();

    $this->addExtensionActionItems($this->actions);
  }

  /**
   * Show the view of the extension.
   */
  public function show() {
    // TODO: get correct text
    $this->htmlStartPage($this->view->getTitle());
    $this->globalNavigation();
    $this->contentStart();
    $this->pageNavigation($this->view->getTitle(), "extension", $this->view);
    $this->view->printViewContent();
    $this->htmlEndPage();
  }
}