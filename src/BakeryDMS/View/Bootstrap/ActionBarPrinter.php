<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\View\Bootstrap;

use BakeryDMS\Core\AccessMode;
use BakeryDMS\Core\Document;
use BakeryDMS\Core\Folder;
use BakeryDMS\Core\User;
use BakeryDMS\Extension\IAction;
use BakeryDMS\Extension\IView;
use BakeryDMS\Settings;

class ActionBarPrinter {
  /**
   * @var IAction[] The additional items in the admin action bar.
   */
  private $adminExtensions;

  /**
   * @var User The current user.
   */
  private $user;
  private $accessObject;

  /** @var string prefix for the paths. */
  private $pathPrefix;

  /**
   * Initializes a new instance of the {@link ActionBarPrinter} class.
   *
   * @param $adminItems IAction[] The items for the admin tool bar.
   * @param $folderItems IAction[] The items for the folder tool bar.
   * @param $user User The current logged in user.
   * @param $accessObject Object The object currently viewing.
   */
  function __construct($adminItems, $folderItems, $user, $accessObject) {

    $this->settings = Settings::getInstance();
    $this->pathPrefix = $this->settings->_httpRoot;

    // extension handling
    $this->adminExtensions = $adminItems;
    $this->folderExtensions = $folderItems;

    // context
    $this->user = $user;
    $this->accessObject = $accessObject;
  }

  /**
   * Prints action the bar.
   *
   * @param $pageTitle string The title of the page.
   * @param $pageType string The type of the bar ("view_folder", "view_document", "my_documents", "my_account",
   *   "admin_tools", "calendar")
   * @param $extra
   */
  public function printBar($pageTitle, $pageType, $extra) {
    if ($pageType != null && strcasecmp($pageType, "noNav")) {
      if ($pageType == "view_folder" || $pageType == "view_document") {
        echo $pageTitle . "";
      }
      echo '<div class="navbar">';
      echo ' <div class="navbar-inner">';
      echo '  <div class="container">';
      echo '   <a class="btn btn-navbar" data-toggle="collapse" data-target=".col2">';
      echo ' 		<span class="icon-bar"></span>';
      echo ' 		<span class="icon-bar"></span>';
      echo ' 		<span class="icon-bar"></span></a>';
      switch ($pageType) {
        case "view_folder":
          $this->folderNavigationBar($extra);
          break;
        case "view_document":
          $this->documentNavigationBar($extra);
          break;
        case "my_documents":
          $this->myDocumentsNavigationBar();
          break;
        case "my_account":
          $this->accountNavigationBar();
          break;
        case "admin_tools":
          $this->adminToolsNavigationBar();
          break;
        case "calendar";
          $this->calendarNavigationBar($extra);
          break;
        case "extension";
          $this->extensionNavigationBar($extra);
          break;
      }
      echo ' 	</div>';
      echo ' </div>';
      echo '</div>';
    }
    else {
      echo '<legend>' . $pageTitle . '</legend>';
    }
  }

  /**
   * Prints out the navigation bar for a folder.
   *
   * @param $folder Folder The folder for what the navigation bar is for.
   */
  private function folderNavigationBar($folder) {
    if (!is_object($folder) || strcasecmp(get_class($folder), Folder::class)) {
      echo '<ul class="nav"></ul>';
      return;
    }

    $user = $this->user;
    $folderID = $folder->getID();
    $accessMode = $folder->getAccessMode($user);

    // get the parent access mode, if not set, use this one.
    $parentAccessMode = $folder->getParent() ? $folder->getParent()->getAccessMode($user) : $accessMode;

    echo '<a href="' . $this->pathPrefix . 'out/out.ViewFolder.php?folderid=' . $folderID . '&showtree=' . showtree() . '" class="brand">' . t("folder") . '</a>';
    echo '<div class="nav-collapse col2">';
    echo '<ul class="nav">';

    if ($accessMode >= AccessMode::READWRITE) {
      $this->printInternalItem('out/out.AddSubFolder.php?folderid=' . $folderID . "&showtree=" . showtree(),
        "add_subfolder");
      $this->printInternalItem('out/out.AddDocument.php?folderid=' . $folderID . "&showtree=" . showtree(),
        "add_document");
      if ($this->isLargeFileUploadEnabled()) {
        $this->printInternalItem('out/out.AddMultiDocument.php?folderid=' . $folderID . "&showtree=" . showtree(),
          "add_multiple_documents");
      }
    }

    if ($parentAccessMode >= AccessMode::READWRITE) {
      $this->printInternalItem('out/out.EditFolder.php?folderid=' . $folderID . "&showtree=" . showtree(),
        "edit_folder_props");
    }

    if ($parentAccessMode == AccessMode::ALL) {
      if ($folderID != $this->getRootFolderID() && $folder->getParent()) {
        $this->printInternalItem('out/out.MoveFolder.php?folderid=' . $folderID . "&showtree=" . showtree(),
          "move_folder");
      }

      if ($folderID != $this->getRootFolderID() && $folder->getParent()) {
        $this->printInternalItem('out/out.RemoveFolder.php?folderid=' . $folderID . "&showtree=" . showtree(),
          "rm_folder");
      }
      $this->printInternalItem('out/out.FolderAccess.php?folderid=' . $folderID . "&showtree=" . showtree(),
        "edit_folder_access");
    }

    if (!empty($this->folderExtensions)) {
      foreach ($this->folderExtensions as $action) {
        $this->printItem($action->getUrl(), $action->getText());
      }
    }
    
    if ($accessMode >= AccessMode::READWRITE && !$this->user->isGuest()) {
      $this->printInternalItem('out/out.FolderNotify.php?folderid=' . $folderID . "&showtree=" . showtree(),
        "edit_folder_notify");
    }
    if ($this->user->isAdmin()) {
      $this->printInternalItem('out/out.Indexer.php?folderid=' . $folderID, "index_folder");
    }
    echo '</ul>';
    echo '</div>';
  }

  /**
   * Prints the navigation bar according to a given document.
   *
   * @param $document Document
   */
  private function documentNavigationBar($document) {
    $accessMode = $document->getAccessMode($this->user);
    $documentId = ".php?documentid=" . $document->getID();
    echo '<a href="' . $this->pathPrefix . 'out/out.ViewDocument' . $documentId . '" class="brand">' . t("document") . '</a>';
    echo '<div class="nav-collapse col2">';
    echo '<ul class="nav">';
    if ($accessMode >= AccessMode::READWRITE) {
      if (!$document->isLocked()) {
        // Check if user is privileged to actualize or lock the document (Admins are always privileged)
        if (!$this->isLockedByWorkflow($document) || ($document->getAccessMode($this->user) == AccessMode::ALL)) {
          $this->printInternalItem('out/out.UpdateDocument' . $documentId, "update_document");
          $this->printInternalItem('op/op.LockDocument' . $documentId, "lock_document");
        }

        $this->printInternalItem('out/out.EditDocument' . $documentId, "edit_document_props");
        $this->printInternalItem('out/out.MoveDocument' . $documentId, "move_document");
      }
      else {
        $lockingUser = $document->getLockingUser();
        if (($lockingUser->getID() == $this->user->getID()) || ($document->getAccessMode($this->user) == AccessMode::ALL)) {
          $this->printInternalItem('out/out.UpdateDocument' . $documentId, "update_document");
          $this->printInternalItem('op/op.UnlockDocument' . $documentId, "unlock_document");
          $this->printInternalItem('out/out.EditDocument' . $documentId, "edit_document_props");
          $this->printInternalItem('out/out.MoveDocument' . $documentId, "move_document");
        }
      }
      if (isset($this->accessObject) && $this->accessObject->maySetExpires()) {
        $this->printInternalItem('out/out.SetExpires' . $documentId, "expires");
      }
    }
    if ($accessMode == AccessMode::ALL) {
      $this->printInternalItem('out/out.RemoveDocument' . $documentId, "rm_document");
      $this->printInternalItem('out/out.DocumentAccess' . $documentId, "edit_document_access");
    }

    if ($accessMode >= AccessMode::READ && !$this->user->isGuest()) {
      $this->printInternalItem('out/out.DocumentNotify' . $documentId, "edit_existing_notify");
    }

    echo '</ul>';
    echo '</div>';
  }

  /**
   * Prints the navigation bar for the account management.
   */
  private function accountNavigationBar() {
    echo '<a href="' . $this->pathPrefix . 'out/out.MyAccount.php" class="brand">' . t("my_account") . '</a>';
    echo '<div class="nav-collapse col2">';
    echo '<ul class="nav">';

    if ($this->user->isAdmin() || !$this->isSelfEditDisabled()) {
      $this->printInternalItem('out/out.EditUserData.php', "edit_user_details");
    }

    if (!$this->user->isAdmin()) {
      $this->printInternalItem('out/out.UserDefaultKeywords.php', "edit_default_keywords");
    }
    $this->printInternalItem('out/out.ManageNotify.php', "edit_existing_notify");

    if ($this->isUsersViewEnabled()) {
      $this->printInternalItem('out/out.UsrView.php', "users");
      $this->printInternalItem('out/out.GroupView.php', "groups");
    }
    echo '</ul>';
    echo '</div>';
  }

  /**
   * Prints the actionbar for the my documents page.
   */
  private function myDocumentsNavigationBar() {
    echo '<a href="' . $this->pathPrefix . 'out/out.MyDocuments.php?inProcess=1" class="brand">' . t("my_documents") . "</a>";
    echo '<div class="nav-collapse col2">';
    echo '<ul class="nav">';

    $this->printInternalItem('out/out.MyDocuments.php?inProcess=1', "documents_in_process");
    $this->printInternalItem('out/out.MyDocuments.php', "all_documents");
    if ($this->isTraditionalWorkflowMode()) {
      $this->printInternalItem('out/out.ReviewSummary.php', "review_summary");
      $this->printInternalItem('out/out.ApprovalSummary.php', "approval_summary");
    }
    else {
      $this->printInternalItem('out/out.WorkflowSummary.php', "workflow_summary");
    }
    echo '</ul>';
    echo '</div>';
  }

  /**
   * Prints the admin tools action bar.
   */
  private function adminToolsNavigationBar() {
    echo '<a href="' . $this->pathPrefix . 'out/out.AdminTools.php" class="brand">' . t("admin_tools") . "</a>";
    echo '<div class="nav-collapse col2">';

    echo '<ul class="nav">';
    echo '<li class="dropdown">';
    $this->printMainItem(t("user_group_management"));
    echo '<ul class="dropdown-menu" role="menu">';
    $this->printInternalItem("out/out.UsrMgr.php", "user_management");
    $this->printInternalItem("out/out.GroupMgr.php", "group_management");
    $this->printInternalItem("out/out.UserList.php", "user_list");
    echo '</ul>';
    echo '</li>';
    echo "</ul>";

    echo '<ul class="nav">';
    echo '<li class="dropdown">';
    $this->printMainItem(t("definitions"));
    echo "<ul class=\"dropdown-menu\" role=\"menu\">";
    $this->printInternalItem("out/out.DefaultKeywords.php", "global_default_keywords");
    $this->printInternalItem("out/out.Categories.php", "global_document_categories");
    $this->printInternalItem("out/out.AttributeMgr.php", "global_attributedefinitions");
    if ($this->isTraditionalWorkflowMode()) {
      $this->printInternalItem("out/out.WorkflowMgr.php", "global_workflows");
      $this->printInternalItem("out/out.WorkflowStatesMgr.php", "global_workflow_states");
      $this->printInternalItem("out/out.WorkflowActionsMgr.php", "global_workflow_actions");
    }
    echo '</ul>';
    echo '</li>';
    echo '</ul>';

    if ($this->isFullTextSearchEnabled()) {
      echo '<ul class="nav">';
      echo '<li class="dropdown">';
      $this->printMainItem(t("fullsearch"));
      echo '<ul class="dropdown-menu" role="menu">';
      $this->printInternalItem("out/out.Indexer.php", "update_fulltext_index");
      $this->printInternalItem("out/out.CreateIndex.php", "create_fulltext_index");
      $this->printInternalItem("out/out.IndexInfo.php", "fulltext_info");
      echo '</ul>';
      echo '</li>';
      echo '</ul>';
    }

    echo '<ul class="nav">';
    echo '<li class="dropdown">';
    $this->printMainItem(t("backup_log_management"));
    echo '<ul class="dropdown-menu" role="menu">';
    $this->printInternalItem("out/out.BackupTools.php", "backup_tools");
    if ($this->isLogFileEnabled()) {
      $this->printInternalItem("out/out.LogManagement.php", "log_management");
    }
    echo '</ul>';
    echo '</li>';
    echo '</ul>';

    echo '<ul class="nav">';
    echo '<li class="dropdown">';
    $this->printMainItem(t("misc"));
    echo '<ul class="dropdown-menu" role="menu">';
    $this->printInternalItem("out/out.Statistic.php", "folders_and_documents_statistic");
    $this->printInternalItem("out/out.Charts.php", "charts");
    $this->printInternalItem("out/out.ObjectCheck.php", "objectcheck");
    $this->printInternalItem("out/out.Info.php", "version_info");
    echo '</ul>';
    echo '</li>';
    echo '</ul>';

    if (!empty($this->adminExtensions)) {
      echo '<ul class="nav">';
      echo '<li class="dropdown">';
      $this->printMainItem(t("extensions"));
      echo '<ul class="dropdown-menu" role="menu">';
      foreach ($this->adminExtensions as $action) {
        $this->printItem($action->getUrl(), $action->getText());
      }
      echo '</ul>';
      echo '</li>';
      echo '</ul>';
    }

    echo '</div>';
  }

  /**
   * Prints the calendar navigation bar.
   *
   * @param $date array The date where 0 = day, 1 = month and 2 = year
   */
  private function calendarNavigationBar($date) {
    echo '<a href="' . $this->pathPrefix . 'out/out.Calendar.php?mode=y" class="brand">' . t("calendar") . "</a>";
    echo '<div class="nav-collapse col2">';
    echo '<ul class="nav">';

    $dateParam = "&day=" . $date[0] . "&month=" . $date[1] . "&year=" . $date[2];
    $this->printInternalItem('out/out.Calendar.php?mode=w' . $dateParam, "week_view");
    $this->printInternalItem('out/out.Calendar.php?mode=m' . $dateParam, "month_view");
    $this->printInternalItem('out/out.Calendar.php?mode=y' . $dateParam, "year_view");
    if (!$this->user->isGuest()) {
      $this->printInternalItem('out/out.AddEvent.php', "add_event");
    }
    echo '</ul>';
    echo '</div>';
  }

  /**
   * Prints the extension navigation bar.
   *
   * @param $view IView The view to print the bar for.
   */
  private function extensionNavigationBar($view) {
    echo '<a href="#" class="brand">' . $view->getTitle() . "</a>";
    echo '<div class="nav-collapse col2">';

    echo '<ul class="nav">';
    foreach ($view->getActions() as $action) {
      $this->printItem($action->getUrl(), $action->getText());
    }
    echo '</ul>';
    echo '</div>';
  }

  private function printInternalItem($url, $textKey) {
    $this->printItem($this->pathPrefix . $url, t($textKey));
  }

  private function printItem($url, $text) {
    echo '<li><a href="' . $url . '">' . $text . '</a></li>';
  }

  private function printMainItem($text) {
    echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' . $text . ' <span class="fa fa-caret-down"></span></a>';
  }

  /**
   * @return int The id of the root folder.
   */
  private function getRootFolderID() {
    return $this->settings->_rootFolderID;
  }

  /**
   * @return bool <c>true</c> if enabled, <c>false</c> otherwise.
   */
  private function isLargeFileUploadEnabled() {
    return $this->settings->_enableLargeFileUpload;
  }

  /**
   * @return bool <c>true</c> if traditional, <c>false</c> otherwise.
   */
  private function isTraditionalWorkflowMode() {
    return $this->settings->_workflowMode == 'traditional';
  }

  /**
   * @return bool <c>true</c> if disabled, <c>false</c> otherwise.
   */
  private function isSelfEditDisabled() {
    return $this->settings->_disableSelfEdit;
  }

  /**
   * @return bool <c>true</c> if enabled, <c>false</c> otherwise.
   */
  private function isFullTextSearchEnabled() {
    return $this->settings->_enableFullSearch;
  }

  /**
   * @return bool <c>true</c> if enabled, <c>false</c> otherwise.
   */
  private function isLogFileEnabled() {
    return $this->settings->_logFileEnable;
  }

  /**
   * @return bool <c>true</c> if enabled, <c>false</c> otherwise.
   */
  private function isUsersViewEnabled() {
    return $this->settings->_enableUsersView;
  }

  /**
   * Checks whether the current user is allowed to perform a workflow action at the current workflow state.
   * In a positive case, the user is privileged to actualize or lock the document – otherwise not.
   * If there are no workflow transitions, the document is at the end of the workflow and all users are privileged
   * to actualize or lock it.
   *
   * @param $document Document The document to check.
   *
   * @return bool <c>true</c> if locked by workflow, <c>false</c> otherwise.
   */
  private function isLockedByWorkflow($document) {
    if (!$this->isTraditionalWorkflowMode()) {
      $latestContent = $document->getLatestContent();
      $workflow = $latestContent->getWorkflow();

      if ($workflow) {
        $transitions = $workflow->getNextTransitions($latestContent->getWorkflowState());

        // Lock if needs no action but still transitions open
        return !($latestContent->needsWorkflowAction($this->user) || empty($transitions));
      }
    }
    return false;
  }

}