<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Routing;

/**
 * Class Path. A simple class for reading and interpreting the URI paths.
 *
 * @package BakeryDMS\Routing
 */
class Path {
  /**
   * @var string The complete path
   */
  private $path;

  /**
   * @var string[] The parts of the path
   */
  private $pathParts;

  /**
   * @param $path string The path parameter.
   */
  function __construct($path) {
    $this->path = $path;
    $this->pathParts = explode("/", $this->path);
  }

  /**
   * Returns <c>true</c> if the path is an extension path, <c>false</c> otherwise.
   *
   * @return bool <c>true</c> if is extension, <c>false</c> otherwise.
   */
  public function isExtensionPath() {
    if (count($this->pathParts) >= 2) {
      return strcasecmp($this->pathParts[0], "extension") == 0;
    }
    return false;
  }

  /**
   * Returns <c>true</c> if the path is a view path, <c>false</c> otherwise.
   *
   * @return bool <c>true</c> if is view, <c>false</c> otherwise.
   */
  public function isViewPath() {
    if (count($this->pathParts) >= 3) {
      return strcasecmp($this->pathParts[2], "view") == 0;
    }
    return false;
  }

  /**
   * Returns <c>true</c> if the path is an operation path, <c>false</c> otherwise.
   *
   * @return bool <c>true</c> if is operation, <c>false</c> otherwise.
   */
  private function isOperationPath() {
    if (count($this->pathParts) >= 3) {
      return strcasecmp($this->pathParts[2], "op") == 0;
    }
    return false;
  }

  /**
   * Returns <c>true</c> if the path is a view path of an extension, <c>false</c> otherwise.
   *
   * @return bool <c>true</c> if is extension view, <c>false</c> otherwise.
   */
  public function isExtensionView() {
    return $this->isExtensionPath()
    && $this->isViewPath();
  }

  public function isExtensionOperation() {
    return $this->isExtensionPath()
    && $this->isOperationPath();
  }

  /**
   * Returns the name of the module.
   *
   * @return string The name of the module.
   */
  public function getModuleName() {
    if (count($this->pathParts) >= 2) {
      return $this->pathParts[1];
    }
    return "";
  }

  /**
   * Returns the name of the view.
   *
   * @return string The name of the view.
   */
  public function getViewName() {
    if (count($this->pathParts) >= 4) {
      return $this->pathParts[3];
    }
    return "";
  }

  /**
   * Returns the name of the operation.
   *
   * @return string The name of the operation.
   */
  public function getOperationName() {
    if (count($this->pathParts) >= 4) {
      return $this->pathParts[3];
    }
    return "";
  }

}