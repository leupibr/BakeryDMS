<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

namespace BakeryDMS\Preview;

use BakeryDMS\Core\DocumentContent;
use BakeryDMS\Core\DocumentFile;
use BakeryDMS\Utils\File;

class Previewer {
  /**
   * @var string $cacheDir location in the file system where all the
   *      cached data like thumbnails are located. This should be an
   *      absolute path.
   */
  public $previewDir;

  /**
   * @var integer $width maximum width/height of resized image
   */
  protected $width;

  function __construct($previewDir, $width = 40) {
    if (!is_dir($previewDir)) {
      if (!File::makeDir($previewDir)) {
        $this->previewDir = '';
      }
      else {
        $this->previewDir = $previewDir;
      }
    }
    else {
      $this->previewDir = $previewDir;
    }
    $this->width = intval($width);
  }

  /**
   * Retrieve the physical filename of the preview image on disk
   *
   * @param $object DocumentContent|DocumentFile
   * @param $width int width of preview image
   * @return string file name of preview image
   */
  protected function getFileName($object, $width) {
    $document = $object->getDocument();
    $dir = $this->previewDir . '/' . $document->getDir();
    switch (get_class($object)) {
      case DocumentContent::class:
        $target = $dir . 'p' . $object->getVersion() . '-' . $width . '.png';
        break;
      case DocumentFile::class:
        $target = $dir . 'f' . $object->getID() . '-' . $width . '.png';
        break;
      default:
        return false;
    }
    return $target;
  }

  public function createPreview($object, $width = 0) {
    if ($width == 0) {
      $width = $this->width;
    }
    else {
      $width = intval($width);
    }
    if (!$this->previewDir) {
      return false;
    }
    $document = $object->getDocument();
    $dir = $this->previewDir . '/' . $document->getDir();
    if (!is_dir($dir)) {
      if (!File::makeDir($dir)) {
        return false;
      }
    }
    $file = $document->_dms->contentDir . $object->getPath();
    if (!file_exists($file)) {
      return false;
    }
    $target = $this->getFileName($object, $width);
    if ($target !== false && !file_exists($target)) {
      $cmd = '';
      switch ($object->getMimeType()) {
        case "image/png":
        case "image/gif":
        case "image/jpeg":
        case "image/jpg":
        case "image/svg+xml":
          $cmd = 'convert -resize ' . $width . 'x ' . $file . ' ' . $target;
          break;
        case "application/pdf":
        case "application/postscript":
          $cmd = 'convert -density 100 -resize ' . $width . 'x ' . $file . '[0] ' . $target;
          break;
        case "text/plain":
          $cmd = 'convert -resize ' . $width . 'x ' . $file . '[0] ' . $target;
          break;
        case "application/x-compressed-tar":
          $cmd = 'tar tzvf ' . $file . ' | convert -density 100 -resize ' . $width . 'x text:-[0] ' . $target;
          break;
      }
      if ($cmd) {
        exec($cmd);
      }
      return true;
    }
    return true;

  }

  public function hasPreview($object, $width = 0) {
    if ($width == 0) {
      $width = $this->width;
    }
    else {
      $width = intval($width);
    }
    if (!$this->previewDir) {
      return false;
    }
    $target = $this->getFileName($object, $width);
    if ($target && file_exists($target)) {
      return true;
    }
    return false;
  }

  public function getPreview($object, $width = 0) {
    if ($width == 0) {
      $width = $this->width;
    }
    else {
      $width = intval($width);
    }
    if (!$this->previewDir) {
      return false;
    }

    $target = $this->getFileName($object, $width);
    if ($target && file_exists($target)) {
      readfile($target);
    }
  }

  public function deletePreview($document, $object, $width = 0) {
    if ($width == 0) {
      $width = $this->width;
    }
    else {
      $width = intval($width);
    }
    if (!$this->previewDir) {
      return false;
    }

    $target = $this->getFileName($object, $width);
  }
}