<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use BakeryDMS\Core\DMS;

require_once('BakeryDMS/Core.php');
include("Log.php");
include("webdav.php");

$db = \BakeryDMS\Core\Persistency\DatabaseAccessFactory::createDatabaseAccess(
  $settings->_dbDriver,
  $settings->_dbHostname,
  $settings->_dbUser,
  $settings->_dbPass,
  $settings->_dbDatabase);

$db->connect() or die ("Could not connect to db-server \"" . $settings->_dbHostname . "\"");
$db->getResult("set names 'utf8'");

$dms = new DMS($db, $settings->_contentDir . $settings->_contentOffsetDir);

if ($settings->_logFileEnable) {
  if ($settings->_logFileRotation == "h") {
    $logname = date("YmdH", time());
  }
  else if ($settings->_logFileRotation == "d") {
    $logname = date("Ymd", time());
  }
  else {
    $logname = date("Ym", time());
  }
  $logname = $settings->_contentDir . "webdav-" . $logname . ".log";
  $log = Log::factory('file', $logname);
}
else {
  $log = null;
}

$server = new HTTP_WebDAV_Server_SeedDMS();
$server->ServeRequest($dms, $log);
//$files = array();
//$options = array('path'=>'/Test1/subdir', 'depth'=>1);
//echo $server->MKCOL(&$options);

?>
