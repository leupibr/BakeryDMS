<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2002-2005 Markus Westphal
// Copyright (C) 2006-2008 Malcolm Cowe
// Copyright (C) 2010 Matteo Lucarelli
// Copyright (C) 2010 Uwe Steinmann
// Copyright (C) 2014-2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use BakeryDMS\Settings;

define('SEEDDMS_INSTALL', 'on');
require_once('BakeryDMS/Core.php');

$settings = Settings::getInstance();
$rootDir = realpath ("..");
$settings->_rootDir = $rootDir.'/';

$theme = "bootstrap";
include("../inc/inc.Language.php");
include "../languages/en_GB/lang.inc";
include("../inc/inc.ClassUI.php");
$LANG['en_GB'] = $text;

UI::htmlStartPage("INSTALL");
UI::contentHeading("BakeryDMS Installation...");
UI::contentContainerStart();
echo "<h2>".getMLText('settings_install_welcome_title')."</h2>";
echo "<div style=\"width: 600px;\">".getMLText('settings_install_welcome_text')."</div>";
echo '<p><a href="install.php">' . getMLText("settings_start_install") . '</a></p>';
UI::contentContainerEnd();
UI::htmlEndPage();
?>
