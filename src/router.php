<?php
// BakeryDMS. PHP based document management system with workflow support.
// Copyright (C) 2015 Bruno Leupi, Tobias Maestrini
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use BakeryDMS\Extension\ExtensionManager;
use BakeryDMS\Extension\IView;
use BakeryDMS\Routing\Path;
use BakeryDMS\Settings;
use BakeryDMS\View\Bootstrap\ExtensionView;

require_once("BakeryDMS/Core.php");

include_once("inc/inc.Language.php");
include_once("inc/inc.ClassUI.php");
include_once("inc/inc.ClassAccessOperation.php");

include_once("views/bootstrap/class.Bootstrap.php");

$path = new Path($_GET['path']);
if ($path->isExtensionView()) {
  /** @var IView $extensionView */
  $extensionView = ExtensionManager::getInstance()->getView($path->getModuleName(), $path->getViewName());

  if (empty($extensionView)) {
    UI::exitError(t("invalid_view"), t("invalid_view_error_description"));
    die();
  }

  /** @var ExtensionView $view */
  $view = UI::factory($theme,
    ExtensionView::class,
    [
      'refferer' => $_SERVER['REQUEST_URI'],
      'session' => $session,
      'sitename' => $settings->_siteName,
      'rootfolderid' => $settings->_rootFolderID,
      'disableselfedit' => $settings->_disableSelfEdit,
      'enableusersview' => $settings->_enableUsersView,
      'enablecalendar' => $settings->_enableCalendar,
      'calendardefaultview' => $settings->_calendarDefaultView,
      'enablefullsearch' => $settings->_enableFullSearch,
      'enablelargefileupload' => $settings->_enableLargeFileUpload,
      'printdisclaimer' => $settings->_printDisclaimer,
      'footnote' => $settings->_footNote,
      'logfileenable' => $settings->_logFileEnable,
      'expandfoldertree' => $settings->_expandFolderTree,
      'enablefoldertree' => $settings->_enableFolderTree,
      'enablelanguageselector' => $settings->_enableLanguageSelector,
      'partitionsize' => $settings->_partitionSize,
      'settings' => $settings,
      'user' => $user,
      'workflowmode' => $settings->_workflowMode,
      'showmissingtranslations' => $settings->_showMissingTranslations
    ]);

  $view->setView($extensionView);
  $view->show();
}
else {
  if ($path->isExtensionOperation()) {
    $operation = ExtensionManager::getInstance()->getOperation($path->getModuleName(), $path->getOperationName());

    if (empty($operation)) {
      UI::exitError(t("invalid_operation"), t("invalid_operation_error_description"));
      die();
    }

    if (!$operation->handle()) {
      UI::exitError($operation->getFailureTitle(), $operation->getFailureDescription());
      die();
    }
    header("Location:" . $operation->getSuccessfulTarget());
  }
}

