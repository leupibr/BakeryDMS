# BakeryDMS
![Version](https://img.shields.io/badge/version-V1.3.1-green.svg)
[![License](https://img.shields.io/badge/license-GPLv2-green.svg)](http://opensource.org/licenses/gpl-2.0)
[![Language](https://img.shields.io/badge/language-PHP%205.5-blue.svg)](http://php.net/)

## About
*BakeryDMS* is a webbased, opensource software with the target of simplifying document exchange and communication. It improves and supports process management and monitoring. Different from other document management systems, *BakeryDMS* combines document management and process management in one single system.

### Origin
*BakeryDMS* is based on the opensource software [SeedDMS](http://www.seeddms.org/) and extends it with a plugin concept that allows to extend and customize the system to a certain environment.

### Features

#### Fully webbased folder and document management
*BakeryDMS* is fully webbased and is accessible everywhere. It organizes directories and documents in a simple, tree-like structure like common file systems, but allows to track changes and earlier versions of the documents.

#### Simple document control with workflows
All documents in *BakeryDMS* can be coupled with a workflow, that allows to implement simple workflows such as a review process or similar.

#### Enterprise authentication integration
*BakeryDMS* can be coupled to comman LDAP systems or a Microsoft Active Directory. Users will be created by their first login and automatically assigned to their organizational unit (OU).

#### Powerful extension concept
The extension concept of *BakeryDMS* allows you to customize and extend the software to a certain environment. Exctension points such as the action and menu bar and an core event system makes it possible to couple any imaginable extension with *BakeryDMS*.

## Contributors
* Bruno Leupi
* Tobias Maestrini

## License
GNU General Public License Version 2
