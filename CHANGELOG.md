## Changes in version 1.3.1

**Features**
* LDAP: match LDAP users automatically to a BakeryDMS group (if existing) according to the LDAP OU.
* add admin feature for deleting or removing multiple users from a group
* UI improvement: change visualization of workflow hint and improve display of status for pending actions

**Bugfixes**
* fix a bug in document notification matters
* fix a bug in notification list retrieval for documents
* fix a bug in workflow view



## Changes in version 1.3.0

**Features**
* implement triggers for workflow events (trigger workflows)
* implement triggers for document update events

**Bugfixes**
* fix a bug that is issued in MS Windows environment
* fix access mode issue


## Changes in version 1.2.1

**Features**
* introduction of extension infrastructure

**Bugfixes**
* cleanup path handling


## Changes in version 1.2.0

**Bugfixes**
* fixed bugs related to the actualization or locking of a document that is attached to a workflow and the user is not privileged to modify at the current workflow state
* fixes in version retrieval
* attach a document with version number "0" (such as a "dummy document") to a workflow is now possible
* allow the setting of an initial version number of a document for an administrator
* fix several core includes

**Improvements**
* workflows: shows the according state instead of the action of a workflow (in the workflow overview of a document)
* admin: now shows the version info of BakeryDMS
* several improvements in the user interface


## Changes in version 1.1.0

**Bugfixes**

* don’t change or lose the workflow definition on a document when updating it

**Features**

* workflow status in a document overview will be displayed in a colored box
* emphasize the actual folder in a tree view


## Changes in version 1.0.3

* add configurable "report issue" button.


## Changes in version 1.0.2

* copy attributes on copyfolder utils too
* allow to copy permission in copyfolder utils


## Changes in version 1.0.1

* fixes in html representation
* add possibility to overwrite owner of newly created folders


## Changes in version 1.0.0

* report link
* allow to set owner for new files
* add utility to copy a complete folder structure
* fix minor issues in access rights handling
* fix drop upload security issue


## Changes in version 0.0.1

* the version 0.0.1 is similar to the SeedDMS version 4.3.10*